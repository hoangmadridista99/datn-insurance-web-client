module.exports = {
  trailingComma: 'es5',
  tabWidth: 2,
  semi: true,
  singleQuote: true,
  endOfLine: 'lf',
  // plugins: [require('prettier-plugin-tailwindcss')],
};
