import React from 'react';
import ListUlInsurance from './listUlInsurance';
import Image from 'next/image';

export default function ContentInsurance({ data, children, id, img }) {
  const styles = 'text-sm sm:text-base';
  return (
    <div key={id} className="flex flex-col gap-2">
      <p className={`text-arsenic ${styles} font-bold`}>
        {data?.insuranceName}
      </p>
      <div>
        <p className={`text-nickel ${styles}`}>{data?.title}</p>
        <ListUlInsurance data={data?.detailTitle} />
        <p className={`text-nickel ${styles}`}>{data?.titleSecond}</p>
      </div>
      {img && (
        <div className="xl:mx-auto xl:w-2/3">
          <Image src={img} alt="bhnt" />
        </div>
      )}
      {children}
    </div>
  );
}
