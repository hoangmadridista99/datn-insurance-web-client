import Image from 'next/image';
import React from 'react';

export default function OverviewHealthInsurance({
  content,
  image,
  smallTitle,
  coreTitle,
}) {
  return (
    <>
      <div>
        <h2 className="mb-2 text-center text-xl font-bold text-ultramarine-blue">
          {smallTitle}
        </h2>
        <h1 className="mb-10 text-center text-pixel-32 font-bold leading-pixel-48 text-arsenic">
          {coreTitle}
        </h1>
      </div>
      <div className="xl:flex xl:gap-10">
        <div className="relative w-full pr-3 xl:w-1/2">
          <div className="rounded-14">
            <Image
              src={image}
              className="h-auto w-full"
              alt="healthInsurance1"
            />
          </div>
          <div className="absolute top-3 left-3 bottom-0 -z-10 h-full w-[calc(100%-12px)] transform rounded-14 border border-fresh-air"></div>
        </div>
        <div className="mt-8 flex flex-col justify-end gap-4 xl:m-0 xl:w-1/2 xl:gap-6">
          {content.map((content) => (
            <div
              key={content.id}
              className="flex gap-4 border-b border-solid border-platinum pb-4 text-base font-medium sm:pb-6"
            >
              <p className="text-xl font-bold text-azure sm:text-2xl">{`0${content.id}`}</p>
              <p className="font-medium sm:text-xl">{content.title}</p>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
