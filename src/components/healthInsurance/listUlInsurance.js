import React from 'react';

export default function ListUlInsurance({
  data,
  styleLi = 'text-nickel sm:text-base text-sm',
}) {
  return (
    <ul className="ml-4.5 list-disc">
      {data?.map((item) => (
        <li key={item?.detailId} className={styleLi}>
          {item?.detail}
        </li>
      ))}
    </ul>
  );
}
