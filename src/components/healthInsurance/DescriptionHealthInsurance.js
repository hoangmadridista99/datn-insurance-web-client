import { useTranslation } from 'next-i18next';
import React from 'react';
import ContentDetailHealthInsurance from './contentDetailHealthInsurance';
import Image from 'next/image';
import ContentInsurance from './contentInsurance';

export default function DescriptionHealthInsurance({
  image,
  insuranceHealthData,
}) {
  const { t } = useTranslation(['healthInsurance']);

  return (
    <div>
      <div className="mt-16 flex flex-col gap-6">
        <p className="border-b border-solid border-platinum pb-4 text-xl font-medium text-raisin-black sm:text-2xl">
          {t('whatIsHealthInsurance')}
        </p>
        <p className="text-sm text-nickel sm:text-base">
          {t('healthInsuranceDefinition1')}
          <br />
          {t('healthInsuranceDefinition2')}
        </p>
        <div className="mx-auto w-full xl:w-2/3">
          <Image src={image} alt="Bhnt2" />
        </div>
        <div className="mt-2 flex flex-col gap-8">
          <ContentDetailHealthInsurance
            classNameAddMore={'pt-2'}
            title={t('popularHeathInsurance')}
          >
            {insuranceHealthData.slice(0, 3).map((item) => (
              <ContentInsurance key={item.id} data={item} />
            ))}
          </ContentDetailHealthInsurance>
          <ContentDetailHealthInsurance title={t('benefitsInsurance')}>
            {insuranceHealthData.slice(3, 6).map((item) => (
              <ContentInsurance key={item.id} data={item} img={item.images} />
            ))}
          </ContentDetailHealthInsurance>
          <ContentDetailHealthInsurance
            title={t('experienceBuyingHealthInsurance')}
          >
            {insuranceHealthData.slice(6, 9).map((item) => (
              <ContentInsurance key={item.id} data={item} />
            ))}
          </ContentDetailHealthInsurance>
        </div>
      </div>
    </div>
  );
}
