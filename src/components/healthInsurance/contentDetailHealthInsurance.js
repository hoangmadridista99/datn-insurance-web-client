import React from 'react';

export default function ContentDetailHealthInsurance({title, children, classNameAddMore}) {
  return (
    <div className='flex flex-col gap-6 pt-2'>
      <p className={`pb-4 text-xl text-raisin-black font-medium border-b border-solid border-platinum sm:text-2xl ${classNameAddMore}`}>{title}</p>
      {children}
    </div>
  );
}
