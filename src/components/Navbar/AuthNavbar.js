/* eslint-disable indent */
import { signOut, useSession } from 'next-auth/react';
import Image from 'next/image';
import Link from 'next/link';
import React, { useState } from 'react';
import { useTranslation } from 'next-i18next';
import IconDropDown, {
  IconConcern,
  IconInsuranceList,
  IconLock,
  IconLogout,
  IconSearch,
  IconUser,
} from 'components/common/icon/Icon';
import { useRouter } from 'next/router';
import InputSearch from 'components/common/InputSearch';
import MenuNavbarMobile from './MenuNavbarMobile';
import DetailListTitleInsurance from './DetailListTitleInsurance';
import SelectLanguages from './SelectLanguages';
import ButtonPrimary from 'components/common/ButtonPrimary';
import ButtonBgWhite from 'components/common/ButtonBgWhite';
// components

export default function Navbar({ hiddenInputSearch, setHiddenInputSearch }) {
  const router = useRouter();
  const session = useSession();

  const profileUsers = session?.data;

  const [hiddenProfileUser, setHiddenProfileUser] = useState(false);
  const [hiddenMenuNavbar, setHiddenMenuNavbar] = useState(false);
  const [hiddenListInsurance, setHiddenListInsurance] = useState(false);
  const [hiddenSelectLanguages, setHiddenSelectLanguages] = useState(false);
  const [hiddenMenuAfterNext, setHiddenMenuAfterNext] = useState(false);
  const [isHovered, setIsHovered] = useState({
    vendor: false,
  });

  const { t } = useTranslation(['auth', 'common']);

  const handleSelectLanguage = (locale) =>
    router.push(router.asPath, undefined, { locale: locale });

  const handleDropProfile = () => setHiddenProfileUser(!hiddenProfileUser);

  const handleClickOnOverlay = () => {
    if (hiddenProfileUser) setHiddenProfileUser(!hiddenProfileUser);
  };

  const handleHiddenInputSearch = () =>
    setHiddenInputSearch(!hiddenInputSearch);

  const handleHiddenMenuNavbar = () => {
    setHiddenMenuNavbar(!hiddenMenuNavbar);
    setHiddenInputSearch(false);
  };

  const handleClickOverlayHiddenMenuNavbar = () => {
    setHiddenMenuNavbar(!hiddenMenuNavbar);
  };

  return (
    <div className="bg-white shadow-headers xl:h-22 xl:shadow-none">
      <div
        className={`absolute inset-0 z-20 bg-transparent ${
          hiddenProfileUser ? 'block' : 'hidden'
        }`}
        onClick={handleClickOnOverlay}
      />

      <div className="comparison-container z-40 w-full">
        <div className="flex items-center justify-between py-3 lg:py-6">
          <div className="flex w-percent-45 items-center gap-4">
            <div className="w-1/3">
              <Link href="/">
                <div className="flex w-32 items-center">
                  <Image
                    width={375}
                    height={96}
                    priority="true"
                    src="/images/navbar/logoComparison.png"
                    alt="comparison-logo"
                  />
                </div>
              </Link>
            </div>
            <div className="hidden w-full lg:block">
              <InputSearch />
            </div>
          </div>

          <div className="flex items-center gap-2 lg:hidden">
            <button
              type="button"
              className={`${
                hiddenInputSearch ? 'text-ultramarine-blue' : 'text-nickel'
              }`}
              onClick={handleHiddenInputSearch}
            >
              <IconSearch />
            </button>
            <button type="button" onClick={handleHiddenMenuNavbar}>
              <Image src="/svg/menu.svg" height={24} width={24} alt="menu" />
            </button>
          </div>

          {hiddenMenuNavbar && (
            <div
              className={'fixed inset-0 z-50 flex bg-rich-black lg:hidden'}
              onClick={handleClickOverlayHiddenMenuNavbar}
            />
          )}

          <div
            className={`${
              hiddenMenuNavbar ? 'translate-x-0 ' : '-translate-x-full'
            } fixed top-0 left-0 z-50 h-screen w-5/6 duration-300 ease-in-out lg:hidden`}
          >
            {!hiddenMenuAfterNext && (
              <MenuNavbarMobile
                hiddenMenuNavbar={hiddenMenuNavbar}
                setHiddenMenuNavbar={setHiddenMenuNavbar}
                hiddenMenuAfterNext={hiddenMenuAfterNext}
                setHiddenMenuAfterNext={setHiddenMenuAfterNext}
                hiddenSelectLanguages={hiddenSelectLanguages}
                setHiddenSelectLanguages={setHiddenSelectLanguages}
                hiddenListInsurance={hiddenListInsurance}
                setHiddenListInsurance={setHiddenListInsurance}
              />
            )}
            {hiddenListInsurance && (
              <DetailListTitleInsurance
                hiddenMenuNavbar={hiddenMenuNavbar}
                setHiddenMenuNavbar={setHiddenMenuNavbar}
                hiddenMenuAfterNext={hiddenMenuAfterNext}
                setHiddenMenuAfterNext={setHiddenMenuAfterNext}
                hiddenListInsurance={hiddenListInsurance}
                setHiddenListInsurance={setHiddenListInsurance}
              />
            )}
            {hiddenSelectLanguages && (
              <SelectLanguages
                hiddenMenuAfterNext={hiddenMenuAfterNext}
                setHiddenMenuAfterNext={setHiddenMenuAfterNext}
                hiddenSelectLanguages={hiddenSelectLanguages}
                setHiddenSelectLanguages={setHiddenSelectLanguages}
              />
            )}
          </div>

          <div className="hidden flex-wrap justify-center gap-1 lg:flex">
            <div className="group relative flex items-center gap-1">
              <button type="button">
                <div className="flex h-8 w-8 items-center">
                  <Image
                    src={t('logoLanguages')}
                    width={32}
                    height={32}
                    priority="true"
                    alt="logo"
                  />
                </div>
              </button>

              <div className="absolute -left-3 top-8 z-50 hidden w-14 flex-col overflow-hidden rounded-lg border bg-white pb-1 shadow-listInsurance group-hover:flex">
                <button
                  type="button"
                  onClick={() => handleSelectLanguage('en')}
                  className="h-8 w-full text-center hover:bg-lavender"
                >
                  <Image
                    src="/images/navbar/logo_english.png"
                    height={32}
                    width={37}
                    priority="true"
                    alt="logoEn"
                    className="m-auto"
                  />
                </button>

                <button
                  onClick={() => handleSelectLanguage('vi')}
                  type="button"
                  className="h-8 w-full text-center hover:bg-lavender"
                >
                  <Image
                    src="/images/navbar/logoVietNam.png"
                    height={32}
                    width={37}
                    priority="true"
                    alt="logoVietNam"
                    className="m-auto"
                  />
                </button>
              </div>
            </div>

            <div className="flex items-center">
              <Link href="/auth/registerVendor">
                <p className="px-3 py-1 text-xs font-normal text-granite-gray hover:text-ultramarine-blue sm:text-sm lg:font-medium">
                  {t('registerAccountVendor')}
                </p>
              </Link>
            </div>

            {!session?.data ? (
              <div className="flex gap-4">
                <Link href="/auth/login">
                  <ButtonBgWhite
                    cases={'small'}
                    title={t('login')}
                    widthBtn={'w-full px-6'}
                  />
                </Link>

                <Link href="/auth/register">
                  <ButtonPrimary
                    cases={'small'}
                    title={t('register')}
                    widthBtn={'w-full px-6'}
                  />
                </Link>
              </div>
            ) : (
              <div className="relative flex items-center">
                <div
                  className="flex items-center gap-2 hover:cursor-pointer"
                  onClick={handleDropProfile}
                >
                  <>
                    {!profileUsers?.avatar_profile_url ? (
                      <div className="h-8 w-8">
                        <Image
                          src={`/images/navbar/${
                            profileUsers?.gender === 'male'
                              ? 'logoUserMale'
                              : 'logoUserFemale'
                          }.png`}
                          height={32}
                          width={32}
                          alt="logoUser"
                          priority="true"
                        />
                      </div>
                    ) : (
                      <div className="h-8 w-8">
                        <Image
                          height={500}
                          width={500}
                          className="h-full w-full rounded-full object-cover"
                          src={profileUsers?.avatar_profile_url}
                          alt="logoCompany"
                        />
                      </div>
                    )}
                  </>
                  <p className="font-bold">
                    {profileUsers?.first_name} {profileUsers?.last_name}
                  </p>
                  <button type="button">
                    <IconDropDown />
                  </button>
                </div>

                {hiddenProfileUser && (
                  <div className="absolute -left-10 top-10 z-50 w-64 rounded-lg border bg-white px-5 py-4 text-3xl shadow-listInsurance">
                    <div className="mb-2 flex items-center gap-2 border-b border-cultured pb-2">
                      <div>
                        {!profileUsers?.avatar_profile_url ? (
                          <div className="h-10 w-10 bg-white">
                            <Image
                              src={
                                profileUsers?.gender === 'male'
                                  ? '/images/navbar/logoUserMale.png'
                                  : '/images/navbar/logoUserFemale.png'
                              }
                              alt="logoUserMale"
                              width={40}
                              height={40}
                              priority="true"
                            />
                          </div>
                        ) : (
                          <div>
                            <Image
                              height={40}
                              width={40}
                              src={profileUsers?.avatar_profile_url}
                              alt="logoUser"
                            />
                          </div>
                        )}
                      </div>

                      <div>
                        <p className="mb-1 text-sm font-normal text-arsenic">
                          {profileUsers?.first_name} {profileUsers?.last_name}
                        </p>
                        <p className="text-xs font-normal text-nickel">
                          {profileUsers?.email}
                        </p>
                      </div>
                    </div>

                    <Link href="/settingUser">
                      <div className="mb-2 flex items-center gap-2 rounded-md py-2 px-1 hover:bg-lavender">
                        <IconUser />
                        <p className="text-base font-normal text-arsenic">
                          {t('setting_account')}
                        </p>
                      </div>
                    </Link>

                    <Link href="/changePassword">
                      <div className="mb-2 flex items-center gap-2 rounded-md border-b border-cultured py-2 px-1 pb-3 hover:bg-lavender">
                        <IconLock />
                        <p className="text-base font-normal text-arsenic">
                          {t('change_password')}
                        </p>
                      </div>
                    </Link>

                    <Link href="/insuranceConcern">
                      <div
                        className="mb-2 flex items-center gap-2 rounded-md py-2 px-1 hover:bg-lavender"
                        onMouseOver={() =>
                          setIsHovered({ ...isHovered, vendor: true })
                        }
                        onMouseOut={() =>
                          setIsHovered({ ...isHovered, vendor: false })
                        }
                      >
                        <IconConcern />

                        <p className="text-base font-normal text-arsenic">
                          {t('interestList')}
                        </p>
                      </div>
                    </Link>
                    <Link href="/listUserInsurance">
                      <div className="mb-2 flex items-center gap-2 rounded-md py-2 px-1 hover:bg-lavender">
                        <div className="p-0.5">
                          <IconInsuranceList />
                        </div>

                        <p className="text-base font-normal text-arsenic">
                          {t('insurance_list')}
                        </p>
                      </div>
                    </Link>
                    <div className="border-t pt-2">
                      <button
                        type="button"
                        className="flex w-full items-center gap-2 rounded-md py-2 px-1 hover:bg-lavender"
                        onClick={signOut}
                      >
                        <div className="flex w-6 items-center justify-center">
                          <IconLogout />
                        </div>
                        <p className="text-base font-normal text-arsenic">
                          {t('sign_out')}
                        </p>
                      </button>
                    </div>
                  </div>
                )}
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
