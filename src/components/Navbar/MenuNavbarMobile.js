import {
  IconDropDown2,
  IconLock,
  IconLogout,
  IconUser,
} from 'components/common/icon/Icon';
import { IconCloseComparison } from 'components/common/icon/Icon';
import { signOut, useSession } from 'next-auth/react';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { INK_LIGHT } from 'src/utils/constants';

export default function MenuNavbarMobile({
  setHiddenMenuNavbar,
  hiddenMenuNavbar,
  hiddenMenuAfterNext,
  setHiddenMenuAfterNext,
  hiddenSelectLanguages,
  setHiddenSelectLanguages,
  hiddenListInsurance,
  setHiddenListInsurance,
}) {
  const router = useRouter();
  const session = useSession();
  const profileUsers = session?.data;
  const { t } = useTranslation(['auth', 'common']);

  const handleHiddenMenuMobile = () => {
    setHiddenMenuNavbar(!hiddenMenuNavbar);
  };

  const handleHiddenDetailListInsurance = () => {
    setHiddenListInsurance(!hiddenListInsurance);
    setHiddenMenuAfterNext(!hiddenMenuAfterNext);
  };

  const handleHiddenSelectLanguages = () => {
    setHiddenSelectLanguages(!hiddenSelectLanguages);
    setHiddenMenuAfterNext(!hiddenMenuAfterNext);
  };

  const handleSignOut = () => {
    signOut();
    setHiddenMenuNavbar(!hiddenMenuNavbar);
  };

  const handleLogin = () => {
    router.push('/auth/login');
    setHiddenMenuNavbar(!hiddenMenuNavbar);
  };
  const handleRegister = () => {
    router.push('/auth/register');
    setHiddenMenuNavbar(!hiddenMenuNavbar);
  };
  const handleSettingUser = () => {
    router.push('/settingUser');
    setHiddenMenuNavbar(!hiddenMenuNavbar);
  };
  const handleChangePassword = () => {
    router.push('/changePassword');
    setHiddenMenuNavbar(!hiddenMenuNavbar);
  };
  const handleBlogs = () => {
    router.push('/blogs');
    setHiddenMenuNavbar(!hiddenMenuNavbar);
  };
  const handleInterestList = () => {
    router.push('/insuranceConcern');
    setHiddenMenuNavbar(!hiddenMenuNavbar);
  };
  const handleRegisterVendor = () => {
    router.push('/auth/registerVendor');
    setHiddenMenuNavbar(!hiddenMenuNavbar);
  };

  return (
    <div className="relative z-50 h-screen w-full bg-white p-6 md:p-12">
      <button
        className="absolute top-4 right-4"
        type="button"
        onClick={handleHiddenMenuMobile}
      >
        <IconCloseComparison width={24} height={24} />
      </button>

      {session?.data && (
        <div className="mb-7 flex items-center gap-2 border-b border-cultured pb-3">
          <div>
            {!profileUsers?.avatar_profile_url ? (
              <div className="h-10 w-10 bg-white">
                <Image
                  src={
                    profileUsers?.gender === 'male'
                      ? '/images/navbar/logoUserMale.png'
                      : '/images/navbar/logoUserFemale.png'
                  }
                  alt="logoUserMale"
                  width={40}
                  height={40}
                  priority="true"
                />
              </div>
            ) : (
              <div>
                <Image
                  height={40}
                  width={40}
                  src={profileUsers?.avatar_profile_url}
                  alt="logoUser"
                />
              </div>
            )}
          </div>
          <div>
            <p className="mb-1 text-sm font-normal text-arsenic">
              {profileUsers?.first_name} {profileUsers?.last_name}
            </p>
            <p className="text-xs font-normal text-nickel">
              {profileUsers?.email}
            </p>
          </div>
        </div>
      )}

      <div className="mb-6 border-b pb-5">
        <div>
          <button type="button" onClick={handleInterestList}>
            <p className="mb-4 text-sm font-bold text-nickel md:text-base">
              {t('interestList')}
            </p>
          </button>
        </div>
        <div>
          <button type="button" onClick={handleRegisterVendor}>
            <p className="text-sm font-bold text-nickel md:text-base">
              {t('registerAccountVendor')}
            </p>
          </button>
        </div>
      </div>

      <div className="mb-6 border-b pb-5">
        <p className="mb-3 text-sm font-normal text-spanish-gray md:text-base">
          {t('common:menu')}
        </p>

        <button
          type="button"
          onClick={handleHiddenDetailListInsurance}
          className="flex w-full items-center justify-between pb-4"
        >
          <p className="text-sm font-bold text-nickel md:text-base">
            {t('auth:insurance')}
          </p>
          <div className="-rotate-90">
            <IconDropDown2 stroke={INK_LIGHT} />
          </div>
        </button>
        <button type="button" onClick={handleBlogs}>
          <p className="text-sm font-bold text-nickel md:text-base">
            {t('common:blog')}
          </p>
        </button>
      </div>

      {session?.data && (
        <div className="mb-6 border-b pb-5">
          <p className="mb-3 text-sm font-normal text-spanish-gray md:text-base">
            {t('common:account')}
          </p>

          <button
            type="button"
            onClick={handleSettingUser}
            className="mb-3 flex items-center gap-2"
          >
            <IconUser />
            <p className="text-sm font-bold text-nickel md:text-base">
              {t('setting_account')}
            </p>
          </button>

          <button
            type="button"
            onClick={handleChangePassword}
            className="flex items-center gap-2"
          >
            <IconLock />
            <p className="text-sm font-bold text-nickel md:text-base">
              {t('change_password')}
            </p>
          </button>
        </div>
      )}

      <div className="mb-6 border-b pb-5">
        <p className="mb-3 text-sm font-normal text-spanish-gray md:text-base">
          {t('common:language')}
        </p>
        <button
          type="button"
          className="flex w-full items-center justify-between "
          onClick={handleHiddenSelectLanguages}
        >
          <div className="flex items-center gap-2">
            <div className="flex h-6 w-6 items-center">
              <Image
                src={t('logoLanguages')}
                width={100}
                height={100}
                priority="true"
                alt="logo"
              />
            </div>
            <p className="text-sm font-bold text-nickel md:text-base">
              {t('auth:nameLanguages')}
            </p>
          </div>
          <div className="flex -rotate-90 items-end">
            <IconDropDown2 stroke={INK_LIGHT} />
          </div>
        </button>
      </div>

      {session?.data ? (
        <button
          type="button"
          className="mb-2 flex items-center gap-2"
          onClick={handleSignOut}
        >
          <span className="mx-1">
            <IconLogout />
          </span>
          <p className="text-sm font-bold text-nickel md:text-base">
            {t('sign_out')}
          </p>
        </button>
      ) : (
        <>
          <button type="button" onClick={handleLogin} className="block">
            <p className="mb-3 text-sm font-bold text-nickel md:text-base">
              {t('login')}
            </p>
          </button>
          <button type="button" onClick={handleRegister} className="block">
            <p className="text-sm font-bold text-nickel md:text-base">
              {t('registerAccount')}
            </p>
          </button>
        </>
      )}
    </div>
  );
}
