import { IconDropDown2 } from 'components/common/icon/Icon';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { INK_LIGHT } from 'src/utils/constants';

export default function SelectLanguages({
  setHiddenSelectLanguages,
  hiddenSelectLanguages,
  setHiddenMenuAfterNext,
  hiddenMenuAfterNext,
}) {
  const router = useRouter();
  const { t } = useTranslation(['auth', 'common']);
  const handleBackMenu = () => {
    setHiddenSelectLanguages(!hiddenSelectLanguages);
    setHiddenMenuAfterNext(!hiddenMenuAfterNext);
  };
  const handleSelectLanguageEn = () => {
    return router.push(router.asPath, undefined, { locale: 'en' });
  };
  const handleSelectLanguageVn = () => {
    return router.push(router.asPath, undefined, { locale: 'vi' });
  };

  return (
    <div className="relative h-screen w-full bg-white px-6 pt-6">
      <button
        type="button"
        onClick={handleBackMenu}
        className="mb-7 flex items-center gap-3 border-b border-cultured pb-3"
      >
        <div className=" rotate-90 ">
          <IconDropDown2 stroke={INK_LIGHT} />
        </div>
        <p className="text-sm font-normal text-arsenic md:text-base">
          {t('auth:back')}
        </p>
      </button>

      <div className="mb-6 border-b pb-5">
        <p className="mb-3 text-sm font-normal text-spanish-gray md:text-base">
          {t('common:language')}
        </p>
        <button
          type="button"
          onClick={handleSelectLanguageEn}
          className="mb-3 flex items-center gap-2"
        >
          <div className="flex h-6 w-6 items-center">
            <Image
              src="/images/navbar/logoEnglish.png"
              width={100}
              height={100}
              priority="true"
              alt="logoEn"
            />
          </div>
          <p className="text-sm font-bold text-nickel md:text-base">English</p>
        </button>
        <button
          type="button"
          onClick={handleSelectLanguageVn}
          className="flex items-center gap-2"
        >
          <div className="flex h-5 w-6 items-center">
            <Image
              src="/images/navbar/logoVietNam.png"
              width={100}
              height={100}
              priority="true"
              alt="logoVietNam"
            />
          </div>
          <p className="text-sm font-bold text-nickel md:text-base">
            Tiếng Việt
          </p>
        </button>
      </div>
    </div>
  );
}
