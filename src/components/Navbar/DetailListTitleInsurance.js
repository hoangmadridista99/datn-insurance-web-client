import { IconDropDown2 } from 'components/common/icon/Icon';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import { LIST_HEADERS } from 'src/utils/constants';

export default function DetailListTitleInsurance({
  hiddenMenuAfterNext,
  setHiddenMenuAfterNext,
  hiddenListInsurance,
  setHiddenListInsurance,
  setHiddenMenuNavbar,
  hiddenMenuNavbar,
}) {
  const { t } = useTranslation(['auth', 'common']);
  const LIST_MENU_INSURANCE = LIST_HEADERS(t)[0].contentHover;

  const handleBackMenu = () => {
    setHiddenListInsurance(!hiddenListInsurance);
    setHiddenMenuAfterNext(!hiddenMenuAfterNext);
  };
  const router = useRouter();
  const handlePush = (child) => {
    router.push(child?.href);
    setHiddenMenuNavbar(!hiddenMenuNavbar);
  };
  const handleCategory = () => {
    router.push('/category');
    setHiddenMenuNavbar(!hiddenMenuNavbar);
  };

  return (
    <div className="relative h-screen w-full bg-white p-6">
      <button
        type="button"
        onClick={handleBackMenu}
        className="mb-7 flex items-center gap-3 border-b border-cultured pb-3"
      >
        <div className="rotate-90">
          <IconDropDown2 stroke="#404249" />
        </div>
        <p className="text-sm font-normal text-arsenic md:text-base">
          {t('auth:back')}
        </p>
      </button>
      <button type="button" onClick={handleCategory}>
        <p className="mb-4 text-sm font-bold text-nickel md:text-base">
          {t('auth:insurance')}
        </p>
      </button>

      {LIST_MENU_INSURANCE.map((title) => (
        <div key={title?.idTitle} className="mb-6 border-b pb-2">
          <p className="mb-3 text-sm font-normal text-spanish-gray md:text-base">
            {title?.titleKey}
          </p>
          <div className="flex flex-col">
            {title.children.map((child) => (
              <div key={child.idChild}>
                <button
                  type="button"
                  onClick={() => handlePush(child)}
                  className="mb-3 text-sm font-bold text-nickel md:text-base"
                >
                  {child.label}
                </button>
              </div>
            ))}
          </div>
        </div>
      ))}
    </div>
  );
}
