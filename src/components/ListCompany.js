import React, { useState } from 'react';
import { useTranslation } from 'next-i18next';

function ListCompany({ register, companies, setError }) {
  const [company, setCompany] = useState(null);

  const { t } = useTranslation();

  const companyOther = {
    id: 'other',
    long_name: t('auth:company_other'),
  };

  const handleSelectCompany = (e) => {
    setError('company_id', { message: '' });
    setCompany(e.target.value);
  };

  return (
    <>
      <select
        id="company_id"
        className={`mt-2 w-full rounded-md border ${
          company ? 'text-black' : 'text-chinese-silver'
        } border-chinese-silver bg-cultured p-3 pr-4 focus:border-ultramarine-blue focus-visible:border-ultramarine-blue`}
        {...register('company_id', { require: true })}
        onChange={handleSelectCompany}
      >
        <option className="text-black" value="">
          {t('auth:company_select')}
        </option>
        {[...companies, companyOther].map((list) => (
          <option value={list?.id} key={list?.id} className="text-black">
            {list?.long_name}
          </option>
        ))}
      </select>
      {company && company === 'other' && (
        <input
          placeholder={t('auth:company_other_placeholder')}
          className="mt-2 w-full rounded-md border border-chinese-silver bg-transparent p-3 text-base font-normal leading-6 outline-none focus:border-ultramarine-blue"
          maxLength={50}
          id="company_other"
          {...register('company_other')}
        />
      )}
    </>
  );
}

export default ListCompany;
