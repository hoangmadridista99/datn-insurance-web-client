import IconEmail from 'components/common/icon/IconEmail';
import IconLocal from 'components/common/icon/IconLocal';
import IconPhone from 'components/common/icon/IconPhone';
import Image from 'next/image';
import Link from 'next/link';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { LIST_ICON } from 'src/utils/constants';

export default function LandingFooter({ backgroundFooter }) {
  const { t } = useTranslation(['auth']);

  const listFooter = [
    { id: 'F1', label: t('home'), url: '/' },
    { id: 'F2', label: t('aboutUs'), url: '/blogs' },
    { id: 'F3', label: t('blog'), url: '/blogs' },
    { id: 'F4', label: t('centerHelp'), url: '/blogs' },
    { id: 'F5', label: t('privatePolicy'), url: '/blogs' },
    { id: 'F6', label: t('termsOfService'), url: '/blogs' },
  ];

  return (
    <div className={backgroundFooter}>
      <footer className="comparison-container z-40 pt-6 sm:pt-16">
        <div className="flex flex-col">
          <div className="mb-10 flex justify-start sm:justify-center">
            <Link href="/">
              <div className="w-32 sm:w-40 lg:w-49">
                <Image
                  src="/images/navbar/logoComparison.png"
                  width={275}
                  height={100}
                  alt="comparison-logo"
                />
              </div>
            </Link>
          </div>
          <div className="flex sm:justify-center">
            <div className="mb-6 flex flex-col gap-5 sm:w-2/3 sm:flex-row sm:flex-wrap sm:justify-center sm:gap-0 lg:mb-8 lg:w-full lg:gap-10">
              {listFooter.map((list) => {
                return (
                  <div key={list?.id}>
                    <Link href={list?.url}>
                      <p className="text-sm font-bold text-arsenic sm:mr-10 sm:mb-4 sm:font-normal lg:mb-0 lg:mr-0 lg:text-base">
                        {list?.label}
                      </p>
                    </Link>
                  </div>
                );
              })}
            </div>
          </div>

          <div className="inline-flex justify-center gap-3 lg:mb-3">
            <div className="flex w-5 justify-center">
              <IconLocal />
            </div>

            <p className="text-sm font-normal text-philippine-gray">
              {t('address_company')}
            </p>
          </div>

          <div className="mb-6 flex flex-col-reverse justify-center text-sm font-normal text-philippine-gray sm:mb-10 sm:flex-row sm:gap-10">
            <div className="mt-2 inline-flex gap-3">
              <IconPhone />
              <p>{t('phone_company')}</p>
            </div>

            <div className="mt-2 inline-flex gap-3">
              <IconEmail />
              <p>{t('mail_company')}</p>
            </div>
          </div>
        </div>

        <div className="flex flex-col-reverse gap-4 border-t border-slate-200 py-6 sm:mb-10 sm:flex-col sm:gap-6 lg:mb-0 lg:flex-row lg:items-center lg:justify-between">
          <p className="text-center text-xs font-normal text-philippine-gray">
            {t('description_company')}
          </p>
          <div className="flex justify-center gap-4 sm:gap-5">
            {LIST_ICON.map((list) => {
              return (
                <div key={list?.id}>
                  <Link href={list?.url}>{list?.icon}</Link>
                </div>
              );
            })}
          </div>
        </div>
      </footer>
    </div>
  );
}
