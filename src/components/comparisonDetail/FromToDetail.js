import { useTranslation } from 'next-i18next';
import numeral from 'numeral';
import React from 'react';
import { AGE, YEAR, MONEY } from 'src/utils/constants';

export default function FromToDetail({ data, item, type }) {
  const { t } = useTranslation(['insuranceDetailsComparison']);
  const styleP = 'flex w-full items-start text-base font-normal text-nickel';
  return (
    <div>
      {(type === AGE || type === YEAR) && (
        <p className={styleP}>
          {!data?.[item?.firstFieldFromApi]?.[item?.secondFieldFromApi]?.from
            ? t('noSupport')
            : `${t('from')} ${
              data?.[item?.firstFieldFromApi]?.[item?.secondFieldFromApi]
                ?.from
            } ${t('to')} ${
              data?.[item?.firstFieldFromApi]?.[item?.secondFieldFromApi]?.to
            } ${t(type)}`}
        </p>
      )}
      {type === MONEY && (
        <p className={styleP}>
          {!data?.[item?.firstFieldFromApi]?.[item?.secondFieldFromApi]?.from
            ? t('noSupport')
            : `${t('from')} ${numeral(
              data?.[item?.firstFieldFromApi]?.[item?.secondFieldFromApi]
                ?.from
            ).format('(0,0)')} ${t('to')} ${numeral(
              data?.[item?.firstFieldFromApi]?.[item?.secondFieldFromApi]?.to
            ).format('(0,0)')} VNĐ`}
        </p>
      )}
    </div>
  );
}
