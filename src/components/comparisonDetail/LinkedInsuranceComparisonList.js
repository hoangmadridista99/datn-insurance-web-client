import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import { Dot } from 'components/common/icon/Icon';
import TitleInformationNoBorderLast from './TitleInformationNoBorderLast';

export default function LinkedInsuranceComparisonList({
  dataInsuranceComparison,
}) {
  const [hiddenDetail, setHiddenDetail] = useState(true);
  const { t } = useTranslation(['insuranceDetailsComparison']);
  return (
    <div className="hidden lg:block">
      <div className="comparison-container">
        <TitleInformationNoBorderLast
          title={t('linkedList')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div className="flex rounded-b-2xl border">
            <div className="w-1/4 p-6">
              <p className="text-sm text-raisin-black">{t('hospitals')}</p>
            </div>
            <div className="flex w-3/4">
              {dataInsuranceComparison?.map((data) => (
                <div key={data?.id} className="w-1/3 border-l p-6">
                  <div className="w-full items-start space-y-4">
                    {data?.hospitals?.map((item) => (
                      <div key={item?.id} className="flex">
                        <div className="mr-1">
                          <Dot />
                        </div>
                        <p className="w-57 text-sm text-nickel">{item?.name}</p>
                      </div>
                    ))}
                  </div>
                </div>
              ))}
              {dataInsuranceComparison?.length == 2 && (
                <div className="border-l"></div>
              )}
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
