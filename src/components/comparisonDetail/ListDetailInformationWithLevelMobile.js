import React from 'react';

function InformationDetailWithLevelMobile({ titleDetail, children }) {
  return (
    <>
      <div className="flex gap-x-[5px]">
        <div className="w-1/3">
          <p className=" text-xs font-normal leading-5 text-arsenic lg:text-sm lg:font-medium">
            {titleDetail}
          </p>
        </div>
        <div className="flex w-2/3">{children}</div>
      </div>
    </>
  );
}

export default InformationDetailWithLevelMobile;
