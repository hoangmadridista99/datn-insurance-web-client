import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';

import TitleInformationNoBorder from './TitleInformationNoBorder';

function BenefitKey({ dataInsuranceComparison }) {
  const { t } = useTranslation(['insuranceDetailsComparison']);
  const [hiddenDetail, setHiddenDetail] = useState(true);

  return (
    <div className="hidden lg:block">
      <div className="comparison-container">
        <TitleInformationNoBorder
          title={t('mainBenefits')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div className="flex flex-row-reverse border border-b-0">
            {(dataInsuranceComparison?.length === 3 && <div></div>) || (
              <div className="w-1/4 border-l"></div>
            )}
            {dataInsuranceComparison?.map((data) => (
              <div key={data?.id} className="w-1/4 border-l p-6">
                <p className="flex w-full items-start text-base font-normal text-nickel">
                  {data?.key_benefits}
                </p>
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
}

export default BenefitKey;
