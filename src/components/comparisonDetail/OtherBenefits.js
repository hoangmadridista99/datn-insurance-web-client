import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import InformationDetailWithLevel from './InformationDetailWithLevel';
import { formatLevelText } from 'src/utils/helper';
import TitleInformationNoBorder from './TitleInformationNoBorder';
import { descriptionDetailInsuranceHealth } from 'src/utils/constants';

export default function OtherBenefits({ dataInsuranceComparison }) {
  const [hiddenDetail, setHiddenDetail] = useState(true);
  const { t } = useTranslation(['insuranceDetailsComparison']);

  return (
    <div className="hidden lg:block">
      <div className="comparison-container">
        <TitleInformationNoBorder
          title={t('otherBenefits')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div className="border">
            <div>
              <div className="flex">
                <h6 className="w-1/4 pl-6 pt-6 text-base font-bold text-arsenic">
                  {t('additionalRights')}
                </h6>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
              </div>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('examination_and_diagnosis')}
                description={descriptionDetailInsuranceHealth.restore_functionality()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.dental?.examination_and_diagnosis?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.dental?.examination_and_diagnosis?.text
                          ? data?.dental?.examination_and_diagnosis?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('gingivitis')}
                description={descriptionDetailInsuranceHealth.gingivitis()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(data?.dental?.gingivitis?.level)}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.dental?.gingivitis?.text
                          ? data?.dental?.gingivitis?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('xray_and_diagnostic_imaging')}
                description={descriptionDetailInsuranceHealth.xray_and_diagnostic_imaging()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.dental?.xray_and_diagnostic_imaging?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.dental?.xray_and_diagnostic_imaging?.text
                          ? data?.dental?.xray_and_diagnostic_imaging?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('filling_teeth_basic')}
                description={descriptionDetailInsuranceHealth.filling_teeth_basic()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.dental?.filling_teeth_basic?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.dental?.filling_teeth_basic?.text
                          ? data?.dental?.filling_teeth_basic?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('root_canal_treatment')}
                description={descriptionDetailInsuranceHealth.root_canal_treatment()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.dental?.root_canal_treatment?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.dental?.root_canal_treatment?.text
                          ? data?.dental?.root_canal_treatment?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('dental_pathology')}
                description={descriptionDetailInsuranceHealth.dental_pathology()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(data?.dental?.dental_pathology?.level)}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.dental?.dental_pathology?.text
                          ? data?.dental?.dental_pathology?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('dental_calculus')}
                description={descriptionDetailInsuranceHealth.dental_calculus()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(data?.dental?.dental_calculus?.level)}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.dental?.dental_calculus?.text
                          ? data?.dental?.dental_calculus?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
            </div>
            <div>
              <div className="flex">
                <h6 className="w-1/4 pl-6 pt-6 text-base font-bold text-arsenic">
                  {t('generalInformation')}
                </h6>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
              </div>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('give_birth_normally')}
                description={descriptionDetailInsuranceHealth.give_birth_normally()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.obstetric?.give_birth_normally?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.obstetric?.give_birth_normally?.text
                          ? data?.obstetric?.give_birth_normally?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('caesarean_section')}
                description={descriptionDetailInsuranceHealth.caesarean_section()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.obstetric?.caesarean_section?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.obstetric?.caesarean_section?.text
                          ? data?.obstetric?.caesarean_section?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('obstetric_complication')}
                description={descriptionDetailInsuranceHealth.obstetric_complication()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.obstetric?.obstetric_complication?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.obstetric?.obstetric_complication?.text
                          ? data?.obstetric?.obstetric_complication?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('give_birth_abnormality')}
                description={descriptionDetailInsuranceHealth.give_birth_abnormality()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.obstetric?.give_birth_abnormality?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.obstetric?.give_birth_abnormality?.text
                          ? data?.obstetric?.give_birth_abnormality?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('after_give_birth_fee')}
                description={descriptionDetailInsuranceHealth.after_give_birth_fee()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.obstetric?.after_give_birth_fee?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.obstetric?.after_give_birth_fee?.text
                          ? data?.obstetric?.after_give_birth_fee?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('before_discharged')}
                description={descriptionDetailInsuranceHealth.before_discharged()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.obstetric?.before_discharged?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.obstetric?.before_discharged?.text
                          ? data?.obstetric?.before_discharged?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('postpartum_childcare_cost')}
                styleP="pb-6"
                description={descriptionDetailInsuranceHealth.postpartum_childcare_cost()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.obstetric?.postpartum_childcare_cost?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.obstetric?.postpartum_childcare_cost?.text
                          ? data?.obstetric?.postpartum_childcare_cost?.text
                          : t('noSupport')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
