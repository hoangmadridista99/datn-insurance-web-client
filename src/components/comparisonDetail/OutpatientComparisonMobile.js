import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import TitleInformationNoBorder from './TitleInformationNoBorder';
import InformationDetailWithLevelMobile from './ListDetailInformationWithLevelMobile';
import { formatLevelText } from 'src/utils/helper';

export default function OutpatientComparisonMobile({dataInsuranceComparison, indexInsuranceComparison}) {
  const { t } = useTranslation(['insuranceDetailsComparison']);
  const [hiddenDetail, setHiddenDetail] = useState(true);
  return (
    <div className="bg-cultured lg:hidden">
      <div className="comparison-container px-4 pb-3 md:px-10 lg:px-6">
        <TitleInformationNoBorder
          title={t('outpatient')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div>
            <div className="flex flex-col gap-y-3 rounded-b-lg border bg-white p-3">
              <InformationDetailWithLevelMobile titleDetail={t('examination_and_treatment')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.outpatient?.examination_and_treatment?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.outpatient?.examination_and_treatment?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile titleDetail={t('testing_and_diagnosis')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.outpatient?.testing_and_diagnosis?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.outpatient?.testing_and_diagnosis?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile titleDetail={t('home_care')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.outpatient?.home_care?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.outpatient?.home_care?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile titleDetail={t('due_to_accident')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.outpatient?.due_to_accident?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.outpatient?.due_to_accident?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile titleDetail={t('due_to_illness')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.outpatient?.due_to_illness?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.outpatient?.due_to_illness?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile titleDetail={t('due_to_cancer')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.
                          outpatient?.due_to_cancer?.level                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {dataInsuranceComparison[indexInsuranceComparison]
                        ?.outpatient?.due_to_cancer?.text

                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile titleDetail={t('restore_functionality')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.outpatient?.restore_functionality?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.outpatient?.restore_functionality?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>
            </div>
          </div>

        )}
      </div>
    </div>
  );
}
