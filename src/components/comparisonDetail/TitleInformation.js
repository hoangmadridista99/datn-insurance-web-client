import { IconDropDown2 } from 'components/common/icon/Icon';
import React from 'react';

function TitleInformation({ title, hiddenDetail, setHiddenDetail }) {
  const handleHiddenDetail = () => {
    setHiddenDetail(!hiddenDetail);
  };
  return (
    <div
      className={
        hiddenDetail
          ? 'flex justify-between rounded-t-md border border-b-0 border-platinum bg-cultured p-3 lg:rounded-none lg:rounded-tl-2xl lg:p-6'
          : 'flex justify-between rounded-md border bg-cultured p-3 lg:rounded-none lg:rounded-tl-2xl lg:border-b-0 lg:p-6'
      }
    >
      <p className=" text-xl font-medium leading-7 text-ultramarine-blue">
        {title}
      </p>
      <button type="button" onClick={handleHiddenDetail}>
        <div className={`${hiddenDetail ? '-rotate-180' : ''} duration-700`}>
          <IconDropDown2 stroke="#2B59FF" />
        </div>
      </button>
    </div>
  );
}

export default TitleInformation;
