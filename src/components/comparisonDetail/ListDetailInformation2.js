import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import TitleInformation from './TitleInformation';
import InformationDetailWithLevel2 from './InformationDetailWithLevel2';
import {
  FORMAT_MONEY,
  FORMAT_TIME_FEE_DEADLINE,
  FROM_TO_AGE,
  FROM_TO_MONEY,
  FROM_TO_YEAR,
  INSURED_PERSON,
  NORMAL,
  PROFESSION,
  descriptionDetailInsurance,
  descriptionDetailInsuranceHealth,
} from 'src/utils/constants';

function ListDetailInformation2({
  dataInsuranceComparison,
  indexInsuranceComparison,
}) {
  const [hiddenDetail, setHiddenDetail] = useState(true);
  const { t } = useTranslation(['insuranceDetailsComparison']);

  const listDetailInformation = [
    {
      id: 1,
      title: t('age_eligibility'),
      firstFieldFromApi: 'terms',
      secondFieldFromApi: 'age_eligibility',
      type: FROM_TO_AGE,
      description: descriptionDetailInsurance.age_eligibility(),
    },
    {
      id: 2,
      title: t('deadline_for_deal'),
      firstFieldFromApi: 'terms',
      secondFieldFromApi: 'deadline_for_deal',
      type: FROM_TO_YEAR,
      description: descriptionDetailInsurance.age_of_contract_termination(),
    },
    {
      id: 3,
      title: t('insurance_minimum_fee'),
      firstFieldFromApi: 'terms',
      secondFieldFromApi: 'insurance_minimum_fee',
      type: FORMAT_MONEY,
      description: descriptionDetailInsurance.insurance_minimum_fee(),
    },
    {
      id: 4,
      title: t('deadline_for_payment'),
      firstFieldFromApi: 'terms',
      secondFieldFromApi: 'deadline_for_payment',
      type: FORMAT_TIME_FEE_DEADLINE,
      description: descriptionDetailInsurance.deadline_for_payment(),
    },
    {
      id: 5,
      title: t('insured_person'),
      firstFieldFromApi: 'terms',
      secondFieldFromApi: 'insured_person',
      type: INSURED_PERSON,
      description: descriptionDetailInsurance.insured_person(),
    },
    {
      id: 6,
      title: t('profession'),
      firstFieldFromApi: 'terms',
      secondFieldFromApi: 'profession',
      type: PROFESSION,
      description: descriptionDetailInsurance.profession(),
    },
    {
      id: 7,
      title: t('termination_conditions'),
      firstFieldFromApi: 'terms',
      secondFieldFromApi: 'termination_conditions',
      type: NORMAL,
      description: descriptionDetailInsuranceHealth.customer_segment(),
    },
    {
      id: 8,
      title: t('total_sum_insured'),
      firstFieldFromApi: 'terms',
      secondFieldFromApi: 'total_sum_insured',
      type: FROM_TO_MONEY,
      description: descriptionDetailInsurance.total_sum_insured(),
    },
    {
      id: 9,
      title: t('monthly_fee'),
      firstFieldFromApi: 'terms',
      secondFieldFromApi: 'monthly_fee',
      type: FROM_TO_MONEY,
      description: descriptionDetailInsurance.monthly_fee(),
    },
  ];
  return (
    <div className="">
      <div className="max-w-360 bg-cultured px-4 pb-3 sm:px-10 lg:bg-white lg:px-16 lg:pb-0 xl:mx-auto xl:px-27">
        <TitleInformation
          title={t('overview')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />

        {hiddenDetail && (
          <div className="rounded-b-md border lg:border-b-0">
            <InformationDetailWithLevel2
              dataInsuranceComparison={dataInsuranceComparison}
              listDetailInformation={listDetailInformation}
              indexInsuranceComparison={indexInsuranceComparison}
            ></InformationDetailWithLevel2>
          </div>
        )}
      </div>
    </div>
  );
}

export default ListDetailInformation2;
