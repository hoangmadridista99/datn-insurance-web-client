import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import TitleInformationNoBorder from './TitleInformationNoBorder';
import { formatLevelText } from 'src/utils/helper';
import InformationDetailWithLevelMobile from './ListDetailInformationWithLevelMobile';

function InformationOtherMobile({
  dataInsuranceComparison,
  indexInsuranceComparison,
}) {
  const { t } = useTranslation(['insuranceDetailsComparison']);
  const [hiddenDetail, setHiddenDetail] = useState(true);
  return (
    <div className="bg-cultured lg:hidden">
      <div className="comparison-container px-4 pb-16 md:px-10 lg:px-6">
        <TitleInformationNoBorder
          title={t('otherInformation')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div>
            <div className="flex flex-col gap-y-3 rounded-b-lg border bg-white p-3">
              <h6 className="text-sm font-bold text-arsenic">
                {t('additionalRights')}
              </h6>
              <InformationDetailWithLevelMobile
                titleDetail={t('death_or_disability')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.death_or_disability?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.death_or_disability?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('serious_illnesses')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.serious_illnesses?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.serious_illnesses?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile titleDetail={t('health_care')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.health_care?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.health_care?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('investment_benefit')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.investment_benefit?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.investment_benefit?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('increasing_value_bonus')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.increasing_value_bonus?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.increasing_value_bonus?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('expiration_benefits')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.expiration_benefits?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.expiration_benefits?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile titleDetail={t('for_child')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.for_child?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.for_child?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('flexible_and_diverse')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.flexible_and_diverse?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.flexible_and_diverse?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('termination_benefits')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="mb-4 w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.termination_benefits?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.additional_benefits?.termination_benefits?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>
              <h6 className="text-sm font-bold text-arsenic">
                {t('customerOrientation')}
              </h6>
              <InformationDetailWithLevelMobile
                titleDetail={t('acceptance_rate')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.customer_orientation?.acceptance_rate?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.customer_orientation?.acceptance_rate?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('completion_time_deal')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.customer_orientation?.completion_time_deal?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.customer_orientation?.end_of_process?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('end_of_process')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.customer_orientation?.end_of_process?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.customer_orientation?.end_of_process?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('withdrawal_time')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.customer_orientation?.withdrawal_time?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.customer_orientation?.withdrawal_time?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('reception_and_processing_time')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.customer_orientation?.reception_and_processing_time
                          ?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.customer_orientation?.reception_and_processing_time
                          ?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>
              <h6 className="text-sm font-bold text-arsenic">
                {t('productBrochure')}
              </h6>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default InformationOtherMobile;
