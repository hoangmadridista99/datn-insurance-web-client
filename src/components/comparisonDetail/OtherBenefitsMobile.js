import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import TitleInformationNoBorder from './TitleInformationNoBorder';
import InformationDetailWithLevelMobile from './ListDetailInformationWithLevelMobile';
import { formatLevelText } from 'src/utils/helper';

export default function OtherBenefitsMobile({
  dataInsuranceComparison,
  indexInsuranceComparison,
}) {
  const { t } = useTranslation(['insuranceDetailsComparison']);
  const [hiddenDetail, setHiddenDetail] = useState(true);
  return (
    <div className="bg-cultured lg:hidden">
      <div className="comparison-container px-4 pb-3 md:px-10 lg:px-6">
        <TitleInformationNoBorder
          title={t('otherBenefits')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div>
            <div className="flex flex-col gap-y-3 rounded-b-lg border bg-white p-3">
              <p className="text-sm font-bold text-raisin-black">
                {t('additionalRights')}
              </p>
              <InformationDetailWithLevelMobile
                titleDetail={t('examination_and_diagnosis')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.examination_and_diagnosis?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.examination_and_diagnosis?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile titleDetail={t('gingivitis')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.gingivitis?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.gingivitis?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('xray_and_diagnostic_imaging')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.xray_and_diagnostic_imaging?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.xray_and_diagnostic_imaging?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('filling_teeth_basic')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.filling_teeth_basic?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.filling_teeth_basic?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('root_canal_treatment')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.root_canal_treatment?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.root_canal_treatment?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('dental_pathology')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.dental_pathology?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.dental_pathology?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('dental_calculus')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.dental_calculus?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.dental?.dental_calculus?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>
              <p className="text-sm font-bold text-raisin-black">
                Thông tin chung
              </p>
              <InformationDetailWithLevelMobile
                titleDetail={t('give_birth_normally')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.give_birth_normally?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.give_birth_normally?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('caesarean_section')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.caesarean_section?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.caesarean_section?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('obstetric_complication')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.obstetric_complication?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.obstetric_complication?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('give_birth_abnormality')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.give_birth_abnormality?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.give_birth_abnormality?.level
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('after_give_birth_fee')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.after_give_birth_fee?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.after_give_birth_fee?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('before_discharged')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.before_discharged?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.before_discharged?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('postpartum_childcare_cost')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.postpartum_childcare_cost?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.obstetric?.postpartum_childcare_cost?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
