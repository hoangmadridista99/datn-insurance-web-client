import { IconGroupHover, IconQuestionMark } from 'components/common/icon/Icon';
import React from 'react';

function InformationDetailWithLevel({
  titleDetail,
  children,
  dataInsuranceComparison,
  styleP,
  description,
}) {
  return (
    <div className="flex">
      <div className="w-1/4">
        <p
          className={`${styleP} mt-4 flex justify-between px-6 pr-8 text-sm font-normal text-arsenic`}
        >
          {titleDetail}
          <div className="hidden lg:block">
            <div className="relative hidden lg:block">
              <div className="group absolute -right-5 hidden h-6 w-6 items-center sm:flex">
                <div className="cursor-pointer">
                  <IconQuestionMark />
                </div>
                <div className=" hidden w-97 text-xs font-bold text-nickel group-hover:block">
                  <div className="flex items-center">
                    <div className="drop-shadow-2xl">
                      <IconGroupHover />
                    </div>
                    <div className="w-[492px] rounded-lg bg-white p-2 shadow-groupHover">
                      {description}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </p>
      </div>
      <div className="flex w-3/4 flex-row-reverse">
        {dataInsuranceComparison?.length !== 3 && (
          <div className="w-1/3 border-l"></div>
        )}
        {children}
      </div>
    </div>
  );
}

export default InformationDetailWithLevel;
