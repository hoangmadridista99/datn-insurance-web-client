import { useTranslation } from 'next-i18next';
import React, { useEffect, useState } from 'react';
import InformationDetailWithLevel from './InformationDetailWithLevel';
import { formatLevelText } from 'src/utils/helper';
import TitleInformationNoBorderLast from './TitleInformationNoBorderLast';
import { descriptionDetailInsurance } from 'src/utils/constants';

function InformationOther({ dataInsuranceComparison }) {
  const { t } = useTranslation(['insuranceDetailsComparison']);

  const [hiddenDetail, setHiddenDetail] = useState(true);
  const [informationOtherDetailList, setInformationOtherDetailList] =
    useState();
  const [customerOrientationDetailList, setCustomerOrientationDetailList] =
    useState();

  const handleInformationOtherList = () => {
    let additionalPermissionsList = [];

    for (const key in dataInsuranceComparison[0]?.additional_benefits) {
      if (
        dataInsuranceComparison[0].additional_benefits.hasOwnProperty.call(
          dataInsuranceComparison[0].additional_benefits,
          key
        )
      ) {
        const element = key;
        additionalPermissionsList.push(element);
      }
    }

    const informationOtherList = [
      {
        sectionDetailId: 1,
        sectionDetailName: 'death_or_disability',
        sectionType: additionalPermissionsList[4],
        description: descriptionDetailInsurance.death_or_disability(),
      },
      {
        sectionDetailId: 2,
        sectionDetailName: 'serious_illnesses',
        sectionType: additionalPermissionsList[5],
        description: descriptionDetailInsurance.serious_illnesses(),
      },
      {
        sectionDetailId: 3,
        sectionDetailName: 'health_care',
        sectionType: additionalPermissionsList[6],
        description: descriptionDetailInsurance.health_care(),
      },
      {
        sectionDetailId: 4,
        sectionDetailName: 'investment_benefit',
        sectionType: additionalPermissionsList[7],
        description: descriptionDetailInsurance.investment_benefit(),
      },
      {
        sectionDetailId: 5,
        sectionDetailName: 'increasing_value_bonus',
        sectionType: additionalPermissionsList[8],
        description: descriptionDetailInsurance.increasing_value_bonus(),
      },
      {
        sectionDetailId: 6,
        sectionDetailName: 'expiration_benefits',
        sectionType: additionalPermissionsList[12],
        description: descriptionDetailInsurance.expiration_benefits(),
      },
      {
        sectionDetailId: 7,
        sectionDetailName: 'for_child',
        sectionType: additionalPermissionsList[9],
        description: descriptionDetailInsurance.for_child(),
      },
      {
        sectionDetailId: 8,
        sectionDetailName: 'flexible_and_diverse',
        sectionType: additionalPermissionsList[10],
        description: descriptionDetailInsurance.flexible_and_diverse(),
      },
      {
        sectionDetailId: 9,
        sectionDetailName: 'termination_benefits',
        sectionType: additionalPermissionsList[11],
        description: descriptionDetailInsurance.termination_benefits(),
      },
    ];
    return setInformationOtherDetailList(informationOtherList);
  };
  const handleCustomerOrientationList = () => {
    let customerOrientationSectionList = [];
    for (const key in dataInsuranceComparison[0]?.customer_orientation) {
      if (
        dataInsuranceComparison[0].customer_orientation.hasOwnProperty.call(
          dataInsuranceComparison[0].customer_orientation,
          key
        )
      ) {
        const element = key;
        customerOrientationSectionList.push(element);
      }
    }
    const customerOrientationList = [
      {
        sectionDetailId: 1,
        sectionDetailName: 'acceptance_rate',
        sectionType: customerOrientationSectionList[5],
        description: descriptionDetailInsurance.acceptance_rate(),
      },
      {
        sectionDetailId: 2,
        sectionDetailName: 'completion_time_deal',
        sectionType: customerOrientationSectionList[6],
        description: descriptionDetailInsurance.completion_time_deal(),
      },
      {
        sectionDetailId: 3,
        sectionDetailName: 'end_of_process',
        sectionType: customerOrientationSectionList[7],
        description: descriptionDetailInsurance.end_of_process(),
      },
      {
        sectionDetailId: 4,
        sectionDetailName: 'withdrawal_time',
        sectionType: customerOrientationSectionList[8],
        description: descriptionDetailInsurance.withdrawal_time(),
      },
      {
        sectionDetailId: 5,
        sectionDetailName: 'reception_and_processing_time',
        sectionType: customerOrientationSectionList[4],
        description: descriptionDetailInsurance.reception_and_processing_time(),
      },
    ];

    return setCustomerOrientationDetailList(customerOrientationList);
  };
  useEffect(() => {
    console.log('tiiasds', dataInsuranceComparison);
    handleInformationOtherList();
    handleCustomerOrientationList();
  }, [dataInsuranceComparison]);
  return (
    <div className="hidden lg:block">
      {/* <div className="comparison-container">
        <TitleInformationNoBorderLast
          title="Thông tin khác"
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div className="mb-[120px] rounded-b-2xl border">
            <div>
              <div className="flex">
                <h6 className="w-1/4 pl-6 pt-6 text-base font-bold text-arsenic">
                  Quyền bổ sung
                </h6>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
              </div>

              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Quyền lợi bảo vệ trước tử vong/thương tật"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.additional_benefits?.death_or_disability?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.additional_benefits?.death_or_disability?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>

              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Quyền lợi bảo vệ trước bệnh ung thư, hiểm nghèo"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.additional_benefits?.serious_illnesses?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.additional_benefits?.serious_illnesses?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>

              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Quyền lợi chăm sóc y tế"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.additional_benefits?.health_care?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.additional_benefits?.health_care?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>

              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Quyền lợi đầu tư của sản phẩm liên kết chung"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.additional_benefits?.investment_benefit?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.additional_benefits?.investment_benefit?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>

              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Các quyền lợi thưởng gia tăng giá trị Hợp đồng"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.additional_benefits?.increasing_value_bonus
                            ?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {
                          data?.additional_benefits?.increasing_value_bonus
                            ?.text
                        }
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>

              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Quyền lợi đáo hạn"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.additional_benefits?.expiration_benefits?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.additional_benefits?.expiration_benefits?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>

              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Quyền lợi cho con"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.additional_benefits?.for_child?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.additional_benefits?.for_child?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>

              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Quyền lợi linh hoạt, đa dạng"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.additional_benefits?.flexible_and_diverse?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.additional_benefits?.flexible_and_diverse?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>

              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Quyền lợi kết thúc hợp đồng"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.additional_benefits?.termination_benefits?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.additional_benefits?.termination_benefits?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
            </div>
            <div>
              <div className="flex">
                <h6 className="w-1/4 pl-6 pt-6 text-base font-bold text-arsenic">
                  Định hướng khách hàng
                </h6>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
              </div>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Tỷ lệ ký hợp đồng thành công"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.customer_orientation?.acceptance_rate?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.customer_orientation?.acceptance_rate?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Thời gian hoàn thành hợp đồng"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.customer_orientation?.completion_time_deal
                            ?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.customer_orientation?.completion_time_deal?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Quy trình kết thúc"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.customer_orientation?.end_of_process?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.customer_orientation?.end_of_process?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Thời gian rút tiền"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.customer_orientation?.withdrawal_time?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.customer_orientation?.withdrawal_time?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail="Thời gian xử lý và tiếp nhận hợp đồng"
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.customer_orientation
                            ?.reception_and_processing_time?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {
                          data?.customer_orientation
                            ?.reception_and_processing_time?.text
                        }
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
            </div>
            <div>
              <div className="flex">
                <h6 className="w-1/4 pl-6 pt-6 text-base font-bold text-arsenic">
                  Tài liệu giới thiệu sản phẩm
                </h6>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
              </div>
            </div>
          </div>
        )}
      </div> */}
      {/* <div>
        <p>
          -----------------------------------------------------------------------------------------------------------------
        </p>

        <InformationDetailWithLevel
          dataInsuranceComparison={dataInsuranceComparison}
          titleDetail="Quyền lợi bảo vệ trước bệnh ung thư, hiểm nghèo"
        >
          {dataInsuranceComparison?.map((data) => (
            <ComparisonDetailPC
              key={data?.id}
              level={data?.additional_benefits?.death_or_disability?.level}
              text={data?.additional_benefits?.death_or_disability?.text}
            />
          ))}
        </InformationDetailWithLevel>
        {informationOtherList?.map((item) => {
          console.log('item', item?.sectionType);
          return (
            <InformationDetailWithLevel
              key={item?.sectionDetailId}
              dataInsuranceComparison={dataInsuranceComparison}
              titleDetail={item?.sectionDetailName}
            >
              {dataInsuranceComparison?.map((data) => (
                <div key={data?.id} className="w-1/3 border-l px-6">
                  <div className="mt-4 flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        data?.additional_benefits?.[item?.sectionType]?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {data?.additional_benefits?.[item?.sectionType]?.text}
                    </p>
                  </div>
                </div>
              ))}
            </InformationDetailWithLevel>
          );
        })}
        <div>-------------------------------------</div>
      </div> */}

      <div className="comparison-container">
        <TitleInformationNoBorderLast
          title={t('otherInformation')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div className="mb-30 rounded-b-2xl border">
            <div>
              <div className="flex">
                <h6 className="w-1/4 pl-6 pt-6 text-base font-bold text-arsenic">
                  {t('additionalRights')}
                </h6>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
              </div>
              {informationOtherDetailList?.map((item) => {
                return (
                  <InformationDetailWithLevel
                    key={item?.sectionDetailId}
                    dataInsuranceComparison={dataInsuranceComparison}
                    titleDetail={t(item?.sectionDetailName)}
                    description={item?.description}
                  >
                    {dataInsuranceComparison?.map((data) => (
                      <div key={data?.id} className="w-1/3 border-l px-6">
                        <div className="mt-4 flex w-full items-start gap-2">
                          <div className="h-6 w-6">
                            {formatLevelText(
                              data?.additional_benefits?.[item?.sectionType]
                                ?.level
                            )}
                          </div>
                          <p className="flex w-full items-start text-base font-normal text-nickel">
                            {data?.additional_benefits?.[item?.sectionType]
                              ?.text
                              ? data?.additional_benefits?.[item?.sectionType]
                                ?.text
                              : t('noSupport')}
                          </p>
                        </div>
                      </div>
                    ))}
                  </InformationDetailWithLevel>
                );
              })}
            </div>
            <div>
              <div className="flex">
                <h6 className="w-1/4 pl-6 pt-6 text-base font-bold text-arsenic">
                  {t('customerOrientation')}
                </h6>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
              </div>
              {customerOrientationDetailList?.map((item) => {
                return (
                  <InformationDetailWithLevel
                    key={item?.sectionDetailId}
                    dataInsuranceComparison={dataInsuranceComparison}
                    titleDetail={t(item?.sectionDetailName)}
                    description={item?.description}
                  >
                    {dataInsuranceComparison?.map((data) => (
                      <div key={data?.id} className="w-1/3 border-l px-6">
                        <div className="mt-4 flex w-full items-start gap-2">
                          <div className="h-6 w-6">
                            {formatLevelText(
                              data?.customer_orientation?.[item?.sectionType]
                                ?.level
                            )}
                          </div>
                          <p className="flex w-full items-start text-base font-normal text-nickel">
                            {data?.customer_orientation?.[item?.sectionType]
                              ?.text
                              ? data?.customer_orientation?.[item?.sectionType]
                                ?.text
                              : t('noSupport')}
                          </p>
                        </div>
                      </div>
                    ))}
                  </InformationDetailWithLevel>
                );
              })}
            </div>
            <div>
              <div className="flex">
                <h6 className="w-1/4 py-6 pl-6 text-base font-bold text-arsenic">
                  {t('productBrochure')}
                </h6>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
                <div className="w-1/4 border-l"></div>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default InformationOther;
