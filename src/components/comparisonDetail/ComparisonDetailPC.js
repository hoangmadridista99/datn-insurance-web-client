import React from 'react';
import { formatLevelText } from 'src/utils/helper';

export default function ComparisonDetailPC({ key, level, text }) {
  return (
    <div key={key} className="w-1/3 border-l px-6">
      <div className="mt-4 flex w-full items-start gap-2">
        <div className="h-6 w-6">{formatLevelText(level)}</div>
        <p className="flex w-full items-start text-base font-normal leading-6 text-nickel">
          {text}
        </p>
      </div>
    </div>
  );
}
