import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import TitleInformationNoBorder from './TitleInformationNoBorder';
import { Dot } from 'components/common/icon/Icon';

export default function LinkedInsuranceComparisonListMobile({
  dataInsuranceComparison,
  indexInsuranceComparison,
}) {
  const { t } = useTranslation(['insuranceDetailsComparison']);
  const [hiddenDetail, setHiddenDetail] = useState(true);
  return (
    <div className="bg-cultured pb-16 sm:pb-20 lg:hidden">
      <div className="comparison-container px-4 pb-3 md:px-10 lg:px-6">
        <TitleInformationNoBorder
          title={t('linkedList')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div className="flex border border-b-0 bg-white p-3">
            <div key={dataInsuranceComparison[indexInsuranceComparison]?.id}>
              <div className="flex w-full flex-col items-start gap-3">
                {dataInsuranceComparison[
                  indexInsuranceComparison
                ]?.hospitals?.map((item) => (
                  <div key={item?.id} className="flex gap-1">
                    <Dot />
                    <p className="text-center text-sm text-raisin-black">
                      {item?.name}
                    </p>
                  </div>
                ))}
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
