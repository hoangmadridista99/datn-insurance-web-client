import { useTranslation } from 'next-i18next';
import numeral from 'numeral';
import React from 'react';
import { AGE, YEAR, MONEY } from 'src/utils/constants';

export default function FromToDetailMobile({ data, index, item, type }) {
  const { t } = useTranslation(['insuranceDetailsComparison']);
  const detailMobileStyle =
    'flex w-full items-start text-sm font-normal text-nickel lg:text-base';
  return (
    <div>
      {(type === AGE || type === YEAR) && (
        <p className={detailMobileStyle}>
          {!data[index]?.[item?.firstFieldFromApi]?.[item?.secondFieldFromApi]
            ?.from
            ? t('noSupport')
            : `${t('from')} ${
              data[index]?.[item?.firstFieldFromApi]?.[item?.secondFieldFromApi]?.from
            } ${t('to')} ${
              data[index]?.[item?.firstFieldFromApi]?.[item?.secondFieldFromApi]?.to
            } ${t(type)}`}
        </p>
      )}
      {type === MONEY && (
        <p className={detailMobileStyle}>
          {
            !data[index]?.[item?.firstFieldFromApi]?.[item?.secondFieldFromApi]?.from
              ? t('noSupport') :`${t('from')} ${numeral(
                data[index]?.[item?.firstFieldFromApi]?.[item?.secondFieldFromApi]?.from
              ).format('(0,0)')} ${t('to')} ${numeral(
                data[index]?.[item?.firstFieldFromApi]?.[item?.secondFieldFromApi]?.to
              ).format('(0,0)')} VNĐ`
          }
        </p>
      )}
    </div>
  );
}
