/* eslint-disable indent */
/* eslint-disable react/jsx-key */
import { useTranslation } from 'next-i18next';
import React, { useContext, useRef } from 'react';
import { IconAddInsurance } from 'components/common/icon/Icon';
import { useRouter } from 'next/router';
import { DataContext } from 'src/store/GlobalState';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Virtual, Navigation, Pagination } from 'swiper';
import Image from 'next/image';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';
import ArrowLeftSwiperJs from 'components/common/icon/arrowLeftSwiperjs';
import ArrowRightSwiperJs from 'components/common/icon/arrowRightSwiperjs';
import ActiveDot from 'components/common/icon/activeDot';
import DisableDot from 'components/common/icon/disableDot';
import numeral from 'numeral';
import { BLUE_DARK, SILVER } from 'src/utils/constants';
import ButtonPrimary from 'components/common/ButtonPrimary';
import ButtonBgWhite from 'components/common/ButtonBgWhite';

SwiperCore.use([Virtual, Navigation, Pagination]);
function InformationKey({
  dataInsuranceComparison,
  dataInsuranceCheck,
  handleSwip,
  indexInsuranceComparison,
}) {
  const { dispatch } = useContext(DataContext);
  const { t } = useTranslation(['insuranceDetailsComparison']);

  const router = useRouter();
  const handleAddMoreCompare = () => {
    dispatch({
      type: 'COMPARISON',
      comparison: { comparison: true },
      updateDataInsurance: { updateDataInsurance: dataInsuranceCheck },
    });
    router.back();
  };

  const backSelectComparisonInsurance = () => {
    router.back();
  };

  const swiperRef = useRef(null);
  const handleTestNext = () => {
    if (swiperRef && swiperRef.current) {
      swiperRef.current.slideNext();
    }
  };
  const handleTestPrev = () => {
    if (swiperRef && swiperRef.current) {
      swiperRef.current.slidePrev();
    }
  };
  const handleTestClickToChange = (page) => {
    if (swiperRef && swiperRef.current) {
      swiperRef.current.slideTo(page);
    }
  };
  return (
    <>
      <div className="comparison-container hidden rounded-tl-2xl pt-4 lg:block">
        <button
          type="button"
          onClick={backSelectComparisonInsurance}
          className="mb-4 flex gap-1"
        >
          <Image
            width={24}
            height={24}
            src="/svg/back-concern.svg"
            alt="back-concern"
          />
          <p className="text-sm font-normal text-azure">{t('comeBack')}</p>
        </button>

        <div className="flex flex-row-reverse">
          <div className="flex w-3/4 flex-row-reverse rounded-t-xl border border-b-0">
            {dataInsuranceComparison.length !== 3 && (
              <button
                type="button"
                className="flex w-1/3 flex-col items-center justify-center "
                onClick={handleAddMoreCompare}
              >
                <div className="mb-6 flex justify-center">
                  <IconAddInsurance />
                </div>
                <p className="text-base font-bold text-nickel">
                  {t('chooseMoreToCompare')}
                </p>
              </button>
            )}

            {dataInsuranceComparison.map((value) => {
              return (
                <div
                  key={value?.id}
                  className="relative w-1/3 border-r p-6 first:border-r-0"
                >
                  <div className="flex h-full flex-col justify-between text-center">
                    <div className="flex h-full w-full flex-col items-center">
                      <div className="h-12 w-12">
                        <img
                          height={64}
                          width={64}
                          src={value?.company?.logo}
                          alt="logoCompany"
                        />
                      </div>
                      <div className="w-full">
                        <p className="mt-1 text-xl font-bold text-arsenic">
                          {value?.name}
                        </p>
                      </div>
                    </div>

                    <div className="w-full">
                      <p className="mt-4 text-2xl font-bold text-ultramarine-blue">
                        {numeral(value?.terms?.monthly_fee?.from).format(
                          '(0,0)'
                        )}
                        <span className="text-base font-normal text-silver">
                          /{t('month')}
                        </span>
                      </p>
                      <div className="mt-4 w-full">
                        <ButtonPrimary cases={'medium'} title={t('register')} />
                      </div>
                      <div className="mt-3 w-full">
                        <ButtonBgWhite
                          cases={'medium'}
                          title={t('requestAQuote')}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
      <div className="lg:hidden">
        <button
          type="button"
          onClick={backSelectComparisonInsurance}
          className="flex w-full items-center gap-1.5 bg-cultured py-4.5 pl-4"
        >
          <Image
            width={24}
            height={24}
            src="/svg/back-concern.svg"
            alt="back-concern"
          />
          <p className="text-sm font-normal text-azure">{t('comeBack')}</p>
        </button>

        <Swiper
          className="bg-cultured"
          id="qqqqqqqqqqq"
          slidesPerView={1.5}
          spaceBetween={20}
          modules={[Pagination]}
          onSlideChange={handleSwip}
          centeredSlides={true}
          onSwiper={(e) => (swiperRef.current = e)}
        >
          {dataInsuranceComparison.length === 3 ? (
            dataInsuranceComparison.map((value) => {
              return (
                <SwiperSlide key={value?.id} id="xxx">
                  <div key={value?.id} className="relative h-full w-full">
                    <div className="mb-2 flex h-full flex-col items-center justify-center rounded-lg bg-white p-4">
                      <div className="flex h-16 w-16 items-center">
                        <img
                          height={64}
                          width={64}
                          src={value?.company?.logo}
                          alt="logoCompany"
                        />
                      </div>
                      <p className="mt-1 max-w-full truncate whitespace-nowrap text-xl font-bold text-arsenic">
                        {value?.name}
                      </p>
                      <p className="mt-4 text-2xl font-bold text-ultramarine-blue">
                        {numeral(value?.terms?.monthly_fee?.from).format(
                          '(0,0)'
                        )}
                        <span className="text-base font-normal text-silver">
                          /{t('month')}
                        </span>
                      </p>
                      <div className="mt-4 w-full">
                        <ButtonPrimary cases={'medium'} title={t('register')} />
                      </div>
                      <div className="mt-3 w-full">
                        <ButtonBgWhite
                          cases={'medium'}
                          title={t('requestAQuote')}
                        />
                      </div>
                    </div>
                  </div>
                </SwiperSlide>
              );
            })
          ) : (
            <div>
              {dataInsuranceComparison.map((value) => {
                return (
                  <SwiperSlide key={value?.id} id="xxx">
                    <div key={value?.id} className="relative w-full">
                      <div className="flex flex-col items-center justify-center rounded-lg bg-white p-4">
                        <div className="flex h-16 w-16 items-center">
                          <img
                            height={64}
                            width={64}
                            src={value?.company?.logo}
                            alt="logoCompany"
                          />
                        </div>
                        <p className="mt-1 max-w-full truncate whitespace-nowrap text-xl font-bold text-arsenic">
                          {value?.name}
                        </p>
                        <p className="mt-4 text-2xl font-bold text-ultramarine-blue">
                          {numeral(value?.terms?.monthly_fee?.from).format(
                            '(0,0)'
                          )}
                          <span className="text-base font-normal text-silver">
                            /{t('month')}
                          </span>
                        </p>
                        <div className="mt-4 w-full">
                          <button
                            type="button"
                            className="w-full rounded-lg border bg-ultramarine-blue py-2 text-base font-bold text-cultured"
                          >
                            {t('register')}
                          </button>
                        </div>
                        <div className="mt-3 w-full">
                          <button
                            type="button"
                            className="w-full rounded-lg border border-nickel py-2 text-base font-bold text-nickel"
                          >
                            {t('requestAQuote')}
                          </button>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                );
              })}
              <SwiperSlide>
                <button
                  type="button"
                  className="m-auto flex h-72 w-full flex-col items-center justify-center rounded-lg bg-white"
                  onClick={handleAddMoreCompare}
                >
                  <div className="mb-6 flex justify-center">
                    <IconAddInsurance />
                  </div>
                  <p className="text-base font-bold text-nickel">
                    {t('chooseMoreToCompare')}
                  </p>
                </button>
              </SwiperSlide>
            </div>
          )}
        </Swiper>
        <div className="flex justify-center bg-cultured pb-4 pt-2">
          <button onClick={handleTestPrev}>
            <ArrowLeftSwiperJs
              color={indexInsuranceComparison > 0 ? BLUE_DARK : SILVER}
            />
          </button>
          <div className="flex items-center gap-2">
            <div className="flex gap-2">
              {dataInsuranceComparison.map((insurance, index) =>
                insurance?.id ===
                dataInsuranceComparison[indexInsuranceComparison]?.id ? (
                  <button key={insurance?.id}>
                    <ActiveDot />
                  </button>
                ) : (
                  <button
                    id={indexInsuranceComparison}
                    type="button"
                    onClick={() => handleTestClickToChange(index)}
                    key={insurance?.id}
                  >
                    <DisableDot />
                  </button>
                )
              )}
              {dataInsuranceComparison.length === 2 && (
                <button>
                  {indexInsuranceComparison === 2 ? (
                    <ActiveDot />
                  ) : (
                    <button
                      onClick={() => handleTestClickToChange(2)}
                      className="flex items-center"
                    >
                      <DisableDot />
                    </button>
                  )}
                </button>
              )}
            </div>
          </div>
          <button onClick={handleTestNext}>
            <ArrowRightSwiperJs
              color={indexInsuranceComparison === 2 ? SILVER : BLUE_DARK}
            />
          </button>
        </div>
      </div>
    </>
  );
}

export default InformationKey;
