import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import TitleInformationNoBorder from './TitleInformationNoBorder';
import { formatLevelText } from 'src/utils/helper';
import InformationDetailWithLevel from './InformationDetailWithLevel';
import { descriptionDetailInsuranceHealth } from 'src/utils/constants';

export default function OutpatientComparison({ dataInsuranceComparison }) {
  const [hiddenDetail, setHiddenDetail] = useState(true);
  const { t } = useTranslation(['insuranceDetailsComparison']);
  return (
    <div className="hidden lg:block">
      <div className="comparison-container">
        <TitleInformationNoBorder
          title={t('outpatient')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div className="border border-b-0">
            <div>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('examination_and_treatment')}
                description={descriptionDetailInsuranceHealth.examination_and_treatment()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.outpatient?.examination_and_treatment?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.outpatient?.examination_and_treatment?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('testing_and_diagnosis')}
                description={descriptionDetailInsuranceHealth.testing_and_diagnosis()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.outpatient?.testing_and_diagnosis?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.outpatient?.testing_and_diagnosis?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('home_care')}
                description={descriptionDetailInsuranceHealth.home_care()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(data?.outpatient?.home_care?.level)}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.outpatient?.home_care?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('due_to_accident')}
                description={descriptionDetailInsuranceHealth.due_to_accident()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.outpatient?.due_to_accident?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.outpatient?.due_to_accident?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('due_to_illness')}
                description={descriptionDetailInsuranceHealth.due_to_illness()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.outpatient?.due_to_illness?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.outpatient?.due_to_illness?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('due_to_cancer')}
                description={descriptionDetailInsuranceHealth.due_to_cancer()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.outpatient?.due_to_cancer?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.outpatient?.due_to_cancer?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('restore_functionality')}
                description={descriptionDetailInsuranceHealth.restore_functionality()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="my-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.outpatient?.restore_functionality?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.outpatient?.restore_functionality?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
