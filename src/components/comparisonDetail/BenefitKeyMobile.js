import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';

import TitleInformationNoBorder from './TitleInformationNoBorder';

function BenefitKeyMobile({
  dataInsuranceComparison,
  indexInsuranceComparison,
}) {
  const { t } = useTranslation(['insuranceDetailsComparison']);
  const [hiddenDetail, setHiddenDetail] = useState(true);

  return (
    <div className="bg-cultured lg:hidden">
      <div className="comparison-container px-4 pb-3 md:px-10 lg:px-6 lg:pb-0">
        <TitleInformationNoBorder
          title={t('mainBenefits')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div className="flex rounded-b-lg border bg-white">
            <div
              key={dataInsuranceComparison[indexInsuranceComparison]?.id}
              className="p-3"
            >
              <p className="flex w-full items-start text-base font-normal text-nickel">
                {
                  dataInsuranceComparison[indexInsuranceComparison]
                    ?.key_benefits
                }
              </p>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default BenefitKeyMobile;
