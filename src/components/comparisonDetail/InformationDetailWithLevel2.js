/* eslint-disable indent */
import { useTranslation } from 'next-i18next';
import numeral from 'numeral';
import React from 'react';
import {
  formatInsuredPerson,
  formatLevelText,
  formatProfession,
  formatTimeFeeDeadline,
} from 'src/utils/helper';
import {
  FORMAT_MONEY,
  FORMAT_TIME_FEE_DEADLINE,
  FROM_TO_AGE,
  FROM_TO_MONEY,
  FROM_TO_YEAR,
  INSURED_PERSON,
  NORMAL,
  NO_SUPPORT,
  PROFESSION,
  AGE,
  YEAR,
  MONEY,
} from 'src/utils/constants';
import FromToDetail from './FromToDetail';
import FromToDetailMobile from './FromToDetailMobile';
import { IconGroupHover, IconQuestionMark } from 'components/common/icon/Icon';
function InformationDetailWithLevel2({
  dataInsuranceComparison,
  listDetailInformation,
  indexInsuranceComparison,
}) {
  const { t } = useTranslation(['insuranceDetailsComparison']);
  const detailMobileStyle =
    'flex w-full items-start text-sm font-normal text-nickel lg:text-base';
  const detailStyle =
    'flex w-full items-start text-base font-normal text-nickel';
  return (
    <div className="space-y-3 rounded-md bg-white p-3 lg:space-y-0 lg:rounded-none lg:p-0">
      {listDetailInformation?.map((item, index) => (
        <div className="flex gap-1 lg:gap-0" key={item?.id}>
          <div className="w-1/3 lg:w-1/4">
            <div
              className={`flex items-start justify-between gap-x-4 text-xs font-normal text-arsenic lg:px-6 lg:pb-4 lg:text-sm ${
                index === 0 && 'lg:pt-4'
              }`}
            >
              <p>{item?.title}</p>
              <div className="relative hidden lg:block">
                <div className="group absolute right-0 hidden h-6 w-6 items-center sm:flex">
                  <div className="cursor-pointer">
                    <IconQuestionMark />
                  </div>
                  <div className=" hidden w-97 text-xs font-bold text-nickel group-hover:block">
                    <div className="flex items-center">
                      <div className="drop-shadow-2xl">
                        <IconGroupHover />
                      </div>
                      <div className="w-97 rounded-lg bg-white p-2 shadow-groupHover xl:w-114">
                        {item?.description}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="flex w-2/3 flex-row-reverse lg:w-3/4">
            {dataInsuranceComparison?.length !== 3 && (
              <div className="hidden w-1/3 border-l lg:block"></div>
            )}
            <div
              className={`hidden lg:flex lg:flex-row-reverse ${
                dataInsuranceComparison?.length !== 3 ? 'w-2/3' : 'w-full'
              }`}
            >
              {dataInsuranceComparison?.map((data) => (
                <div
                  key={data?.id}
                  className={`px-6 pb-4 lg:border-l ${index === 0 && 'pt-4'} ${
                    dataInsuranceComparison?.length !== 3 ? 'w-1/2' : 'w-1/3'
                  }`}
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        data?.[item?.firstFieldFromApi]?.[
                          item?.secondFieldFromApi
                        ]?.level
                      )}
                    </div>
                    {item?.type === FROM_TO_AGE && (
                      <FromToDetail data={data} item={item} type={AGE} />
                    )}
                    {item?.type === NORMAL && (
                      <p className={detailStyle}>
                        {!data?.[item?.firstFieldFromApi]?.[
                          item?.secondFieldFromApi
                        ]?.text
                          ? t('noSupport')
                          : data?.[item?.firstFieldFromApi]?.[
                              item?.secondFieldFromApi
                            ]?.text}
                      </p>
                    )}
                    {item?.type === FORMAT_MONEY && (
                      <p className={detailStyle}>
                        {!data?.[item?.firstFieldFromApi]?.[
                          item?.secondFieldFromApi
                        ]?.value ? (
                          t('noSupport')
                        ) : (
                          <span>
                            {numeral(
                              data?.[item?.firstFieldFromApi]?.[
                                item?.secondFieldFromApi
                              ]?.value
                            ).format('0,0')}
                            &nbsp;VNĐ
                          </span>
                        )}
                      </p>
                    )}
                    {item?.type === FORMAT_TIME_FEE_DEADLINE && (
                      <p className={detailStyle}>
                        {!data?.[item?.firstFieldFromApi]?.[
                          item?.secondFieldFromApi
                        ]?.value
                          ? t('noSupport')
                          : data?.[item?.firstFieldFromApi]?.[
                              item?.secondFieldFromApi
                            ]?.value
                              .map((value) => formatTimeFeeDeadline(value))
                              .join(', ')}
                      </p>
                    )}
                    {item?.type === FROM_TO_YEAR && (
                      <FromToDetail data={data} item={item} type={YEAR} />
                    )}
                    {item?.type === INSURED_PERSON && (
                      <p className={detailStyle}>
                        {!data?.[item?.firstFieldFromApi]?.[
                          item?.secondFieldFromApi
                        ]?.value
                          ? t('noSupport')
                          : data?.[item?.firstFieldFromApi]?.[
                              item?.secondFieldFromApi
                            ]?.value
                              .map((value) => formatInsuredPerson(value))
                              .join(', ')}
                      </p>
                    )}
                    {item?.type === PROFESSION && (
                      <p className={detailStyle}>
                        {!data?.[item?.firstFieldFromApi]?.[
                          item?.secondFieldFromApi
                        ]?.text
                          ? t('noSupport')
                          : data?.[item?.firstFieldFromApi]?.[
                              item?.secondFieldFromApi
                            ]?.text
                              .map((value) => formatProfession(value))
                              .join(', ')}
                      </p>
                    )}
                    {item?.type === FROM_TO_MONEY && (
                      <FromToDetail data={data} item={item} type={MONEY} />
                    )}
                    {item?.type === NO_SUPPORT && (
                      <p className={detailStyle}>{t('noSupport')}</p>
                    )}
                  </div>
                </div>
              ))}
            </div>

            <div className="flex w-full gap-1 lg:hidden">
              <div className="h-6 w-6">
                {formatLevelText(
                  dataInsuranceComparison[indexInsuranceComparison]?.[
                    item?.firstFieldFromApi
                  ]?.[item?.secondFieldFromApi]?.level
                )}
              </div>
              {item?.type === FROM_TO_AGE && (
                <FromToDetailMobile
                  data={dataInsuranceComparison}
                  index={indexInsuranceComparison}
                  type={AGE}
                  item={item}
                />
              )}
              {item?.type === NORMAL && (
                <p className={detailMobileStyle}>
                  {!dataInsuranceComparison[indexInsuranceComparison]?.[
                    item?.firstFieldFromApi
                  ]?.[item?.secondFieldFromApi]?.text
                    ? t('noSupport')
                    : dataInsuranceComparison[indexInsuranceComparison]?.[
                        item?.firstFieldFromApi
                      ]?.[item?.secondFieldFromApi]?.text}
                </p>
              )}
              {item?.type === FORMAT_MONEY && (
                <p className={detailMobileStyle}>
                  {!dataInsuranceComparison[indexInsuranceComparison]?.[
                    item?.firstFieldFromApi
                  ]?.[item?.secondFieldFromApi]?.value ? (
                    t('noSupport')
                  ) : (
                    <span>
                      {numeral(
                        dataInsuranceComparison[indexInsuranceComparison]?.[
                          item?.firstFieldFromApi
                        ]?.[item?.secondFieldFromApi]?.value
                      ).format('0,0')}
                      &nbsp;VNĐ
                    </span>
                  )}
                </p>
              )}
              {item?.type === FORMAT_TIME_FEE_DEADLINE && (
                <p className={detailMobileStyle}>
                  {!dataInsuranceComparison[indexInsuranceComparison]?.[
                    item?.firstFieldFromApi
                  ]?.[item?.secondFieldFromApi]?.value
                    ? t('noSupport')
                    : dataInsuranceComparison[indexInsuranceComparison]?.[
                        item?.firstFieldFromApi
                      ]?.[item?.secondFieldFromApi]?.value
                        .map((value) => formatTimeFeeDeadline(value))
                        .join(', ')}
                </p>
              )}
              {item?.type === FROM_TO_YEAR && (
                <FromToDetailMobile
                  data={dataInsuranceComparison}
                  index={indexInsuranceComparison}
                  type={YEAR}
                  item={item}
                />
              )}
              {item?.type === INSURED_PERSON && (
                <p className={detailMobileStyle}>
                  {dataInsuranceComparison[indexInsuranceComparison]?.[
                    item?.firstFieldFromApi
                  ]?.[item?.secondFieldFromApi]?.value
                    ? t('noSupport')
                    : dataInsuranceComparison[indexInsuranceComparison]?.[
                        item?.firstFieldFromApi
                      ]?.[item?.secondFieldFromApi]?.value
                        .map((value) => formatInsuredPerson(value))
                        .join(', ')}
                </p>
              )}
              {item?.type === PROFESSION && (
                <p className={detailMobileStyle}>
                  {dataInsuranceComparison[indexInsuranceComparison]?.[
                    item?.firstFieldFromApi
                  ]?.[item?.secondFieldFromApi]?.text
                    ? t('noSupport')
                    : dataInsuranceComparison[indexInsuranceComparison]?.[
                        item?.firstFieldFromApi
                      ]?.[item?.secondFieldFromApi]?.text
                        .map((value) => formatProfession(value))
                        .join(', ')}
                </p>
              )}
              {item?.type === FROM_TO_MONEY && (
                <FromToDetailMobile
                  data={dataInsuranceComparison}
                  index={indexInsuranceComparison}
                  type={MONEY}
                  item={item}
                />
              )}
              {item?.type === NO_SUPPORT && (
                <p className={detailMobileStyle}>{t('noSupport')}</p>
              )}
            </div>
          </div>
        </div>
      ))}
    </div>
  );
}

export default InformationDetailWithLevel2;
