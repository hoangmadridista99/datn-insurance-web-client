import { IconDropDown2 } from 'components/common/icon/Icon';
import React from 'react';

function TitleInformationNoBorder({ title, hiddenDetail, setHiddenDetail }) {
  const handleHiddenDetail = () => {
    setHiddenDetail(!hiddenDetail);
  };
  return (
    <div
      className={`${
        hiddenDetail ? 'rounded-t-md border-b-0' : 'rounded-md'
      } flex justify-between border border-platinum bg-cultured p-3 lg:mt-0 lg:rounded-none lg:p-6`}
    >
      <p className=" text-xl font-medium text-ultramarine-blue">{title}</p>
      <button type="button" onClick={handleHiddenDetail}>
        <div className={`${hiddenDetail && '-rotate-180'} duration-700`}>
          <IconDropDown2 stroke="#2B59FF" />
        </div>
      </button>
    </div>
  );
}

export default TitleInformationNoBorder;
