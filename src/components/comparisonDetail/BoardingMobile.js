import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import InformationDetailWithLevelMobile from './ListDetailInformationWithLevelMobile';
import { formatLevelText, formatRoomType } from 'src/utils/helper';
import TitleInformationNoBorder from './TitleInformationNoBorder';
import numeral from 'numeral';

export default function BoardingMobile({
  dataInsuranceComparison,
  indexInsuranceComparison,
}) {
  const { t } = useTranslation(['insuranceDetailsComparison']);
  const [hiddenDetail, setHiddenDetail] = useState(true);
  return (
    <div className="bg-cultured lg:hidden">
      <div className="comparison-container px-4 pb-3 md:px-10 lg:px-6">
        <TitleInformationNoBorder
          title={t('inPatient')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div>
            <div className="flex flex-col gap-y-3 rounded-b-lg border bg-white p-3">
              <InformationDetailWithLevelMobile titleDetail={t('room_type')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.room_type?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {dataInsuranceComparison[
                        indexInsuranceComparison
                      ]?.inpatient?.room_type?.values
                        .map((value) => formatRoomType(value))
                        .join(', ')}
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile titleDetail={t('for_cancer')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_cancer?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_cancer?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile titleDetail={t('for_cancer')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_cancer?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_illnesses?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('for_accidents')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_accidents?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_accidents?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile titleDetail={t('for_surgical')}>
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_surgical?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_surgical?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('for_hospitalization')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_hospitalization?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {`${t('maximum')} ${t('to')} ${numeral(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_hospitalization?.value
                      ).format('(0,0)')} VNĐ/${t('day')}`}
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('for_intensive_care')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_intensive_care?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_intensive_care?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('for_administrative')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_administrative?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_administrative?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>

              <InformationDetailWithLevelMobile
                titleDetail={t('for_organ_transplant')}
              >
                <div
                  key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                  className="mb-4 w-full"
                >
                  <div className="flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_organ_transplant?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {
                        dataInsuranceComparison[indexInsuranceComparison]
                          ?.inpatient?.for_organ_transplant?.text
                      }
                    </p>
                  </div>
                </div>
              </InformationDetailWithLevelMobile>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
