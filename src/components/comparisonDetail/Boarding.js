import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import TitleInformationNoBorder from './TitleInformationNoBorder';
import InformationDetailWithLevel from './InformationDetailWithLevel';
import { formatLevelText } from 'src/utils/helper';
import { formatRoomType } from 'src/utils/helper';
import numeral from 'numeral';
import { descriptionDetailInsuranceHealth } from 'src/utils/constants';

export default function Boarding({ dataInsuranceComparison }) {
  const [hiddenDetail, setHiddenDetail] = useState(true);
  const { t } = useTranslation(['insuranceDetailsComparison']);

  return (
    <div className="hidden lg:block">
      <div className="comparison-container">
        <TitleInformationNoBorder
          title={t('inPatient')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />
        {hiddenDetail && (
          <div className="border border-b-0">
            <div>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('room_type')}
                description={descriptionDetailInsuranceHealth.room_type()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(data?.inpatient?.room_type?.level)}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.inpatient?.room_type?.values
                          .map((value) => formatRoomType(value))
                          .join(', ')}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('for_cancer')}
                description={descriptionDetailInsuranceHealth.for_cancer()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(data?.inpatient?.for_cancer?.level)}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.inpatient?.for_cancer?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('for_illnesses')}
                description={descriptionDetailInsuranceHealth.for_illnesses()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(data?.inpatient?.for_illnesses?.level)}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.inpatient?.for_illnesses?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('for_accidents')}
                description={descriptionDetailInsuranceHealth.for_accidents()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(data?.inpatient?.for_accidents?.level)}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.inpatient?.for_accidents?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('for_surgical')}
                description={descriptionDetailInsuranceHealth.for_surgical()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(data?.inpatient?.for_surgical?.level)}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.inpatient?.for_surgical?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('for_hospitalization')}
                description={descriptionDetailInsuranceHealth.for_hospitalization()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.inpatient?.for_hospitalization?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {`${t('maximum')} ${t('to')} ${numeral(
                          data?.inpatient?.for_hospitalization?.value
                        ).format('(0,0)')} VNĐ/${t('day')}`}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('for_intensive_care')}
                description={descriptionDetailInsuranceHealth.for_intensive_care()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.inpatient?.for_intensive_care?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.inpatient?.for_intensive_care?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('for_administrative')}
                description={descriptionDetailInsuranceHealth.for_administrative()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="mt-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.inpatient?.for_administrative?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.inpatient?.for_administrative?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
              <InformationDetailWithLevel
                dataInsuranceComparison={dataInsuranceComparison}
                titleDetail={t('for_organ_transplant')}
                description={descriptionDetailInsuranceHealth.for_organ_transplant()}
              >
                {dataInsuranceComparison?.map((data) => (
                  <div key={data?.id} className="w-1/3 border-l px-6">
                    <div className="my-4 flex w-full items-start gap-2">
                      <div className="h-6 w-6">
                        {formatLevelText(
                          data?.inpatient?.for_organ_transplant?.level
                        )}
                      </div>
                      <p className="flex w-full items-start text-base font-normal text-nickel">
                        {data?.inpatient?.for_organ_transplant?.text}
                      </p>
                    </div>
                  </div>
                ))}
              </InformationDetailWithLevel>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}
