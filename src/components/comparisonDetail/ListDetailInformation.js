import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import InformationDetailWithLevel from './InformationDetailWithLevel';
import {
  formatInsuredPerson,
  formatLevelText,
  formatProfession,
  formatTimeFeeDeadline,
} from 'src/utils/helper';
import TitleInformation from './TitleInformation';
import numeral from 'numeral';

function ListDetailInformation({ dataInsuranceComparison }) {
  const [hiddenDetail, setHiddenDetail] = useState(true);
  const { t } = useTranslation(['insuranceDetailsComparison']);
  return (
    <div className="hidden lg:block">
      <div className="comparison-container">
        <TitleInformation
          title={t('overview')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />

        {hiddenDetail && (
          <div className="border border-b-0">
            <InformationDetailWithLevel
              dataInsuranceComparison={dataInsuranceComparison}
              titleDetail={t('age_eligibility')}
            >
              {dataInsuranceComparison?.map((data) => (
                <div key={data?.id} className="w-1/3 border-l px-6">
                  <div className="mt-4 flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(data?.terms?.age_eligibility?.level)}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {t('from')}&nbsp;{data?.terms?.age_eligibility?.from}
                      &nbsp;{t('to')}&nbsp;{data?.terms?.age_eligibility?.to}
                      &nbsp;{t('age')}
                    </p>
                  </div>
                </div>
              ))}
            </InformationDetailWithLevel>

            <InformationDetailWithLevel
              dataInsuranceComparison={dataInsuranceComparison}
              titleDetail={t('deadline_for_deal')}
            >
              {dataInsuranceComparison?.map((data) => (
                <div key={data?.id} className="w-1/3 border-l px-6">
                  <div className="mt-4 flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(data?.terms?.deadline_for_deal?.level)}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {t('from')}&nbsp;{data?.terms?.deadline_for_deal?.from}
                      &nbsp;{t('to')}&nbsp;{data?.terms?.deadline_for_deal?.to}
                      &nbsp;{t('year')}
                    </p>
                  </div>
                </div>
              ))}
            </InformationDetailWithLevel>

            <InformationDetailWithLevel
              dataInsuranceComparison={dataInsuranceComparison}
              titleDetail={t('insurance_minimum_fee')}
            >
              {dataInsuranceComparison?.map((data) => (
                <div key={data?.id} className="w-1/3 border-l px-6">
                  <div className="mt-4 flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(data?.terms?.deadline_for_deal?.level)}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {numeral(
                        data?.terms?.insurance_minimum_fee?.value
                      ).format('(0,0)')}
                      &nbsp;VNĐ
                    </p>
                  </div>
                </div>
              ))}
            </InformationDetailWithLevel>

            <InformationDetailWithLevel
              dataInsuranceComparison={dataInsuranceComparison}
              titleDetail={t('deadline_for_payment')}
            >
              {dataInsuranceComparison?.map((data) => (
                <div key={data?.id} className="w-1/3 border-l px-6">
                  <div className="mt-4 flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        data?.terms?.deadline_for_payment?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {data?.terms?.deadline_for_payment?.value
                        .map((value) => formatTimeFeeDeadline(value))
                        .join(', ')}
                    </p>
                  </div>
                </div>
              ))}
            </InformationDetailWithLevel>

            <InformationDetailWithLevel
              dataInsuranceComparison={dataInsuranceComparison}
              titleDetail={t('insured_person')}
            >
              {dataInsuranceComparison?.map((data) => (
                <div key={data?.id} className="w-1/3 border-l px-6">
                  <div className="mt-4 flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(data?.terms?.insured_person?.level)}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {data?.terms?.insured_person?.value
                        .map((value) => formatInsuredPerson(value))
                        .join(', ')}
                    </p>
                  </div>
                </div>
              ))}
            </InformationDetailWithLevel>

            <InformationDetailWithLevel
              dataInsuranceComparison={dataInsuranceComparison}
              titleDetail={t('profession')}
            >
              {dataInsuranceComparison?.map((data) => (
                <div key={data?.id} className="w-1/3 border-l px-6">
                  <div className="mt-4 flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(data?.terms?.profession?.level)}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {data?.terms?.profession?.text
                        .map((value) => formatProfession(value))
                        .join(', ')}
                    </p>
                  </div>
                </div>
              ))}
            </InformationDetailWithLevel>

            <InformationDetailWithLevel
              dataInsuranceComparison={dataInsuranceComparison}
              titleDetail={t('termination_conditions')}
            >
              {dataInsuranceComparison?.map((data) => (
                <div key={data?.id} className="w-1/3 border-l px-6">
                  <div className="mt-4 flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(
                        data?.terms?.termination_conditions?.level
                      )}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {data?.terms?.termination_conditions?.text}
                    </p>
                  </div>
                </div>
              ))}
            </InformationDetailWithLevel>

            <InformationDetailWithLevel
              dataInsuranceComparison={dataInsuranceComparison}
              titleDetail={t('total_sum_insured')}
            >
              {dataInsuranceComparison?.map((data) => (
                <div key={data?.id} className="w-1/3 border-l px-6">
                  <div className="mt-4 flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(data?.terms?.total_sum_insured?.level)}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {t('from')}&nbsp;
                      {numeral(data?.terms?.total_sum_insured?.from).format(
                        '(0,0)'
                      )}
                      &nbsp;VNĐ&nbsp;{t('to')} &nbsp;
                      {numeral(data?.terms?.total_sum_insured?.to).format(
                        '(0,0)'
                      )}
                      &nbsp;VNĐ
                    </p>
                  </div>
                </div>
              ))}
            </InformationDetailWithLevel>

            <InformationDetailWithLevel
              titleDetail={t('monthly_fee')}
              dataInsuranceComparison={dataInsuranceComparison}
            >
              {dataInsuranceComparison?.map((data) => (
                <div key={data?.id} className="w-1/3 border-l px-6">
                  <div className="mt-4 mb-4 flex w-full items-start gap-2">
                    <div className="h-6 w-6">
                      {formatLevelText(data?.terms?.monthly_fee?.level)}
                    </div>
                    <p className="flex w-full items-start text-base font-normal text-nickel">
                      {t('from')}&nbsp;
                      {numeral(data?.terms?.monthly_fee?.from).format('(0,0)')}
                      &nbsp; VNĐ&nbsp;{t('to')}&nbsp;
                      {numeral(data?.terms?.monthly_fee?.to).format('(0,0)')}
                      &nbsp; VNĐ
                    </p>
                  </div>
                </div>
              ))}
            </InformationDetailWithLevel>
          </div>
        )}
      </div>
    </div>
  );
}

export default ListDetailInformation;
