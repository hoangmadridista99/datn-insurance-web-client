import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import {
  formatInsuredPerson,
  formatLevelText,
  formatProfession,
  formatTimeFeeDeadline,
} from 'src/utils/helper';
import TitleInformation from './TitleInformation';
import numeral from 'numeral';
import InformationDetailWithLevelMobile from './ListDetailInformationWithLevelMobile';

function ListDetailInformationMobile({
  dataInsuranceComparison,
  indexInsuranceComparison,
}) {
  const [hiddenDetail, setHiddenDetail] = useState(true);
  const { t } = useTranslation(['insuranceDetailsComparison']);

  return (
    <div className="bg-cultured lg:hidden">
      <div className="comparison-container px-4 pb-3 md:px-10 lg:px-6 lg:pb-0">
        <TitleInformation
          title={t('overview')}
          hiddenDetail={hiddenDetail}
          setHiddenDetail={setHiddenDetail}
        />

        {hiddenDetail && (
          <div className="flex flex-col gap-y-3 rounded-b-lg border bg-white p-3">
            <InformationDetailWithLevelMobile
              titleDetail={t('age_eligibility')}
            >
              <div
                key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                className="w-full"
              >
                <div className="flex w-full items-start gap-2">
                  <div className="h-6 w-6">
                    {formatLevelText(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.age_eligibility?.level
                    )}
                  </div>
                  <p className="flex w-full items-start text-sm font-normal text-nickel lg:text-base">
                    {t('from')}&nbsp;
                    {
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.age_eligibility?.from
                    }
                    &nbsp;{t('to')}&nbsp;
                    {
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.age_eligibility?.to
                    }
                    &nbsp;
                    {t('age')}
                  </p>
                </div>
              </div>
            </InformationDetailWithLevelMobile>

            <InformationDetailWithLevelMobile
              titleDetail={t('deadline_for_deal')}
            >
              <div
                key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                className="w-full"
              >
                <div className="flex w-full items-start gap-2">
                  <div className="h-6 w-6">
                    {formatLevelText(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.deadline_for_deal?.level
                    )}
                  </div>
                  <p className="flex w-full items-start text-sm font-normal text-nickel lg:text-base">
                    {t('from')}&nbsp;
                    {
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.deadline_for_deal?.from
                    }
                    &nbsp;
                    {t('to')}&nbsp;
                    {
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.deadline_for_deal?.to
                    }
                    {t('year')}
                  </p>
                </div>
              </div>
            </InformationDetailWithLevelMobile>

            <InformationDetailWithLevelMobile
              titleDetail={t('insurance_minimum_fee')}
            >
              <div
                key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                className="w-full"
              >
                <div className="flex w-full items-start gap-2">
                  <div className="h-6 w-6">
                    {formatLevelText(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.deadline_for_deal?.level
                    )}
                  </div>
                  <p className="flex w-full items-start text-sm font-normal text-nickel lg:text-base">
                    {numeral(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.insurance_minimum_fee?.value
                    ).format('(0,0)')}
                    &nbsp;VNĐ
                  </p>
                </div>
              </div>
            </InformationDetailWithLevelMobile>

            <InformationDetailWithLevelMobile
              titleDetail={t('deadline_for_payment')}
            >
              <div
                key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                className="w-full"
              >
                <div className="flex w-full items-start gap-2">
                  <div className="h-6 w-6">
                    {formatLevelText(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.deadline_for_payment?.level
                    )}
                  </div>
                  <p className="flex w-full items-start text-sm font-normal text-nickel lg:text-base">
                    {dataInsuranceComparison[
                      indexInsuranceComparison
                    ]?.terms?.deadline_for_payment?.value
                      .map((value) => formatTimeFeeDeadline(value))
                      .join(', ')}
                  </p>
                </div>
              </div>
            </InformationDetailWithLevelMobile>

            <InformationDetailWithLevelMobile titleDetail={t('insured_person')}>
              <div
                key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                className="w-full"
              >
                <div className="flex w-full items-start gap-2">
                  <div className="h-6 w-6">
                    {formatLevelText(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.insured_person?.level
                    )}
                  </div>
                  <p className="flex w-full items-start text-sm font-normal text-nickel lg:text-base">
                    {dataInsuranceComparison[
                      indexInsuranceComparison
                    ]?.terms?.insured_person?.value
                      .map((value) => formatInsuredPerson(value))
                      .join(', ')}
                  </p>
                </div>
              </div>
            </InformationDetailWithLevelMobile>

            <InformationDetailWithLevelMobile titleDetail={t('profession')}>
              <div
                key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                className="w-full"
              >
                <div className="flex w-full items-start gap-2">
                  <div className="h-6 w-6">
                    {formatLevelText(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.profession?.level
                    )}
                  </div>
                  <p className="flex w-full items-start text-sm font-normal text-nickel lg:text-base">
                    {dataInsuranceComparison[
                      indexInsuranceComparison
                    ]?.terms?.profession?.text
                      .map((value) => formatProfession(value))
                      .join(', ')}
                  </p>
                </div>
              </div>
            </InformationDetailWithLevelMobile>

            <InformationDetailWithLevelMobile
              titleDetail={t('termination_conditions')}
            >
              <div
                key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                className="w-full"
              >
                <div className="flex w-full items-start gap-2">
                  <div className="h-6 w-6">
                    {formatLevelText(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.termination_conditions?.level
                    )}
                  </div>
                  <p className="flex w-full items-start text-sm font-normal text-nickel lg:text-base">
                    {
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.termination_conditions?.text
                    }
                  </p>
                </div>
              </div>
            </InformationDetailWithLevelMobile>

            <InformationDetailWithLevelMobile
              titleDetail={t('total_sum_insured')}
            >
              <div
                key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                className="w-full"
              >
                <div className="flex w-full items-start gap-2">
                  <div className="h-6 w-6">
                    {formatLevelText(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.total_sum_insured?.level
                    )}
                  </div>
                  <p className="flex w-full items-start text-sm font-normal text-nickel lg:text-base">
                    {t('from')}&nbsp;
                    {numeral(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.total_sum_insured?.from
                    ).format('(0,0)')}
                    &nbsp;VNĐ &nbsp;{t('to')}&nbsp;
                    {numeral(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.total_sum_insured?.to
                    ).format('(0,0)')}
                    &nbsp;VNĐ
                  </p>
                </div>
              </div>
            </InformationDetailWithLevelMobile>

            <InformationDetailWithLevelMobile titleDetail={t('monthly_fee')}>
              <div
                key={dataInsuranceComparison[indexInsuranceComparison]?.id}
                className="w-full"
              >
                <div className="flex w-full items-start gap-2">
                  <div className="h-6 w-6">
                    {formatLevelText(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.monthly_fee?.level
                    )}
                  </div>
                  <p className="flex w-full items-start text-sm font-normal text-nickel lg:text-base">
                    {t('from')}&nbsp;
                    {numeral(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.monthly_fee?.from
                    ).format('(0,0)')}
                    &nbsp;VNĐ &nbsp;{t('to')}&nbsp;
                    {numeral(
                      dataInsuranceComparison[indexInsuranceComparison]?.terms
                        ?.monthly_fee?.to
                    ).format('(0,0)')}
                    &nbsp;VNĐ
                  </p>
                </div>
              </div>
            </InformationDetailWithLevelMobile>
          </div>
        )}
      </div>
    </div>
  );
}

export default ListDetailInformationMobile;
