import React from 'react';
import { useTranslation } from 'next-i18next';
import { IconClosePartnerInformation } from 'components/common/icon/Icon';
import Image from 'next/image';

export default function PartnerInformation({ isOpen, handleClose, partners }) {
  const { t } = useTranslation(['common']);

  return (
    <div
      className={
        isOpen ? 'fixed inset-0 z-50 flex items-center bg-rich-black' : 'hidden'
      }
    >
      <div className="mx-auto w-full rounded-2xl bg-white pb-4 sm:w-2/3 sm:pb-8 lg:w-1/2">
        <div className="mb-4 flex items-center border-b border-platinum px-2 py-4">
          <p className="w-full text-center text-sm font-bold lg:text-base">
            {t('partnerInformation')}
          </p>
          <p className="h-8 w-8 cursor-pointer p-2" onClick={handleClose}>
            <IconClosePartnerInformation />
          </p>
        </div>
        <div className="mb-4 flex items-center gap-4 px-4 lg:px-8">
          <div className="h-10 w-10">
            <Image
              src={partners?.company_logo_url}
              alt="logoInsurance"
              width={50}
              height={50}
            />
          </div>
          <p className="text-base font-bold lg:text-xl">
            {t('libertyVietnamInsurance')}
          </p>
        </div>
        <div className="px-4 lg:px-8">{partners?.insurance_description}</div>
      </div>
    </div>
  );
}
