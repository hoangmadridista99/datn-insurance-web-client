/* eslint-disable react/jsx-key */
import React, { useEffect, useState } from 'react';
import AcceptInformation from './AcceptInformation';
import { useTranslation } from 'next-i18next';
import {
  convertTypeCar,
  convertTypeCarBusiness,
  formatTimeDate,
} from 'src/utils/helper';
import LineDetailInformation from './LineDetailInformation';
import numeral from 'numeral';

const listInformationInsurance = ({
  information,
  partners,
  totalFeeOfYear,
  timeNew,
  valueSeat,
  t,
}) => [
  { id: 1, label: t('service'), data: partners?.insurance_type },
  { id: 2, label: t('partner'), data: partners?.namePartner },
  {
    id: 3,
    label: t('fee_insurance'),
    data: <>{numeral(totalFeeOfYear).format('(0,0)')} VNĐ</>,
  },
  {
    id: 4,
    label: t('deadline_deal'),
    data: (
      <p className="flex gap-1">
        {formatTimeDate(information?.expiration_from)}-{formatTimeDate(timeNew)}
      </p>
    ),
  },
  {
    id: 5,
    label: t('target_use_car'),
    data: convertTypeCarBusiness(information?.is_transportation_business),
  },
  { id: 6, label: t('type_card'), data: convertTypeCar(information?.car_type) },
  {
    id: 7,
    label: t('number_seats'),
    data: valueSeat ? valueSeat : information?.car_seats,
  },
];
const listInformationRegisterCard = ({ information, t }) => [
  { id: 1, label: t('nameCard'), data: information?.registration_holder_name },
  { id: 2, label: t('address'), data: information?.registration_address },
  { id: 3, label: t('mall'), data: information?.registration_brand },
  {
    id: 4,
    label: t('car_license_plate'),
    data: information?.registration_car_license_plate,
  },
];
const listInformationCertificateRecipient = ({ information, t }) => [
  { id: 1, label: t('fullName'), data: information?.certificate_holder_name },
  {
    id: 2,
    label: t('cmnd_cccd'),
    data: information?.certificate_holder_identification_card,
  },
  {
    id: 3,
    label: t('phone_number'),
    data: information?.certificate_holder_phone,
  },
  { id: 4, label: t('email'), data: information?.certificate_holder_email },
  { id: 5, label: t('address'), data: information?.certificate_holder_address },
];

export default function DetailsStepTwo({
  dataInformationInsurance,
  partners,
  totalFeeOfYear,
  setValidateRule,
  validateRule,
  timeNew,
  valueSeat,
}) {
  const { t } = useTranslation(['common']);
  const [informationPartner, setInformationPartner] = useState({});
  const valuePartnerById = () => {
    partners.map((value) => {
      if (value.id === dataInformationInsurance?.car_insurance_id) {
        return setInformationPartner({
          namePartner: value?.insurance_name,
          insurance_type: value?.insurance_type,
        });
      }
    });
  };
  useEffect(() => {
    valuePartnerById();
  }, []);

  const informationInsurance = () => {
    return (
      <>
        {listInformationInsurance({
          valueSeat: valueSeat,
          timeNew: timeNew,
          information: dataInformationInsurance,
          partners: informationPartner,
          totalFeeOfYear: totalFeeOfYear,
          t: t,
        }).map((values) => (
          <LineDetailInformation
            keys={values?.id}
            label={values?.label}
            data={values?.data}
          />
        ))}
      </>
    );
  };

  const informationRegisterCard = () => {
    return (
      <>
        {listInformationRegisterCard({
          information: dataInformationInsurance,
          t: t,
        }).map((values) => (
          <LineDetailInformation
            keys={values?.id}
            label={values?.label}
            data={values?.data}
          />
        ))}
      </>
    );
  };

  const informationCertificateRecipient = () => {
    return (
      <>
        {listInformationCertificateRecipient({
          information: dataInformationInsurance,
          t: t,
        }).map((values) => (
          <LineDetailInformation
            keys={values?.id}
            label={values?.label}
            data={values?.data}
          />
        ))}
      </>
    );
  };

  const handleRule = () => {
    setValidateRule(!validateRule);
  };

  return (
    <div>
      <AcceptInformation
        title={t('information_insurance')}
        details={informationInsurance()}
      />
      <AcceptInformation
        title={t('information_car')}
        details={informationRegisterCard()}
      />
      <AcceptInformation
        title={t('information_user_certi')}
        details={informationCertificateRecipient()}
      />
      <div className="mb-8 flex items-center gap-3">
        <input type="checkbox" onClick={handleRule} className="h-6 w-6" />
        <p className="text-sm font-normal text-nickel lg:text-base">
          <span className="mr-1 text-vermilion">*</span>
          {t('read_and_accept')}
          <span className="text-ultramarine-blue hover:cursor-pointer">
            {t('rule_insurance')}
          </span>
          .
        </p>
      </div>
    </div>
  );
}
