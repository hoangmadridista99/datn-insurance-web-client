import React from 'react';

export default function AcceptInformation({ title, details }) {
  return (
    <div className="mb-8 rounded-lg border border-platinum p-4 lg:mb-10 lg:p-8">
      <p className="mb-8 text-2xl font-bold text-arsenic lg:mb-10 lg:text-pixel-32 lg:leading-pixel-48">
        {title}
      </p>
      <div>{details}</div>
    </div>
  );
}
