import React from 'react';

export default function LineDetailInformation({ keys, label, data }) {
  return (
    <div
      key={keys}
      className="mt-4 flex w-full justify-between border-b border-cultured pb-3"
    >
      <p className="text-sm font-normal text-nickel lg:text-base">{label}</p>
      <p className="text-sm font-bold text-arsenic lg:text-base lg:font-medium">
        {data}
      </p>
    </div>
  );
}
