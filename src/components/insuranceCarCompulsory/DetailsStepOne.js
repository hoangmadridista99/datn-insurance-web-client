import DetailsInformation from 'components/common/DetailsInformation';
import InputBase from 'components/common/InputBase';
import { IconRefresh } from 'components/common/icon/Icon';
import React, { useEffect, useState } from 'react';
import ListPartnerInformation from './ListPartnerInformation';
import { Controller } from 'react-hook-form';
import DatePicker from 'react-date-picker';
import 'react-date-picker/dist/DatePicker.css';
import 'react-calendar/dist/Calendar.css';
import { useTranslation } from 'next-i18next';
import { CalendarIcon } from 'src/utils/constants';
import {
  numberInputInvalidChars,
  regexCarLicense,
  regexEmail,
  regexPhoneNumber,
} from 'src/utils/helper';
import SelectFilter from 'components/common/SelectFilter';
import { getInsuranceCarSetting } from 'pages/api/insuranceCarMaterial';

const LIST_TYPE_CAR = (t) => [
  {
    value: false,
    name: 'noBusiness',
    label: t('no_business_car'),
  },
  { value: true, name: 'business', label: t('business_car') },
];

const TYPE_CAR_BUSINESS = ({ valueFee, t }) => [
  {
    value: 'less-than-six',
    name: t('less_than_six'),
    totalFeeYear: valueFee?.business_less_than,
  },
  {
    value: 6,
    name: t('six_seat'),
    totalFeeYear: valueFee?.business_six_seats,
  },
  {
    value: 7,
    name: t('seven_seat'),
    totalFeeYear: valueFee?.business_seven_seats,
  },
  {
    value: 8,
    name: t('eight_seat'),
    totalFeeYear: valueFee?.business_eight_seats,
  },
  {
    value: 9,
    name: t('nine_seat'),
    totalFeeYear: valueFee?.business_nine_seats,
  },
  {
    value: 10,
    name: t('ten_seat'),
    totalFeeYear: valueFee?.business_ten_seats,
  },
  {
    value: 11,
    name: t('eleven_seat'),
    totalFeeYear: valueFee?.business_eleven_seats,
  },
  {
    value: 'passenger-cargo-car',
    name: t('passenger_cargo_car'),
    totalFeeYear: valueFee?.business_passenger_cargo_car,
  },
];

const TYPE_CAR_NO_BUSINESS = ({ valueFee, t }) => [
  {
    value: 'less-than-six',
    name: t('less_than_six'),
    totalFeeYear: valueFee?.non_business_less_than,
  },
  {
    value: 'six-to-eleven',
    name: t('six_eleven_seat'),
    totalFeeYear: valueFee?.non_business_more_than_or_equal,
  },
  {
    value: 'passenger-cargo-car',
    name: t('passenger_cargo_car'),
    totalFeeYear: valueFee?.non_business_passenger_cargo_car,
  },
];
const SEATS_LESS_THAN_6 = (t) => [
  { value: 2, name: t('two_seat') },
  { value: 3, name: t('three_seat') },
  { value: 4, name: t('four_seat') },
  { value: 5, name: t('five_seat') },
];
const SEATS_6_11 = (t) => [
  { value: 6, name: t('six_seat') },
  { value: 7, name: t('seven_seat') },
  { value: 8, name: t('eight_seat') },
  { value: 9, name: t('nine_seat') },
  { value: 10, name: t('ten_seat') },
  { value: 11, name: t('eleven_seat') },
];
const SEATS_OTHER = (t) => [
  { value: 2, name: t('two_seat') },
  { value: 3, name: t('three_seat') },
  { value: 4, name: t('four_seat') },
  { value: 5, name: t('five_seat') },
  { value: 6, name: t('six_seat') },
  { value: 7, name: t('seven_seat') },
  { value: 8, name: t('eight_seat') },
];
export default function DetailsStepOne({
  register,
  control,
  errors,
  setError,
  partners,
  setValue,
  setTotalFeeOfYear,
  timeNew,
  setTimeDeadlineForDeal,
  typeCarBusiness,
  setTypeCarBusiness,
  setValueTypeCarNoBusiness,
  valueTypeCarNoBusiness,
  setValueCarSeats,
  valueCarSeats,
  refreshLabelInput,
  setRefreshLabelInput,
  setValueSeat,
  valueSeat,
  resetField,
}) {
  const { t } = useTranslation(['common', 'auth']);

  const [selectSeatsCar, setSelectSeatsCar] = useState();
  const [valueSettingCarInsurance, setValueSettingCarInsurance] = useState();
  const [valuePartner, setValuePartner] = useState();

  const dataSettingCarInsurance = async () => {
    try {
      const response = await getInsuranceCarSetting({
        params: {
          insurance_type: 'mandatory',
          limit: 10,
          page: 1,
        },
      });
      setValueSettingCarInsurance(response?.data);
    } catch (error) {
      console.log('🚀 ~ file: index.js:154 ~ nextSteps ~ error:', error);
    }
  };

  useEffect(() => {
    dataSettingCarInsurance();
  }, []);

  const onChangePhoneNumberCertificateHolder = (value) => {
    const checkPhoneNumber = regexPhoneNumber.test(value.target.value);
    if (!checkPhoneNumber) {
      setError('certificate_holder_phone', {
        message: t('auth:schema.phone_matches'),
      });
      return;
    }
    setError('certificate_holder_phone', {
      message: '',
    });
  };

  const onChangeEmailCertificateHolder = (value) => {
    const checkEmail = regexEmail.test(value.target.value);

    if (!checkEmail) {
      setError('certificate_holder_email', {
        message: t('auth:schema.email'),
      });
      return;
    }
    setError('certificate_holder_email', {
      message: '',
    });
  };

  const handleTypeCarBusiness = (value) => {
    setTypeCarBusiness(value);
  };

  const listTypeCarBusiness = () => {
    switch (typeCarBusiness) {
      case 'business':
        return TYPE_CAR_BUSINESS({
          valueFee: valueSettingCarInsurance,
          t: t,
        });
      case 'noBusiness':
        return TYPE_CAR_NO_BUSINESS({
          valueFee: valueSettingCarInsurance,
          t: t,
        });
    }
  };

  const handleTypeCarNoBusiness = (type) => {
    setTotalFeeOfYear(type.totalFeeYear);
    const checkTypeValue = typeof type.value;
    setValueTypeCarNoBusiness(type);
    setError('car_type', { message: '' });
    setValueCarSeats();
    if (checkTypeValue === 'string') {
      setValue('car_type', type.value);
      setValueSeat();
    }
    if (checkTypeValue === 'number') {
      const valueSeatString = String(type.value);
      setValue('car_type', valueSeatString);
      setValue('car_seats');
      setValueCarSeats();
    }
  };

  const listSeatsCar = (value) => {
    switch (value) {
      case 'less-than-six':
        return setSelectSeatsCar(SEATS_LESS_THAN_6(t));
      case 'six-to-eleven':
        return setSelectSeatsCar(SEATS_6_11(t));
      case 'passenger-cargo-car':
        return setSelectSeatsCar(SEATS_OTHER(t));
      default:
        return setValueSeat(value);
    }
  };
  useEffect(() => {
    listSeatsCar(valueTypeCarNoBusiness?.value);
  }, [valueTypeCarNoBusiness?.value]);

  const handleCarSeats = (seat) => {
    setValueCarSeats(seat);
    setValue('car_seats', seat.value);
    setError('car_seats', { message: '' });
  };

  const handleCarLicense = (value) => {
    const checkCarLicense = regexCarLicense.test(value.target.value);

    if (!value.target.value || checkCarLicense) {
      setError('registration_car_license_plate', {
        message: '',
      });
      return;
    }
    if (!checkCarLicense) {
      setError('registration_car_license_plate', {
        message: t('please_registration_car'),
      });
      return;
    }
  };

  const onChangeCertificate = (event) => {
    const {
      target: { value },
    } = event;

    if (value.length !== 12 && value.length !== 9) {
      setError('certificate_holder_identification_card', {
        message: 'Vui lòng nhâp đúng số CMND hoặc CCCD',
      });
      return;
    }
    setError('certificate_holder_identification_card', {
      message: '',
    });
  };

  const informationInsurance = () => {
    return (
      <>
        {LIST_TYPE_CAR(t).map((type) => (
          <div key={type.value} className="mb-4 flex gap-4">
            <input
              type="radio"
              id="is_transportation_business"
              value={type?.value}
              className="h-5 w-5"
              onClick={() => handleTypeCarBusiness(type.name)}
              {...register('is_transportation_business')}
            />
            <p className="text-sm font-normal text-arsenic lg:text-base">
              {type?.label}
            </p>
          </div>
        ))}
        {errors?.is_transportation_business && (
          <p className=" text-red-500 ">
            {errors?.is_transportation_business?.message}
          </p>
        )}

        <div className="mb-4">
          <p className="mb-1 flex justify-between text-sm font-bold text-nickel lg:text-base">
            {t('type_card')}
          </p>
          <SelectFilter
            errors={errors?.car_type?.message}
            options={listTypeCarBusiness()}
            selected={valueTypeCarNoBusiness}
            onChange={handleTypeCarNoBusiness}
            placeholder={t('please_type_card')}
          />
        </div>
        {errors?.car_type && (
          <p className=" text-red-500 ">{errors?.car_type?.message}</p>
        )}

        <div className="mb-4">
          <p className="mb-1 flex justify-between text-sm font-bold text-nickel lg:text-base">
            {t('number_seats')}
          </p>
          {!valueSeat ? (
            <SelectFilter
              errors={errors?.car_seats?.message}
              options={selectSeatsCar}
              selected={valueCarSeats}
              onChange={handleCarSeats}
              placeholder={t('please_number_seats')}
            />
          ) : (
            <input
              type="number"
              className="w-full rounded-md border border-chinese-silver p-3 [appearance:textfield] md:text-base [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:appearance-none"
              // id={'car_seats'}
              value={Number(valueSeat)}
            />
          )}
        </div>
        {errors?.car_seats && (
          <p className=" text-red-500 ">{errors?.car_seats?.message}</p>
        )}
      </>
    );
  };

  const informationRegisterCard = () => {
    const changeLabel = () => {
      const refreshLabel = () => {
        resetField('registration_car_license_plate');
        resetField('registration_chassis_number');
        setRefreshLabelInput(!refreshLabelInput);
      };
      return (
        <>
          <button
            type="button"
            className="flex items-center gap-1 text-xs font-normal text-ultramarine-blue lg:text-sm"
            onClick={refreshLabel}
          >
            <IconRefresh />
            {refreshLabelInput
              ? t('please_frame_number')
              : t('please_car_license_plate')}
          </button>
        </>
      );
    };
    return (
      <>
        <InputBase
          className="mb-4"
          label={t('nameCard')}
          placeholder={t('please_nameCard')}
          register={register}
          id="registration_holder_name"
          errors={errors}
        />
        <InputBase
          className="mb-4"
          label={t('addressRegister')}
          placeholder={t('please_addressRegister')}
          register={register}
          id="registration_address"
          errors={errors}
        />
        <InputBase
          className="mb-4"
          label={t('mall')}
          placeholder={t('please_mall')}
          register={register}
          id="registration_brand"
          errors={errors}
        />
        <InputBase
          type="text"
          className={`${!refreshLabelInput && 'mb-4'}`}
          label={refreshLabelInput ? t('car_license_plate') : t('frame_number')}
          changeLabel={changeLabel()}
          placeholder={refreshLabelInput ? t('please_car') : t('please_number')}
          register={register}
          onChange={refreshLabelInput ? handleCarLicense : null}
          id={
            refreshLabelInput
              ? 'registration_car_license_plate'
              : 'registration_chassis_number'
          }
          errors={errors}
        />
        {!refreshLabelInput && (
          <InputBase
            type="number"
            className="mb-4"
            label={t('engine_number')}
            placeholder={t('please_registration_engine_number')}
            register={register}
            id="registration_engine_number"
            errors={errors}
          />
        )}
      </>
    );
  };

  const certificateRecipient = () => {
    return (
      <>
        <InputBase
          className="mb-4"
          label={t('fullName')}
          placeholder={t('please_fullName')}
          register={register}
          id="certificate_holder_name"
          errors={errors}
        />
        <div className="mb-4 gap-4 lg:flex">
          <InputBase
            type="number"
            numberInputInvalidChars={numberInputInvalidChars}
            className="mb-4 w-full lg:mb-0"
            label={t('cmnd_cccd')}
            placeholder={t('please_CMND')}
            register={register}
            id="certificate_holder_identification_card"
            errors={errors}
            onChange={onChangeCertificate}
          />
          <InputBase
            className="mb-4 w-full lg:mb-0"
            label={t('phone_number')}
            placeholder={t('please_phone_number')}
            register={register}
            id="certificate_holder_phone"
            errors={errors}
            type="number"
            numberInputInvalidChars={numberInputInvalidChars}
            onChange={onChangePhoneNumberCertificateHolder}
          />
        </div>
        <InputBase
          className="mb-4"
          label="Email"
          placeholder={t('please_email')}
          register={register}
          id="certificate_holder_email"
          errors={errors}
          onChange={onChangeEmailCertificateHolder}
        />
        <InputBase
          label={t('address')}
          placeholder={t('please_address')}
          register={register}
          id="certificate_holder_address"
          errors={errors}
        />
      </>
    );
  };
  const listPartnerInsurance = () => {
    return (
      <div className="gap-4 sm:grid-cols-2 lg:grid lg:grid-cols-3">
        {partners?.map((partner) => (
          <div key={partner.id}>
            <ListPartnerInformation
              setValuePartner={setValuePartner}
              valuePartner={valuePartner}
              register={register}
              partners={partner}
            />
          </div>
        ))}
        {errors?.car_insurance_id && (
          <p className=" text-red-500 ">{errors?.car_insurance_id?.message}</p>
        )}
      </div>
    );
  };

  const deadlineForDeal = () => {
    const handleTimeValueFrom = (e, field) => {
      field.onChange(e.toISOString());
    };
    return (
      <div className="gap-4 lg:flex">
        <div className="mb-4 w-full lg:mb-0">
          <p className="mb-1 flex justify-between text-sm font-bold text-nickel lg:text-base">
            {t('start')}
          </p>
          <div className="rounded-md border border-platinum p-2 md:p-3">
            <Controller
              control={control}
              name="expiration_from"
              render={({ field }) => (
                <DatePicker
                  id="expiration_from"
                  name="expiration_from"
                  onChange={(e) => handleTimeValueFrom(e, field)}
                  monthPlaceholder="MM"
                  dayPlaceholder="DD"
                  yearPlaceholder="YYYY"
                  onClick={setTimeDeadlineForDeal(field.value)}
                  value={field.value}
                  clearIcon={null}
                  className="h-6 w-full"
                  calendarIcon={CalendarIcon}
                  minDate={new Date()}
                />
              )}
              {...register('expiration_from')}
            />
          </div>
          {errors?.expiration_from && (
            <p className=" text-red-500 ">{errors?.expiration_from?.message}</p>
          )}
        </div>
        <div className="w-full">
          <p className="mb-1 flex justify-between text-sm font-bold text-nickel lg:text-base">
            {t('end')}
          </p>
          <div className="rounded-md border border-platinum bg-anti_flash_white p-2 md:p-3">
            <Controller
              control={control}
              name="expiration_to"
              render={({ field }) => (
                <DatePicker
                  id="expiration_to"
                  name="expiration_to"
                  onChange={(e) => field.onChange(e)}
                  monthPlaceholder="MM"
                  dayPlaceholder="DD"
                  yearPlaceholder="YYYY"
                  value={timeNew}
                  clearIcon={null}
                  disabled
                  className="h-6 w-full"
                  calendarIcon={CalendarIcon}
                />
              )}
            />
          </div>
        </div>
      </div>
    );
  };

  return (
    <div>
      <DetailsInformation
        title={t('information_insurance')}
        details={informationInsurance()}
      />
      <DetailsInformation
        title={t('information_car')}
        details={informationRegisterCard()}
      />
      <DetailsInformation
        title={t('user_certi')}
        details={certificateRecipient()}
      />
      <DetailsInformation
        title={t('partner')}
        details={listPartnerInsurance()}
      />
      <DetailsInformation
        title={t('deal_for_insurance')}
        details={deadlineForDeal()}
      />
    </div>
  );
}
