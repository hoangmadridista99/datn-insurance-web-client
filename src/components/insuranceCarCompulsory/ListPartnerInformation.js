import React, { Fragment, useState } from 'react';
import PartnerInformation from './PartnerInformation';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import { Dialog, Transition } from '@headlessui/react';

export default function ListPartnerInformation({
  partners,
  register,
  setValuePartner,
  valuePartner,
}) {
  const { t } = useTranslation(['common']);

  const [isOpen, setIsOpen] = useState(false);

  function closeModal() {
    setIsOpen(false);
  }

  function openModal() {
    setIsOpen(true);
  }

  const handleChoicePartners = (value) => {
    setValuePartner(value.target.value);
  };

  return (
    <div
      className={`mb-6 rounded-lg border lg:mb-0 ${
        valuePartner === partners.id
          ? 'border-ultramarine-blue'
          : 'border-platinum'
      }`}
    >
      <div className="flex items-center gap-3 p-2">
        <input
          type="radio"
          id="car_insurance_id"
          onClick={handleChoicePartners}
          value={partners.id}
          className="h-5 w-5"
          {...register('car_insurance_id')}
        />
        <div className="h-8 w-8">
          <Image
            src={partners.company_logo_url}
            alt="logoPartnerInformation"
            width={32}
            height={32}
            className="h-8 w-8"
          />
        </div>
        <p>{partners.insurance_name}</p>
      </div>
      <div>
        <p
          className="cursor-pointer rounded-b-lg bg-cultured text-center text-sm font-normal"
          onClick={openModal}
        >
          {t('seeDetails')}
        </p>
        {isOpen && (
          <PartnerInformation
            partners={partners}
            isOpen={isOpen}
            handleClose={closeModal}
          />
        )}
        <Transition appear show={isOpen} as={Fragment}>
          <Dialog
            as="div"
            className="relative z-10"
            onClose={closeModal}
          ></Dialog>
        </Transition>
      </div>
    </div>
  );
}
