import { useTranslation } from 'next-i18next';
import Image from 'next/image';

import React from 'react';
import carDescription from '../../../public/images/carInsurance/carDescription.png';
import tree from '../../../public/images/carInsurance/tree.png';
export default function Description() {
  const { t } = useTranslation(['carInsurance']);
  const styleH1 = 'text-xl sm:text-2xl font-medium text-raisin-black';
  const styleH2 = 'text-sm sm:text-base font-medium';
  const styleH3 = 'text-sm font-bold text-nickel';
  const styleH4 = 'text-base text-nickel list-outside';
  const styleLi = 'text-base text-nickel list-outside pl-2';
  return (
    <div className="comparison-container flex flex-col gap-8">
      <div className="flex flex-col gap-6">
        <div className="pb-4 pt-16">
          <div className="border-b border-platinum pb-4">
            <p className={styleH1}>{t('whatIsCarInsurance')}</p>
          </div>
        </div>
        <div>
          <p className={styleH4}>{t('carInsuranceDefinition1')}</p>
          <p className={styleH4}>{t('carInsuranceDefinition2')}</p>
          <p className={styleH4}>{t('carInsuranceDefinition3')}</p>
          <p className={styleH4}>{t('carInsuranceDefinition4')}</p>
          <p className={styleH4}>{t('carInsuranceDefinition5')}</p>
          <p className={styleH4}>{t('carInsuranceDefinition6')}</p>
        </div>
        <div className="mx-auto w-full lg:w-2/3">
          <Image
            src={carDescription}
            alt="carDescription"
            width={1000}
            height={1000}
          />
        </div>
      </div>
      <div className="flex flex-col gap-6">
        <div className="border-b border-platinum pb-4">
          <p className={styleH1}>{t('typesOfAutoInsurance')}</p>
        </div>
        <div className="flex flex-col gap-4">
          <div className="flex flex-col gap-2">
            <p className={styleH2}>
              {t('compulsoryInsuranceForCivilLiability')}
            </p>
            <p className={styleH4}>
              {t('compulsoryInsuranceForCivilLiabilityDetail')}
            </p>
          </div>
          <div className="flex flex-col gap-2">
            <p className={styleH3}>{t('theInsured')}</p>
            <p className={styleH4}>{t('theInsuredDetail')}</p>
          </div>
          <div className="flex flex-col gap-2">
            <p className={styleH3}>{t('scopeOfCompensation')}</p>
            <div>
              <p className={styleH4}>{t('scopeOfCompensationTitleDetail1')}</p>
              <p className={styleH4}>{t('scopeOfCompensationTitleDetail2')}</p>
              <ul className="ml-6 list-disc">
                <li className={styleLi}>{t('scopeOfCompensationDetail1')}</li>
                <li className={styleLi}>{t('scopeOfCompensationDetail2')}</li>
                <li className={styleLi}>{t('scopeOfCompensationDetail3')}</li>
                <li className={styleLi}>{t('scopeOfCompensationDetail4')}</li>
                <li className={styleLi}>{t('scopeOfCompensationDetail5')}</li>
                <li className={styleLi}>{t('scopeOfCompensationDetail6')}</li>
                <li className={styleLi}>{t('scopeOfCompensationDetail7')}</li>
              </ul>
            </div>
          </div>
          <div className="flex flex-col gap-2">
            <p className={styleH3}>{t('levelOfLiabilityInsurance')}</p>
            <div>
              <p className={styleH4}>
                {t('levelOfLiabilityInsuranceTitleDetail1')}
              </p>
              <p className={styleH4}>
                {t('levelOfLiabilityInsuranceTitleDetail2')}
              </p>
              <ul className="ml-6 list-disc">
                <li className={styleLi}>
                  {t('levelOfLiabilityInsuranceDetail1')}
                </li>
                <li className={styleLi}>
                  {t('levelOfLiabilityInsuranceDetail2')}
                </li>
              </ul>

              <p className={styleH4}>
                {t('levelOfLiabilityInsuranceTitleDetail3')}
              </p>
              <p className={styleH4}>
                {t('levelOfLiabilityInsuranceTitleDetail4')}
              </p>
            </div>
          </div>
        </div>
        <div className="flex flex-col gap-4">
          <div className="flex flex-col gap-2">
            <p className={styleH2}>{t('materialInsurance')}</p>
            <p className={styleH4}>{t('materialInsuranceDetail')}</p>
          </div>
          <div className="flex flex-col gap-2">
            <p className={styleH3}>{t('theInsured')}</p>
            <p className={styleH4}>{t('theInsuredDetail')}</p>
          </div>
          <div className="flex flex-col gap-2">
            <p className={styleH3}>{t('scopeOfCompensation')}</p>
            <div>
              <p className={styleH4}>{t('scopeOfCompensationTitleDetail3')}</p>
              <ul className="ml-6 list-disc">
                <li className={styleLi}>{t('scopeOfCompensationDetail21')}</li>
                <li className={styleLi}>{t('scopeOfCompensationDetail22')}</li>
                <li className={styleLi}>{t('scopeOfCompensationDetail23')}</li>
                <li className={styleLi}>M{t('scopeOfCompensationDetail24')}</li>
              </ul>
              <p className={styleH4}>{t('scopeOfCompensationTitleDetail4')}</p>
              <ul className="ml-6 list-disc">
                <li className={styleLi}>{t('scopeOfCompensationDetail25')}</li>
                <li className={styleLi}>{t('scopeOfCompensationDetail26')}</li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className="flex flex-col gap-6">
        <div className="border-b border-platinum pb-4">
          <p className={styleH1}>{t('whyBuyAutoInsurance')}</p>
        </div>
        <p className={styleH4}>{t('whyBuyAutoInsuranceDetail')}</p>
        <div className="mx-auto w-full lg:w-2/3">
          <Image src={tree} alt="tree" width={1000} height={1000} />
        </div>
      </div>
      <div className="flex flex-col gap-6">
        <div className="border-b border-platinum pb-4">
          <p className={styleH1}>{t('notCompensated')}</p>
        </div>
        <div>
          <p className={styleH4}>{t('notCompensatedTitleDetail1')}</p>
          <p className={styleH4}>{t('notCompensatedTitleDetail2')}</p>
          <li className={styleLi}>{t('notCompensatedDetail1')}</li>
          <li className={styleLi}>{t('notCompensatedDetail2')}</li>
          <li className={styleLi}>{t('notCompensatedDetail3')}</li>
          <li className={styleLi}>{t('notCompensatedDetail4')}</li>
          <li className={styleLi}>{t('notCompensatedDetail5')}</li>
        </div>
      </div>
    </div>
  );
}
