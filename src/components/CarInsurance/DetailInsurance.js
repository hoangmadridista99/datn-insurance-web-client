import React from 'react';

export default function DetailInsurance({ data }) {
  return (
    <div className='comparison-container'>
      {data.map(items => <div key={items.id}>
        <p className='text-2xl font-medium pb-4'>{items.title}</p>
        {
          items?.details?.map(item => <div key={item.detailId}>
            <p className='text-base text-nickel'>{item.detail}</p>
          </div>)
        }
      </div>)}
    </div>
  );
}
