import Image from 'next/image';
import React from 'react';

export default function AboutInsurance({contentTable, images, smallTitle, coreTitle}) {
  return (
    <div className='comparison-container'>
      <div>
        <h2 className='text-xl text-center text-ultramarine-blue font-bold mb-2 mt-16 sm:mt-22 lg:mt-34'>{smallTitle}</h2>
        <h1 className='text-pixel-32 leading-pixel-48 text-center text-arsenic font-bold'>{coreTitle}</h1>
      </div>
      <div className='xl:flex xl:gap-10'>
        <div className='w-full relative pr-3 xl:w-1/2'>
          <div className='rounded-[14px] mt-10'>
            <Image src={images}
              className='w-full h-auto'
              alt='healthInsurance1'
            />
          </div>
          <div className='flex-1'></div>
        </div>
        <div className='flex flex-col gap-4 mt-8 xl:w-1/2 xl:m-0 xl:gap-6 justify-end'>
          {contentTable.map(content => (<div key={content.id} className='flex gap-4 text-base font-medium pb-4 border-b border-solid border-platinum sm:pb-6'>
            <p className='text-azure text-xl font-bold sm:text-2xl'>{`0${content.id}`}</p>
            <p className='sm:text-xl font-medium'>{content.title}</p>
          </div>))}
        </div>
      </div>
    </div>
  );
}
