import ReactPaginate from 'react-paginate';
import React, { useState, useEffect } from 'react';
import Image from 'next/image';

const Pagination = ({ page, pages, onClick }) => {
  const [pageRange, setPageRange] = useState(5);

  useEffect(() => {
    const handleResize = () => {
      const width = window.innerWidth;

      if (width < 640) {
        setPageRange(2);
      } else {
        setPageRange(5);
      }
    };

    window.addEventListener('resize', handleResize);

    return () => window.removeEventListener('resize', handleResize);
  }, []);

  const ArrowLeft = () => (
    <div className="flex h-8 w-8 items-center justify-center rounded-md">
      <Image
        src="/svg/arrow-left-1.svg"
        width={24}
        height={24}
        alt="Arrow Left"
      />
    </div>
  );

  const ArrowRight = () => (
    <div className="flex h-8 w-8 items-center justify-center rounded-md">
      <Image
        src="/svg/arrow-right.svg"
        width={24}
        height={24}
        alt="Arrow Right"
      />
    </div>
  );

  if (pages <= 1) return null;

  return (
    <ReactPaginate
      breakLabel="..."
      nextLabel={<ArrowRight />}
      pageRangeDisplayed={pageRange}
      pageCount={pages}
      previousLabel={<ArrowLeft />}
      renderOnZeroPageCount={null}
      forcePage={page - 1}
      onPageChange={({ selected }) => onClick(selected)}
      className="flex max-w-full flex-wrap items-center justify-center [&>li]:my-1 md:[&>li]:my-0"
      pageLinkClassName="w-8 h-8 mx-1 text-nickel border border-platinum rounded-md block flex justify-center items-center transition duration-300 hover:bg-ultramarine-blue hover:text-cultured"
      activeLinkClassName="bg-ultramarine-blue text-white"
    />
  );
};

export default Pagination;
