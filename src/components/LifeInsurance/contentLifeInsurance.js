import Image from 'next/image';
import React from 'react';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';
import ButtonPrimary from 'components/common/ButtonPrimary';

function ContentLifeInsurance() {
  const { t } = useTranslation(['home', 'auth']);

  return (
    <div className="relative">
      <div className="hidden lg:absolute lg:inset-0 lg:-z-10 lg:block lg:h-[488px] lg:w-full">
        <Image src="/images/category/i1.png" fill alt="banner" />
      </div>
      <div className="comparison-container pt-4 lg:flex lg:items-start lg:justify-between lg:gap-8 lg:pt-14">
        <div className="mb-10 mt-3 flex flex-col justify-center lg:w-1/2 lg:max-w-[740px]">
          <h3 className="mb-2 text-2xl font-bold leading-8 text-ultramarine-blue">
            So sánh các gói bảo hiểm
          </h3>
          <h2 className="mb-3 text-pixel-40 font-bold leading-pixel-56 text-arsenic">
            {t('auth:insuranceLife')}
          </h2>
          <p className="mb-10 text-base font-normal leading-6 text-nickel">
            Giải pháp tài chính tối ưu giúp bảo vệ gia đình và người thân trước
            những rủi ro trong cuộc sống. Hãy cùng Sosanh24 tìm ra sản phẩm phù
            hợp nhất cho bản thân và gia đình bạn.
          </p>
          <Link href="/filterLifeInsurance">
            <ButtonPrimary
              cases={'medium'}
              title={t('auth:comparisonNow')}
              widthBtn={'w-2/5'}
            />
          </Link>
        </div>
        <div className="mb-10 flex justify-center">
          <div className="relative max-w-[484px]">
            <Image
              src="/images/lifeInsurance/insuranceLife1.png"
              height={484}
              width={484}
              priority="true"
              alt="insuranceLife1"
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default ContentLifeInsurance;
