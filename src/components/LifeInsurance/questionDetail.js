import { IconDropdownQuestion } from 'components/common/icon/Icon';
import React from 'react';

function QuestionDetail({
  question,
  children,
  handleOptionSelected,
  questionsOption,
  idQuestion,
}) {
  return (
    <div
      className={`${
        questionsOption === idQuestion ? 'border-azure' : 'border-platinum'
      } flex flex-col rounded-lg border p-6`}
      onClick={() => handleOptionSelected(question.id)}
    >
      <div className="mb-3 flex cursor-pointer items-start justify-between gap-3">
        <p
          className={`${
            questionsOption === idQuestion && 'font-medium'
          } text-base text-raisin-black`}
        >
          {question?.title}
        </p>
        <button type="button">
          <div
            className={`duration-200 ${
              questionsOption === idQuestion && '-rotate-180'
            } w-5`}
          >
            <IconDropdownQuestion />
          </div>
        </button>
      </div>
      <div className="flex w-full flex-col items-start text-sm font-normal text-nickel">
        {question?.description && (
          <p className="mb-3 w-full">{question?.description}</p>
        )}
        {questionsOption === idQuestion && (
          <>
            <p className="mb-3 w-full">{question?.question}</p>
            <ul className="list-disc pl-4">
              {question?.details?.map((detail) => (
                <li key={detail?.detailId}>{detail?.detail}</li>
              ))}
            </ul>
            {children}
          </>
        )}
      </div>
    </div>
  );
}

export default QuestionDetail;
