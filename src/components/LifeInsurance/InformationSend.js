import ButtonPrimary from 'components/common/ButtonPrimary';
import Image from 'next/image';
import React from 'react';

function InformationSend() {
  return (
    <div className=" relative bg-gradient-to-r from-egyptian-blue to-ultramarine-blue">
      <div className="absolute top-0 left-0">
        <Image
          src="/images/lifeInsurance/Polygon1.png"
          width={150}
          height={150}
          alt="polygon1"
        />
      </div>
      <div className="absolute top-0 left-0 z-10">
        <Image
          src="/images/lifeInsurance/Polygon2.png"
          width={230}
          height={100}
          alt="polygon2"
        />
      </div>
      <div className="absolute bottom-0 left-[682px]">
        <Image
          src="/images/lifeInsurance/Polygon3.png"
          width={150}
          height={150}
          alt="polygon3"
        />
      </div>
      <div className="absolute right-0 bottom-16">
        <Image
          src="/images/lifeInsurance/Polygon4.png"
          width={160}
          height={160}
          alt="polygon4"
        />
      </div>
      <div className="absolute right-0 top-2">
        <Image
          src="/images/lifeInsurance/Polygon5.png"
          width={40}
          height={40}
          alt="polygon5"
        />
      </div>
      <div className="comparison-container flex flex-col gap-5 py-10 lg:flex-row lg:gap-32">
        <div className="flex items-center lg:w-1/2">
          <div>
            <p className="text-pixel-40 font-bold leading-pixel-56 text-white">
              Liên hệ để được tư vấn
            </p>
            <p className="mt-4 text-base font-normal leading-6 text-cultured">
              Liên hệ ngay với Sosanh24 để được giải đáp mọi thắc mắc và tìm
              hiểu rõ về thông tin các dịch vụ.
            </p>
          </div>
        </div>
        <div className="z-20 rounded-xl bg-white p-8 lg:w-1/2">
          <input
            type="text"
            placeholder="Họ và tên của bạn"
            className="mb-5 w-full rounded-md border border-spanish-gray p-3 outline-none"
          />
          <input
            type="text"
            placeholder="Địa chỉ email của bạn"
            className="mb-5 w-full rounded-md border border-spanish-gray p-3 outline-none"
          />
          <textarea
            rows={4}
            type="text"
            placeholder="Lời nhắn"
            className="mb-6 w-full rounded-md border border-spanish-gray p-3 outline-none"
          />
          <div className="flex justify-center">
            <ButtonPrimary cases={'medium'} title={'Gửi ngay'} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default InformationSend;
