import Image from 'next/image';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { listKeyInformation } from 'src/utils/constants';

function KeyInformation() {
  const { t } = useTranslation(['lifeInsurance']);
  return (
    <div className="comparison-container pt-30 pb-8 sm:pb-10 lg:pb-16">
      <div>
        <h2 className="mb-2 text-center text-2xl font-bold text-ultramarine-blue">
          {t('shareKnowledge')}
        </h2>
        <h1 className="text-center text-pixel-40 font-bold leading-pixel-56 text-arsenic">
          {t('knowWellAboutLifeInsurance')}
        </h1>
      </div>

      <div className="flex flex-col items-start gap-10 lg:flex-row lg:items-end">
        <Image
          src="/images/lifeInsurance/insuranceLife2.png"
          width={592}
          height={376}
          priority="true"
          alt="insuranceLife2"
        />
        <div>
          {listKeyInformation(t).map((list) => (
            <div
              key={list.id}
              className="mt-6 flex items-center gap-4 border-b pb-6"
            >
              <p className="text-2xl font-bold text-azure">{list.id}</p>
              <p className="text-xl font-medium text-raisin-black">
                {list.label}
              </p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}

export default KeyInformation;
