import React, { useState } from 'react';
import QuestionDetail from './questionDetail';

export default function Question({
  questions,
  smallTitle = 'answers',
  coreTitle = 'frequentlyAskedQuestions',
  currentTabQuestion = 1,
  partSecondQuestions,
  titleTabQuestion,
  handleSwitchingTabQuestion,
}) {
  const [questionsOption, setQuestionOptions] = useState(null);
  const handleOptionSelected = (option) => {
    if (questionsOption === option) {
      setQuestionOptions(null);
    } else {
      setQuestionOptions(option);
    }
  };
  return (
    <div className="mb-14 mt-10 sm:mb-22 lg:mb-28">
      <div className="comparison-container flex flex-col items-center justify-center">
        <h1 className="mb-2 flex justify-center text-2xl font-bold text-ultramarine-blue">
          {smallTitle}
        </h1>
        <h2 className="mb-8 text-pixel-32 font-bold leading-pixel-48 text-arsenic lg:text-pixel-40 lg:leading-pixel-56">
          {coreTitle}
        </h2>
        {currentTabQuestion && (
          <div className="mb-8 flex gap-3 text-sm text-nickel sm:gap-4 sm:text-base">
            {titleTabQuestion?.map((item) => (
              <button
                key={item?.id}
                className={`${
                  currentTabQuestion === item?.id && 'font-bold'
                } p-2`}
                onClick={() => handleSwitchingTabQuestion(item?.id)}
              >
                {item?.title}
              </button>
            ))}
          </div>
        )}
        <div className="w-full space-y-4">
          {(currentTabQuestion === 1 ? questions : partSecondQuestions)?.map(
            (question) => (
              <div key={question?.id}>
                <QuestionDetail
                  question={question}
                  handleOptionSelected={handleOptionSelected}
                  questionsOption={questionsOption}
                  idQuestion={question?.id}
                >
                  {question?.detail}
                  {question?.answerList && (
                    <ul>
                      {question?.answerList?.map((item) => (
                        <li key={item?.id}>{item?.detailQuestion}</li>
                      ))}
                    </ul>
                  )}
                </QuestionDetail>
              </div>
            )
          )}
        </div>
      </div>
    </div>
  );
}
