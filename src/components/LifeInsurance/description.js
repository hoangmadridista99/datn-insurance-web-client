import Image from 'next/image';
import React from 'react';
import { useTranslation } from 'next-i18next';
const listAnswer = (t) => [
  {
    id: 1,
    detail: t('shouldBuyInsuranceAnswer1'),
  },
  {
    id: 2,
    detail: t('shouldBuyInsuranceAnswer2'),
  },
  {
    id: 3,
    detail: t('shouldBuyInsuranceAnswer3'),
  },
  {
    id: 4,
    detail: t('shouldBuyInsuranceAnswer4'),
  },
];

const stylesH1 = 'mb-6 border-b pb-4 text-2xl font-medium text-raisin-black';
const stylesP = 'mb-6 text-base font-normal text-nickel';

function Description() {
  const { t } = useTranslation(['lifeInsurance']);

  return (
    <div className="comparison-container pb-10">
      <h1 className={stylesH1}>{t('WhatIsLifeInsurance')}</h1>
      <p className={stylesP}>{t('insuranceDefinition1')}</p>
      <div className="mb-6 flex justify-center">
        <Image
          src="/images/lifeInsurance/insuranceLife3.png"
          alt="insuranceLife3"
          width={808}
          height={504}
          className="rounded-2xl border"
        />
      </div>
      <p className={stylesP}>{t('insuranceDefinition2')}</p>
      <p className={stylesP}>{t('insuranceDefinition3')}</p>
      <h1 className={stylesH1}>{t('shouldBuyInsurance')}</h1>
      <ul className="mb-6 ml-2 list-disc pl-4">
        {listAnswer(t).map((list) => (
          <li key={list.id} className="text-base font-normal text-nickel">
            {list.detail}
          </li>
        ))}
      </ul>
      <p className={stylesP}>{t('shouldBuyInsuranceAnswerConclusion')}</p>
      <h1 className={stylesH1}>{t('buyLifeInsurance')}</h1>
      <p className={stylesP}>{t('buyLifeInsuranceAnswer')}</p>
      <h1 className={stylesH1}>{t('howMuchLifeInsuranceIsSafe')}</h1>
      <p className={stylesP}>{t('howMuchLifeInsuranceIsSafeAnswer')}</p>
      <div className="mb-6 flex justify-center">
        <Image
          width={808}
          height={504}
          className="rounded-2xl border"
          src="/images/lifeInsurance/insuranceLife4.png"
          alt="insuranceLife4"
        />
      </div>
      <h1 className={stylesH1}>{t('yearsOfPremiumPayment')}</h1>
      <p className={stylesP}>{t('yearsOfPremiumPaymentAnswer')}</p>
    </div>
  );
}

export default Description;
