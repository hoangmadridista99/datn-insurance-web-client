import { useRouter } from 'next/router';
import numeral from 'numeral';
import React, { useContext, useEffect } from 'react';
import { toast } from 'react-toastify';
import { DataContext } from 'src/store/GlobalState';
import { IconCloseComparison } from './common/icon/Icon';
import ButtonPrimary from './common/ButtonPrimary';
import ButtonBgWhite from './common/ButtonBgWhite';
import { useTranslation } from 'next-i18next';

const ComparisonInsurance = () => {
  const { t } = useTranslation(['common']);
  const { dispatch, state } = useContext(DataContext);
  const { comparison } = state;
  const { updateDataInsurance } = state;
  const dataInsuranceCheck = updateDataInsurance?.updateDataInsurance;

  const router = useRouter();

  useEffect(() => {
    dataInsuranceCheck;
  }, [state]);

  useEffect(() => {
    if (router.pathname === '/listHealthInsurance' && dataInsuranceCheck) {
      dispatch({
        type: 'COMPARISON',
        comparison: { comparison: true },
        updateDataInsurance: { updateDataInsurance: dataInsuranceCheck },
      });
      return;
    }

    if (router.pathname === '/comparisonDetail') {
      dispatch({
        type: 'COMPARISON',
        comparison: { comparison: false },
        updateDataInsurance: { updateDataInsurance: dataInsuranceCheck },
      });
      return;
    }

    if (router.pathname === '/listLifeInsurance' && dataInsuranceCheck) {
      dispatch({
        type: 'COMPARISON',
        comparison: { comparison: true },
        updateDataInsurance: { updateDataInsurance: dataInsuranceCheck },
      });
      return;
    }

    if (router.pathname === '/insuranceConcern' && dataInsuranceCheck) {
      dispatch({
        type: 'COMPARISON',
        comparison: { comparison: true },
        updateDataInsurance: { updateDataInsurance: dataInsuranceCheck },
      });
      return;
    }

    dispatch({
      type: 'COMPARISON',
      comparison: { comparison: false },
    });
  }, [router.pathname]);

  const handleHiddenComparison = () => {
    return dispatch({
      type: 'COMPARISON',
      comparison: { comparison: false },
      updateDataInsurance: { updateDataInsurance: [] },
    });
  };

  const comparisonDetail = (dataInsuranceCheck) => {
    if (dataInsuranceCheck?.length === 1) {
      return toast.error(t('toast_error_comparison'));
    }
    dispatch({
      type: 'COMPARISON',
      comparison: { comparison: false },
      updateDataInsurance: { updateDataInsurance: dataInsuranceCheck },
    });
    router.push({
      pathname: '/comparisonDetail',
      query: {
        testData: dataInsuranceCheck?.map((insurance) => insurance?.id),
      },
    });
  };

  const handleDeleteInsurance = (data) => {
    let array = dataInsuranceCheck;
    const removeById = (array, id) => {
      return array.filter((obj) => obj.id !== id);
    };
    array = removeById(array, data.id);
    dispatch({
      type: 'COMPARISON',
      comparison: { comparison: true },
      updateDataInsurance: { updateDataInsurance: array },
    });
    if (dataInsuranceCheck.length === 1)
      return dispatch({
        type: 'COMPARISON',
        comparison: { comparison: false },
      });
  };

  return (
    <>
      {comparison?.comparison && (
        <div className="fixed left-0 bottom-0 z-50 flex w-full justify-center bg-black-bg">
          <div className="comparison-container flex flex-col justify-between p-4 sm:px-10 lg:flex-row lg:justify-between xl:flex-row xl:py-6 xl:px-27">
            <div className="flex justify-center gap-2 sm:gap-6 lg:w-percent-70 lg:justify-between">
              {dataInsuranceCheck?.map((data) => (
                <div
                  key={data?.id}
                  className="w-1/3 rounded bg-white p-2 sm:rounded-2xl sm:p-4 lg:w-percent-30"
                >
                  <div className="flex h-full flex-col justify-between">
                    <div className="flex h-6 w-full justify-between xl:mb-2">
                      <div className="flex h-6 items-center">
                        <img
                          className="h-full"
                          src={data?.company?.logo}
                          alt="logoCompany"
                        />
                      </div>
                      <div>
                        <button
                          type="button"
                          onClick={() => handleDeleteInsurance(data)}
                        >
                          <IconCloseComparison />
                        </button>
                      </div>
                    </div>
                    <p className="text-base font-bold text-arsenic xl:mb-3">
                      {data?.name}
                    </p>
                    <div className="hidden items-center gap-1 sm:flex">
                      <div className="hidden items-center gap-1 xl:flex">
                        <p className="text-sm font-bold text-ultramarine-blue">
                          {numeral(data?.terms?.monthly_fee?.from).format(
                            '(0,0)'
                          )}
                        </p>
                        <p className="text-sm font-normal text-silver">
                          /{t('month')}
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              ))}

              {dataInsuranceCheck?.length < 3 && (
                <>
                  {dataInsuranceCheck?.length === 1 && (
                    <div className="flex w-1/3 flex-col items-center justify-center rounded-2xl border-2 border-dashed border-philippine-gray lg:w-percent-30">
                      <p className="text-2xl font-bold text-cultured sm:text-pixel-32 sm:leading-pixel-48">
                        2
                      </p>
                      <p className="px-2 text-xs font-normal text-platinum sm:text-sm">
                        {t('choice_comparison')}
                      </p>
                    </div>
                  )}
                  <div className="flex w-1/3 flex-col items-center justify-center rounded-2xl border-2 border-dashed border-philippine-gray lg:w-percent-30">
                    <p className="text-2xl font-bold text-cultured sm:text-pixel-32 sm:leading-pixel-48">
                      3
                    </p>
                    <p className="px-2 text-xs font-normal text-platinum sm:text-sm">
                      {t('choice_comparison')}
                    </p>
                  </div>
                </>
              )}
            </div>
            <div className="mt-4 flex items-center justify-end gap-3 lg:mt-0 lg:w-percent-23 lg:justify-between xl:gap-6">
              <div className="w-2/5 sm:w-1/3 lg:w-percent-45">
                <ButtonPrimary
                  cases={'medium'}
                  title={t('comparison')}
                  onClick={() => comparisonDetail(dataInsuranceCheck)}
                  widthBtn={'w-full'}
                />
              </div>
              <div className="w-2/5 sm:w-1/3 lg:w-percent-45">
                <ButtonBgWhite
                  cases={'medium'}
                  title={t('close')}
                  widthBtn={'bg-lavender text-ultramarine-blue w-full'}
                  onClick={handleHiddenComparison}
                />
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ComparisonInsurance;
