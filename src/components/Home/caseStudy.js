import Image from 'next/image';
import React from 'react';
import ListCaseStudy from './listCaseStudy';
import {
  listCaseStudy1,
  listCaseStudy2,
  listCaseStudy3,
} from 'components/Home/dummy';
import { useTranslation } from 'next-i18next';

function CaseStudy() {
  const { t } = useTranslation(['home']);

  return (
    <div className="relative -z-10 bg-cultured py-20">
      <div className="absolute inset-0 -z-10 sm:hidden">
        <Image
          src="/images/home/bg-caseStudy-mobile.png"
          fill
          alt="bgCaseStudy"
        />
      </div>
      <div className="absolute inset-0 -z-10 hidden sm:block">
        <Image src="/images/home/bg-caseStudy.png" fill alt="bgCaseStudy" />
      </div>
      <div className="comparison-container flex flex-col justify-center lg:py-10">
        <div className="flex flex-col">
          <h2 className="mb-2 flex justify-center font-bold text-maize lg:text-2xl">
            {t('caseStudyH2')}
          </h2>
          <h1 className="mb-4 flex justify-center text-xl font-bold text-white lg:text-pixel-40 lg:leading-pixel-56">
            {t('caseStudyH1')}
          </h1>
          <p className="mb-16 flex items-center text-center text-[#BAC9FF]">
            {t('caseStudyP')}
          </p>
        </div>
        <div className="gap-6 md:flex">
          <div>
            <ListCaseStudy listCaseStudy={listCaseStudy1} />
          </div>
          <div>
            <ListCaseStudy listCaseStudy={listCaseStudy2} />
          </div>
          <div>
            <ListCaseStudy listCaseStudy={listCaseStudy3} />
          </div>
        </div>
      </div>
    </div>
  );
}

export default CaseStudy;
