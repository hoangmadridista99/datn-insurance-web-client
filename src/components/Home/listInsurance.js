import ButtonBgWhite from 'components/common/ButtonBgWhite';
import React from 'react';
import { listInsurance } from './dummy';

function ListInsurance() {
  return (
    <div className="mb-10 flex w-full flex-col justify-center gap-8 sm:mb-12 sm:flex-row sm:flex-wrap sm:gap-6 lg:mb-16 lg:gap-8">
      {listInsurance.map((list) => {
        return (
          <div
            key={list?.id}
            className="flex w-full flex-col items-start justify-between rounded-lg p-6 shadow-other duration-700 hover:scale-105 sm:w-percent-46 lg:w-percent-30"
          >
            <div>
              <div className="mb-4">{list.logo}</div>
              <p className="mb-1 text-base font-bold text-arsenic">
                {list?.name}
              </p>
              <div className="mb-4 flex items-center">
                <p className="text-pixel-32 font-bold leading-pixel-48 text-ultramarine-blue">
                  {list?.price}
                </p>
                <p className="text-base font-normal text-philippine-gray">
                  /tháng
                </p>
              </div>
              <p className="mb-4 text-base font-normal text-philippine-gray">
                {list.description}
              </p>
            </div>
            <ButtonBgWhite
              cases={'medium'}
              title={'So sánh bảo hiểm'}
              widthBtn={'w-full'}
            />
          </div>
        );
      })}
    </div>
  );
}

export default ListInsurance;
