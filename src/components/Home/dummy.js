import Image from 'next/image';
import logoDaiIchi from '/public/images/home/logoDaiIchi.png';
import logoAIA from '/public/images/home/logoAIA.png';
import logoManulife from '/public/images/home/logoManulife.png';
import avatar1 from '/public/images/home/avatar1.png';
import avatar2 from '/public/images/home/avatar2.png';
import avatar3 from '/public/images/home/avatar3.png';
import avatar4 from '/public/images/home/avatar4.png';
import avatar5 from '/public/images/home/avatar5.png';
import avatar6 from '/public/images/home/avatar6.png';
import React from 'react';

export const listInsurance = [
  {
    id: 1,
    logo: <Image src={logoDaiIchi} alt="logoDaiIchi" />,
    name: 'An Thịnh Đầu Tư',
    price: '220,000 VNĐ',
    description:
      'Dai-ichi Life Việt Nam mang đến cho bạn giải pháp tài chính kết hợp giữa đầu tư và bảo hiểm. Bạn toàn quyền quyết định đầu tư trong hôm nay và linh hoạt thay đổi vào ngày sau.',
  },
  {
    id: 2,
    logo: <Image src={logoAIA} alt="logoAIA" />,
    name: 'Bảo Hiểm An Bình Ưu Việt',
    price: '220,000 VNĐ',
    description:
      'Giải pháp tài chính mang lại sự bảo vệ cao giúp khách hàng hoàn toàn an tâm để trải nghiệm cuộc sống phong phú này một cách trọn vẹn.',
  },
  {
    id: 3,
    logo: <Image src={logoManulife} alt="logoManulife" />,
    name: 'Bảo hiểm Nhân thọ',
    price: '220,000 VNĐ',
    description:
      'Manulife mong muốn mang đến cho bạn và những người thân yêu. Bảo hiểm nhân thọ là giải pháp toàn diện giúp bạn an tâm tận hưởng trọn vẹn cuộc sống',
  },
  {
    id: 4,
    logo: <Image src={logoDaiIchi} alt="logoDaiIchi" />,
    name: 'An Thịnh Đầu Tư',
    price: '220,000 VNĐ',
    description:
      'Bảo hiểm Chăm sóc sức khỏe - sản phẩm bảo hiểm y tế toàn diện đầu tiên trên thị trường bảo hiểm nhân thọ, chính là giải pháp ưu việt giúp bạn bảo vệ mọi thành viên trong gia đình',
  },
  {
    id: 5,
    logo: <Image src={logoAIA} alt="logoAIA" />,
    name: 'Bảo Hiểm An Bình Ưu Việt',
    price: '220,000 VNĐ',
    description:
      'Sản phẩm Bước Đến Tương Lai từ AIA sẽ giúp bạn gia tăng tài sản từ kết quả đầu tư dài hạn, quản lý rủi ro trong cả đầu tư cũng như trong cuộc sống',
  },
  {
    id: 6,
    logo: <Image src={logoManulife} alt="logoManulife" />,
    name: 'Bảo hiểm Nhân thọ',
    price: '220,000 VNĐ',
    description:
      'Đừng để những chi phí khám chữa bệnh trở thành gánh nặng của cả gia đình. Hãy để Manulife giúp bạn tạo dựng quỹ sức khỏe đáp ứng mọi nhu cầu về dịch vụ y tế.',
  },
];

export const listCaseStudy1 = [
  {
    id: 1,
    avatar: <Image src={avatar1} alt="avatar1" />,
    name: 'Esther Howard',
    label: 'Lorem ipsum dolor ',
    description:
      'Lorem ipsum dolor sit amet consectetur. Quam non nunc dui quis massa. Porta vitae interdum cras diam tortor nunc dis arcu ultricies. Vel ac et duis diam laoreet felis nec. Purus nunc nisl diam tortor aliquet nisl amet purus nunc.',
  },
  {
    id: 2,
    avatar: <Image src={avatar2} alt="avatar2" />,
    name: 'Guy Hawkins',
    label: 'Lorem ipsum dolor ',
    description:
      'Lorem ipsum dolor sit amet consectetur. Quam non nunc dui quis massa. Porta vitae interdum cras diam tortor nunc dis arcu ultricies. Vel ac et duis diam laoreet felis nec.',
  },
];

export const listCaseStudy2 = [
  {
    id: 1,
    avatar: <Image src={avatar3} alt="avatar3" />,
    name: 'Albert Flores',
    label: 'Lorem ipsum dolor ',
    description:
      'Lorem ipsum dolor sit amet consectetur. Elit vel vestibulum non tristique sed aliquet. In vulputate nisi rhoncus faucibus risus sit vestibulu sed mi. Viverra vitae habitasse orci ',
  },
  {
    id: 2,
    avatar: <Image src={avatar4} alt="avatar4" />,
    name: 'Cody Fisher',
    label: 'Lorem ipsum dolor ',
    description:
      'Lorem ipsum dolor sit amet consectetur. Quam non nunc dui quis massa. Porta vitae interdum cras diam tortor nunc dis arcu ultricies. Vel ac et duis diam laoreet felis nec. Purus nunc nisl diam tortor aliquet nisl amet purus nunc. Congue ante commodo dignissim nulla eget enim elementum accumsan. Pellentesque iaculis lobortis nunc proin felis auctor consequat. ',
  },
];

export const listCaseStudy3 = [
  {
    id: 1,
    avatar: <Image src={avatar5} alt="avatar5" />,
    name: 'Cameron Williamson',
    label: 'Lorem ipsum dolor ',
    description:
      'Lorem ipsum dolor sit amet consectetur. Quam non nunc dui quis massa. Porta vitae interdum cras diam tortor nunc dis arcu ultricies. Vel ac et duis diam laoreet felis nec. Purus nunc nisl diam tortor aliquet nisl amet purus nunc. Congue ante commodo dignissim nulla eget enim elementum accumsan. Pellentesque iaculis lobortis nunc proin felis auctor consequat. ',
  },
  {
    id: 2,
    avatar: <Image src={avatar6} alt="avatar6" />,
    name: 'Ralph Edwards',
    label: 'Lorem ipsum dolor ',
    description:
      'Lorem ipsum dolor sit amet consectetur. Quam non nunc dui quis massa. Porta vitae interdum cras diam tortor nunc dis arcu ultricies. ',
  },
];
