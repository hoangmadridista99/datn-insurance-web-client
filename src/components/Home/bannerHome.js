import Image from 'next/image';
import React from 'react';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';
import ButtonPrimary from 'components/common/ButtonPrimary';

function BannerHome() {
  const { t } = useTranslation('home');

  return (
    <div className="relative">
      <div className="absolute inset-0 -z-10">
        <Image
          src="/images/home/bg-content1.png"
          fill
          priority="true"
          alt="bg-content1"
        />
      </div>

      <div className="comparison-container justify-between py-10 sm:py-16 lg:flex lg:gap-8 lg:py-20 lg:pr-5">
        <div className="mb-10 flex flex-col items-start lg:w-2/3 xl:w-3/5">
          <p className="mb-2 text-xl font-bold text-arsenic lg:text-2xl">
            {t('contentH2')}
          </p>
          <p className="mb-4 text-4xl font-bold text-arsenic lg:text-pixel-56 lg:leading-pixel-76">
            {t('contentH11')}
            <span className="text-ultramarine-blue">{t('contentH12')}</span>
            {t('contentH13')}
          </p>
          <p className="mb-4 text-sm text-philippine-gray lg:mb-10 lg:text-base lg:font-normal">
            {t('contentP')}
          </p>
          <Link href="/category" className="w-2/3 sm:w-1/3">
            <ButtonPrimary cases={'large'} title={t('contentBnt')} />
          </Link>
        </div>

        <div className="mx-auto max-w-152.5 lg:flex lg:w-percent-46 lg:items-center">
          <Image
            src="/images/home/familyBanner.png"
            width={800}
            height={880}
            priority="true"
            alt="ellipse"
          />
        </div>
      </div>
    </div>
  );
}

export default BannerHome;
