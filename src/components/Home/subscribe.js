import Image from 'next/image';
import React from 'react';
import { useTranslation } from 'next-i18next';

function Subscribe() {
  const { t } = useTranslation(['home']);

  return (
    <div className="relative py-20 xl:py-30">
      <div className="absolute inset-0 -z-10 sm:hidden">
        <Image
          src="/images/home/bg-subscribe-mobile.png"
          fill
          alt="bgSubscribeMb"
        />
      </div>
      <div className="absolute inset-0 -z-10 hidden sm:block">
        <Image src="/images/home/bg-subscribe.png" fill alt="bgSubscribe" />
      </div>

      <div className="comparison-container flex flex-col">
        <p className="mb-4 flex justify-center text-center text-xl font-bold text-chinese-black lg:text-pixel-40 lg:leading-pixel-56 ">
          {t('subscribeP1')}
        </p>
        <p className="mb-10 flex justify-center text-center font-normal text-chinese-black lg:text-base">
          {t('subscribeP2')}
        </p>
        <div className="flex justify-center">
          <div className="flex items-center justify-between rounded-xl border border-crayola bg-white p-1 lg:w-97">
            <input
              type="text"
              placeholder="Enter email address"
              className="w-full pl-2 outline-none"
            />
            <button
              type="button"
              className="rounded-lg bg-chinese-black px-6 py-2 text-base font-medium text-white"
            >
              {t('subscribeBtn')}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Subscribe;
