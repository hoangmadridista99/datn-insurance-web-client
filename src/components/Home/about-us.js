import FilterContent from 'components/common/FilterContent';
import Image from 'next/image';
import React from 'react';
// import { useTranslation } from 'next-i18next';

function AboutUs() {
  // const { t } = useTranslation(['home']);
  const filter = 'Về chúng tôi';

  return (
    <div className="comparison-container my-10 flex flex-col-reverse lg:my-40 lg:flex-row lg:items-center lg:justify-between">
      <div className="mb-10 w-full lg:mb-0 lg:w-1/2 lg:pr-10">
        <Image
          src="/images/home/aboutUs.png"
          width={600}
          height={600}
          priority
          alt="aboutUs"
          className="mx-auto"
        />
      </div>

      <div className="w-full lg:w-1/2 lg:pl-10">
        <FilterContent
          filter={filter}
          title={'Lorem ipsum dolor sit amet consectetur.'}
          content={
            'Lorem ipsum dolor sit amet consectetur. Elit vel vestibulum non tristique sed aliquet. In vulputate nisi rhoncus faucibus risus sit vestibulum sed mi. Viverra vitae habitasse orci et at. Consectetur amet sit viverra mauris massa arcu ut netus risus. Risus maecenas consequat scelerisque tristique leo quam. Enim pellentesque scelerisque at proin euismod magna. Arcu elementum ut scelerisque tortor augue egestas sit tempor lacus.'
          }
        />
      </div>
    </div>
  );
}

export default AboutUs;
