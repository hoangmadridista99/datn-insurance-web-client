import FilterContent from 'components/common/FilterContent';
import Image from 'next/image';
import React from 'react';
import { useTranslation } from 'next-i18next';

function SelectNeed() {
  const { t } = useTranslation(['home']);

  const filter = t('selectNeedFilter');

  const title = t('selectNeedTitle');

  const content = t('selectNeedContent');
  return (
    <div className="comparison-container lg:my-40 lg:flex lg:items-center lg:justify-between">
      <div className="my-10 pr-0 lg:w-1/2 lg:pr-20">
        <FilterContent filter={filter} content={content} title={title} />
      </div>

      <div className="mb-10 lg:mb-0 lg:w-1/2">
        <Image
          src="/images/home/selectNeed.png"
          width={653}
          height={653}
          alt="selectNeed"
          className="mx-auto w-2/3 lg:w-full"
        />
      </div>
    </div>
  );
}

export default SelectNeed;
