import React from 'react';
import ListInsurance from './listInsurance';
import { useTranslation } from 'next-i18next';
import ButtonPrimary from 'components/common/ButtonPrimary';

function OtherInsurance() {
  const { t } = useTranslation(['home']);

  return (
    <>
      <div className="comparison-container mt-20 mb-10 lg:mt-44 lg:mb-22">
        <div className="flex flex-col">
          <div className="flex flex-col justify-center">
            <h2 className="mb-4 flex justify-center text-base font-bold text-ultramarine-blue lg:text-2xl">
              {t('otherInsurance')}
            </h2>
            <h1 className="mb-12 flex justify-center text-xl font-bold text-arsenic lg:mb-16 lg:text-pixel-40 lg:leading-pixel-56">
              {t('otherInsuranceH1')}
            </h1>
          </div>
          <ListInsurance />
          <div className="flex justify-center px-10">
            <ButtonPrimary
              cases={'large'}
              title={t('otherInsuranceBtn')}
              widthBtn={'w-full sm:w-1/2 lg:w-1/4 lg:px-3'}
            />
          </div>
        </div>
      </div>
    </>
  );
}

export default OtherInsurance;
