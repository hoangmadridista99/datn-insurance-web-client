import Image from 'next/image';
import React from 'react';
import FilterContent from 'components/common/FilterContent';
import { useTranslation } from 'next-i18next';

function SelectRate() {
  const { t } = useTranslation(['home']);

  const filter = t('selectRateFilter');

  const title = t('selectRateTitle');

  const content = t('selectRateContent');

  return (
    <div className="relative py-20 lg:py-0">
      <div className="absolute inset-0 -z-10">
        <Image src="/images/home/bg-selectRate.png" fill alt="bgSelectRate" />
      </div>

      <div className="comparison-container flex flex-col-reverse lg:flex-row lg:items-center lg:pt-32 lg:pb-48">
        <div className="w-full lg:w-1/2">
          <Image
            src="/images/home/selectRate.png"
            width={542}
            height={542}
            alt="selectRate"
            className="mx-auto w-2/3 lg:w-full"
          />
        </div>

        <div className="my-10 w-full flex-1 lg:w-1/2 lg:pl-20">
          <FilterContent filter={filter} content={content} title={title} />
        </div>
      </div>
    </div>
  );
}

export default SelectRate;
