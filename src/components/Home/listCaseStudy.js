import React from 'react';

function ListCaseStudy({ listCaseStudy }) {
  return (
    <div>
      {listCaseStudy.map((list) => {
        return (
          <div
            key={list.id}
            className="mb-6 rounded-lg border bg-white p-6 duration-700 hover:scale-105 lg:max-w-98"
          >
            <div className="mb-6 flex items-center gap-4">
              <div>{list?.avatar}</div>
              <div>
                <p className="text-base font-bold text-arsenic">{list?.name}</p>
                <p className="text-base font-normal text-philippine-gray">
                  {list?.label}
                </p>
              </div>
            </div>
            <p className="text-base font-normal text-philippine-gray">
              {list?.description}
            </p>
          </div>
        );
      })}
    </div>
  );
}

export default ListCaseStudy;
