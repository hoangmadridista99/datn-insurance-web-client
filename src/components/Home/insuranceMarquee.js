import React from 'react';
import Marquee from 'react-fast-marquee';
import { useIntersection } from 'src/utils/customHooks';
import Image from 'next/image';

function InsuranceMarquee() {
  const intersectionRef = React.useRef(null);
  const intersection = useIntersection(intersectionRef, {
    root: null,
    rootMargin: '120px',
    threshold: 1,
  });

  return (
    <div className="mb-16 md:mb-20" ref={intersectionRef}>
      <Marquee
        gradient={false}
        speed={50}
        play={intersection}
        className="flex cursor-default items-center justify-between py-6 lg:py-16"
      >
        <div className="mr-20 transition duration-300">
          <div className="w-[135px] lg:w-[192px]">
            <Image
              src="/images/home/insurance1.png"
              width={192}
              height={192}
              alt="insurance1"
            />
          </div>
        </div>
        <div className="mr-20 transition duration-300">
          <div className="w-[135px] lg:w-[192px]">
            <Image
              src="/images/home/insurance2.png"
              width={192}
              height={192}
              alt="insurance2"
            />
          </div>
        </div>
        <div className="mr-20 transition duration-300">
          <div className="w-[135px] lg:w-[192px]">
            <Image
              src="/images/home/insurance3.png"
              width={192}
              height={192}
              alt="insurance3"
            />
          </div>
        </div>
        <div className="mr-20 transition duration-300">
          <div className="w-[135px] lg:w-[192px]">
            <Image
              src="/images/home/insurance4.png"
              width={192}
              height={192}
              alt="insurance4"
            />
          </div>
        </div>
        <div className="mr-20 transition duration-300">
          <div className="w-[135px] lg:w-[192px]">
            <Image
              src="/images/home/insurance1.png"
              width={192}
              height={192}
              alt="insurance1"
            />
          </div>
        </div>
        <div className="mr-20 transition duration-300">
          <div className="w-[135px] lg:w-[192px]">
            <Image
              src="/images/home/insurance2.png"
              width={192}
              height={192}
              alt="insurance2"
            />
          </div>
        </div>
        <div className="mr-20 transition duration-300">
          <div className="w-[135px] lg:w-[192px]">
            <Image
              src="/images/home/insurance3.png"
              width={192}
              height={192}
              alt="insurance3"
            />
          </div>
        </div>
      </Marquee>
    </div>
  );
}

export default InsuranceMarquee;
