import Image from 'next/image';
import React from 'react';
import FilterContent from 'components/common/FilterContent';
import { useTranslation } from 'next-i18next';

function SelectInsurance() {
  const { t } = useTranslation(['home']);

  const filter = t('selectInsuranceFilter');

  const title = t('selectInsuranceTitle');

  const content = t('selectInsuranceContent');

  return (
    <div className="relative py-16">
      <div className="absolute inset-0 -z-10">
        <Image src="/images/home/bg-select.png" fill alt="bgSelect" />
      </div>

      <div className="comparison-container flex flex-col-reverse lg:flex-row lg:items-center lg:justify-between lg:pt-20 lg:pb-16 ">
        <div className="lg:w-1/2">
          <Image
            src="/images/home/select.png"
            width={542}
            height={542}
            alt="ellipse"
            className="mx-auto w-2/3 lg:w-full"
          />
        </div>

        <div className="my-10 w-full flex-1 pl-0 lg:w-1/2 lg:pl-20">
          <FilterContent filter={filter} title={title} content={content} />
        </div>
      </div>
    </div>
  );
}

export default SelectInsurance;
