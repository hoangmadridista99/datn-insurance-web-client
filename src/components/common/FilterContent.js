import React from 'react';

function FilterContent({ filter, title, content }) {
  return (
    <div>
      <p className="mb-2 font-bold text-ultramarine-blue lg:text-2xl">
        {filter}
      </p>

      <h1 className="mb-8 text-2xl font-bold text-arsenic lg:text-pixel-40 lg:leading-pixel-56">
        {title}
      </h1>

      <p className="text-philippine-gray lg:text-base lg:font-normal">
        {content}
      </p>
    </div>
  );
}

export default FilterContent;
