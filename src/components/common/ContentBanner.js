import Image from 'next/image';
import React from 'react';
import banner from '../../../public//images/category/i1.png';
import bgMobile from '../../../public/images/healthInsurance/bannerMobile.png';
import bgTablet from '../../../public/images/healthInsurance/bannerIpad.png';
import ButtonPrimary from './ButtonPrimary';
import { IconLeft, IconRight, Tick } from './icon/Icon';
import Link from 'next/link';
import ButtonBgWhite from './ButtonBgWhite';
export default function ContentBanner({
  bannerID = 1,
  listDescriptions,
  handleBanner,
  detail,
  bannerList,
}) {
  const styleArrow = 'p-2.5 cursor-pointer rounded-full border sm:p-3';
  return (
    <div className="relative">
      <div>
        <Image
          src={banner}
          alt="bgBanner"
          className="absolute -z-50 hidden h-full max-h-112 w-full lg:block"
        />
      </div>
      <div className="absolute inset-0 -z-10 w-full sm:hidden">
        <Image src={bgMobile} fill alt="banner" />
      </div>
      <div className="absolute inset-0 -z-10 hidden w-full sm:block lg:hidden">
        <Image src={bgTablet} fill alt="banner" />
      </div>
      {bannerList?.map((item) => (
        <div key={item?.id}>
          {item?.id === bannerID && (
            <div className="comparison-container pb-10">
              <div className="flex flex-col justify-between gap-8 pt-10 sm:gap-10 lg:flex-row lg:gap-0 lg:pt-8 xl:pt-14">
                <div className="flex flex-col gap-4 lg:w-[56%]">
                  <div className="flex flex-col gap-2">
                    <p className="text-xl font-bold text-spanish-gray sm:text-2xl">
                      {item?.smallTitle}
                    </p>
                    <p className="text-pixel-32 font-bold leading-pixel-48 text-raisin-black sm:text-5xl">
                      {item?.coreTitle}
                    </p>
                  </div>
                  <div>
                    {listDescriptions && (
                      <div className="space-y-2 lg:h-30 xl:h-auto">
                        {listDescriptions?.map((description) => (
                          <div
                            key={description?.id}
                            className="flex items-start gap-x-3"
                          >
                            <div className="my-auto h-auto w-4">
                              <Tick />
                            </div>
                            <p className="text-sm font-normal text-nickel sm:text-base">
                              {description?.detail}
                            </p>
                          </div>
                        ))}
                      </div>
                    )}

                    {detail && (
                      <p className="mb-10 text-base font-normal text-nickel">
                        {detail}
                      </p>
                    )}
                    <div className="mt-6 flex w-full gap-3 sm:mt-10 lg:mt-8 lg:gap-4">
                      {item?.buttonList?.map((item) => (
                        <Link
                          key={item?.id}
                          href={item?.href}
                          className={`${item?.styleButton}`}
                        >
                          {item?.type === 'primary' && (
                            <ButtonPrimary
                              cases={item?.case}
                              title={item?.title}
                              widthBtn={'w-full'}
                            />
                          )}

                          {item?.type === 'bgWhite' && (
                            <ButtonBgWhite
                              cases={item?.case}
                              title={item?.title}
                              widthBtn={
                                'text-ultramarine-blue border-ultramarine-blue w-full'
                              }
                            />
                          )}
                        </Link>
                      ))}
                    </div>
                    {bannerList.length > 1 && (
                      <div className="mt-4 hidden gap-4 lg:mt-12 lg:flex">
                        <div
                          className={`${
                            bannerID >= 2
                              ? 'border-lavender bg-lavender'
                              : 'border-chinese-silver'
                          } ${styleArrow}`}
                          onClick={() =>
                            handleBanner(
                              bannerID === 1 ? (bannerID = 1) : bannerID - 1
                            )
                          }
                        >
                          <IconLeft
                            className={
                              bannerID >= 2
                                ? 'text-azure'
                                : 'text-chinese-silver'
                            }
                          />
                        </div>
                        <div
                          className={`${
                            bannerID < 3
                              ? 'border-lavender bg-lavender'
                              : 'border-chinese-silver'
                          } ${styleArrow}`}
                          onClick={() =>
                            handleBanner(
                              bannerID === 3 ? (bannerID = 3) : bannerID + 1
                            )
                          }
                        >
                          <IconRight
                            className={
                              bannerID < 3
                                ? 'text-azure'
                                : 'text-chinese-silver'
                            }
                          />
                        </div>
                      </div>
                    )}
                  </div>
                </div>
                <div className="sm:mx-auto sm:w-3/4 lg:mx-0 lg:h-98 lg:w-110.75">
                  <Image
                    src={item?.image}
                    alt="banner"
                    className="h-full w-full"
                  />
                </div>
              </div>
              {bannerList.length > 1 && (
                <div className="mt-4 flex gap-4 lg:hidden">
                  <div
                    className={`${
                      bannerID >= 2
                        ? 'border-lavender bg-lavender'
                        : 'border-chinese-silver'
                    } ${styleArrow}`}
                    onClick={() =>
                      handleBanner(
                        bannerID === 1 ? (bannerID = 1) : bannerID - 1
                      )
                    }
                  >
                    <IconLeft
                      className={
                        bannerID >= 2 ? 'text-azure' : 'text-chinese-silver'
                      }
                    />
                  </div>
                  <div
                    className={`${
                      bannerID < 3
                        ? 'border-lavender bg-lavender'
                        : 'border-chinese-silver'
                    } ${styleArrow}`}
                    onClick={() =>
                      handleBanner(
                        bannerID === 3 ? (bannerID = 3) : bannerID + 1
                      )
                    }
                  >
                    <IconRight
                      className={
                        bannerID < 3 ? 'text-azure' : 'text-chinese-silver'
                      }
                    />
                  </div>
                </div>
              )}
            </div>
          )}
        </div>
      ))}
    </div>
  );
}
