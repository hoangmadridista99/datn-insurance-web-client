import React from 'react';
import { IconClosePartnerInformation, IconInformationAlert } from './icon/Icon';
import success from '../../../public/images/AlertSuccess/success.png';
import Image from 'next/image';
import ButtonBgWhite from './ButtonBgWhite';
import ButtonPrimary from './ButtonPrimary';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';
export default function AlertSuccess() {
  const { t } = useTranslation(['common']);
  return (
    <div className="relative rounded-2xl bg-white lg:w-1/2">
      <Link href={'/'} className="absolute right-7 top-5 cursor-pointer">
        <IconClosePartnerInformation />
      </Link>
      <div className="mx-auto px-10 py-12">
        <div className="mb-6 flex justify-center">
          <Image src={success} alt="successTick" />
        </div>
        <p className="mb-3 text-center text-2xl font-bold sm:text-pixel-32 sm:leading-pixel-48">
          {t('successfullyEnteredInformation')}
        </p>
        <p className=" mb-10 text-center text-sm font-normal text-nickel sm:text-base">
          {t('pendingVerification')}
        </p>
        <div className="justify-center gap-4 sm:flex">
          <Link href="/">
            <div className="mb-4 sm:mb-0 sm:h-10 sm:w-57">
              <ButtonBgWhite cases={'large'} title={t('back_home')} />
            </div>
          </Link>
          <Link href="/listUserInsurance">
            <div className="sm:h-10 sm:w-57">
              <ButtonPrimary cases={'large'} title={t('to_list_insurance')} />
            </div>
          </Link>
        </div>
      </div>
      <div className="flex items-start justify-center gap-1 rounded-b-2xl bg-lavender p-4">
        <div className="h-5 w-5 pt-1">
          <IconInformationAlert />
        </div>
        <p className="text-center text-xs font-normal text-azure sm:text-sm">
          {t('alert_success')}
        </p>
      </div>
    </div>
  );
}
