import React from 'react';
import success from '/public/images/AlertSuccess/success.png';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';
import { IconClosePartnerInformation } from '../icon/Icon';
export default function AlertSuccessRating({ setHiddenAlertSuccessRating }) {
  const { t } = useTranslation(['common']);
  const handleCloseAlertSuccessRating = () => {
    setHiddenAlertSuccessRating(false);
  };
  return (
    <div className="relative rounded-2xl bg-white lg:w-1/2">
      <button
        type="button"
        onClick={handleCloseAlertSuccessRating}
        className="absolute right-7 top-5 cursor-pointer"
      >
        <IconClosePartnerInformation />
      </button>
      <div className="mx-auto p-10">
        <div className="mb-6 flex justify-center">
          <Image src={success} alt="successTick" />
        </div>
        <p className="mb-3 text-center text-2xl font-bold sm:text-pixel-32 sm:leading-pixel-48">
          {t('create_rating_success')}
        </p>
        <p className="text-center text-sm font-normal text-nickel sm:text-base">
          {t('rating_show_after_accept')}
        </p>
      </div>
    </div>
  );
}
