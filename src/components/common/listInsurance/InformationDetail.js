import React from 'react';
import { IconGroupHover, IconTickQuestion } from 'components/common/icon/Icon';
import { formatLevelText } from 'src/utils/helper';
import { useTranslation } from 'next-i18next';

function InformationDetail({ label, content, level, description }) {
  const { t } = useTranslation(['common']);
  return (
    <div className="mb-3 flex sm:gap-2">
      <div className="relative flex w-percent-45 justify-between sm:gap-2">
        <p className="w-full pr-7 text-sm font-normal text-nickel sm:text-base">
          {label}
        </p>
        <div className="group absolute right-0 hidden h-6 w-6 items-center sm:flex">
          <div className="cursor-pointer">
            <IconTickQuestion />
          </div>
          <div className=" hidden text-xs font-bold text-nickel group-hover:block">
            <div className="flex items-center">
              <div className="drop-shadow-2xl">
                <IconGroupHover />
              </div>
              <div className="w-97 rounded-lg bg-white p-2 shadow-groupHover xl:w-114">
                {description}
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="flex w-percent-55 items-start gap-2 sm:w-full">
        <div className="h-6 w-6">{formatLevelText(level)}</div>
        <div className="w-full text-sm font-normal text-nickel sm:text-base">
          {content || t('no_support')}
        </div>
      </div>
    </div>
  );
}

export default InformationDetail;
