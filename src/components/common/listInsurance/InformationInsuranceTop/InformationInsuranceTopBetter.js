import numeral from 'numeral';
import React from 'react';

import HiddenDetailTop from './HiddenDetailTop';
import Image from 'next/image';
import ButtonPrimary from 'components/common/ButtonPrimary';
import ButtonBgWhite from 'components/common/ButtonBgWhite';
import { useTranslation } from 'next-i18next';

export default function InformationInsuranceTopBetter({
  insurance,
  selectChangeBg,
  dataInsuranceComparison,
  setDataInsuranceComparison,
  setHiddenDetailTop,
  setHiddenRatingInsuranceTop,
  hiddenDetailTop,
  hiddenRatingInsuranceTop,
  setSelectChangeBg,
  titleBetter,
  ListDetailsLine,
}) {
  const { t } = useTranslation(['common']);

  return (
    <div
      className={`flex h-full flex-col justify-between rounded-2xl border p-4 lg:px-6 lg:pt-6 lg:pb-4 ${
        (selectChangeBg === insurance &&
          'border-white bg-lavender bg-opacity-40') ||
        'border-platinum'
      }`}
    >
      <div>
        <div className="mb-3 flex justify-start">
          <div className="flex gap-3 rounded-2xl bg-lavender px-5 py-1">
            <p className="text-sm font-normal text-azure">{titleBetter}</p>
            <Image
              src="/svg/info-circle.svg"
              width={20}
              height={20}
              alt="info-circle"
            />
          </div>
        </div>

        <div className="mb-2 flex items-center gap-4">
          <div>
            <img
              height={64}
              width={64}
              src={insurance?.company?.logo}
              alt="logoCompany"
            />
          </div>

          <div>
            <p className="text-xl font-bold text-arsenic xl:text-2xl">
              {insurance?.name}
            </p>
            <p className="text-sm font-normal text-nickel xl:text-base xl:font-medium">
              {insurance?.company?.long_name}
            </p>
          </div>
        </div>

        <div className="inline-flex items-end gap-1">
          <p className="text-2xl font-bold text-ultramarine-blue">
            {numeral(insurance?.terms?.monthly_fee?.from).format('(0,0)')} VNĐ
          </p>

          <p className="text-base text-silver">/{t('month')}</p>
        </div>
      </div>

      <ListDetailsLine insurance={insurance} />

      <div>
        <div className="flex flex-col gap-3 border-b pb-4 xl:mt-6 xl:flex-row">
          <ButtonPrimary
            cases={'medium'}
            title={t('register')}
            widthBtn={'xl:w-1/2'}
          />
          <ButtonBgWhite
            cases={'medium'}
            title={t('request_price')}
            widthBtn={'xl:w-1/2'}
          />
        </div>
        <HiddenDetailTop
          insurance={insurance}
          id={insurance?.id}
          dataInsuranceComparison={dataInsuranceComparison}
          setDataInsuranceComparison={setDataInsuranceComparison}
          setHiddenDetailTop={setHiddenDetailTop}
          setHiddenRatingInsuranceTop={setHiddenRatingInsuranceTop}
          hiddenDetailTop={hiddenDetailTop}
          hiddenRatingInsuranceTop={hiddenRatingInsuranceTop}
          setSelectChangeBg={setSelectChangeBg}
        />
      </div>
    </div>
  );
}
