import React from 'react';
import RatingInsuranceTop from './RatingInsuranceTop';
import InformationInsuranceTopBetter from './InformationInsuranceTopBetter';
import { useTranslation } from 'next-i18next';

export default function InformationInsuranceTop({
  dataInsuranceTopObjectives,
  dataInsuranceTopTotalSumInsured,
  dataInsuranceComparison,
  setDataInsuranceComparison,
  currentPage,
  hiddenDetailTop,
  setHiddenDetailTop,
  hiddenRatingInsuranceTop,
  setHiddenRatingInsuranceTop,
  selectChangeBg,
  setSelectChangeBg,
  DetailInsurance,
  ListDetailsLine,
  setHiddenAlertSuccessRating,
}) {
  const { t } = useTranslation(['common']);

  if (currentPage > 1) return null;
  return (
    <>
      {dataInsuranceTopObjectives && (
        <div className="flex h-full w-full flex-col gap-4 xl:flex-row">
          <div className="xl:w-1/2 ">
            <InformationInsuranceTopBetter
              ListDetailsLine={ListDetailsLine}
              insurance={dataInsuranceTopObjectives}
              selectChangeBg={selectChangeBg}
              dataInsuranceComparison={dataInsuranceComparison}
              setDataInsuranceComparison={setDataInsuranceComparison}
              setHiddenDetailTop={setHiddenDetailTop}
              setHiddenRatingInsuranceTop={setHiddenRatingInsuranceTop}
              hiddenDetailTop={hiddenDetailTop}
              hiddenRatingInsuranceTop={hiddenRatingInsuranceTop}
              setSelectChangeBg={setSelectChangeBg}
              titleBetter={t('recommended_buy')}
            />
          </div>
          <div className="xl:w-1/2 ">
            <InformationInsuranceTopBetter
              ListDetailsLine={ListDetailsLine}
              insurance={dataInsuranceTopTotalSumInsured}
              selectChangeBg={selectChangeBg}
              dataInsuranceComparison={dataInsuranceComparison}
              setDataInsuranceComparison={setDataInsuranceComparison}
              setHiddenDetailTop={setHiddenDetailTop}
              setHiddenRatingInsuranceTop={setHiddenRatingInsuranceTop}
              hiddenDetailTop={hiddenDetailTop}
              hiddenRatingInsuranceTop={hiddenRatingInsuranceTop}
              setSelectChangeBg={setSelectChangeBg}
              titleBetter={t('best_price')}
            />
          </div>
        </div>
      )}
      <div className={(hiddenDetailTop || hiddenRatingInsuranceTop) && 'mt-4'}>
        <div>
          {hiddenDetailTop && (
            <div className="rounded-2xl bg-lavender bg-opacity-40 p-6">
              <DetailInsurance id={hiddenDetailTop?.id} />
            </div>
          )}
          {hiddenRatingInsuranceTop && (
            <div className="rounded-2xl bg-lavender bg-opacity-40 p-6 ">
              <RatingInsuranceTop
                setHiddenAlertSuccessRating={setHiddenAlertSuccessRating}
                insurance={hiddenRatingInsuranceTop}
                id={hiddenRatingInsuranceTop?.id}
              />
            </div>
          )}
        </div>
      </div>
    </>
  );
}
