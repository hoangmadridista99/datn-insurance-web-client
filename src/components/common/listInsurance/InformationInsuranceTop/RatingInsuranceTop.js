import { createRatingInsurance, getRatingInsurance } from 'pages/api/insurance';
import React, { useEffect } from 'react';
import { useState } from 'react';
import Image from 'next/image';
import { useForm } from 'react-hook-form';
import { useSession } from 'next-auth/react';
import { FILTER_RATINGS, ratingInsuranceOfUsers } from 'src/utils/constants';
import { convertRatingStar, timeCreateRating } from 'src/utils/helper';
import {
  IconPaginationLeft,
  IconPaginationRight,
  IconRating,
} from 'components/common/icon/Icon';
import ButtonPrimary from 'components/common/ButtonPrimary';
import Select from 'components/common/Select';
import { useTranslation } from 'next-i18next';
import { WindowScrollTop } from 'components/common/WindowScrollTop';

export default function RatingInsuranceTop({
  id,
  insurance,
  setHiddenAlertSuccessRating,
}) {
  const { t } = useTranslation(['common']);

  const { register, handleSubmit, reset } = useForm({});
  const session = useSession();
  const pageSize = 5;
  const [currentPage, setCurrentPage] = useState(1);
  const [dataRatingInsurance, setDataRatingInsurance] = useState([]);
  const [totalRatingInsurance, setTotalRatingInsurance] = useState([]);
  const [sortBy, setSortBy] = useState(FILTER_RATINGS(t)[0]);
  const profileUsers = session?.data;

  const getRatingInsuranceId = async () => {
    try {
      const { data } = await getRatingInsurance({
        id: id,
        params: {
          limit: pageSize,
          page: currentPage,
          sort_by: sortBy.value,
        },
      });
      setDataRatingInsurance(data?.data);
      setTotalRatingInsurance(data?.meta);
    } catch (error) {
      console.log(
        '🚀 ~ file: RatingInsuranceTop.js:43 ~ getRatingInsuranceId ~ error:',
        error
      );
    }
  };

  const handlePaginationDown = () => {
    setCurrentPage(currentPage == 1 ? currentPage : currentPage - 1);
  };
  const handlePaginationUp = () => {
    setCurrentPage(
      currentPage == totalRatingInsurance?.totalPages
        ? currentPage
        : currentPage + 1
    );
  };
  useEffect(() => {
    getRatingInsuranceId();
  }, [id, pageSize, currentPage, sortBy]);

  const [scoreRating, setScoreRating] = useState(0);
  const [errorRating, setErrorRating] = useState('');

  const handleRatingOfUser = (value) => {
    setErrorRating('');
    setScoreRating(Number(value));
  };

  const handleFilterRating = (e) => {
    setSortBy(e);
    setCurrentPage(1);
  };

  const ShowTotalCommentsInCurrentPage = () => {
    return (
      <>
        {(currentPage - 1) * pageSize + 1}-
        {totalRatingInsurance?.totalItems < currentPage * pageSize
          ? totalRatingInsurance?.totalItems
          : currentPage * pageSize}
      </>
    );
  };
  const handleCreateRatingInsurance = async (values) => {
    if (!scoreRating) {
      return setErrorRating(t('please_rating'));
    }
    try {
      await createRatingInsurance(session?.data?.accessToken, {
        params: {
          ...values,
          insurance_id: id,
          score: scoreRating,
        },
      });
      reset();
      setScoreRating(0);
      // lockScroll();
      setHiddenAlertSuccessRating(true);
      WindowScrollTop(0);
    } catch (error) {
      console.log(
        '🚀 ~ file: RatingInsurance.js:96 ~ handleCreateRatingInsurance ~ error:',
        error
      );
    }
  };
  return (
    <>
      <form onSubmit={handleSubmit(handleCreateRatingInsurance)}>
        <div className="rounded-lg border border-platinum bg-cultured bg-opacity-50 p-3 sm:p-4">
          <div className="mb-1 flex items-center gap-1">
            {(!profileUsers?.avatar_profile_url && (
              <div>
                <Image
                  src="/images/listInsurance/logoUser.png"
                  height={20}
                  width={20}
                  alt="logoUser"
                />
              </div>
            )) || (
              <div>
                <img
                  height={20}
                  width={20}
                  src={profileUsers?.avatar_profile_url}
                  alt="logoCompany"
                />
              </div>
            )}

            <p className="ml-1 text-sm font-bold text-arsenic">
              {profileUsers?.first_name}
            </p>
            <p className="text-sm font-bold text-arsenic">
              {profileUsers?.last_name}
            </p>
          </div>

          <div className="flex flex-col items-start gap-2 sm:flex-row sm:items-center">
            <p className="text-sm font-normal text-spanish-gray">
              {t('ratings')}
            </p>
            <div className="flex gap-2">
              {ratingInsuranceOfUsers(scoreRating).map((values) => (
                <button
                  key={values.value}
                  type="button"
                  className={values.className}
                  onClick={() => handleRatingOfUser(values.value)}
                >
                  <IconRating />
                </button>
              ))}
            </div>
            <p className="text-red-500 ">{errorRating}</p>
          </div>

          <div className="mt-2 flex items-center gap-2 rounded-md border border-platinum bg-white p-2">
            <input
              placeholder={t('write_rating')}
              type="text"
              id="comment"
              {...register('comment')}
              className="w-full outline-none"
            />
            <ButtonPrimary
              type={'submit'}
              cases={'small'}
              title={t('send')}
              widthBtn={'w-1/3 sm:w-1/6'}
            />
          </div>
        </div>
      </form>

      <div className="mt-2 flex flex-col-reverse items-center justify-between sm:mt-4 sm:flex-row">
        <div className="mt-4 flex w-full items-end gap-2 border-l-2 pl-2 text-base font-normal text-silver sm:mt-0">
          <p>
            <span className="text-xl font-bold text-arsenic sm:text-2xl">
              {insurance?.rating_scores}
            </span>
            /5
          </p>
          <p className="text-sm font-normal text-silver sm:text-base">{`(${
            totalRatingInsurance?.totalItems
          } ${t('rating')})`}</p>
        </div>

        <div className="w-full sm:w-1/3">
          <Select
            options={FILTER_RATINGS(t)}
            onChange={handleFilterRating}
            selected={sortBy}
          />
        </div>
      </div>

      <div className="mb-5">
        {dataRatingInsurance.map((data) => (
          <div className="p-4" key={data.id}>
            <div className="mb-1 flex items-center gap-1">
              {(!data?.user?.avatar_profile_url && (
                <div>
                  <Image
                    src="/images/listInsurance/logoUser.png"
                    height={20}
                    width={20}
                    alt="logoUser"
                  />
                </div>
              )) || (
                <div>
                  <img
                    height={20}
                    width={20}
                    src={data?.user?.avatar_profile_url}
                    alt="logoCompany"
                  />
                </div>
              )}

              <p className="ml-1 text-sm font-bold text-arsenic">
                {data?.user?.first_name}
              </p>
              <p className="text-sm font-bold text-arsenic">
                {data?.user?.last_name}
              </p>
            </div>
            <div className="mb-3 flex items-center gap-2">
              <div>{convertRatingStar(data?.score)}</div>
              <p className="text-xs font-normal text-spanish-gray">
                {timeCreateRating(data?.created_at)}
              </p>
            </div>
            <p className="text-sm font-normal text-nickel">{data?.comment}</p>
          </div>
        ))}
      </div>
      <div className="flex justify-between text-chinese-silver">
        {totalRatingInsurance?.totalItems !== 0 && (
          <p>
            <ShowTotalCommentsInCurrentPage /> {t('above')}
            <span className="font-bold text-nickel">
              {totalRatingInsurance?.totalItems}
            </span>{' '}
            {t('rating')}
          </p>
        )}
        <div
          className={`${
            totalRatingInsurance?.totalPages > 1
              ? 'flex cursor-pointer gap-6'
              : 'hidden'
          }`}
        >
          <div
            onClick={handlePaginationDown}
            className={
              currentPage == 1 ? 'cursor-not-allowed' : 'text-ultramarine-blue'
            }
          >
            <IconPaginationLeft />
          </div>
          <div
            onClick={handlePaginationUp}
            className={
              currentPage == totalRatingInsurance?.totalPages
                ? 'cursor-not-allowed'
                : 'text-ultramarine-blue'
            }
          >
            <IconPaginationRight />
          </div>
        </div>
      </div>
    </>
  );
}
