import React, { useContext, useState } from 'react';
import { useSession } from 'next-auth/react';
import { toast } from 'react-toastify';
import { DataContext } from 'src/store/GlobalState';
import {
  IconConcern,
  IconDropDown2,
  IconRatingStarTotal,
} from 'components/common/icon/Icon';
import { addInsuranceConcern } from 'pages/api/insurance';
import { useEffect } from 'react';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import { BLUE_DARK, INK_LIGHT, INK_LIGHT_TEST } from 'src/utils/constants';
import { lockScroll } from 'src/utils/helper';
import { useTranslation } from 'next-i18next';

function HiddenDetailTop({
  insurance,
  id,
  dataInsuranceComparison,
  setDataInsuranceComparison,
  setHiddenDetailTop,
  setHiddenRatingInsuranceTop,
  hiddenDetailTop,
  hiddenRatingInsuranceTop,
  setSelectChangeBg,
}) {
  const { t } = useTranslation(['common']);

  const session = useSession();
  const { dispatch, state } = useContext(DataContext);
  const { updateDataInsurance } = state;
  const dataInsuranceCheck = updateDataInsurance?.updateDataInsurance;
  const [insuranceConcern, setInsuranceConcern] = useState(insurance?.isSaved);

  const [isHover, setIsHover] = useState({
    rating: false,
    details: false,
    concern: false,
  });

  useEffect(() => {
    setInsuranceConcern(insurance?.isSaved);
  }, [insurance]);

  const handleHiddenDetail = () => {
    if (!session?.data) {
      dispatch({ type: 'LOGIN', loginNoAccount: { login: true } });
      toast.error(t('please_login_see_details'));
      lockScroll();
      WindowScrollTop(0);
      return;
    }

    if (hiddenDetailTop === insurance) {
      setHiddenDetailTop();
      setSelectChangeBg();
      return;
    }
    setHiddenDetailTop(insurance);
    setHiddenRatingInsuranceTop();
    setSelectChangeBg(insurance);
  };

  const handleHiddenRatingInsurance = () => {
    if (!session?.data) {
      dispatch({ type: 'LOGIN', loginNoAccount: { login: true } });
      toast.error(t('please_login_see_rating'));
      lockScroll();
      WindowScrollTop(0);
      return;
    }
    if (hiddenRatingInsuranceTop === insurance) {
      setHiddenRatingInsuranceTop();
      setSelectChangeBg();
      return;
    }
    setHiddenRatingInsuranceTop(insurance);
    setHiddenDetailTop();
    setSelectChangeBg(insurance);
  };

  const handleComparison = (event) => {
    if (!session?.data) {
      dispatch({ type: 'LOGIN', loginNoAccount: { login: true } });
      toast.error(t('please_login_comparison_insurance'));
      lockScroll();
      WindowScrollTop(0);
      return;
    }
    if (event?.target?.checked) {
      if (dataInsuranceCheck?.length === 3)
        return toast(t('choice_max_three_insurance'));
      const updateDataInsurance = [...dataInsuranceComparison, insurance];
      setDataInsuranceComparison(updateDataInsurance);
      dispatch({
        type: 'COMPARISON',
        comparison: { comparison: true },
        updateDataInsurance: { updateDataInsurance: updateDataInsurance },
      });
    } else {
      const arrayDataInsuranceCheck = [...dataInsuranceCheck].filter(
        (obj) => obj.id !== insurance.id
      );

      dispatch({
        type: 'COMPARISON',
        comparison: { comparison: true },
        updateDataInsurance: { updateDataInsurance: arrayDataInsuranceCheck },
      });

      setDataInsuranceComparison(arrayDataInsuranceCheck);

      if (arrayDataInsuranceCheck?.length === 0)
        return dispatch({
          type: 'COMPARISON',
          comparison: { comparison: false },
        });
    }
  };

  const handleInsuranceConcern = async () => {
    if (!session?.data) {
      dispatch({ type: 'LOGIN', loginNoAccount: { login: true } });
      toast.error(t('please_login_concern'));
      lockScroll();
      WindowScrollTop(0);
      return;
    }
    try {
      await addInsuranceConcern(session?.data?.accessToken, {
        params: { insuranceId: id },
      });
      setInsuranceConcern(!insuranceConcern);
      insuranceConcern
        ? toast.error(t('remove_list_concern'))
        : toast.success(t('add_list_concern'));
    } catch (err) {
      console.log(
        '🚀 ~ file: HiddenDetail.js:83 ~ handleInsuranceConcern ~ err:',
        err
      );
    }
  };

  return (
    <>
      <div className="mt-5 flex justify-between gap-1">
        <div className="flex gap-1 lg:gap-2">
          <input
            id={id}
            type="checkbox"
            className="h-6 w-6 hover:cursor-pointer"
            onChange={handleComparison}
            checked={dataInsuranceComparison?.some((item) => item?.id === id)}
          />
          <label
            htmlFor={id}
            className="text-sm font-normal text-arsenic hover:cursor-pointer lg:text-base "
          >
            {t('comparison')}
          </label>
        </div>

        <button
          type="button"
          onClick={handleHiddenRatingInsurance}
          className={`flex cursor-pointer justify-center gap-1 text-base ${
            hiddenRatingInsuranceTop === insurance
              ? 'text-ultramarine-blue'
              : 'text-spanish-gray'
          }`}
          onMouseOver={() => setIsHover({ ...isHover, rating: true })}
          onMouseOut={() => setIsHover({ ...isHover, rating: false })}
        >
          <p
            className={`${
              hiddenRatingInsuranceTop === insurance || isHover.rating
                ? ' text-ultramarine-blue '
                : ' text-arsenic'
            }`}
          >
            {insurance?.rating_scores}
          </p>
          <IconRatingStarTotal />
          <div
            className={`${
              hiddenRatingInsuranceTop === insurance
                ? 'origin-center -rotate-180 duration-200'
                : 'duration-200'
            } `}
          >
            <IconDropDown2
              id={'details'}
              stroke={`${
                hiddenRatingInsuranceTop === insurance || isHover.rating
                  ? BLUE_DARK
                  : INK_LIGHT
              } `}
            />
          </div>
        </button>

        <button
          type="button"
          onClick={handleHiddenDetail}
          className={`flex justify-center gap-1 text-sm font-normal lg:text-base ${
            hiddenDetailTop === insurance ? 'text-arsenic' : 'text-spanish-gray'
          }`}
          onMouseOver={() => setIsHover({ ...isHover, details: true })}
          onMouseOut={() => setIsHover({ ...isHover, details: false })}
        >
          <p
            className={`${
              hiddenDetailTop === insurance || isHover.details
                ? ' text-ultramarine-blue'
                : '  text-arsenic'
            } `}
          >
            {t('details')}
          </p>
          <div
            className={`${
              hiddenDetailTop === insurance
                ? 'origin-center -rotate-180 duration-200'
                : 'duration-200'
            } `}
          >
            <IconDropDown2
              stroke={`${
                hiddenDetailTop === insurance || isHover.details
                  ? BLUE_DARK
                  : INK_LIGHT
              } `}
            />
          </div>
        </button>
        <div className="w-1/7 flex items-center justify-end gap-2">
          <button
            onClick={handleInsuranceConcern}
            onMouseOver={() => setIsHover({ ...isHover, concern: true })}
            onMouseOut={() => setIsHover({ ...isHover, concern: false })}
          >
            <div>
              <IconConcern
                fill={insuranceConcern ? BLUE_DARK : 'none'}
                stroke={
                  insuranceConcern || isHover.concern
                    ? BLUE_DARK
                    : INK_LIGHT_TEST
                }
              />
            </div>
          </button>
        </div>
      </div>
    </>
  );
}

export default HiddenDetailTop;
