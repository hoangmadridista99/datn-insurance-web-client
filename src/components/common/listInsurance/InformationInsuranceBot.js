import React from 'react';
import numeral from 'numeral';

import HiddenDetail from './HiddenDetail';
import ButtonPrimary from 'components/common/ButtonPrimary';
import ButtonBgWhite from 'components/common/ButtonBgWhite';
import ListDetailsLineLifeInsurance from 'components/listLifeInsurance/ListDetailsLineLifeInsurance';
import ListDetailsLineHealthInsurance from 'components/listHealthInsurance/ListDetailsLineHealthInsurance';
import { HEALTH, LIFE } from 'src/utils/constants';
import { useTranslation } from 'next-i18next';
const categoryInsurance = (value) => (
  <p className="rounded-2xl bg-cultured px-2 py-1 text-center text-sm font-normal text-nickel sm:px-5">
    {value}
  </p>
);

function InformationInsuranceBot({
  dataInsurance,
  setDataInsuranceComparison,
  dataInsuranceComparison,
  ListDetailsLine,
  DetailInsurance,
  setHiddenAlertSuccessRating,
}) {
  const { t } = useTranslation('common', 'insuranceLocale');
  return (
    <div className="flex w-full flex-col gap-6">
      {dataInsurance?.map((insurance) => {
        const ListDetailsLineByCategory = () => {
          switch (insurance?.insurance_category?.label) {
            case LIFE:
              return (
                <ListDetailsLineLifeInsurance t={t} insurance={insurance} />
              );
            case HEALTH:
              return (
                <ListDetailsLineHealthInsurance t={t} insurance={insurance} />
              );
            default:
              return <ListDetailsLine insurance={insurance} />;
          }
        };

        const CategoryInsurance = () => {
          switch (insurance?.insurance_category?.label) {
            case LIFE:
              return categoryInsurance(t('lifeInsurance'));
            case HEALTH:
              return categoryInsurance(t('healthInsurance'));
            default:
              return;
          }
        };

        return (
          <div
            key={insurance?.id}
            className="w-full rounded-2xl bg-white p-6 shadow-listInsurance"
          >
            <div className="flex flex-col-reverse justify-between sm:flex-row">
              <div className="mb-5 flex items-center gap-4">
                <img
                  height={64}
                  width={64}
                  className="bg-none"
                  src={insurance?.company?.logo}
                  alt="logoCompany"
                />
                <div>
                  <p className="text-2xl font-bold text-arsenic">
                    {insurance?.name}
                  </p>
                  <p className="text-sm font-normal text-nickel sm:text-base sm:font-medium">
                    {insurance?.company?.long_name}
                  </p>
                </div>
              </div>

              <div className="mb-3 sm:mb-0">
                <p className="w-1/2 sm:w-full">{CategoryInsurance()}</p>
              </div>
            </div>
            <div className="flex items-center justify-start gap-1 xl:hidden">
              <p className="text-2xl font-bold text-ultramarine-blue">
                {numeral(insurance?.terms?.monthly_fee?.from).format('(0,0)')}
                VNĐ
              </p>
              <p className="text-base font-normal text-silver">/{t('month')}</p>
            </div>

            <div className="flex flex-col gap-2 border-b pb-3 xl:flex-row xl:justify-between">
              {ListDetailsLineByCategory()}

              <div className="flex flex-col items-end justify-end gap-2">
                <div className="hidden items-center gap-1 xl:flex">
                  <p className="text-2xl font-bold text-ultramarine-blue">
                    {numeral(insurance?.terms?.monthly_fee?.from).format(
                      '(0,0)'
                    )}
                    VNĐ
                  </p>
                  <p className="text-base font-normal text-silver">
                    /{t('month')}
                  </p>
                </div>
                <div className="flex w-full flex-col gap-3 xl:w-48">
                  <ButtonPrimary cases={'medium'} title={t('register')} />
                  <ButtonBgWhite cases={'medium'} title={t('request_price')} />
                </div>
              </div>
            </div>

            <HiddenDetail
              setHiddenAlertSuccessRating={setHiddenAlertSuccessRating}
              DetailInsurance={DetailInsurance}
              insurance={insurance}
              id={insurance.id}
              setDataInsuranceComparison={setDataInsuranceComparison}
              dataInsuranceComparison={dataInsuranceComparison}
            />
          </div>
        );
      })}
    </div>
  );
}

export default InformationInsuranceBot;
