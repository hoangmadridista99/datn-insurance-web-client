import React from 'react';
import { useTranslation } from 'next-i18next';

function InformationDetailNoNone({ label, content, level }) {
  const { t } = useTranslation(['common']);
  return (
    <div className="mb-3 flex sm:gap-2">
      <div className="flex w-percent-45 justify-between gap-2">
        <p className="w-full text-sm font-normal text-nickel sm:text-base">
          {label}
        </p>
      </div>
      <div className="flex w-percent-55 items-start gap-2 sm:w-full">
        <div className="h-6 w-6">{level}</div>
        <div className="w-full text-sm font-normal text-nickel sm:text-base">
          {content || t('no_support')}
        </div>
      </div>
    </div>
  );
}

export default InformationDetailNoNone;
