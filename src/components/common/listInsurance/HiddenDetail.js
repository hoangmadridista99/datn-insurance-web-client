import React, { useContext, useEffect, useState } from 'react';
import { useSession } from 'next-auth/react';
import { toast } from 'react-toastify';
import { DataContext } from 'src/store/GlobalState';
import {
  IconConcern,
  IconDropDown2,
  IconRatingStarTotal,
} from 'components/common/icon/Icon';
import RatingInsurance from './RatingInsurance';
import { addInsuranceConcern } from 'pages/api/insurance';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import {
  HEALTH,
  LIFE,
  BLUE_DARK,
  INK_LIGHT,
  INK_LIGHT_TEST,
} from 'src/utils/constants';
import DetailLifeInsurance from 'components/listLifeInsurance/DetailLifeInsurance';
import DetailHealthInsurance from 'components/listHealthInsurance/DetailHealthInsurance';
import { lockScroll } from 'src/utils/helper';
import { useTranslation } from 'next-i18next';

function HiddenDetail({
  insurance,
  id,
  dataInsuranceComparison,
  setDataInsuranceComparison,
  DetailInsurance,
  setHiddenAlertSuccessRating,
}) {
  const { t } = useTranslation(['common']);
  const session = useSession();
  const controller = new AbortController();
  const signal = controller.signal;
  const { dispatch, state } = useContext(DataContext);
  const { updateDataInsurance } = state;
  const dataInsuranceCheck = updateDataInsurance?.updateDataInsurance;

  const [hiddenDetail, setHiddenDetail] = useState(true);
  const [hiddenRatingInsurance, setHiddenRatingInsurance] = useState(true);
  const [insuranceConcern, setInsuranceConcern] = useState(insurance?.isSaved);

  const [isHover, setIsHover] = useState({
    rating: false,
    details: false,
    concern: false,
  });

  useEffect(() => {
    setInsuranceConcern(insurance?.isSaved);
  }, [insurance]);

  useEffect(() => {
    if (!dataInsuranceCheck) {
      setDataInsuranceComparison([]);
      return;
    }
    setDataInsuranceComparison(dataInsuranceCheck);
  }, [dataInsuranceCheck]);

  const handleHiddenDetail = () => {
    if (!session?.data) {
      dispatch({
        type: 'LOGIN',
        loginNoAccount: { login: true },
      });
      toast.error(t('please_login_see_details'));
      lockScroll();
      WindowScrollTop(0);
      return;
    }
    setHiddenDetail(!hiddenDetail);
    setHiddenRatingInsurance(true);
  };

  const handleHiddenRatingInsurance = () => {
    if (!session?.data) {
      dispatch({
        type: 'LOGIN',
        loginNoAccount: { login: true },
      });
      toast.error(t('please_login_see_rating'));
      lockScroll();
      WindowScrollTop(0);
      return;
    }
    setHiddenRatingInsurance(!hiddenRatingInsurance);
    setHiddenDetail(true);
  };

  const handleComparison = (event) => {
    if (!session?.data) {
      dispatch({
        type: 'LOGIN',
        loginNoAccount: { login: true },
      });
      toast.error(t('please_login_comparison_insurance'));
      lockScroll();
      WindowScrollTop(0);
      return;
    }
    if (event?.target?.checked) {
      if (dataInsuranceCheck?.length === 3)
        return toast(t('choice_max_three_insurance'));
      const updateDataInsurance = [...dataInsuranceComparison, insurance];
      setDataInsuranceComparison(updateDataInsurance);
      dispatch({
        type: 'COMPARISON',
        comparison: { comparison: true },
        updateDataInsurance: { updateDataInsurance: updateDataInsurance },
      });
    } else {
      const arrayDataInsuranceCheck = [...dataInsuranceCheck].filter(
        (obj) => obj.id !== insurance.id
      );

      dispatch({
        type: 'COMPARISON',
        comparison: { comparison: true },
        updateDataInsurance: {
          updateDataInsurance: arrayDataInsuranceCheck,
        },
      });
      setDataInsuranceComparison(arrayDataInsuranceCheck);

      if (arrayDataInsuranceCheck?.length === 0)
        return dispatch({
          type: 'COMPARISON',
          comparison: { comparison: false },
        });
    }
  };

  const handleInsuranceConcern = async () => {
    if (!session?.data) {
      dispatch({
        type: 'LOGIN',
        loginNoAccount: { login: true },
      });
      toast.error(t('please_login_concern'));
      lockScroll();
      WindowScrollTop(0);
      return;
    }
    try {
      await addInsuranceConcern(
        session?.data?.accessToken,
        {
          params: { insuranceId: id },
        },
        { signal }
      );
      setInsuranceConcern(!insuranceConcern);
      insuranceConcern
        ? toast.error(t('remove_list_concern'))
        : toast.success(t('add_list_concern'));
    } catch (err) {
      console.log(
        '🚀 ~ file: HiddenDetail.js:83 ~ handleInsuranceConcern ~ err:',
        err
      );
    }
  };

  const DetailInsuranceByCategory = () => {
    switch (insurance?.insurance_category?.label) {
      case LIFE:
        return <DetailLifeInsurance data={insurance} id={id} />;
      case HEALTH:
        return <DetailHealthInsurance data={insurance} id={id} />;
      default:
        return <DetailInsurance data={insurance} id={id} />;
    }
  };

  return (
    <>
      <div className="mt-5 flex justify-between gap-1 lg:gap-2">
        <div className="flex gap-1 xl:w-1/5 xl:gap-2">
          <input
            id={id}
            type="checkbox"
            className="h-6 w-6 hover:cursor-pointer"
            onChange={handleComparison}
            checked={dataInsuranceComparison?.some((item) => item?.id === id)}
          />
          <label
            htmlFor={id}
            className="text-sm font-normal text-arsenic hover:cursor-pointer lg:text-base"
          >
            {t('comparison')}
          </label>
        </div>
        <div className="flex justify-center xl:w-1/3">
          <button
            type="button"
            onClick={handleHiddenRatingInsurance}
            className={`gap-4 px-2 xl:px-5 ${
              !hiddenRatingInsurance
                ? 'text-ultramarine-blue'
                : 'text-spanish-gray'
            }`}
            onMouseOver={() => setIsHover({ ...isHover, rating: true })}
            onMouseOut={() => setIsHover({ ...isHover, rating: false })}
          >
            <div className="flex items-center gap-1 text-base font-normal text-spanish-gray">
              <p
                className={`${
                  !hiddenRatingInsurance || isHover.rating
                    ? 'text-ultramarine-blue'
                    : 'text-arsenic'
                } text-base`}
              >
                {insurance?.rating_scores}
              </p>
              <IconRatingStarTotal />
              <p className="hidden xl:block">{`(${insurance?.total_rating} ${t(
                'rating'
              )})`}</p>
              <div
                className={`${
                  !hiddenRatingInsurance && '-rotate-180'
                } origin-center duration-200`}
              >
                <IconDropDown2
                  stroke={`${
                    !hiddenRatingInsurance || isHover.rating
                      ? BLUE_DARK
                      : INK_LIGHT
                  } `}
                />
              </div>
            </div>
          </button>
        </div>
        <div className="flex justify-end xl:w-1/4">
          <button
            type="button"
            onClick={handleHiddenDetail}
            className={`flex gap-1 lg:px-2 xl:gap-4 xl:px-3 ${
              !hiddenDetail ? 'text-arsenic' : 'text-spanish-gray'
            }`}
            onMouseOver={() => setIsHover({ ...isHover, details: true })}
            onMouseOut={() => setIsHover({ ...isHover, details: false })}
          >
            <p
              className={`${
                !hiddenDetail || isHover.details
                  ? 'text-ultramarine-blue'
                  : 'text-arsenic'
              } flex w-full text-sm font-normal xl:text-base`}
            >
              <span className="hidden xl:block">{t('seeDetails')}</span>
              <span className="xl:hidden">{t('details')}</span>
            </p>
            <div
              className={`${
                !hiddenDetail && '-rotate-180'
              } origin-center duration-200`}
            >
              <IconDropDown2
                stroke={`${
                  !hiddenDetail || isHover.details ? BLUE_DARK : INK_LIGHT
                } `}
              />
            </div>
          </button>
        </div>
        <div className="flex justify-end xl:w-1/4">
          <button
            onClick={handleInsuranceConcern}
            className="flex items-center justify-center gap-2 "
            onMouseOver={() => setIsHover({ ...isHover, concern: true })}
            onMouseOut={() => setIsHover({ ...isHover, concern: false })}
          >
            <div>
              <IconConcern
                fill={insuranceConcern ? BLUE_DARK : 'none'}
                stroke={
                  insuranceConcern || isHover.concern
                    ? BLUE_DARK
                    : INK_LIGHT_TEST
                }
              />
            </div>
            <p
              className={`hidden text-base font-normal ${
                insuranceConcern || isHover.concern
                  ? 'text-ultramarine-blue'
                  : 'text-arsenic'
              } xl:block`}
            >
              {t('concern')}
            </p>
          </button>
        </div>
      </div>

      {!hiddenDetail && DetailInsuranceByCategory()}
      {!hiddenRatingInsurance && (
        <RatingInsurance
          insurance={insurance}
          id={id}
          setHiddenAlertSuccessRating={setHiddenAlertSuccessRating}
        />
      )}
    </>
  );
}

export default HiddenDetail;
