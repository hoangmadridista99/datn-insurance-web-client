import Link from 'next/link';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { LIST_HEADERS } from 'src/utils/constants';

function HeaderInsurance() {
  const { t } = useTranslation(['auth', 'common']);

  return (
    <div className="bg-ultramarine-blue">
      <div className="comparison-container relative flex">
        {LIST_HEADERS(t).map((list) => (
          <div key={list.id} className="group">
            <Link rel="noopener noreferrer" href={list?.href}>
              <button type="button" className={list?.className}>
                <span className="mr-1">{list?.label}</span>
                {list?.arrow_down}
              </button>
            </Link>

            <div className="comparison-container absolute left-0 top-14 w-full">
              <div className={list?.classNameHover}>
                {list?.contentHover?.map((title) => (
                  <div key={title?.idTitle} className="flex w-full flex-col">
                    <p className="mb-4 px-3 text-xl font-bold text-raisin-black">
                      {title?.titleKey}
                    </p>

                    {title?.children.map((child) => (
                      <Link key={child?.idChild} href={child?.href}>
                        <p className="rounded-lg p-3 text-base font-normal text-arsenic hover:bg-lavender hover:text-ultramarine-blue">
                          {child?.label}
                        </p>
                      </Link>
                    ))}
                  </div>
                ))}
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}

export default HeaderInsurance;
