import React from 'react';

function ButtonSort({ type = 'button', key, list, sort, title, onClick }) {
  return (
    <button
      type={type}
      key={key}
      className={`${
        list?.filterSort === sort
          ? 'border-ultramarine-blue bg-lavender text-ultramarine-blue'
          : 'bg-cultured hover:bg-lavender'
      } w-full rounded-md border border-platinum py-2 text-base font-normal text-spanish-gray hover:border-ultramarine-blue hover:text-ultramarine-blue lg:px-4`}
      onClick={onClick}
    >
      {title}
    </button>
  );
}

export default ButtonSort;
