import React from 'react';
function Button({ type = 'button', cases = 'normal', title, list, sort, onclick, width, key, valueFilter, item }) {
  const buttonList = {
    primary: 'w-full rounded-lg bg-ultramarine-blue py-2 text-base font-bold leading-6 text-cultured hover:bg-gradient-to-r from-egyptian-blue to-ultramarine-blue',
    normal: 'rounded-lg bg-ultramarine-blue px-10 py-3 text-white text-base font-bold hover:bg-gradient-to-r from-egyptian-blue to-ultramarine-blue',
    disable: 'w-full rounded-lg border border-nickel py-2 px-3 text-base font-bold leading-6 text-nickel hover:text-ultramarine-blue hover:border-ultramarine-blue',
    fullWidth: 'mb-6 w-full rounded-lg bg-ultramarine-blue py-2 text-base font-bold leading-6 text-cultured md:py-3`',
    sort: `${list?.filterSort === sort
      ? 'border-ultramarine-blue bg-lavender text-ultramarine-blue'
      : 'bg-cultured hover:bg-lavender'
    } w-[150px] rounded-md border border-platinum  py-2 text-base font-normal leading-6 text-spanish-gray hover:border-ultramarine-blue hover:text-ultramarine-blue lg:w-auto lg:px-4 xl:w-[170px]`,
    formFilterInsurance: `${valueFilter === item ? 'bg-lavender-button border-ultramarine-blue text-ultramarine-blue bg-fresh-air' : 'border-platinum bg-cultured text-spanish-gray hover:border-ultramarine-blue hover:text-ultramarine-blue'} w-full rounded-md border p-3 text-base font-normal leading-6`,
    signIn: 'rounded border border-nickel bg-white px-6 py-1 text-xs font-normal leading-4 text-granite-gray hover:border-ultramarine-blue hover:bg-lavender hover:text-ultramarine-blue sm:text-sm sm:leading-5 lg:font-medium',
    signUp:'rounded border-ultramarine-blue bg-ultramarine-blue from-cyan-500 to-blue-500 px-6 py-1 text-xs font-normal leading-4 text-white hover:bg-gradient-to-r hover:from-egyptian-blue hover:to-ultramarine-blue sm:text-sm sm:leading-5 lg:font-medium'


  };
  return (
    <div className={width}>
      <button key={key} type={type} onClick={onclick} className={buttonList[`${cases}`]}>
        {title}
      </button>
    </div>
  );
}

export default Button;