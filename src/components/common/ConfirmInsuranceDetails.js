import EachPartInsuranceDetail from 'components/listUserInsurance/EachPartInsuranceDetail';
import React from 'react';
import { useTranslation } from 'next-i18next';

import {
  appraisalInformation,
  bankBeneficiaryVehicleInformation,
  carInformation,
  contractInformationList,
  electronicBill,
  listCheckbox,
} from 'src/utils/constants';

export default function ConfirmInsuranceDetails({ insurance }) {
  const { t } = useTranslation(['listUserInsurance']);

  return (
    <>
      <EachPartInsuranceDetail
        carInformation={carInformation(t, insurance)}
        title={'vehicleInformation'}
      />
      <EachPartInsuranceDetail
        title={'insuranceBenefits'}
        listCheckbox={listCheckbox(insurance)}
      />
      <EachPartInsuranceDetail
        carInformation={appraisalInformation(insurance)}
        title={'appraisalInformation'}
      />
      <EachPartInsuranceDetail
        carInformation={contractInformationList(insurance)}
        title={'contractInformation'}
      />
      <EachPartInsuranceDetail
        carInformation={bankBeneficiaryVehicleInformation(insurance)}
        title={'bankBeneficiaryVehicleInformation'}
      />
      <EachPartInsuranceDetail
        carInformation={electronicBill(t, insurance)}
        title={'InformationOnIssuingElectronicInvoices'}
      />
    </>
  );
}
