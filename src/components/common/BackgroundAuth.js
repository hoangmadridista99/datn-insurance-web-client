import Image from 'next/image';
import React from 'react';
import { useTranslation } from 'next-i18next';
import Link from 'next/link';

function BackgroundAuth({ children }) {
  const { t } = useTranslation(['auth', 'common']);

  return (
    <div className="max-w-screen flex max-h-screen bg-gradient-to-r from-egyptian-blue to-ultramarine-blue">
      <div className="z-10 flex h-screen w-full flex-col overflow-y-auto rounded-none bg-cultured pt-[30px] pb-0 md:pb-[30px] lg:w-[56.5%] lg:rounded-r-2xl">
        <div className="mb-[30px] pl-5 md:pl-10 xl:pl-[108px]">
          <Link href={'/'}>
            <Image
              src="/images/navbar/logoComparison.png"
              width={111}
              height={111}
              priority="true"
              alt="comparisonLogo"
            />
          </Link>
        </div>

        <div className="relative flex items-center justify-between bg-gradient-to-r from-egyptian-blue to-ultramarine-blue lg:hidden">
          <div className="absolute top-0 right-0 w-[36px] overflow-hidden sm:w-[50px]">
            <div className="h-[50px] w-[50px] bg-[url('/images/auth/Polygon1.png')] bg-contain bg-right-top bg-no-repeat sm:h-[65px] sm:w-[65px]" />
          </div>

          <div className="absolute top-8 right-0 z-0 overflow-hidden">
            <div className="h-[115px] w-[90px] bg-[url('/images/auth/Polygon2.png')] bg-contain bg-left-bottom bg-no-repeat sm:h-[155px] sm:w-[110px]" />
          </div>

          <div className="absolute bottom-0 -left-7 z-0 h-10 overflow-hidden sm:h-[4.25rem]">
            <div className="h-[100px] w-[100px] bg-[url('/images/auth/Polygon2.png')] bg-contain bg-left-bottom bg-no-repeat sm:h-[150px] sm:w-[150px]" />
          </div>

          <div className="z-0 w-2/5 pl-4 sm:w-9/12 md:w-3/5 md:pl-10">
            <p className="whitespace-nowrap text-sm font-bold text-cultured sm:hidden sm:text-base">
              {t('authContentFirstLine')}
              <br /> {t('authContentSecondLine')}
              <br /> {t('authContentThirdLine')}
            </p>

            <p className="mb-3 hidden text-sm font-bold text-cultured sm:block sm:text-xl">
              {t('BackgroundAuth1')}
            </p>

            <p className="hidden text-xs font-normal text-platinum sm:block lg:hidden">
              {t('BackgroundAuth2')}
            </p>
          </div>

          <div className="z-10 h-[160px] w-[230px] bg-[url('/images/auth/family-large.png')] bg-contain bg-left-bottom bg-no-repeat sm:h-[260px] sm:w-[360px]" />
        </div>

        <div className="flex w-full flex-1 items-center py-8 pl-5 sm:pl-10 xl:pl-[108px]">
          <div className={'w-full pr-5 sm:pr-10 xl:w-[80%] xl:pr-[0px]'}>
            {children}
          </div>
        </div>
      </div>

      <div className="relative hidden h-screen w-full flex-1 bg-[url('/images/auth/family.png')] bg-contain bg-left-bottom bg-no-repeat pl-14 pt-28 lg:block">
        <div className="absolute z-10 hidden w-[70%] lg:block">
          <p className="mb-3 text-[32px] font-bold leading-[48px] text-cultured">
            {t('BackgroundAuth1')}
          </p>

          <p className="text-base font-normal leading-6 text-platinum">
            {t('BackgroundAuth2')}
          </p>
        </div>

        <div className="absolute top-0 right-0 hidden lg:block">
          <Image
            src="/images/auth/Polygon1.png"
            height={100}
            width={100}
            priority="true"
            alt="polygon1"
          />
        </div>

        <div className="absolute top-16 right-0 hidden lg:block">
          <Image
            src="/images/auth/Polygon2.png"
            width={243}
            height={243}
            priority="true"
            alt="polygon2"
          />
        </div>
      </div>
    </div>
  );
}

export default BackgroundAuth;
