import LayoutWithHeader from 'components/common/LayoutWithHeader';
import Image from 'next/image';
import React from 'react';
import OurCustomer from './OurCustomer';

function FormFilter({ children }) {
  return (
    <LayoutWithHeader>
      <div className="relative">
        <div className="absolute inset-0 -z-10 h-[500px] w-full">
          <Image
            src="/images/formFilter/bg-formFilter.png"
            alt="bgFormFilter"
            fill
            priority="true"
          />
        </div>
        <div className="absolute -top-10 right-0 -z-10 hidden xl:block">
          <Image
            src="/images/formFilter/Polygon1.png"
            height={40}
            width={40}
            priority="true"
            alt="polygon1"
          />
        </div>
        <div className="absolute right-0  top-24 -z-10 hidden xl:block">
          <Image
            src="/images/formFilter/Polygon2.png"
            priority="true"
            width={120}
            height={120}
            alt="polygon2"
          />
        </div>
        <div className="absolute -top-16  left-0 -z-10 hidden xl:block">
          <Image
            src="/images/formFilter/Polygon3.png"
            width={80}
            height={30}
            priority="true"
            alt="polygon3"
          />
        </div>
        <div className="absolute -top-28  left-0 -z-10 hidden xl:block">
          <Image
            src="/images/formFilter/Polygon4.png"
            width={160}
            height={160}
            priority="true"
            alt="polygon4"
          />
        </div>
        <div className="comparison-container pt-5 pb-10 xl:pb-32 xl:pt-16">
          {children}
        </div>
      </div>

      <OurCustomer />
    </LayoutWithHeader>
  );
}

export default FormFilter;
