import React from 'react';

function ButtonPrimary({
  title,
  type = 'button',
  cases = 'small',
  widthBtn = 'w-full',
  onClick,
  disabled,
}) {
  const sizeBtn = {
    small: 'py-1 text-sm rounded',
    medium: 'py-2 text-base rounded-lg',
    large: 'py-3 text-base rounded-lg',
  };
  return (
    <button
      type={type}
      onClick={onClick}
      className={`${sizeBtn[cases]} ${widthBtn} bg-ultramarine-blue from-egyptian-blue to-ultramarine-blue font-bold text-cultured hover:bg-gradient-to-r`}
      disabled={disabled}
    >
      {title}
    </button>
  );
}

export default ButtonPrimary;
