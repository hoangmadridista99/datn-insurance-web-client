import React from 'react';

export const TextFieldBlur = ({
  placeholder = 'Text holder',
  className,
  icon,
  error,
  disabled = false,
  value = '',
}) => {
  return (
    <div className="relative">
      <input
        type="text"
        placeholder={placeholder}
        className={` ${className} ${
          error ? 'style-error' : 'style-text-field-blur'
        }`}
        value={value}
        disabled={disabled}
      />
      {icon && (
        <div className={`absolute right-1 top-3 ${className}`}>{icon}</div>
      )}
      {error && <p className="text-vermilion">{error}</p>}
    </div>
  );
};
