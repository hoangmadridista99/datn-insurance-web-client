export const WindowScrollTop = (top) =>
  window?.scrollTo({
    top: top,
    left: 0,
    behavior: 'smooth',
  });
