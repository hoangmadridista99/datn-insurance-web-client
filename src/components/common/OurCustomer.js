import Image from 'next/image';
import React from 'react';
import formBot1 from '../../../public/images/formFilter/formBot1.png';
import formBot2 from '../../../public/images/formFilter/formBot2.png';
import formBot3 from '../../../public/images/formFilter/formBot3.png';

const OUR_CUSTOMER_LIST = [
  {
    id: 1,
    coreTitle: 'Best offer',
    titleDetail: 'Save up to 65% with our comparison',
    image: formBot1,
  },
  {
    id: 2,
    coreTitle: 'Competently',
    titleDetail: 'Individual advice from our experts',
    image: formBot2,
  },
  {
    id: 3,
    coreTitle: 'Comfortable',
    titleDetail: 'Conclude easily and paperless online',
    image: formBot3,
  },
];
export default function OurCustomer() {
  return (
    <div className="bg-maize py-6 sm:py-10 lg:py-20">
      <div className="comparison-container">
        <p className="mb-12 flex text-2xl font-bold text-raisin-black sm:mx-auto sm:w-fit md:text-left lg:justify-center lg:text-center">
          Our customer promise for term life insurence
        </p>
        <div className="mx-auto grid max-w-360 grid-cols-1 gap-10 sm:grid-cols-3 sm:gap-4 sm:px-2 lg:max-w-360 lg:gap-20 lg:px-0">
          {OUR_CUSTOMER_LIST.map((item) => (
            <div key={item.id} className="flex gap-3 lg:gap-4">
              <div className="h-12 w-12 sm:h-7 sm:w-7 lg:h-16 lg:w-16">
                <Image src={item.image} alt="formBot1" />
              </div>
              <div className="sm:w-4/5 lg:w-9/12">
                <p className="text-lg font-bold text-zinnwaldite-brown sm:text-xl">
                  {item.coreTitle}
                </p>
                <p className="text-sm font-normal text-dark-bronze sm:text-base">
                  {item.titleDetail}
                </p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
