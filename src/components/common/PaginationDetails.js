import React from 'react';
import { useTranslation } from 'next-i18next';

export default function PaginationDetails({
  currentPage,
  pageSize,
  totalItems,
}) {
  const { t } = useTranslation(['common']);
  return (
    <p className="pl-8">
      {t('from')} {(currentPage - 1) * pageSize + 1} -&nbsp;
      {totalItems < currentPage * pageSize
        ? totalItems
        : currentPage * pageSize}
      &nbsp;{t('on')}&nbsp;
      {totalItems}
      &nbsp;{t('searchResults')}
    </p>
  );
}
