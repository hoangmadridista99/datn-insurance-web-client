import { Fragment } from 'react';
import { Listbox, Transition } from '@headlessui/react';
import React from 'react';
import Image from 'next/image';

export default function Select({ options, selected, onChange }) {
  return (
    <div className="w-full bg-white">
      <Listbox value={selected.value} onChange={onChange}>
        <div className="relative">
          <Listbox.Button className="flex w-full justify-between rounded-md border border-chinese-silver px-4 py-2 text-xs sm:text-sm lg:text-base">
            <span
              className={`${!selected.value ? 'text-nickel' : 'text-arsenic '}`}
            >
              {selected.name}
            </span>
            <span className="pointer-events-none flex items-center">
              <Image
                src="/svg/arrow-down.svg"
                width={24}
                height={24}
                alt="arrowDown"
              />
            </span>
          </Listbox.Button>
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md border-platinum bg-white p-4 text-xs shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm lg:text-base">
              {options.map((person, personIdx) => (
                <Listbox.Option
                  key={personIdx}
                  className={({ active }) =>
                    `relative cursor-default select-none py-1 text-xs sm:text-sm lg:text-base ${
                      active
                        ? 'bg-lavender text-ultramarine-blue'
                        : 'text-nickel'
                    }`
                  }
                  value={person}
                >
                  {({ selected }) => (
                    <>
                      <span
                        className={`block truncate ${
                          selected ? 'font-medium' : 'font-normal'
                        }`}
                      >
                        {person.name}
                      </span>
                      {selected ? (
                        <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                          x
                        </span>
                      ) : null}
                    </>
                  )}
                </Listbox.Option>
              ))}
            </Listbox.Options>
          </Transition>
        </div>
      </Listbox>
    </div>
  );
}
