import React from 'react';

function ButtonBgWhite({
  title,
  disabled,
  type = 'button',
  cases = 'small',
  widthBtn = 'w-full',
  onClick,
}) {
  const sizeBtn = {
    small: 'py-1 text-sm rounded',
    medium: 'py-2 text-base rounded-lg',
    large: 'py-3 text-base rounded-lg',
  };
  return (
    <button
      type={type}
      onClick={onClick}
      disabled={disabled}
      className={`${sizeBtn[cases]} ${widthBtn} box-border border border-nickel font-bold text-nickel hover:border-ultramarine-blue hover:text-ultramarine-blue active:bg-fresh-air`}
    >
      {title}
    </button>
  );
}

export default ButtonBgWhite;
