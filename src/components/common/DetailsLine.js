import React from 'react';

export default function DetailsLine({ icon, toolTipContent, label, value }) {
  return (
    <div className="flex items-center gap-2">
      <div className="group relative h-6 w-6">
        <div className="cursor-pointer">{icon}</div>
        <div className="xl:w-100 w-75 absolute top-6 -left-20 z-50 hidden rounded-lg border bg-white p-2 text-sm font-bold text-nickel group-hover:block">
          {toolTipContent}
        </div>
      </div>

      <p className="w-full text-sm font-normal leading-5 text-nickel">
        <span className="mr-1">{label}</span>
        {value}
      </p>
    </div>
  );
}
