import { Fragment, useEffect, useState } from 'react';
import { Listbox, Transition } from '@headlessui/react';
import React from 'react';
import { IconDropDownListInsurance } from './icon/Icon';

export default function SelectFilter({
  options,
  selected,
  onChange,
  defaultValue,
  errors,
  placeholder,
}) {
  const [defaultName, setDefaultName] = useState();
  const CheckValueDefault = () => {
    options?.filter((values) => {
      if (values.value == defaultValue) {
        setDefaultName(values.name);
      }
    });
  };

  const checkValueSelect = () => {
    if (!selected?.value) return <>{placeholder}</>;
    return selected?.name;
  };

  useEffect(() => {
    CheckValueDefault();
  }, [defaultValue]);
  return (
    <div className="w-full">
      <Listbox value={selected?.value} onChange={onChange}>
        <div className="relative">
          <Listbox.Button
            className={`${
              errors ? 'border-red-500' : 'focus:border-ultramarine-blue'
            } flex w-full justify-between rounded-md border border-chinese-silver p-3 text-base`}
          >
            <span
              className={`${
                !selected?.value && !defaultValue
                  ? 'text-silver'
                  : 'text-arsenic'
              }`}
            >
              {defaultValue ? defaultName : checkValueSelect()}
            </span>
            <span className="">
              <IconDropDownListInsurance />
            </span>
          </Listbox.Button>
          <Transition
            as={Fragment}
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Listbox.Options className="absolute z-30 mt-1 max-h-60 w-full overflow-auto rounded-md border-platinum bg-white p-4 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none">
              {options?.map((person, personIdx) => (
                <Listbox.Option
                  key={personIdx}
                  className={({ active }) =>
                    `relative cursor-default select-none py-1 text-base ${
                      active
                        ? 'bg-lavender text-ultramarine-blue'
                        : 'text-nickel'
                    }`
                  }
                  value={person}
                >
                  {({ selected }) => (
                    <>
                      <span
                        className={`block truncate ${
                          selected ? 'font-medium' : 'font-normal'
                        }`}
                      >
                        {person.name}
                      </span>
                      {selected.value ? (
                        <span className="absolute inset-y-0 right-0 flex items-center pl-3 text-ultramarine-blue">
                          v
                        </span>
                      ) : null}
                    </>
                  )}
                </Listbox.Option>
              ))}
            </Listbox.Options>
          </Transition>
        </div>
      </Listbox>
    </div>
  );
}
