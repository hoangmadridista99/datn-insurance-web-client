import React from 'react';
import { IconClosePartnerInformation } from './icon/Icon';
import alertEdit from '../../../public/images/AlertEdit/AlertEdit.png';
import Image from 'next/image';
import ButtonBgWhite from './ButtonBgWhite';
import ButtonPrimary from './ButtonPrimary';
import { useTranslation } from 'next-i18next';
import { EDIT } from 'src/utils/constants';
import { useRouter } from 'next/router';
export default function AlertEdit({ valueEditInsurance, setToggleAlertEdit }) {
  const { t } = useTranslation(['common']);
  const router = useRouter();

  const queryAddStatus = { ...valueEditInsurance, status: EDIT };

  const handleEditInsuranceCarMaterial = () => {
    router.push({
      pathname: '/insuranceCarMaterial',
      query: queryAddStatus,
    });
  };

  return (
    <div className="relative mx-4 rounded-2xl bg-white sm:mx-7">
      <button
        onClick={() => setToggleAlertEdit(false)}
        className="absolute right-7 top-5 mx-4 cursor-pointer"
      >
        <IconClosePartnerInformation />
      </button>
      <div className="mx-auto p-5 sm:p-10">
        <div className="mb-6 flex justify-center">
          <Image src={alertEdit} alt="successTick" />
        </div>
        <p className=" mb-10 text-center text-sm font-normal text-nickel sm:text-base">
          Sau khi cập nhật chỉnh sửa, hệ thống sẽ tự động thiết lập lại trạng
          thái chờ thẩm định và cần phê duyệt lại. Bạn có chắc chắn muốn chỉnh
          sửa thông tin?
        </p>
        <div className="mx-auto flex items-start justify-between gap-4 sm:w-percent-77 sm:justify-center">
          <div className="w-percent-48 sm:h-10 sm:w-57">
            <ButtonBgWhite
              cases={'large'}
              title={t('close')}
              onClick={() => setToggleAlertEdit(false)}
            />
          </div>

          <div className="w-percent-48 sm:h-10 sm:w-57">
            <ButtonPrimary
              onClick={handleEditInsuranceCarMaterial}
              cases={'large'}
              title={t('edit')}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
