import LandingFooter from 'components/Footers/LandingFooter';
import Navbar from 'components/Navbar/AuthNavbar';
import React, { useState } from 'react';
import HeaderInsurance from './HeaderInsurance';
import InputSearch from './InputSearch';

function LayoutWithHeader({ children, backgroundFooter }) {
  const [hiddenInputSearch, setHiddenInputSearch] = useState(false);
  return (
    <div className="relative md:h-screen">
      <div className="fixed z-40 w-full bg-white">
        <Navbar
          hiddenInputSearch={hiddenInputSearch}
          setHiddenInputSearch={setHiddenInputSearch}
        />
        {hiddenInputSearch && (
          <div className="comparison-container z-20 my-4 h-full bg-white lg:hidden">
            <InputSearch />
          </div>
        )}
        <div className="hidden lg:block">
          <HeaderInsurance />
        </div>
      </div>
      <div className="pt-20 lg:pt-36">{children}</div>
      <LandingFooter backgroundFooter={backgroundFooter} />
    </div>
  );
}

export default LayoutWithHeader;
