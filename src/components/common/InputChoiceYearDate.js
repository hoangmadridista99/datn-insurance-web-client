import { Popover, Transition } from '@headlessui/react';
import YearDate from 'components/YearDate';
import Image from 'next/image';
import React, { Fragment } from 'react';
import { numberInputInvalidChars, onKeyDown } from 'src/utils/helper';

export default function InputChoiceYearDate({
  yearValue,
  register,
  errorValueNoNumber,
  errors,
  onChangeYearOfBirth,
  setValue,
  setYearValue,
  setError,
  setErrorValueNoNumber,
  title,
  classNameTitle = 'mb-4 text-base text-start font-bold text-arsenic',
  placeholder,
  id,
  listYearShow,
  setNumberYear,
  numberYear,
  numberYearMax,
}) {
  return (
    <div className="mb-8 w-full">
      <Popover className="relative">
        <>
          <h2 className={classNameTitle}>{title}</h2>
          <Popover.Button className="w-full">
            <div className="relative flex w-full justify-between">
              <input
                type="number"
                value={yearValue}
                onKeyDown={(e) => onKeyDown(e, numberInputInvalidChars)}
                placeholder={placeholder}
                id={id}
                readOnly
                {...register(id)}
                className={`w-full rounded-md border border-chinese-silver outline-none [appearance:textfield] hover:border-ultramarine-blue [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:appearance-none ${
                  errorValueNoNumber?.errorValueYearOfBirth ||
                  errors?.[id]?.message
                    ? 'border-red-500'
                    : 'focus:border-ultramarine-blue'
                } p-3`}
                onChange={onChangeYearOfBirth}
              />

              <div type="button" className="absolute top-3 right-3">
                <Image
                  src="/svg/calendar.svg"
                  width={20}
                  height={20}
                  alt="calendar"
                />
              </div>
            </div>
          </Popover.Button>
          <Transition
            as={Fragment}
            enter="transition ease-out duration-200"
            enterFrom="opacity-0 translate-y-1"
            enterTo="opacity-100 translate-y-0"
            leave="transition ease-in duration-150"
            leaveFrom="opacity-100 translate-y-0"
            leaveTo="opacity-0 translate-y-1"
          >
            <Popover.Panel className="absolute -right-34.25 z-10 mt-3 -translate-x-1/2 transform px-0 lg:max-w-3xl">
              <div className="overflow-hidden">
                <YearDate
                  numberYearMax={numberYearMax}
                  numberYear={numberYear}
                  setNumberYear={setNumberYear}
                  listYearShow={listYearShow}
                  setValue={setValue}
                  setYearValue={setYearValue}
                  yearValue={yearValue}
                  setError={setError}
                  id={id}
                  setErrorValueNoNumber={setErrorValueNoNumber}
                />
              </div>
            </Popover.Panel>
          </Transition>
          {errorValueNoNumber?.errorValueYearOfBirth && (
            <p className=" text-red-500 ">
              {errorValueNoNumber?.errorValueYearOfBirth}
            </p>
          )}
          {errors?.[id] && (
            <p className=" text-red-500 ">{errors?.[id]?.message}</p>
          )}
        </>
      </Popover>
    </div>
  );
}
