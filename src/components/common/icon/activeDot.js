import React from 'react';

export default function ActiveDot() {
  return (
    <svg width="11" height="12" viewBox="0 0 11 12" fill="none" xmlns="http://www.w3.org/2000/svg">
      <circle cx="5.85672" cy="6.00028" r="5.14286" fill="#008DFF" />
    </svg>

  );
}
