import React from 'react';

export default function ArrowRightSwiperJs({ color }) {
  return (
    <svg width="27" height="26" viewBox="0 0 27 26" fill={color} xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M15.664 14.0014H7.59375V11.9996H15.664L12.5688 8.70362L13.8981 7.28809L19.2625 13.0005L13.8981 18.7129L12.5688 17.2974L15.664 14.0014Z" fill={color} />
    </svg>

  );
}
