import React from 'react';

export default function ArrowLeftSwiperJs({ color }) {
  return (
    <svg width="27" height="26" viewBox="0 0 27 26" fill={color} xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M11.0774 12.0361H19.3566V13.9646H11.0774L14.2527 17.1399L12.889 18.5036L7.38574 13.0004L12.889 7.49707L14.2527 8.86078L11.0774 12.0361Z" fill={color} />
    </svg>

  );
}
