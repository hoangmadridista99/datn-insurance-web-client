import dayjs from 'dayjs';
import React from 'react';

export default function FormatDate(date) {
  if (!date) return;
  return <span>{dayjs(date).format('DD/MM/YYYY - HH:mm')}</span>;
}
