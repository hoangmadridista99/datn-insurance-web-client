import Image from 'next/image';
import React, { useState } from 'react';
import { useTranslation } from 'next-i18next';

export default function InputSearch() {
  const { t } = useTranslation(['auth', 'common']);

  const [isInputFocused, setIsInputFocused] = useState(false);

  return (
    <div>
      <div
        className={`flex w-full gap-2 rounded-md border p-2 ${
          isInputFocused ? 'border-ultramarine-blue' : 'border-chinese-silver'
        }`}
      >
        <button type="button">
          <Image
            src="/images/navbar/search.png"
            alt="search"
            width={20}
            height={20}
            priority="true"
          />
        </button>
        <input
          type="text"
          placeholder={t('auth:Enter_insurance_name')}
          className="w-full outline-none"
          onFocus={() => setIsInputFocused(true)}
          onBlur={() => setIsInputFocused(false)}
        />
      </div>
    </div>
  );
}
