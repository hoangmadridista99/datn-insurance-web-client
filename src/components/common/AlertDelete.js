import React from 'react';
import { IconClosePartnerInformation } from './icon/Icon';
import alertDelete from '../../../public/images/AlertRegisterCancer/alertDelete.png';
import Image from 'next/image';
import ButtonBgWhite from './ButtonBgWhite';
import ButtonPrimary from './ButtonPrimary';
import { useTranslation } from 'next-i18next';
export default function AlertDelete({
  handleCancerRegister,
  handleDeleteInsurance,
  idInsurance,
}) {
  const { t } = useTranslation(['common']);
  const handleDelete = () => {
    handleCancerRegister(false);
    handleDeleteInsurance(idInsurance);
  };
  return (
    <div className="relative mx-4 rounded-2xl bg-white sm:mx-7">
      <button
        className="absolute right-7 top-5 mx-4 cursor-pointer"
        onClick={() => handleCancerRegister(false)}
      >
        <IconClosePartnerInformation />
      </button>
      <div className="mx-auto p-5 sm:p-10">
        <div className="mb-6 flex justify-center">
          <Image src={alertDelete} alt="successTick" />
        </div>
        <p className=" mb-10 text-center text-sm font-normal text-nickel sm:text-base">
          {t('pendingRegisterCancer')}
        </p>
        <div className="mx-auto flex items-start justify-between gap-4 sm:w-percent-77 sm:justify-center">
          <div className="w-percent-48 sm:h-10 sm:w-57">
            <ButtonBgWhite
              cases={'large'}
              title={t('close')}
              onClick={() => handleCancerRegister(false)}
            />
          </div>

          <div className="w-percent-48 sm:h-10 sm:w-57">
            <ButtonPrimary
              cases={'large'}
              title={t('cancerRegister')}
              onClick={handleDelete}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
