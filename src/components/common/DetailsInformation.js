import React, { useState } from 'react';
import { IconDropDownListInsurance } from './icon/Icon';

export default function DetailsInformation({ title, details, switchBtn }) {
  const [hiddenDetails, setHiddenDetails] = useState(false);

  const handleHiddenDetails = () => {
    setHiddenDetails(!hiddenDetails);
  };
  return (
    <div className="mb-8 rounded-lg border border-platinum p-4 lg:mb-10 lg:p-8">
      <div
        className={`${
          !hiddenDetails && 'mb-8 lg:mb-10'
        } flex items-start justify-between gap-2 lg:items-center`}
      >
        <div className="mb-2 flex items-center gap-3 text-2xl font-bold text-arsenic lg:text-pixel-32 lg:leading-pixel-48">
          {title}
          {switchBtn}
        </div>
        <button
          type="button"
          onClick={handleHiddenDetails}
          className={`${hiddenDetails && 'rotate-180'} duration-200 `}
        >
          <IconDropDownListInsurance />
        </button>
      </div>
      {!hiddenDetails && <div>{details}</div>}
    </div>
  );
}
