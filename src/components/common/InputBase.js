import React from 'react';
import { onKeyDown, onKeyDownForTax } from 'src/utils/helper';

export default function InputBase({
  label,
  placeholder,
  register,
  id,
  className,
  changeLabel,
  errors,
  valueSwitch = null,
  hiddenSwitch,
  type = 'text',
  numberInputInvalidChars = [],
  onChange = null,
  min,
  max,
  onKeyDownTax,
}) {
  const typeInput = () => {
    if (hiddenSwitch) {
      return (
        <input
          type={type}
          className={`w-full rounded-md border p-3 [appearance:textfield] hover:border-ultramarine-blue md:text-base [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:appearance-none ${
            valueSwitch
              ? 'border-platinum bg-cultured text-platinum'
              : 'border-chinese-silver bg-white'
          } `}
          placeholder={placeholder}
          id={id}
          disabled={valueSwitch}
          onKeyDown={(e) => {
            onKeyDownTax
              ? onKeyDownForTax(e)
              : onKeyDown(e, numberInputInvalidChars);
          }}
          onChange={onChange}
          {...register(id)}
          min={min}
          max={max}
          autoComplete="off"
        />
      );
    }
    if (onChange) {
      return (
        <input
          type={type}
          className={`[appearance:textfield] hover:border-ultramarine-blue md:text-base [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:appearance-none
          ${errors?.[id] ? 'style-error' : 'style-text-field-blur'}
          `}
          placeholder={placeholder}
          id={id}
          onKeyDown={(e) => onKeyDown(e, numberInputInvalidChars)}
          onChange={onChange}
          {...register(id)}
          min={min}
          max={max}
          autoComplete="off"
        />
      );
    }
    return (
      <input
        type={type}
        className={`[appearance:textfield] hover:border-ultramarine-blue md:text-base [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:appearance-none
          ${errors?.[id] ? 'style-error' : 'style-text-field-blur'}
          `}
        placeholder={placeholder}
        id={id}
        onKeyDown={(e) => onKeyDown(e, numberInputInvalidChars)}
        {...register(id)}
        min={min}
        max={max}
        autoComplete="off"
      />
    );
  };

  return (
    <div className={className}>
      <div className="mb-1 flex justify-between text-sm font-bold text-nickel lg:text-base">
        {label}
        <div>{changeLabel}</div>
      </div>
      <>{typeInput()}</>
      {errors?.[id] && (
        <p className=" text-red-500 ">{errors?.[id]?.message}</p>
      )}
    </div>
  );
}
