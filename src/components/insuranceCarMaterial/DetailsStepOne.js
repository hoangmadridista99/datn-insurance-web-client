import DetailsInformation from 'components/common/DetailsInformation';
import InputBase from 'components/common/InputBase';
import { IconTickQuestion } from 'components/common/icon/Icon';
import React, { useState } from 'react';
import SelectFilter from 'components/common/SelectFilter';
import TitleBase from './TitleBase';
import InputChoiceYearDate from 'components/common/InputChoiceYearDate';
import { useTranslation } from 'next-i18next';
import {
  LIST_BENEFIT_ADVANCED,
  LIST_BENEFIT_INSURANCE_MATERIAL,
  LIST_TYPE_INSURANCE,
  STEP,
  TARGET_USE_BUSINESS,
  USE_BUSINESS,
} from 'src/utils/constants';
import { numberInputInvalidChars, regexCarLicense } from 'src/utils/helper';

const yearProduction = 1999;
const currentYear = new Date().getFullYear();

export default function DetailsStepOne({
  register,
  setValue,
  setError,
  errors,
  defaultValueTargetUseBusiness,
}) {
  const { t } = useTranslation(['common']);

  const [hiddenSelectTargetUse, setHiddenSelectTargetUse] = useState(true);
  const [valueTargetUseBusiness, setValueTargetUseBusiness] = useState();
  const [errorValueNoNumber, setErrorValueNoNumber] = useState([]);
  const [yearValue, setYearValue] = useState();

  const handleChoseUseBusiness = (value) => {
    setValueTargetUseBusiness(value);
    setValue('purpose', value.value);
    setError('purpose', { message: '' });
  };

  const onChangeYearOfBirth = (event) => {
    const {
      target: { value },
    } = event;

    if (value?.length === 0) return setYearValue(0);

    setYearValue(parseInt(value));

    if (value > currentYear || value < yearProduction) {
      return setErrorValueNoNumber({
        errorValueYearOfBirth: t('please_year_production'),
      });
    }
    setError('year_of_production', { message: '' });
    setErrorValueNoNumber({ errorValueYearOfBirth: '' });
  };

  const handleSeatCar = (value) => {
    if (value.target.value <= 1 || value.target.value > 30) {
      setError('seating_capacity', {
        message: t('please_seating_capacity'),
      });
      return;
    }
    setError('seating_capacity', {
      message: '',
    });
  };

  const handleCarLicense = (value) => {
    const checkCarLicense = regexCarLicense.test(value.target.value);

    if (!value.target.value || checkCarLicense) {
      setError('car_license_plate', {
        message: '',
      });
      return;
    }
    if (!checkCarLicense) {
      setError('car_license_plate', {
        message: t('please_registration_car'),
      });
      return;
    }
  };

  const informationCar = () => {
    const toggleSelectTargetUse = (value) => {
      setHiddenSelectTargetUse(value);
      setError('purpose', { message: '' });
    };
    const currentYear = new Date().getFullYear();

    const [numberYear, setNumberYear] = useState(0);

    const listYearOfBirth = [
      ...Array.from(
        { length: currentYear - 1999 },
        (_, i) => currentYear - i
      ).reverse(),
    ];

    const listYearShow = listYearOfBirth?.slice(numberYear, numberYear + STEP);

    return (
      <>
        <div className="mb-4 flex gap-2 text-sm font-bold text-nickel lg:text-base">
          {t('type_current')}
          <div className="group relative cursor-pointer">
            <IconTickQuestion />
            <div className="absolute -left-48 top-6 z-50 hidden w-75 rounded-lg border bg-white p-2 text-sm group-hover:block lg:-top-7 lg:left-10 xl:w-90">
              <p className="font-bold text-arsenic">
                {t('type_current_insurance')}
              </p>
              <p className="font-normal text-nickel">{t('rules_insurance')}</p>
            </div>
          </div>
        </div>

        <div className="mb-4">
          {LIST_TYPE_INSURANCE(t).map((type) => (
            <div key={type.value} className="mt-4 flex gap-4">
              <input
                type="radio"
                id={type?.value}
                value={type?.value}
                className="h-5 w-5"
                {...register('form_type')}
              />
              <label
                className="text-sm font-normal text-arsenic lg:text-base"
                htmlFor={type?.value}
              >
                {type?.label}
              </label>
            </div>
          ))}
          {errors?.form_type && (
            <p className="text-red-500">{errors?.form_type?.message}</p>
          )}
        </div>

        <div className="mb-4 text-sm font-bold text-nickel lg:text-base">
          {t('your_use_business')}
        </div>
        {USE_BUSINESS(t).map((type) => (
          <div key={type.value} className="mb-4 flex gap-4">
            <input
              type="radio"
              id="is_transportation_business"
              value={type?.value}
              defaultChecked={type.value}
              className="h-5 w-5"
              onClick={() => toggleSelectTargetUse(type.value)}
              {...register('is_transportation_business')}
            />
            <p className="text-sm font-normal text-arsenic lg:text-base">
              {type?.label}
            </p>
          </div>
        ))}
        {hiddenSelectTargetUse && (
          <div className="">
            <SelectFilter
              errors={errors?.purpose?.message}
              defaultValue={defaultValueTargetUseBusiness}
              options={TARGET_USE_BUSINESS(t)}
              selected={valueTargetUseBusiness}
              onChange={handleChoseUseBusiness}
              placeholder={t('target_use')}
            />
          </div>
        )}
        {errors?.purpose && (
          <p className="text-red-500 ">{errors?.purpose?.message}</p>
        )}

        <TitleBase title={t('information_detail')} />
        <div className="mb-4 gap-4 lg:flex">
          <InputBase
            className="mb-4 w-full lg:mb-0"
            label={t('car_license_plate')}
            placeholder={'Định dạng: 12A-123.45'}
            // placeholder={t('please_car')}
            register={register}
            id="car_license_plate"
            errors={errors}
            onChange={handleCarLicense}
          />

          <InputBase
            type="number"
            className="mb-4 w-full lg:mb-0"
            label={t('number_seats')}
            placeholder={t('please_number_seats')}
            register={register}
            id="seating_capacity"
            errors={errors}
            onChange={handleSeatCar}
            numberInputInvalidChars={numberInputInvalidChars}
          />
        </div>
        <div className="mb-4 gap-4 lg:flex">
          <InputBase
            className="mb-4 w-full lg:mb-0"
            label={t('mall')}
            placeholder={t('please_mall')}
            register={register}
            id="car_brand"
            errors={errors}
          />
          <InputBase
            className="mb-4 w-full lg:mb-0"
            label={t('car_model')}
            placeholder={t('please_car_model')}
            register={register}
            id="car_model"
            errors={errors}
          />
        </div>
        <div className="mb-4 gap-4 lg:flex">
          <InputBase
            className="mb-4 w-full lg:mb-0"
            label={t('car_version')}
            placeholder={t('please_car_version')}
            register={register}
            id="car_version"
            errors={errors}
          />
          <div className="mb-4 w-full lg:mb-0">
            <InputChoiceYearDate
              yearValue={yearValue}
              register={register}
              errorValueNoNumber={errorValueNoNumber}
              errors={errors}
              onChangeYearOfBirth={onChangeYearOfBirth}
              setValue={setValue}
              setYearValue={setYearValue}
              setError={setError}
              setErrorValueNoNumber={setErrorValueNoNumber}
              title={t('year_of_production')}
              classNameTitle="mb-1 flex justify-between text-sm font-bold text-nickel lg:text-base"
              placeholder={t('please_year_of_production')}
              id="year_of_production"
              listYearShow={listYearShow}
              setNumberYear={setNumberYear}
              numberYear={numberYear}
              numberYearMax={currentYear - 2000}
            />
          </div>
        </div>
      </>
    );
  };

  const benefitInsurance = () => {
    return (
      <>
        <TitleBase title={t('benefit_base')} />
        <div className="mb-10">
          {LIST_BENEFIT_INSURANCE_MATERIAL(t).map((list) => (
            <div
              key={list?.value}
              className="mb-4 mt-2 flex items-center gap-4"
            >
              <input
                type="checkbox"
                className="h-6 w-6 hover:cursor-pointer"
                defaultValue={true}
                defaultChecked={true}
                disabled
              />
              <p className="w-full text-base font-normal text-arsenic">
                {list?.label}
              </p>
            </div>
          ))}
        </div>

        <TitleBase title={t('benefit_hard')} />
        <div className="mb-10">
          {LIST_BENEFIT_ADVANCED(t).map((list) => (
            <div
              key={list?.value}
              className="mb-4 mt-2 flex items-center gap-4"
            >
              <input
                type="checkbox"
                id={list?.value}
                className="h-6 w-6 hover:cursor-pointer"
                {...register(list.value)}
              />
              <p className="w-full text-base font-normal text-arsenic">
                {list?.label}
              </p>
            </div>
          ))}
        </div>

        <TitleBase title={t('benefit_add')} />
        <div className="flex items-center gap-4">
          <input
            type="checkbox"
            id="is_insured_for_each_person"
            className="h-6 w-6 hover:cursor-pointer"
            {...register('is_insured_for_each_person')}
          />
          <p className="w-full text-base font-normal text-arsenic">
            {t('is_insured_for_each_person')}
          </p>
        </div>
      </>
    );
  };

  return (
    <div>
      <DetailsInformation
        title={t('information_by_car')}
        details={informationCar()}
      />
      <DetailsInformation
        title={t('benefit_insurance')}
        details={benefitInsurance()}
      />
    </div>
  );
}
