import React from 'react';
import TitleBase from './TitleBase';
import { Controller } from 'react-hook-form';
import DatePicker from 'react-date-picker';
import { CalendarIcon } from 'src/utils/constants';
import InputBase from 'components/common/InputBase';
import { useTranslation } from 'next-i18next';
import {
  numberInputInvalidChars,
  regexEmail,
  regexPhoneNumber,
} from 'src/utils/helper';

export default function InformationInsuranceNew({
  control,
  register,
  errors,
  setError,
  timeNew,
  setTimeDeadlineForDeal,
  information,
  newDateCurrentCertificateExpiration,
  newYearCurrentCertificateExpiration,
}) {
  const { t } = useTranslation(['auth']);

  const onChangePhoneNumberPolicyholder = (value) => {
    const checkPhoneNumber = regexPhoneNumber.test(value.target.value);
    if (!checkPhoneNumber) {
      setError('policyholder_phone_number', {
        message: t('schema.phone_matches'),
      });
      return;
    }
    setError('policyholder_phone_number', {
      message: '',
    });
  };

  const onChangeEmailPolicyholder = (value) => {
    const checkEmail = regexEmail.test(value.target.value);
    if (!checkEmail) {
      setError('policyholder_email', {
        message: t('schema.email'),
      });
      return;
    }
    setError('policyholder_email', {
      message: '',
    });
  };
  const handleTimeValueFrom = (e, field) => {
    field.onChange(e.toISOString());
  };

  return (
    <>
      <TitleBase
        title={t('deal_insurance_year')}
        description={t('description_deal_insurance_year')}
      />
      <div className="gap-4 lg:flex">
        <div className="mb-4 w-full lg:mb-0">
          <p className="mb-1 flex justify-between text-sm font-bold text-nickel lg:text-base">
            {t('start')}
          </p>
          <div
            className={`rounded-md border p-2 md:p-3 ${
              information?.form_type === 'unexpired'
                ? 'border-platinum bg-anti_flash_white'
                : ''
            }`}
          >
            <Controller
              control={control}
              name="insurance_period_from"
              render={({ field }) => (
                <DatePicker
                  id="insurance_period_from"
                  name="insurance_period_from"
                  onChange={(e) => handleTimeValueFrom(e, field)}
                  monthPlaceholder="MM"
                  dayPlaceholder="DD"
                  yearPlaceholder="YYYY"
                  onClick={setTimeDeadlineForDeal(field.value)}
                  value={
                    information?.form_type === 'unexpired'
                      ? newDateCurrentCertificateExpiration
                      : field?.value
                  }
                  disabled={information?.form_type === 'unexpired'}
                  clearIcon={null}
                  className="h-6 w-full"
                  calendarIcon={CalendarIcon}
                  minDate={new Date()}
                />
              )}
              {...register('insurance_period_from')}
            />
          </div>
          {errors?.insurance_period_from && (
            <p className=" text-red-500 ">
              {errors?.insurance_period_from?.message}
            </p>
          )}
        </div>
        <div className="w-full">
          <p className="mb-1 flex justify-between text-sm font-bold text-nickel lg:text-base">
            {t('end')}
          </p>
          <div className="rounded-md border border-platinum bg-anti_flash_white p-2 md:p-3">
            <Controller
              control={control}
              name="insurance_period_to"
              render={({ field }) => (
                <DatePicker
                  // id="insurance_period_to"
                  name="insurance_period_to"
                  onChange={(e) => field.onChange(e)}
                  monthPlaceholder="MM"
                  dayPlaceholder="DD"
                  yearPlaceholder="YYYY"
                  value={
                    information?.form_type === 'unexpired'
                      ? newYearCurrentCertificateExpiration
                      : timeNew
                  }
                  clearIcon={null}
                  disabled
                  className="h-6 w-full"
                  calendarIcon={CalendarIcon}
                />
              )}
            />
          </div>
        </div>
      </div>

      <TitleBase title={t('policyholder')} />
      <InputBase
        className="mb-4"
        label={t('fullName')}
        placeholder={t('please_fullName')}
        register={register}
        id="policyholder_full_name"
        errors={errors}
      />
      <InputBase
        className="mb-4"
        label={t('address')}
        placeholder={t('schema.address_required')}
        register={register}
        id="policyholder_address"
        errors={errors}
      />
      <InputBase
        className="mb-4"
        label={t('phone')}
        placeholder={t('please_phone')}
        register={register}
        id="policyholder_phone_number"
        errors={errors}
        type="number"
        numberInputInvalidChars={numberInputInvalidChars}
        onChange={onChangePhoneNumberPolicyholder}
      />
      <InputBase
        className="mb-1"
        label={t('email')}
        placeholder={t('please_email')}
        register={register}
        id="policyholder_email"
        errors={errors}
        onChange={onChangeEmailPolicyholder}
      />
      <p className="text-sm font-normal text-nickel">{t('send_email')}</p>
    </>
  );
}
