import React from 'react';

export default function TitleBase({ title, description }) {
  return (
    <div className="mb-6 border-b border-platinum py-3 text-xl font-bold text-arsenic">
      <p>{title}</p>
      {description && (
        <p className="mt-2 text-base font-normal text-nickel">{description}</p>
      )}
    </div>
  );
}
