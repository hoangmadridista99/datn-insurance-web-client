export default function checkDefaultValues({ values }) {
  const defaultValues = {
    bank_branch: values.bank_branch,
    bank_name: values.bank_name,
    branch_address: values.branch_address,
    car_brand: values.car_brand,
    car_license_plate: values.car_license_plate,
    car_model: values.car_model,
    car_version: values.car_version,
    form_type: values.form_type,
    insurance_period_from: values.insurance_period_from,
    insurance_period_to: values.insurance_period_to,
    invoice_address: values.invoice_address,
    invoice_email: values.invoice_email,
    invoice_name: values.invoice_name,
    invoice_tax_id: values.invoice_tax_id,
    invoice_type: values.invoice_type,
    is_choosing_service_center: JSON.parse(values.is_choosing_service_center),
    is_component_vehicle_theft: JSON.parse(values.is_component_vehicle_theft),
    is_insured_for_each_person: JSON.parse(values.is_insured_for_each_person),
    is_no_depreciation_cost: JSON.parse(values.is_no_depreciation_cost),
    is_transportation_business: values.is_transportation_business,
    is_water_damage: JSON.parse(values.is_water_damage),
    policyholder_address: values.policyholder_address,
    policyholder_email: values.policyholder_email,
    policyholder_full_name: values.policyholder_full_name,
    policyholder_phone_number: values.policyholder_phone_number,
    purpose: values.purpose,
    seating_capacity: values.seating_capacity,
    staff_full_name: values.staff_full_name,
    staff_other_requirements: values.staff_other_requirements,
    staff_phone_number: values.staff_phone_number,
    year_of_production: values.year_of_production,
  };
  if (values.form_type === 'unexpired')
    return {
      ...defaultValues,
      certificate_expiration_date: values.certificate_expiration_date,
    };

  return defaultValues;
}
