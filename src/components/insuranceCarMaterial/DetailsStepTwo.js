/* eslint-disable indent */
import React, { useRef, useState } from 'react';
import DetailsInformation from 'components/common/DetailsInformation';
import { Controller } from 'react-hook-form';
import DatePicker from 'react-date-picker';
import { CalendarIcon, REQUIRE_UPLOAD_IMAGES } from 'src/utils/constants';
import 'react-date-picker/dist/DatePicker.css';
import 'react-calendar/dist/Calendar.css';
import { toast } from 'react-toastify';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import Invoice from './Invoice';
import BankBeneficiaryCar from './BankBeneficiaryCar';
import { MotionUploadImage } from './MotionUploadImage';
import InformationInsuranceNew from './InformationInsuranceNew';
import BorderUploadImage from './BorderUploadImage';
import ImageAfterUpload from './ImageAfterUpload';
import numeral from 'numeral';
import { IconTakePhoto, IconUploadImage } from 'components/common/icon/Icon';
import ButtonSwitchInvoice from './ButtonSwitchInvoice';
import ButtonSwitchBankBenefit from './ButtonSwitchBankBenefit';

export default function DetailsStepTwo({
  register,
  control,
  setValue,
  information,
  errors,
  setError,
  getValues,
  setFileImage,
  fileImage,
  defaultImage,
  defaultImageMultiple,
  setUpdateImageUrl,
  updateImageUrl,
  defaultInvoiceType,
  valueLabelInvoice,
  setValueLabelInvoice,
  valueSwitchInvoice,
  setValueSwitchInvoice,
  valueSwitchBankBenefit,
  setValueSwitchBankBenefit,
  resetField,
  timeNew,
  setTimeDeadlineForDeal,
  newDateCurrentCertificateExpiration,
  setTimeCertificateExpirationDate,
  newYearCurrentCertificateExpiration,
}) {
  const { t } = useTranslation(['auth']);
  const inputFileRef = useRef(null);

  const [valueDefaultImageMultiple, setValueDefaultImageMultiple] =
    useState(defaultImageMultiple);
  const [valueDefaultImage, setValueDefaultImage] = useState(defaultImage);
  const [hiddenUploadImage, setHiddenUploadImage] = useState(true);
  const [dataFileImage, setDataFileImage] = useState();
  const [fileImageMultiple, setFileImageMultiple] = useState([]);

  const handlePostImage = (event) => {
    const maxSize = 1024 * 1024 * 50;
    const file = event?.target?.files[0];

    if (file?.size > maxSize) {
      return toast.error(t('auth:photo_max'));
    }

    const data = {
      name: file.name,
      size: file.size,
      urlImage: URL.createObjectURL(file),
    };

    setFileImage(data);
    setHiddenUploadImage(true);
    setDataFileImage(file);
    setValue('files', file);
    setError('files', { message: '' });
  };

  const handlePatchImage = (event) => {
    const file = event?.target?.files[0];
    const data = URL.createObjectURL(file);

    setValueDefaultImage(data);
    setUpdateImageUrl([defaultImage]);
    setValue('files', [file]);
  };

  const handlePostImageMultiple = (event) => {
    // const maxSize = 1024 * 1024 * 50;
    const files = event?.target.files;

    const fileList = getValues('files');

    const checkLength = [...(fileList ?? []), ...Object.values(files)];
    if (checkLength?.length > 5) {
      setError('files', { message: t('please_choice_right_quantity') });
      return;
    }

    Object.values(files).forEach((item) => {
      const data = {
        name: item.name,
        size: item.size,
        urlImage: URL.createObjectURL(item),
      };

      setFileImageMultiple((preState) => [...preState, data]);

      setDataFileImage(files);
      setHiddenUploadImage(true);
      setError('files', { message: '' });
    });

    setValue('files', [...(fileList ?? []), ...Object.values(files)]);
  };

  const handleChangeImageMultiple = (event, index) => {
    const files = event?.target.files[0];
    const fileList = getValues('files');

    const data = {
      name: files.name,
      size: files.size,
      urlImage: URL.createObjectURL(files),
    };
    const updateFilesValue = fileList.map((item, i) =>
      index === i ? files : item
    );

    const updateFileImageMultiple = fileImageMultiple.map((item, i) =>
      index === i ? data : item
    );
    setValue('files', updateFilesValue);

    setFileImageMultiple(updateFileImageMultiple);
  };

  const handlePatchImageMultiple = (event, index, images) => {
    const files = event?.target.files[0];

    const fileList = getValues('files');

    const data = URL.createObjectURL(files);

    const updateValueDefaultImageMultiple = valueDefaultImageMultiple.map(
      (item, i) => (index === i ? data : item)
    );
    setUpdateImageUrl([...updateImageUrl, images]);
    setValueDefaultImageMultiple(updateValueDefaultImageMultiple);
    setValue('files', [...fileList, files]);
  };

  const handleCloseImage = () => {
    setFileImage(null);
    setFileImageMultiple([]);
  };

  const loadingUploadImage = ({ nameImage, sizeImage }) => {
    return (
      <div className="flex justify-between gap-2 rounded-lg border border-platinum p-4">
        <div className="h-10 w-10">
          <Image
            width={40}
            height={40}
            src={'/images/noImage.png'}
            alt="noImages"
          />
        </div>
        <div className="w-full">
          <div className="mb-2 flex justify-between">
            <div>
              <p className="text-base font-normal text-arsenic">{nameImage}</p>
              <p className="text-sm font-normal text-spanish-gray">
                {sizeImage}KB
              </p>
            </div>
            <button type="button" onClick={handleCloseImage}>
              <div className="h-6 w-6">
                <Image
                  width={24}
                  height={24}
                  src={'/images/ic_round-close.png'}
                  alt="noImages"
                />
              </div>
            </button>
          </div>
          <div className="w-full">
            <MotionUploadImage setHiddenUploadImage={setHiddenUploadImage} />
          </div>
        </div>
      </div>
    );
  };

  const appraisalInformation = () => {
    const handleTimeValue = (e, field) => {
      field.onChange(e.toISOString());
    };
    return (
      <>
        <div className="mb-4 w-full">
          <p className="mb-1 flex justify-between text-sm font-bold text-nickel lg:text-base">
            {t('deal_GCN')}
          </p>
          <div className="rounded-md border p-2 md:p-3">
            <Controller
              control={control}
              name="certificate_expiration_date"
              render={({ field }) => {
                return (
                  <DatePicker
                    id="certificate_expiration_date"
                    name="certificate_expiration_date"
                    onChange={(e) => handleTimeValue(e, field)}
                    monthPlaceholder="MM"
                    dayPlaceholder="DD"
                    yearPlaceholder="YYYY"
                    value={field.value}
                    clearIcon={null}
                    onClick={setTimeCertificateExpirationDate(field.value)}
                    className="h-6 w-full"
                    calendarIcon={CalendarIcon}
                    minDate={new Date()}
                  />
                );
              }}
              {...register('certificate_expiration_date')}
            />
          </div>
          {errors?.certificate_expiration_date && (
            <p className=" text-red-500 ">
              {errors?.certificate_expiration_date?.message}
            </p>
          )}
        </div>

        <p className="mb-4 flex justify-between text-sm font-bold text-nickel lg:text-base">
          {t('upload_image_GCN')}
        </p>
        <div className="mb-4 flex w-full gap-3 lg:hidden">
          <button
            type="button"
            className="flex w-full items-center justify-center gap-2 rounded border border-platinum py-2"
            onClick={() => inputFileRef?.current?.click()}
          >
            <input
              ref={inputFileRef}
              type="file"
              className="hidden"
              accept="image/*"
              capture="camera"
            />
            <IconTakePhoto />
            <p className="text-sm font-normal text-nickel sm:text-base">
              {t('take_photo')}
            </p>
          </button>

          <button
            type="button"
            className="flex w-full items-center justify-center gap-2 rounded border border-platinum py-2"
            onClick={() => inputFileRef?.current?.click()}
          >
            <input
              ref={inputFileRef}
              type="file"
              id="certificate_image_url"
              className="hidden"
              onChange={handlePostImage}
              accept=".jpg, .jpeg, .png"
            />
            <IconUploadImage />
            <p className="text-sm font-normal text-nickel sm:text-base">
              {t('up_image')}
            </p>
          </button>
        </div>
        {fileImage || valueDefaultImage ? (
          (fileImage && (
            <>
              {hiddenUploadImage ? (
                <div>
                  {loadingUploadImage({
                    nameImage: dataFileImage?.name,
                    sizeImage: numeral(dataFileImage?.size / 1024).format(0, 0),
                  })}
                </div>
              ) : (
                <>
                  <ImageAfterUpload
                    onChange={handlePostImage}
                    multiple={false}
                    dataImage={fileImage?.urlImage}
                    data={fileImage}
                  />
                </>
              )}
            </>
          )) ||
          (valueDefaultImage && (
            <>
              <ImageAfterUpload
                onChange={handlePatchImage}
                multiple={false}
                dataImage={valueDefaultImage}
              />
            </>
          ))
        ) : (
          <div>
            <BorderUploadImage onChange={handlePostImage} multiple={false} />
          </div>
        )}
        {errors?.files && (
          <p className=" text-red-500 ">{errors?.files?.message}</p>
        )}
      </>
    );
  };

  const appraisalInformationCar = () => {
    return (
      <>
        <div className="mb-10 text-base text-nickel">
          <ul className="mb-2">{t('please_upload_by_request')}</ul>
          {REQUIRE_UPLOAD_IMAGES(t).map((image) => (
            <li className="ml-3" key={image?.id}>
              {image.label}
            </li>
          ))}
        </div>
        <div>
          <p className="mb-1 flex justify-between text-sm font-bold text-nickel lg:text-base">
            {t('upload_image_car')}
          </p>
        </div>

        {!valueDefaultImageMultiple ? (
          fileImageMultiple?.length !== 5 && (
            <>
              <BorderUploadImage
                onChange={handlePostImageMultiple}
                multiple={true}
              />
            </>
          )
        ) : (
          <>
            {valueDefaultImageMultiple?.map((images, index) => {
              return (
                <div className="mt-6" key={index}>
                  <ImageAfterUpload
                    onChange={(event) =>
                      handlePatchImageMultiple(event, index, images)
                    }
                    multiple={true}
                    dataImage={images}
                  />
                </div>
              );
            })}
          </>
        )}

        {fileImageMultiple && (
          <>
            {hiddenUploadImage
              ? fileImageMultiple?.map((images) => (
                  <div key={images.name}>
                    {loadingUploadImage({
                      nameImage: images.name,
                      sizeImage: images.size / 1000,
                    })}
                  </div>
                ))
              : fileImageMultiple?.map((images, index) => {
                  return (
                    <div className="mt-6" key={images.name}>
                      <ImageAfterUpload
                        onChange={(event) =>
                          handleChangeImageMultiple(event, index)
                        }
                        data={images}
                        dataImage={images.urlImage}
                      />
                    </div>
                  );
                })}
          </>
        )}

        {errors?.files && (
          <p className=" text-red-500 ">{errors?.files?.message}</p>
        )}
      </>
    );
  };

  return (
    <>
      {information?.form_type === 'unexpired' ? (
        <DetailsInformation
          title={t('information_appraisal')}
          details={appraisalInformation()}
        />
      ) : (
        <DetailsInformation
          title={t('information_appraisal_car')}
          details={appraisalInformationCar()}
        />
      )}

      <DetailsInformation
        title={t('information_insurance_new')}
        details={
          <InformationInsuranceNew
            newYearCurrentCertificateExpiration={
              newYearCurrentCertificateExpiration
            }
            newDateCurrentCertificateExpiration={
              newDateCurrentCertificateExpiration
            }
            information={information}
            timeNew={timeNew}
            setTimeDeadlineForDeal={setTimeDeadlineForDeal}
            control={control}
            register={register}
            errors={errors}
            setError={setError}
          />
        }
      />
      <DetailsInformation
        title={t('benefit_bank_car')}
        details={
          <BankBeneficiaryCar
            setError={setError}
            errors={errors}
            register={register}
            valueSwitch={valueSwitchBankBenefit}
          />
        }
        switchBtn={
          <ButtonSwitchBankBenefit
            resetField={resetField}
            setError={setError}
            setValueSwitch={setValueSwitchBankBenefit}
            valueSwitch={valueSwitchBankBenefit}
          />
        }
      />
      <DetailsInformation
        title={t('invoice')}
        details={
          <Invoice
            valueLabelInvoice={valueLabelInvoice}
            setValueLabelInvoice={setValueLabelInvoice}
            defaultInvoiceType={defaultInvoiceType}
            errors={errors}
            setError={setError}
            register={register}
            valueSwitch={valueSwitchInvoice}
          />
        }
        switchBtn={
          <ButtonSwitchInvoice
            resetField={resetField}
            setError={setError}
            setValueSwitch={setValueSwitchInvoice}
            valueSwitch={valueSwitchInvoice}
          />
        }
      />
    </>
  );
}
