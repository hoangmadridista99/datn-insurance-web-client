import React from 'react';
import TitleBase from './TitleBase';
import { useTranslation } from 'next-i18next';
import InputBase from 'components/common/InputBase';
import { numberInputInvalidChars } from 'src/utils/helper';

export default function BankBeneficiaryCar({ register, valueSwitch, errors }) {
  const { t } = useTranslation(['common', 'auth']);

  return (
    <>
      <TitleBase title={t('information_bank')} />
      <InputBase
        valueSwitch={valueSwitch}
        hiddenSwitch={true}
        className="mb-4"
        label={t('bank_name')}
        placeholder={t('please_bank_name')}
        register={register}
        errors={errors}
        id="bank_name"
      />
      <InputBase
        valueSwitch={valueSwitch}
        hiddenSwitch={true}
        className="mb-4"
        label={t('bank_branch')}
        placeholder={t('please_bank_branch')}
        register={register}
        errors={errors}
        id="bank_branch"
      />
      <InputBase
        valueSwitch={valueSwitch}
        hiddenSwitch={true}
        className="mb-10"
        label={t('branch_address')}
        placeholder={t('please_branch_address')}
        errors={errors}
        register={register}
        id="branch_address"
      />

      <TitleBase title={t('information_lead')} />
      <InputBase
        valueSwitch={valueSwitch}
        hiddenSwitch={true}
        className="mb-4"
        label={t('staff_full_name')}
        placeholder={t('please_staff_full_name')}
        register={register}
        id="staff_full_name"
        errors={errors}
      />
      <InputBase
        valueSwitch={valueSwitch}
        hiddenSwitch={true}
        className="mb-4"
        label={t('phone_number')}
        placeholder={t('please_phone_number')}
        register={register}
        id="staff_phone_number"
        type="number"
        errors={errors}
        min={0}
        numberInputInvalidChars={numberInputInvalidChars}
      />
      <InputBase
        valueSwitch={valueSwitch}
        hiddenSwitch={true}
        label={t('request_other')}
        placeholder={t('please_request')}
        register={register}
        id="staff_other_requirements"
      />
    </>
  );
}
