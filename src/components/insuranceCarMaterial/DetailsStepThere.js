import { useSession } from 'next-auth/react';
import { getAppraisalDetails } from 'pages/api/insurance';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { DataContext } from 'src/store/GlobalState';
import { AUTHENTICATED } from 'src/utils/constants';
import ConfirmInsuranceDetails from 'components/common/ConfirmInsuranceDetails';

export default function DetailsStepThere({
  idInsurance,
  acceptRule,
  setAcceptRule,
}) {
  const [insurance, setInsurance] = useState();

  const session = useSession();
  const { dispatch } = useContext(DataContext);
  const getAppraisalFormsDetails = useCallback(
    async (id) => {
      dispatch({ type: 'LOADING', payload: { loading: true } });
      try {
        const response = await getAppraisalDetails(
          session?.data?.accessToken,
          id
        );
        setInsurance(response?.data);
      } catch (error) {
        console.log('🚀 ========= error:', error);
      }
      dispatch({ type: 'LOADING', payload: { loading: false } });
    },
    [session?.data?.accessToken]
  );

  useEffect(() => {
    if (session.status === AUTHENTICATED) {
      getAppraisalFormsDetails(idInsurance);
    }
  }, [session?.data?.accessToken, idInsurance]);

  const handleAcceptRules = () => {
    setAcceptRule(!acceptRule);
  };
  return (
    <div className="space-y-6 lg:space-y-8">
      <ConfirmInsuranceDetails insurance={insurance} />
      <div className="my-6 flex lg:my-8">
        <div>
          <input
            type="checkbox"
            onClick={handleAcceptRules}
            className="h-6 w-6 hover:cursor-pointer"
          />
        </div>
        <p className="ml-3 text-sm font-normal text-nickel sm:text-base">
          <span className="mr-1 text-vermilion">*</span>Tôi đã đọc và đồng ý với
          các điều khoản có trong&nbsp;
          <span className="text-ultramarine-blue">Quy tắc bảo hiểm.</span>
        </p>
      </div>
    </div>
  );
}
