import { IconSwitch } from 'components/common/icon/Icon';
import React from 'react';

export default function ButtonSwitchInvoice({
  setValueSwitch,
  valueSwitch,
  setError,
  resetField,
}) {
  const handleSwitch = () => {
    setValueSwitch(!valueSwitch);
    resetField('invoice_type');
    resetField('invoice_address');
    resetField('invoice_email');
    resetField('invoice_name');
    resetField('invoice_tax_id');
    setError('invoice_type', {
      message: '',
    });
    setError('invoice_address', {
      message: '',
    });
    setError('invoice_email', {
      message: '',
    });
    setError('invoice_name', {
      message: '',
    });
    setError('invoice_tax_id', {
      message: '',
    });
  };
  return (
    <button
      type="button"
      onClick={handleSwitch}
      className={`relative h-6 w-12 rounded-2xl duration-500 ${
        !valueSwitch ? 'bg-ultramarine-blue' : 'bg-chinese-silver'
      } `}
    >
      <div
        className={`absolute top-0 pt-0.25 duration-1000 ${
          !valueSwitch ? 'right-0' : 'left-0'
        }`}
      >
        <IconSwitch />
      </div>
    </button>
  );
}
