import { IconUploadImages } from 'components/common/icon/Icon';
import React, { useRef } from 'react';
import { useTranslation } from 'next-i18next';

export default function BorderUploadImage({ onChange, multiple }) {
  const inputFileRef = useRef(null);
  const { t } = useTranslation(['auth']);

  return (
    <div className="rounded-lg border-2 border-dashed border-chinese-silver py-12 text-center">
      <input
        ref={inputFileRef}
        type="file"
        id="certificate_image_url"
        multiple={multiple}
        className="hidden"
        onChange={onChange}
        accept=".jpg, .jpeg, .png"
      />
      <div className="mb-3 flex justify-center">
        <IconUploadImages />
      </div>
      <div className="mb-3 flex justify-center gap-1 text-base font-normal">
        <button
          type="button"
          onClick={() => inputFileRef?.current?.click()}
          className="font-bold text-ultramarine-blue"
        >
          {t('uploadImage')}
        </button>
        <p className="text-arsenic">{t('drop_image')}</p>
      </div>
      <p className="text-sm font-normal text-nickel">{t('max_size_50mb')}</p>
    </div>
  );
}
