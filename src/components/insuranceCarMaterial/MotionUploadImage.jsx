import React, { useEffect, useState } from 'react';

export function MotionUploadImage({
  min = 0,
  max = 100,
  setHiddenUploadImage,
}) {
  const [loadingImage, setLoadingImage] = useState(0);

  const onLoadingImage = (loading) => {
    setLoadingImage(parseFloat(loading.target.value));
  };

  const renderUploadProcessByPercent = (loading) => {
    setLoadingImage(parseFloat(loading.target.value) || 0);
  };

  function countdown(number) {
    if (number < max) {
      setTimeout(function () {
        countdown(number + 1);
      }, 20);
    }
    setLoadingImage(number);
    if (number === max) {
      setHiddenUploadImage(false);
    }
  }

  useEffect(() => {
    countdown(1);
  }, []);

  return (
    <div className="flex items-center">
      <input
        value={loadingImage}
        type="range"
        className="w-full"
        min={min}
        max={max}
        onChange={(loading) => onLoadingImage(loading)}
      />
      <input
        className="text-end text-xs font-normal text-arsenic outline-none [appearance:textfield] md:text-base [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:appearance-none"
        type="number"
        value={loadingImage}
        min={min}
        max={max}
        onChange={(loading) => renderUploadProcessByPercent(loading)}
      />
      <p>%</p>
    </div>
  );
}
