import { IconSwitch } from 'components/common/icon/Icon';
import React from 'react';

export default function ButtonSwitchBankBenefit({
  setValueSwitch,
  valueSwitch,
  setError,
  resetField,
}) {
  const handleSwitch = () => {
    setValueSwitch(!valueSwitch);
    resetField('bank_name');
    resetField('bank_branch');
    resetField('branch_address');
    resetField('staff_full_name');
    resetField('staff_phone_number');
    resetField('staff_other_requirements');

    setError('bank_name', {
      message: '',
    });
    setError('bank_branch', {
      message: '',
    });
    setError('branch_address', {
      message: '',
    });
    setError('staff_phone_number', {
      message: '',
    });
    setError('staff_full_name', {
      message: '',
    });
    setError('staff_other_requirements', {
      message: '',
    });
  };
  return (
    <button
      type="button"
      onClick={handleSwitch}
      className={`relative h-6 w-12 rounded-2xl duration-500 ${
        !valueSwitch ? 'bg-ultramarine-blue' : 'bg-chinese-silver'
      } `}
    >
      <div
        className={`absolute top-0 pt-0.25 duration-1000 ${
          !valueSwitch ? 'right-0' : 'left-0'
        }`}
      >
        <IconSwitch />
      </div>
    </button>
  );
}
