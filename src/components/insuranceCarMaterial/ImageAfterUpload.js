import React, { useRef } from 'react';
import { useTranslation } from 'next-i18next';
import Image from 'next/image';
import ButtonBgWhite from 'components/common/ButtonBgWhite';
import numeral from 'numeral';

export default function ImageAfterUpload({
  dataImage,
  data,
  onChange,
  multiple,
}) {
  const inputFileRef = useRef(null);

  const { t } = useTranslation(['auth']);
  return (
    <div className="flex justify-between gap-2 rounded-lg border border-platinum p-4">
      <input
        ref={inputFileRef}
        type="file"
        id="certificate_image_url"
        multiple={multiple}
        className="hidden"
        onChange={onChange}
        accept=".jpg, .jpeg, .png"
      />
      <div className="h-auto w-1/3 sm:w-24">
        <Image
          width={100}
          height={80}
          src={dataImage}
          alt="imageGCN"
          className="rounded"
        />
      </div>
      <div className="w-2/3 justify-between sm:flex sm:w-full">
        <div>
          <p className="text-base font-normal text-arsenic">
            {data?.name && data?.name}
          </p>
          <p className="text-sm font-normal text-spanish-gray">
            {data?.size && <>{numeral(data?.size / 1024).format(0.0)} KB</>}
          </p>
        </div>
        <div className="flex justify-end">
          <div className="h-auto w-2/3 font-bold text-nickel sm:w-full">
            <ButtonBgWhite
              onClick={() => inputFileRef?.current?.click()}
              cases={'small'}
              title={t('change_image')}
              widthBtn={'w-full px-6'}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
