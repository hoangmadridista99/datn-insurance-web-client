import InputBase from 'components/common/InputBase';
import React, { useCallback } from 'react';
import { useTranslation } from 'next-i18next';
import { BUSINESS, BUSINESS_OR_PERSONAL } from 'src/utils/constants';
import { regexEmail } from 'src/utils/helper';

export default function Invoice({
  register,
  setError,
  valueSwitch,
  errors,
  defaultInvoiceType,
  setValueLabelInvoice,
  valueLabelInvoice,
}) {
  const { t } = useTranslation(['common', 'auth']);

  const handleLabelInvoice = (value) => {
    setValueLabelInvoice(value);
  };
  const onChangeEmailPolicyholder = (value) => {
    const checkEmailInvoice = regexEmail.test(value.target.value);
    if (!checkEmailInvoice) {
      setError('invoice_email', {
        message: t('auth:schema.email'),
      });
      return;
    }
    setError('invoice_email', {
      message: '',
    });
  };
  const onChangeTax = (value) => {
    const isValid = /^\d{10}$|^\d{10}-\d{3}$/;
    const checkTax = isValid.test(value.target.value);
    if (!checkTax) {
      setError('invoice_tax_id', {
        message: 'Sai định dạng mã số thuế',
      });
      return;
    }
    setError('invoice_tax_id', {
      message: '',
    });
  };

  const handleInvoiceType = useCallback(
    ({ business, person }) => {
      if (valueLabelInvoice) {
        return valueLabelInvoice === BUSINESS ? business : person;
      }
      if (defaultInvoiceType) {
        return defaultInvoiceType === BUSINESS ? business : person;
      }
    },
    [valueLabelInvoice, defaultInvoiceType]
  );
  return (
    <>
      <div className="mb-4 flex gap-10">
        {BUSINESS_OR_PERSONAL(t).map((type) => (
          <div key={type.value} className="mb-4 flex gap-4">
            <input
              disabled={valueSwitch}
              type="radio"
              id="invoice_type"
              value={type?.value}
              onClick={() => handleLabelInvoice(type.value)}
              className="h-5 w-5"
              {...register('invoice_type')}
            />
            <p className="text-sm font-normal text-arsenic lg:text-base">
              {type?.label}
            </p>
          </div>
        ))}
        {errors?.invoice_type && (
          <p className=" text-red-500 ">{errors?.invoice_type?.message}</p>
        )}
      </div>
      <InputBase
        valueSwitch={valueSwitch}
        hiddenSwitch={true}
        className="mb-4"
        label={handleInvoiceType({
          business: t('name_business'),
          person: t('name_person'),
        })}
        placeholder={handleInvoiceType({
          business: t('please_name_business'),
          person: t('please_name_person'),
        })}
        register={register}
        errors={errors}
        id="invoice_name"
      />
      <InputBase
        valueSwitch={valueSwitch}
        hiddenSwitch={true}
        className="mb-4"
        type="text"
        label={handleInvoiceType({
          business: t('tax_business'),
          person: t('tax_person'),
        })}
        placeholder={handleInvoiceType({
          business: t('please_tax_business'),
          person: t('please_tax_person'),
        })}
        register={register}
        id="invoice_tax_id"
        errors={errors}
        onChange={onChangeTax}
        onKeyDownTax
      />
      <InputBase
        valueSwitch={valueSwitch}
        hiddenSwitch={true}
        className="mb-4"
        label={handleInvoiceType({
          business: t('address_business'),
          person: t('address_person'),
        })}
        placeholder={handleInvoiceType({
          business: t('please_address_business'),
          person: t('please_address_person'),
        })}
        register={register}
        errors={errors}
        id="invoice_address"
      />
      <InputBase
        valueSwitch={valueSwitch}
        hiddenSwitch={true}
        className="mb-1"
        label={t('email_send_to')}
        placeholder={t('please_email_send_to')}
        register={register}
        id="invoice_email"
        errors={errors}
        onChange={onChangeEmailPolicyholder}
      />
      <p className="text-sm font-normal text-nickel">
        {t('rules_invoice_email')}
      </p>
    </>
  );
}
