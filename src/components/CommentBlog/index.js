import React from 'react';
import Image from 'next/image';
import dayjs from 'dayjs';

const CommentBlog = (props) => (
  <div className="mb-8 border-b border-b-cultured p-4 last:mb-0">
    <div className="mb-3 flex items-center justify-between">
      <div className="flex items-center">
        <Image
          src={props.user.avatar_profile_url ?? '/svg/avatar-default.svg'}
          width={20}
          height={20}
          alt={`Avatar ${props.user.first_name} ${props.user.last_name}`}
          className="h-[20px] rounded-full object-cover"
        />
        <span className="ml-2 text-sm font-bold text-arsenic">
          {`${props.user.first_name} ${props.user.last_name}`}
        </span>
      </div>
      <span className="text-xs text-spanish-gray">
        {dayjs(new Date(props.created_at)).format('DD/MM/YYYY - hh:mm')}
      </span>
    </div>
    <p className="text-sm text-nickel">{props.comment}</p>
  </div>
);

export default CommentBlog;
