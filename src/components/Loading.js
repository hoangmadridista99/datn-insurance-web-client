import React, { useContext } from 'react';
import { DataContext } from 'src/store/GlobalState';

const Loading = () => {
  const { state } = useContext(DataContext);
  const { notify } = state;

  return (
    <>
      {notify.loading && (
        <div
          className="loading fixed h-screen w-screen text-center"
          style={{
            background: '#0008',
            color: 'white',
            top: 0,
            left: 0,
            zIndex: 1000,
          }}
        >
          <svg width="205" height="250" viewBox="0 0 50 50">
            <polygon
              strokeWidth="1"
              stroke="#fff"
              fill="none"
              points="20,1 40,40 1,40"
            ></polygon>
            <text fill="#fff" x="5" y="47">
              Loading
            </text>
          </svg>
        </div>
      )}
    </>
  );
};

export default Loading;
