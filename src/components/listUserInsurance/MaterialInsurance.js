import React, { useCallback, useContext, useEffect, useState } from 'react';
import {
  IconClock,
  IconClosePartnerInformation,
  IconFailure,
  IconSuccessful,
} from 'components/common/icon/Icon';
import { useSession } from 'next-auth/react';
import { DataContext } from 'src/store/GlobalState';
import ButtonBgWhite from 'components/common/ButtonBgWhite';
import ButtonPrimary from 'components/common/ButtonPrimary';
import { useTranslation } from 'next-i18next';
import {
  APPROVED,
  AUTHENTICATED,
  PENDING,
  REJECTED,
} from 'src/utils/constants';
import { getAppraisalDetails } from 'pages/api/insurance';
import ConfirmInsuranceDetails from 'components/common/ConfirmInsuranceDetails';
import { useRouter } from 'next/router';

export default function MaterialInsurance({
  idInsurance,
  setIsOpenModal,
  setToggleAlertEdit,
  setValueEditInsuranceCarMaterial,
}) {
  const { t } = useTranslation(['listUserInsurance']);
  const [insurance, setInsurance] = useState();
  const router = useRouter();
  const session = useSession();
  const { dispatch } = useContext(DataContext);
  const getAppraisalFormsDetails = useCallback(async () => {
    dispatch({ type: 'LOADING', payload: { loading: true } });
    try {
      const response = await getAppraisalDetails(
        session?.data?.accessToken,
        idInsurance
      );
      setInsurance(response?.data);
      setValueEditInsuranceCarMaterial(response?.data);
    } catch (error) {
      console.log('🚀 ========= error:', error);
    }
    dispatch({ type: 'LOADING', payload: { loading: false } });
  }, [session?.data?.accessToken]);

  useEffect(() => {
    if (session.status === AUTHENTICATED) {
      getAppraisalFormsDetails();
    }
  }, [session?.data?.accessToken]);

  const statusInsurance = (status) => {
    switch (status) {
      case APPROVED:
        return (
          <p className="flex items-center gap-1.5 text-apple">
            <IconSuccessful />
            {t('successfulAppraisal')}
          </p>
        );
      case PENDING:
        return (
          <p className="flex items-center gap-1.5 text-royal-orange">
            <IconClock />
            {t('waitingForAppraisal')}
          </p>
        );
      case REJECTED:
        return (
          <p className="flex items-center gap-1.5 text-vermilion">
            <IconFailure />
            {t('appraisalFailed')}
          </p>
        );
    }
  };

  const handlePushEdit = () => {
    setToggleAlertEdit(true);
  };

  return (
    <div className="fixed inset-0 z-50 bg-rich-black">
      <div className="mt-12">
        <div className="comparison-container first-modal-material-insurance">
          <div className="h-full rounded-t-xl bg-white p-4 pb-0 text-base font-normal sm:px-8 sm:pt-6 lg:px-8">
            <div className="border-b border-platinum pb-4 sm:pb-6">
              <p className="mb-3 text-2xl font-bold sm:text-pixel-32 sm:leading-pixel-48">
                {t('materialInsuranceCars')}
              </p>
              <div className="flex flex-col justify-between gap-2 text-nickel sm:flex-row">
                <div className="flex gap-2">
                  {t('status')}
                  {statusInsurance(insurance?.form_status)}
                </div>
                <div className="flex gap-2">
                  {t('code')}
                  <span className="font-bold">{insurance?.form_code}</span>
                </div>
              </div>
            </div>
          </div>
          <button
            className="absolute top-2 right-3 p-2"
            onClick={() => setIsOpenModal(false)}
          >
            <IconClosePartnerInformation />
          </button>
        </div>
        <div className="comparison-container second-modal-material-insurance">
          <ConfirmInsuranceDetails insurance={insurance} />
        </div>
        <div className="comparison-container last-modal-material-insurance">
          <div className="flex justify-end gap-4">
            <ButtonBgWhite
              onClick={handlePushEdit}
              cases="large"
              title={t('edit')}
              widthBtn="w-auto lg:px-10 px-6 py-2"
            />
            {insurance?.form_status === APPROVED && (
              <ButtonPrimary
                cases="large"
                title={t('seeQuote')}
                widthBtn="w-auto lg:px-10 px-6 py-2"
                onClick={() =>
                  router.push({
                    pathname: '/quoteViewList',
                    query: {
                      id: idInsurance,
                    },
                  })
                }
              />
            )}
          </div>
        </div>
      </div>
    </div>
  );
}
