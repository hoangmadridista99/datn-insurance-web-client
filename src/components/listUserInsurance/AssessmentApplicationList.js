import React from 'react';
import { useTranslation } from 'next-i18next';
import EachTypeAssessmentApplication from './EachTypeAssessmentApplication';

export default function AssessmentApplicationList({
  insuranceList,
  handleOpenModal,
  handleCancerRegister,
  setToggleAlertEdit,
  setValueEditInsuranceCarMaterial,
}) {
  const { t } = useTranslation(['listUserInsurance']);

  return (
    <div className="mb-10 sm:border sm:border-t-0 sm:border-platinum sm:p-6">
      <div className="mb-4 hidden justify-between px-4 text-base font-bold text-silver lg:flex">
        <p className="w-1/12">ID</p>
        <p className="w-1/5">{t('typeOfInsurance')}</p>
        <p className="w-percent-10">{t('licensePlates')}</p>
        <p className="w-1/5">{t('registrationTime')}</p>
        <p className="w-1/5">{t('status')}</p>
        <p className="w-percent-11"></p>
      </div>
      {insuranceList?.map((insurance) => (
        <div key={insurance?.id} className="space-y-4">
          <EachTypeAssessmentApplication
            setValueEditInsuranceCarMaterial={setValueEditInsuranceCarMaterial}
            setToggleAlertEdit={setToggleAlertEdit}
            item={insurance}
            handleButton={handleOpenModal}
            handleCancerRegister={handleCancerRegister}
          />
        </div>
      ))}
    </div>
  );
}
