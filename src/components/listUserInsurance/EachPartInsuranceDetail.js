import {
  IconDisableCheckBox,
  IconTickCheckBox,
} from 'components/common/icon/Icon';
import Image from 'next/image';
import React from 'react';
import { useTranslation } from 'next-i18next';

export default function EachPartInsuranceDetail({
  carInformation,
  listCheckbox,
  title,
}) {
  const { t } = useTranslation(['listUserInsurance']);
  return (
    <div className="rounded-lg border border-platinum bg-white p-4 sm:p-8">
      <p className="mb-6 text-xl font-bold sm:text-2xl lg:mb-5 lg:font-bold">
        {t(title)}
      </p>
      <div className="space-y-3 text-sm text-nickel sm:text-base">
        {carInformation?.map((info) => (
          <div
            key={info?.id}
            className={
              info?.image
                ? 'space-y-3'
                : 'flex flex-col gap-1 border-b border-cultured pb-2 sm:flex-row sm:justify-between sm:pb-3'
            }
          >
            <p className="font-normal">{t(info?.title)}</p>
            {info?.image ? (
              info?.image?.length === 5 ? (
                <div className="flex flex-wrap justify-between gap-3">
                  {info?.image?.map((item) => (
                    <div key={item}>
                      <Image
                        src={item}
                        alt="Appraisal pictures"
                        width={171}
                        height={124}
                        className="h-31"
                      />
                    </div>
                  ))}
                </div>
              ) : (
                <Image
                  src={info?.image}
                  alt="Appraisal pictures"
                  width={164}
                  height={164}
                />
              )
            ) : (
              <div className="font-bold text-arsenic sm:font-medium">
                {info?.value && <p>{info?.value}</p>}
                {(info?.from || info?.to) && (
                  <p>
                    <span>{info?.from}</span>&nbsp;-&nbsp;
                    <span>{info?.to}</span>
                  </p>
                )}
              </div>
            )}
          </div>
        ))}
        {listCheckbox && (
          <div className="flex flex-col gap-3 rounded-lg bg-cultured p-2 lg:p-6">
            {listCheckbox?.map((benefit) => (
              <div key={benefit?.benefitId}>
                <p className="mb-3 border-b border-platinum pb-2 text-sm font-bold text-nickel sm:text-base">
                  {benefit?.title}
                </p>
                <div className="space-y-3">
                  {benefit?.benefitDetails?.map((item) => (
                    <div className="flex" key={item?.id}>
                      {item?.value ? (
                        <div>
                          <IconTickCheckBox />
                        </div>
                      ) : (
                        <div>
                          <IconDisableCheckBox />
                        </div>
                      )}
                      <p className="ml-3 font-normal text-arsenic lg:ml-4">
                        {t(item?.title)}
                      </p>
                    </div>
                  ))}
                </div>
              </div>
            ))}
          </div>
        )}
      </div>
    </div>
  );
}
