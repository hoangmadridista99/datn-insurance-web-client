import React from 'react';
import { insuranceTypes } from 'src/utils/constants';
const styleActive = 'border-b-white text-azure';
import { useTranslation } from 'next-i18next';

export default function TabSwitching({ currentTab, handleTabSwitching }) {
  const { t } = useTranslation(['listUserInsurance']);
  return (
    <div className="flex w-3/5 justify-between">
      {insuranceTypes(t)?.map((item) => (
        <div key={item?.id} className="flex w-1/4">
          <button
            className={`style-button-user-insurance-list w-full ${
              currentTab === item?.id ? styleActive : 'bg-cultured'
            }`}
            onClick={() => handleTabSwitching(item?.id)}
          >
            {item?.title}
          </button>
          <div className="w-1 border-b border-platinum "></div>
        </div>
      ))}
    </div>
  );
}
