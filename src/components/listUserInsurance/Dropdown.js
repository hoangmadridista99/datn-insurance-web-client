import React from 'react';
import { Menu, Transition } from '@headlessui/react';
import { Fragment } from 'react';
import {
  IconDropdown,
  IconThreeDotVertical,
  IconThreeDotVerticalMobile,
} from 'components/common/icon/Icon';
import { useTranslation } from 'next-i18next';
import {
  EDIT,
  SEE_DETAILS,
  SEE_QUOTE,
  userInsuranceButtonList,
} from 'src/utils/constants';
import { useRouter } from 'next/router';

export default function DropDown({
  handleButton,
  idInsurance,
  statusInsurance,
  handleCancerRegister,
  values,
  setToggleAlertEdit,
  setValueEditInsuranceCarMaterial,
}) {
  const router = useRouter();
  const { t } = useTranslation(['listUserInsurance']);

  const buttonList = userInsuranceButtonList(statusInsurance);
  const handleSelectButtonDropdown = (value, status, idInsurance) => {
    switch (value) {
      case EDIT:
        return () => {
          setToggleAlertEdit(true);
          setValueEditInsuranceCarMaterial(values);
        };

      case SEE_DETAILS:
        return () => handleButton(status, idInsurance);

      case SEE_QUOTE:
        return () =>
          router.push({
            pathname: '/quoteViewList',
            query: {
              id: idInsurance,
            },
          });
      case 'cancelRegistration':
        return () => handleCancerRegister(true, idInsurance);
    }
  };
  return (
    <div className="text-right">
      <Menu as="div" className="relative inline-block text-left">
        <div>
          <Menu.Button className="hidden rounded-lg border border-ultramarine-blue bg-lavender py-2 px-4 text-ultramarine-blue lg:flex">
            <p>{t('operation')}</p>
            <IconDropdown />
          </Menu.Button>
          <Menu.Button className="flex rounded-lg text-ultramarine-blue lg:hidden">
            <div className="hidden sm:block lg:hidden">
              <IconThreeDotVertical />
            </div>
            <div className="sm:hidden">
              <IconThreeDotVerticalMobile />
            </div>
          </Menu.Button>
        </div>
        <Transition
          as={Fragment}
          enter="transition ease-out duration-100"
          enterFrom="transform opacity-0 scale-95"
          enterTo="transform opacity-100 scale-100"
          leave="transition ease-in duration-75"
          leaveFrom="transform opacity-100 scale-100"
          leaveTo="transform opacity-0 scale-95"
        >
          <Menu.Items className="absolute right-0 z-40 mt-2 w-25.5 origin-top-right divide-y divide-gray-100 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none lg:w-full">
            <div className="p-1">
              {buttonList?.map((item) => (
                <Menu.Item key={item?.id}>
                  {({ active }) => (
                    <button
                      className={`${
                        active && 'text bg-lavender text-ultramarine-blue'
                      } group flex w-full items-center rounded-md px-2 py-2 text-left text-sm ${
                        item?.state ? 'text-chinese-silver' : 'text-gray-900'
                      }`}
                      onClick={handleSelectButtonDropdown(
                        item?.value,
                        true,
                        idInsurance
                      )}
                      disabled={item?.state}
                    >
                      {t(item?.title)}
                    </button>
                  )}
                </Menu.Item>
              ))}
            </div>
          </Menu.Items>
        </Transition>
      </Menu>
    </div>
  );
}
