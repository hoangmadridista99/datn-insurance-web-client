import { IconInfo } from 'components/common/icon/Icon';
import React from 'react';
import DropDown from './Dropdown';
import { useTranslation } from 'next-i18next';
import { REJECTED } from 'src/utils/constants';
import FormatDate from 'components/common/FormatDate';
import { statusInsurance, statusTextInsurance } from 'src/utils/helper';

export default function EachTypeAssessmentApplication({
  item,
  handleButton,
  handleCancerRegister,
  setToggleAlertEdit,
  setValueEditInsuranceCarMaterial,
}) {
  const { t } = useTranslation(['listUserInsurance']);
  return (
    <div className="my-4 flex justify-between rounded-xl border border-platinum p-3 text-xs font-normal text-nickel sm:p-4 lg:text-base">
      <div className="flex w-percent-94 flex-wrap items-center justify-between gap-y-3 lg:w-full">
        <div className="hidden w-full sm:flex sm:flex-col sm:space-y-1.5 lg:hidden">
          <div
            className={`${statusInsurance(
              item?.form_status
            )} flex w-3/5 items-center gap-2 rounded-3xl py-1 px-3 sm:w-fit sm:px-4`}
          >
            {statusTextInsurance(t, item?.form_status)}
            {item?.form_status === REJECTED && <IconInfo />}
          </div>
        </div>
        <div className="content-insurance-list lg:w-1/12">
          <p className="insurance-list-title-mobile">{t('code')}</p>
          <p className="details-contents-insurance-list text-arsenic">
            {item?.form_code}
          </p>
        </div>
        <div className="content-insurance-list lg:w-1/5 lg:font-bold">
          <p className="insurance-list-title-mobile">{t('typeOfInsurance')}</p>
          <p className="details-contents-insurance-list text-ultramarine-blue">
            {t('materialInsuranceCars')}
          </p>
        </div>
        <div className="content-insurance-list lg:w-percent-10">
          <p className="insurance-list-title-mobile">{t('licensePlate')}</p>
          <p className="details-contents-insurance-list text-arsenic">
            {item?.car_license_plate}
          </p>
        </div>
        <div className="content-insurance-list lg:w-1/5">
          <p className="insurance-list-title-mobile">{t('registrationTime')}</p>
          <p className="details-contents-insurance-list text-arsenic sm:space-y-1.5 lg:space-y-0">
            {FormatDate(item?.created_at)}
          </p>
        </div>
        <div className="content-insurance-list sm:hidden lg:flex lg:w-1/5">
          <p className="block w-percent-36 sm:w-percent-49 lg:hidden">
            {t('status')}
          </p>
          <div
            className={`${statusInsurance(
              item?.form_status
            )} flex w-3/5 items-center gap-2 rounded-3xl py-1 px-3 sm:w-fit sm:px-4`}
          >
            {statusTextInsurance(t, item?.form_status)}
            {item?.form_status === REJECTED && <IconInfo />}
          </div>
        </div>
        <div className="hidden lg:block lg:w-percent-11">
          <DropDown
            setValueEditInsuranceCarMaterial={setValueEditInsuranceCarMaterial}
            setToggleAlertEdit={setToggleAlertEdit}
            values={item}
            handleButton={handleButton}
            idInsurance={item?.id}
            statusInsurance={item?.form_status}
            handleCancerRegister={handleCancerRegister}
          />
        </div>
      </div>
      <div className="w-percent-4 lg:hidden">
        <DropDown
          setValueEditInsuranceCarMaterial={setValueEditInsuranceCarMaterial}
          setToggleAlertEdit={setToggleAlertEdit}
          handleButton={handleButton}
          idInsurance={item?.id}
          statusInsurance={item?.form_status}
          handleCancerRegister={handleCancerRegister}
        />
      </div>
    </div>
  );
}
