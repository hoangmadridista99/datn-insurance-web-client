import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'next-i18next';
import { toast } from 'react-toastify';
import { DataContext } from 'src/store/GlobalState';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { schemaSignInWithEmail, schemaSignInWithPhone } from 'src/utils/error';
import Link from 'next/link';
import { signIn } from 'next-auth/react';
import Image from 'next/image';
import { unlockScroll } from 'src/utils/helper';
import { useRouter } from 'next/router';

const LoginNoAccount = () => {
  const { state } = useContext(DataContext);
  const { login } = state;

  const { t } = useTranslation(['auth']);
  const router = useRouter();

  const { dispatch } = useContext(DataContext);

  const [signInPhone, setSignInPhone] = useState(true);
  const [signInEmail, setSignInEmail] = useState(false);

  const {
    register,
    handleSubmit,
    reset,

    formState: { errors },
  } = useForm({
    // mode: 'onChange',
    resolver: yupResolver(
      signInPhone ? schemaSignInWithEmail(t) : schemaSignInWithPhone(t)
    ),
  });

  useEffect(() => {
    dispatch({ type: 'LOGIN', loginNoAccount: { login: false } });
    unlockScroll();
  }, [router.pathname]);

  const handleSignIn = async (data) => {
    try {
      dispatch({ type: 'LOADING', payload: { loading: true } });
      const response = await signIn('credentials', {
        destination: data.destination,
        password: data.password,
        redirect: false,
      });

      if (!response.ok) {
        toast.error(response.error);

        return;
      }
      unlockScroll();
      toast.success(t('loginSuccess'));
      dispatch({ type: 'LOGIN', loginNoAccount: { login: false } });
    } catch (error) {
      toast.error(t('loginFailure'));

      console.log('🚀 ===== error:', error);
    } finally {
      dispatch({ type: 'LOADING', payload: { loading: false } });
    }
  };

  const hiddenEmail = () => {
    setSignInPhone(true);
    setSignInEmail(false);
    reset();
  };

  const hiddenPhone = () => {
    setSignInPhone(false);
    setSignInEmail(true);
    reset();
  };

  const handleCloseLogin = () => {
    unlockScroll();
    dispatch({ type: 'LOGIN', loginNoAccount: { login: false } });
  };

  return (
    <>
      {login?.login && (
        <>
          <div className="absolute inset-0 z-50 flex h-screen items-center justify-center bg-rich-black">
            <div className="h-[80vh] overflow-auto rounded-2xl border bg-white p-6 shadow-formFilter">
              <div className="flex justify-end">
                <button
                  onClick={handleCloseLogin}
                  className="rounded-full border px-3 py-1 text-chinese-silver"
                >
                  X
                </button>
              </div>

              <div className="pb-10 lg:px-10">
                <div className="mb-12 flex h-8 items-center">
                  <Image
                    src="/images/navbar/logoComparison.png"
                    width={125}
                    height={32}
                    alt="comparison-logo"
                  />
                </div>
                <form onSubmit={handleSubmit(handleSignIn)}>
                  <h4 className="mb-10 text-2xl font-bold text-arsenic md:text-pixel-32 md:leading-pixel-48">
                    {t('login')}
                  </h4>
                  <p className="mb-8 text-base font-normal text-spanish-gray">
                    Vui lòng đăng nhập hoặc đăng ký tài khoản để thực hiện các
                    thao tác
                  </p>

                  <div className="mb-6 flex justify-center md:mb-9 md:block">
                    <button
                      className={`w-1/2 px-6 py-3 text-sm font-bold text-spanish-gray md:w-fit md:text-base ${
                        signInPhone && 'rounded-lg bg-platinum text-black'
                      }`}
                      type="button"
                      onClick={hiddenEmail}
                    >
                      {t('useEmail')}
                    </button>

                    <button
                      className={`w-1/2 px-6 py-3 text-sm font-bold text-spanish-gray md:w-fit md:text-base ${
                        signInEmail && 'rounded-lg bg-platinum text-black'
                      }`}
                      type="button"
                      onClick={hiddenPhone}
                    >
                      {t('usePhone')}
                    </button>
                  </div>

                  {signInPhone && (
                    <div className="relative mb-6">
                      <label
                        htmlFor="destination"
                        className="text-sm font-bold text-arsenic md:text-base "
                      >
                        {t('email')}
                      </label>

                      <div className="mt-2 rounded-md border border-chinese-silver p-2 md:p-3">
                        <input
                          autoComplete="off"
                          type="text"
                          id="destination"
                          placeholder={t('addressEmail')}
                          className="w-full bg-transparent text-sm font-normal outline-none md:text-base"
                          {...register('destination', { required: true })}
                        />
                      </div>

                      {errors.destination && (
                        <p className=" text-red-500 ">
                          {errors.destination.message}
                        </p>
                      )}
                    </div>
                  )}

                  {signInEmail && (
                    <div className="relative mb-6">
                      <label
                        htmlFor="destination"
                        className="text-sm font-bold text-arsenic md:text-base "
                      >
                        {t('phone')}
                      </label>
                      <div className="mt-2 rounded-md border border-chinese-silver p-2 md:p-3">
                        <input
                          autoComplete="off"
                          type="text"
                          id="destination"
                          placeholder={t('phone')}
                          className="w-full bg-transparent text-sm font-normal outline-none md:text-base "
                          {...register('destination', { required: true })}
                        />
                      </div>
                      {errors.destination && (
                        <p className=" text-red-500 ">
                          {errors.destination.message}
                        </p>
                      )}
                    </div>
                  )}

                  <div className="relative mb-10">
                    <label
                      htmlFor="password"
                      className="text-sm font-bold text-arsenic md:text-base"
                    >
                      {t('password')}
                    </label>

                    <div className="mt-2 rounded-md border border-chinese-silver p-2 md:p-3">
                      <input
                        type="password"
                        id="password"
                        placeholder={t('pass')}
                        className="w-full bg-transparent text-sm font-normal outline-none md:text-base"
                        {...register('password', { required: true })}
                      />
                    </div>

                    {errors.password && (
                      <p className=" text-red-500 ">
                        {errors.password.message}
                      </p>
                    )}
                  </div>

                  <button
                    className="mb-6 w-full rounded-lg bg-ultramarine-blue py-2 text-base font-bold text-cultured md:py-3"
                    type="submit"
                  >
                    {t('login')}
                  </button>

                  <div className="flex flex-wrap justify-center gap-2">
                    <p className="text-sm font-normal text-nickel md:text-base">
                      {t('youUseAccount')}
                    </p>

                    <Link
                      className="text-sm font-bold text-ultramarine-blue underline md:text-base"
                      href="/auth/register"
                    >
                      {t('register')}
                    </Link>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default LoginNoAccount;
