import DetailsLine from 'components/common/DetailsLine';
import {
  IconTickGood,
  IconTickGoodBetter,
  IconTickInformation,
} from 'components/common/icon/Icon';
import React from 'react';
import { descriptionDetailInsurance } from 'src/utils/constants';
import {
  formatTimeFeeDeadline,
  formatTypeDeadlineForDeal,
} from 'src/utils/helper';
import { useTranslation } from 'next-i18next';

export default function ListDetailsLineLifeInsurance({ insurance }) {
  const { t } = useTranslation('common');

  return (
    <div className="flex flex-col gap-2 py-2">
      <DetailsLine
        icon={<IconTickGoodBetter />}
        toolTipContent={descriptionDetailInsurance.acceptance_rate()}
        label={t('acceptance_rate_deal')}
        value={insurance?.customer_orientation?.acceptance_rate?.text}
      />

      <DetailsLine
        icon={<IconTickGood />}
        toolTipContent={descriptionDetailInsurance.reception_and_processing_time()}
        label={t('time_reception')}
        value={
          insurance?.customer_orientation?.reception_and_processing_time?.text
        }
      />

      <DetailsLine
        icon={<IconTickInformation />}
        toolTipContent={descriptionDetailInsurance.age_of_contract_termination()}
        label={t('deadline')}
        value={`${t('To')} ${
          insurance?.terms?.deadline_for_deal?.to
        } ${formatTypeDeadlineForDeal(
          insurance?.terms?.deadline_for_deal?.type
        )} `}
      />

      <DetailsLine
        icon={<IconTickInformation />}
        toolTipContent={descriptionDetailInsurance.deadline_for_payment()}
        label={t('time_deadline_payment')}
        value={insurance?.terms?.deadline_for_payment?.value
          .map((value) => formatTimeFeeDeadline(value))
          .join(', ')}
      />
    </div>
  );
}
