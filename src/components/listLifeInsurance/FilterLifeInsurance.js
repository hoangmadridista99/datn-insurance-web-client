/* eslint-disable indent */
import numeral from 'numeral';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import {
  LIST_BENEFIT_INSURANCE,
  LIST_INSURED_PERSON,
  LIST_WORKS,
  MONEY_SUM,
  STEP,
  TIME_INSURANCE_FILTER,
} from 'src/utils/constants';
import {
  convertObjectives,
  numberInputInvalidChars,
  onKeyDown,
  unlockScroll,
} from 'src/utils/helper';
import { IconDropDownListInsurance } from 'components/common/icon/Icon';
import Image from 'next/image';
import { yupResolver } from '@hookform/resolvers/yup';
import { schemaFilterInsurance } from 'src/utils/error';
import { useTranslation } from 'next-i18next';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import ButtonPrimary from 'components/common/ButtonPrimary';
import InputChoiceYearDate from 'components/common/InputChoiceYearDate';
import SelectFilter from 'components/common/SelectFilter';

const currentYear = new Date().getFullYear();
function FilterLifeInsurance({
  data,
  insuranceObjectives,
  setHiddenFilter,
  setCurrentPage,
  setIsSuggestion,
  getListInsurance,
  currentPage,
  sort,
  dispatch,
}) {
  const { t } = useTranslation(['common']);

  const {
    register,
    handleSubmit,
    setError,
    setValue,
    formState: { errors },
  } = useForm({
    // mode: 'onChange',
    resolver: yupResolver(schemaFilterInsurance(t)),
    values: data,
  });

  const [defaultValueProfession, setDefaultValueProfession] = useState(
    data?.profession
  );
  const [defaultValueTotalSum, setDefaultValueTotalSum] = useState(
    data?.total_sum_insured
  );
  const [defaultValueTimeInsurance, setDefaultValueTimeInsurance] = useState(
    data?.deadline_for_deal
  );

  const checkValueDefaultTotalSum = MONEY_SUM(t).some(
    (item) => item.value === parseInt(defaultValueTotalSum)
  );
  const checkValueDefaultTimeInsurance = TIME_INSURANCE_FILTER(t).some(
    (item) => item.value === parseInt(defaultValueTimeInsurance)
  );

  const [hiddenInsuranceObjectives, setHiddenInsuranceObjectives] =
    useState(false);
  const [hiddenInsuranceBenefit, setHiddenInsuranceBenefit] = useState(false);
  const [errorValueNoNumber, setErrorValueNoNumber] = useState([]);
  const [valueTotalSumMoney, setValueTotalSumMoney] = useState(
    checkValueDefaultTotalSum ? MONEY_SUM(t)[0] : undefined
  );
  const [valueTimeInsurance, setValueTimeInsurance] = useState(
    checkValueDefaultTimeInsurance ? TIME_INSURANCE_FILTER(t)[0] : undefined
  );
  const [yearValue, setYearValue] = useState();
  const [valueProfession, setValueProfession] = useState(LIST_WORKS(t)[0]);
  const [numberYear, setNumberYear] = useState(48);

  const listYearOfBirth = [
    ...Array.from({ length: 106 }, (_, i) => currentYear - i).reverse(),
  ];
  const listYearShow = listYearOfBirth?.slice(numberYear, numberYear + STEP);
  const handleChoseProfession = (e) => {
    setDefaultValueProfession(e.value);
    setValueProfession(e);
    setValue('profession', e.value);
    setError('profession', { message: '' });
  };

  const onChangeYearOfBirth = (event) => {
    const {
      target: { value },
    } = event;

    setValue('year_of_birth', value);
    setYearValue(value);
    if (value > currentYear || value < currentYear - 100) {
      setError('year_of_birth', { message: '' });
      setErrorValueNoNumber({
        errorValueYearOfBirth: t('error_year'),
      });
      return;
    }
    setError('year_of_birth', { message: '' });
    setErrorValueNoNumber({
      errorValueYearOfBirth: '',
    });
  };

  const handleFilterInsurance = async (values) => {
    setIsSuggestion(false);
    setCurrentPage(1);
    const { ...data } = values;

    const isPassCheck = onCheckValueForm(values);
    if (!isPassCheck) return;

    const query = {
      ...Object.keys(data).reduce((result, key) => {
        if (data[key]) return { ...result, [key]: data[key] };

        return result;
      }, {}),
    };

    try {
      getListInsurance(query, currentPage, sort).finally(() => {
        dispatch({ type: 'LOADING', payload: { loading: false } });
      });

      // router.push({ pathname: router.pathname, query: query }, undefined, {
      //   shallow: false,
      // });
      setHiddenFilter(false);
    } catch (error) {
      console.log(
        '🚀 ~ file: FilterLifeInsurance.js:59 ~ handleFilterInsurance ~ err:',
        error
      );
    }
  };

  const handleHiddenInsuranceObjectives = () => {
    setHiddenInsuranceObjectives(!hiddenInsuranceObjectives);
  };

  const handleHiddenBenefitInsurance = () => {
    setHiddenInsuranceBenefit(!hiddenInsuranceBenefit);
  };

  const handleInputTotalSumInsured = (e) => {
    setDefaultValueTotalSum(e.value);
    setValue('total_sum_insured', e.value);
    setError('total_sum_insured', { message: '' });
    setValueTotalSumMoney();
  };
  const handleInputTotalSum = (event) => {
    const {
      target: { value },
    } = event;

    const formattedValue = numeral(value).format('(0,0)');
    setError('total_sum_insured', { message: '' });
    setValue('total_sum_insured', parseInt(formattedValue.replace(/,/g, '')));
    setValueTotalSumMoney(formattedValue);
  };

  const handleInputDeadlineForDeal = (e) => {
    setDefaultValueTimeInsurance(e.value);
    setValue('deadline_for_deal', e.value);
    setError('deadline_for_deal', { message: '' });
    setValueTimeInsurance();
  };
  const onChangeDeadlineForDeal = (event) => {
    const {
      target: { value },
    } = event;

    const formattedValue = numeral(value).format('(0,0)');

    setError('deadline_for_deal', { message: '' });
    setValueTimeInsurance(formattedValue);
    setValue('deadline_for_deal', formattedValue);
  };

  const onCheckValueForm = ({
    objective,
    insured_person,
    total_sum_insured,
    deadline_for_deal,
    profession,
    year_of_birth,
  }) => {
    if (!objective || objective.length === 0) {
      setError('objective', { message: t('error_objective') });
    }
    if (!profession) {
      setError('profession', { message: t('error_profession') });
    }
    if (!year_of_birth) {
      setError('year_of_birth', { message: t('error_year_of_birth') });
    }
    if (total_sum_insured == 0 || total_sum_insured === 'hidden') {
      setError('total_sum_insured', {
        message: t('error_total_sum_insured'),
      });
    }
    if (!insured_person || insured_person.length === 0) {
      setError('insured_person', {
        message: t('error_insured_person'),
      });
    }
    if (deadline_for_deal == 0 || deadline_for_deal === 'hidden') {
      setError('deadline_for_deal', {
        message: t('error_deadline_for_deal_no'),
      });
    }
    if (deadline_for_deal > 150) {
      setError('deadline_for_deal', {
        message: t('error_deadline_for_deal'),
      });
    }
    WindowScrollTop(200);
    if (
      objective.length > 0 &&
      insured_person.length > 0 &&
      total_sum_insured !== 'hidden' &&
      total_sum_insured > 0 &&
      deadline_for_deal !== 'hidden' &&
      deadline_for_deal > 0 &&
      deadline_for_deal < 150 &&
      profession &&
      currentYear - 100 < parseInt(year_of_birth) &&
      parseInt(year_of_birth) < currentYear
    ) {
      return true;
    }
  };

  const handleCloseFilter = () => {
    setHiddenFilter(false);
    unlockScroll();
  };

  return (
    <div>
      <form onSubmit={handleSubmit(handleFilterInsurance)}>
        <div className="bg-white p-6 shadow-listInsurance lg:rounded-2xl">
          <div className="flex items-center justify-between border-b pb-6 text-2xl font-bold text-nickel">
            <p>{t('information_search')}</p>
            <div className="lg:hidden">
              <button type="button" onClick={handleCloseFilter}>
                <Image
                  src="/svg/close-circle.svg"
                  height={24}
                  width={24}
                  alt="close"
                />
              </button>
            </div>
          </div>
          <div className="flex justify-between">
            <p className="pt-4 text-base font-bold text-arsenic">
              {t('target_insurance')}
            </p>
            <div className="pt-4">
              <button
                type="button"
                onClick={handleHiddenInsuranceObjectives}
                className={`${
                  !hiddenInsuranceObjectives && 'rotate-90 pr-0 pt-3'
                } pr-3`}
              >
                <IconDropDownListInsurance />
              </button>
            </div>
          </div>
          {hiddenInsuranceObjectives && (
            <div>
              {insuranceObjectives.map((list) => (
                <div
                  key={list?.id}
                  className="mb-5 mt-4 flex items-center gap-4"
                >
                  <input
                    type="checkbox"
                    className="h-6 w-6 hover:cursor-pointer"
                    id="objective"
                    {...register('objective')}
                    value={list?.objective_type}
                  />
                  <p>{convertObjectives(list?.objective_type)}</p>
                </div>
              ))}
            </div>
          )}
          {errors?.objective && (
            <p className="text-red-500 ">{errors?.objective?.message}</p>
          )}

          <p className="mb-2 pt-4 text-base font-bold text-arsenic">
            {t('total_sum_insured')}
          </p>

          <div className="mb-4">
            {((defaultValueTotalSum === 'hidden' ||
              !checkValueDefaultTotalSum) && (
              <>
                <div className="mt-2 flex">
                  <input
                    type="text"
                    className={` w-full rounded-l-md border border-chinese-silver ${
                      errors?.total_sum_insured?.message
                        ? 'border-red-500'
                        : 'focus:border-ultramarine-blue'
                    } p-3 outline-none`}
                    onKeyDown={(e) => onKeyDown(e, numberInputInvalidChars)}
                    defaultValue={numeral(defaultValueTotalSum).format('(0,0)')}
                    value={valueTotalSumMoney}
                    onChange={handleInputTotalSum}
                  />
                  <p className="rounded-r-md border-y border-r border-chinese-silver bg-platinum px-6 py-3 text-base font-normal text-silver">
                    VND
                  </p>
                </div>
                {errors?.total_sum_insured && (
                  <p className=" text-red-500 ">
                    {errors?.total_sum_insured?.message}
                  </p>
                )}
              </>
            )) || (
              <SelectFilter
                onChange={handleInputTotalSumInsured}
                selected={valueTotalSumMoney}
                options={MONEY_SUM(t)}
                defaultValue={defaultValueTotalSum}
                errors={errors?.total_sum_insured?.message}
              />
            )}
          </div>

          <div className="mb-8">
            <p className="pt-4 text-base font-bold text-arsenic">
              {t('insured_person')}
            </p>
            {LIST_INSURED_PERSON(t).map((values) => (
              <div
                key={values.value}
                className="mb-4 mt-2 flex items-center gap-4"
              >
                <input
                  type="checkbox"
                  id="insured_person"
                  {...register('insured_person')}
                  className="h-5 w-5"
                  value={values?.value}
                />
                <p className="text-base font-normal text-arsenic">
                  {values?.label}
                </p>
              </div>
            ))}
            {errors?.insured_person && (
              <p className=" text-red-500 ">
                {errors?.insured_person?.message}
              </p>
            )}
          </div>

          <InputChoiceYearDate
            yearValue={yearValue}
            register={register}
            errorValueNoNumber={errorValueNoNumber}
            errors={errors}
            onChangeYearOfBirth={onChangeYearOfBirth}
            setValue={setValue}
            setYearValue={setYearValue}
            setError={setError}
            setErrorValueNoNumber={setErrorValueNoNumber}
            title={t('year')}
            placeholder={t('choice_year')}
            id="year_of_birth"
            listYearShow={listYearShow}
            setNumberYear={setNumberYear}
            numberYear={numberYear}
            numberYearMax={96}
          />

          <div className="mb-4">
            <p className="mb-2 text-base font-bold text-arsenic">
              {t('profession')}
            </p>
            <SelectFilter
              onChange={handleChoseProfession}
              selected={valueProfession}
              options={LIST_WORKS(t)}
              defaultValue={defaultValueProfession}
              errors={errors?.profession?.message}
            />
            {errors?.profession && (
              <p className=" text-red-500 ">{errors?.profession?.message}</p>
            )}
          </div>

          <p className="mb-2 pt-4 text-base font-bold text-arsenic">
            {t('deadline_for_deal')}
          </p>
          {((defaultValueTimeInsurance === 'hidden' ||
            !checkValueDefaultTimeInsurance) && (
            <>
              <div className="mb-4 flex w-full">
                <input
                  type="text"
                  maxLength={3}
                  autoComplete="off"
                  className={`w-full rounded-l-md border border-chinese-silver [appearance:textfield] [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:appearance-none ${
                    errors?.deadline_for_deal?.message
                      ? 'border-red-500'
                      : 'focus:border-ultramarine-blue'
                  } p-3 outline-none`}
                  onChange={onChangeDeadlineForDeal}
                  onKeyDown={(e) => onKeyDown(e, numberInputInvalidChars)}
                  defaultValue={numeral(defaultValueTimeInsurance).format(
                    '(0,0)'
                  )}
                  value={valueTimeInsurance}
                />
                <p className="rounded-r-md border-y border-r border-chinese-silver bg-platinum px-6 py-3 text-base font-normal text-silver">
                  {t('Year')}
                </p>
              </div>
              {errors?.deadline_for_deal && (
                <p className=" text-red-500 ">
                  {errors?.deadline_for_deal?.message}
                </p>
              )}
            </>
          )) || (
            <SelectFilter
              onChange={handleInputDeadlineForDeal}
              selected={valueTimeInsurance}
              options={TIME_INSURANCE_FILTER(t)}
              defaultValue={defaultValueTimeInsurance}
              errors={errors?.deadline_for_deal?.message}
            />
          )}

          <div className="mb-5 flex justify-between">
            <p className="pt-4 text-base font-bold text-arsenic">
              {t('benefit_add')}
            </p>
            <div className="pt-4">
              <button
                type="button"
                onClick={handleHiddenBenefitInsurance}
                className={`${
                  !hiddenInsuranceBenefit && 'rotate-90 pr-0 pt-3'
                } pr-3`}
              >
                <IconDropDownListInsurance />
              </button>
            </div>
          </div>
          {hiddenInsuranceBenefit && (
            <div>
              {LIST_BENEFIT_INSURANCE(t).map((list) => (
                <div
                  key={list?.value}
                  className="mb-8 mt-2 flex items-center gap-4"
                >
                  <input
                    type="checkbox"
                    id={list?.value}
                    className="h-6 w-6 hover:cursor-pointer"
                    {...register(list.value)}
                  />
                  <p className="w-full">{list?.label}</p>
                </div>
              ))}
            </div>
          )}
          <ButtonPrimary
            type={'submit'}
            cases={'large'}
            title={t('recalculation')}
          />
        </div>
      </form>
    </div>
  );
}

export default FilterLifeInsurance;
