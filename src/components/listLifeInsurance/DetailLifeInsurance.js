import React, { useEffect, useState } from 'react';
import {
  convertUrl,
  formatCategory,
  formatInsuredPerson,
  formatProfession,
  formatTimeFeeDeadline,
} from 'src/utils/helper';
import { descriptionDetailInsurance } from 'src/utils/constants';
import Link from 'next/link';
import Image from 'next/image';
import numeral from 'numeral';
import { getInsuranceIdAPI } from 'pages/api/insurance';
import InformationDetailNoNone from 'components/common/listInsurance/InformationDetailNoNone';
import InformationDetail from 'components/common/listInsurance/InformationDetail';
import { useTranslation } from 'next-i18next';

function DetailLifeInsurance({ id }) {
  const { t } = useTranslation('common');

  const [handleSelectBenefit, setHandleSelectBenefit] = useState(true);
  const [handleSelectInformation, setHandleSelectInformation] = useState(false);
  const [detailInsurance, setDetailInsurance] = useState();
  const data = detailInsurance;

  const getInsuranceIds = async () => {
    try {
      const response = await getInsuranceIdAPI(id);

      setDetailInsurance(response?.data);
    } catch (error) {
      console.log('🚀 ~ file: index.js:17 ~ getInsuranceIds ~ error:', error);
    }
  };

  useEffect(() => {
    getInsuranceIds();
  }, [id]);

  const handleBenefit = () => {
    setHandleSelectBenefit(true);
    setHandleSelectInformation(false);
  };
  const handleInformation = () => {
    setHandleSelectBenefit(false);
    setHandleSelectInformation(true);
  };
  return (
    <div className="pt-4">
      <div className="flex">
        <button
          type="button"
          className={`w-1/2 py-2 text-base hover:bg-slate-100 ${
            (handleSelectBenefit &&
              'border-b-2 border-ultramarine-blue font-medium text-arsenic') ||
            'border-b border-cultured font-normal text-spanish-gray'
          }`}
          onClick={handleBenefit}
        >
          {t('over_view')}
        </button>
        <button
          type="button"
          className={`w-1/2 py-2 text-base  hover:bg-slate-100 ${
            (handleSelectInformation &&
              'border-b-2 border-ultramarine-blue font-medium text-arsenic') ||
            'border-b border-cultured font-normal text-spanish-gray'
          }`}
          onClick={handleInformation}
        >
          {t('information_other')}
        </button>
      </div>
      <div>
        {handleSelectBenefit && (
          <div>
            <h2 className="mt-6 mb-4 text-base font-medium text-arsenic sm:text-xl">
              {t('information')} {data?.name}
            </h2>
            <p className="text-sm font-normal text-nickel sm:text-base">
              {data?.description_insurance}
            </p>
            <h2 className="mt-6 mb-4 text-base font-medium text-arsenic sm:text-xl">
              {t('information_global')}
            </h2>
            <InformationDetailNoNone
              label={t('company_insurance')}
              content={data?.company?.long_name}
            />
            <InformationDetailNoNone
              label={t('type_insurance')}
              content={formatCategory(data?.insurance_category?.label)}
            />
            <InformationDetailNoNone
              label={t('name_insurance')}
              content={data?.name}
            />
            <InformationDetailNoNone
              label={t('target_of_insurance')}
              content=<>
                {data?.objective_of_insurance?.map((e) => {
                  return <p key={e.id}>{e.label}.</p>;
                })}
              </>
            />
            <h2 className="mt-6 mb-4 text-base font-medium text-arsenic sm:text-xl">
              {t('key_benefit')}
            </h2>
            <p className="text-sm font-normal text-nickel sm:text-base">
              {data?.key_benefits}
            </p>
          </div>
        )}
        {handleSelectInformation && (
          <div>
            <h2 className="mt-6 mb-4 text-base font-medium text-arsenic sm:text-xl">
              {t('condition_join')}
            </h2>
            <InformationDetail
              label={t('age_join')}
              content=<p>
                {t('from')}
                {data?.terms?.age_eligibility?.from} {t('to')}
                {data?.terms?.age_eligibility?.to} {t('age')}
              </p>
              level={data?.terms?.age_eligibility?.level}
              description={descriptionDetailInsurance.age_eligibility()}
            />
            <InformationDetail
              label={t('deadline_for_deal')}
              content=<p>
                {t('from')} {data?.terms?.deadline_for_deal?.from} {t('to')}
                {data?.terms?.deadline_for_deal?.to} {t('yearDate')}
              </p>
              level={data?.terms?.deadline_for_deal?.level}
              description={descriptionDetailInsurance.age_of_contract_termination()}
            />
            <InformationDetail
              label={t('insurance_minimum_fee')}
              content=<p>
                {numeral(data?.terms?.insurance_minimum_fee?.value).format(
                  '(0,0)'
                )}
                VNĐ
              </p>
              level={data?.terms?.insurance_minimum_fee?.level}
              description={descriptionDetailInsurance.insurance_minimum_fee()}
            />
            <InformationDetail
              content={data?.terms?.deadline_for_payment?.value
                .map((value) => formatTimeFeeDeadline(value))
                .join(', ')}
              label={t('deadline_for_payment')}
              level={data?.terms?.deadline_for_payment?.level}
              description={descriptionDetailInsurance.deadline_for_payment()}
            />
            <InformationDetail
              content={data?.terms?.insured_person?.value
                .map((value) => formatInsuredPerson(value))
                .join(', ')}
              label={t('insured_person_insurance')}
              level={data?.terms?.insured_person?.level}
              description={descriptionDetailInsurance.insured_person()}
            />
            <InformationDetail
              content={data?.terms?.profession?.text
                .map((value) => formatProfession(value))
                .join(', ')}
              label={t('profession')}
              level={data?.terms?.profession?.level}
              description={descriptionDetailInsurance.profession()}
            />
            <InformationDetail
              content={data?.terms?.termination_conditions?.text}
              label={t('termination_conditions')}
              level={data?.terms?.termination_conditions?.level}
              description={descriptionDetailInsurance.termination_conditions()}
            />
            <InformationDetail
              content=<p>
                {t('from')}
                {numeral(data?.terms?.total_sum_insured?.from).format(
                  '(0,0)'
                )}{' '}
                VNĐ {t('to')}
                {numeral(data?.terms?.total_sum_insured?.to).format(
                  '(0,0)'
                )}{' '}
                VNĐ
              </p>
              label={t('total_sum_insurance')}
              level={data?.terms?.total_sum_insured?.level}
              description={descriptionDetailInsurance.total_sum_insured()}
            />
            <InformationDetail
              description={descriptionDetailInsurance.monthly_fee()}
              content=<p>
                {t('from')}
                {numeral(data?.terms?.monthly_fee?.from).format('(0,0)')} VNĐ
                {t('to')}
                {numeral(data?.terms?.monthly_fee?.to).format('(0,0)')} VNĐ
              </p>
              label={t('monthly_fee')}
              level={data?.terms?.monthly_fee?.level}
            />

            <h2 className="mt-6 mb-4 text-base font-medium text-arsenic sm:text-xl">
              {t('additionalRights')}
            </h2>
            <InformationDetail
              description={descriptionDetailInsurance.death_or_disability()}
              label={t('death_or_disability')}
              content={data?.additional_benefits?.death_or_disability?.text}
              level={data?.additional_benefits?.death_or_disability?.level}
            />
            <InformationDetail
              description={descriptionDetailInsurance.serious_illnesses()}
              label={t('serious_illnesses')}
              content={data?.additional_benefits?.serious_illnesses?.text}
              level={data?.additional_benefits?.serious_illnesses?.level}
            />
            <InformationDetail
              description={descriptionDetailInsurance.health_care()}
              label={t('health_care')}
              content={data?.additional_benefits?.health_care?.text}
              level={data?.additional_benefits?.health_care?.level}
            />
            <InformationDetail
              description={descriptionDetailInsurance.investment_benefit()}
              content={data?.additional_benefits?.investment_benefit?.text}
              label={t('investment_benefit')}
              level={data?.additional_benefits?.investment_benefit?.level}
            />
            <InformationDetail
              description={descriptionDetailInsurance.increasing_value_bonus()}
              content={data?.additional_benefits?.increasing_value_bonus?.text}
              label={t('increasing_value_bonus')}
              level={data?.additional_benefits?.increasing_value_bonus?.level}
            />
            <InformationDetail
              description={descriptionDetailInsurance.expiration_benefits()}
              label={t('expiration_benefits')}
              content={data?.additional_benefits?.expiration_benefits?.text}
              level={data?.additional_benefits?.expiration_benefits?.level}
            />
            <InformationDetail
              description={descriptionDetailInsurance.for_child()}
              content={data?.additional_benefits?.for_child?.text}
              level={data?.additional_benefits?.for_child?.level}
              label={t('for_child')}
            />
            <InformationDetail
              description={descriptionDetailInsurance.flexible_and_diverse()}
              level={data?.additional_benefits?.flexible_and_diverse?.level}
              content={data?.additional_benefits?.flexible_and_diverse?.text}
              label={t('flexible_and_diverse')}
            />
            <InformationDetail
              description={descriptionDetailInsurance.termination_benefits()}
              level={data?.additional_benefits?.termination_benefits?.level}
              content={data?.additional_benefits?.termination_benefits?.text}
              label={t('termination_benefits')}
            />
            <h2 className="mt-6 mb-4 text-base font-medium text-arsenic sm:text-xl">
              {t('customerOrientation')}
            </h2>
            <InformationDetail
              description={descriptionDetailInsurance.acceptance_rate()}
              level={data?.customer_orientation?.acceptance_rate?.level}
              content={data?.customer_orientation?.acceptance_rate?.text}
              label={t('acceptance_rate')}
            />
            <InformationDetail
              description={descriptionDetailInsurance.completion_time_deal()}
              level={data?.customer_orientation?.completion_time_deal?.level}
              content={data?.customer_orientation?.completion_time_deal?.text}
              label={t('completion_time_deal')}
            />
            <InformationDetail
              description={descriptionDetailInsurance.end_of_process()}
              level={data?.customer_orientation?.end_of_process?.level}
              content={data?.customer_orientation?.end_of_process?.text}
              label={t('end_of_process')}
            />
            <InformationDetail
              description={descriptionDetailInsurance.withdrawal_time()}
              level={data?.customer_orientation?.withdrawal_time?.level}
              content={data?.customer_orientation?.withdrawal_time?.text}
              label={t('withdrawal_time')}
            />
            <InformationDetail
              description={descriptionDetailInsurance.reception_and_processing_time()}
              level={
                data?.customer_orientation?.reception_and_processing_time?.level
              }
              content={
                data?.customer_orientation?.reception_and_processing_time?.text
              }
              label={t('reception_and_processing_time')}
            />
            <h2 className="mt-6 mb-4 text-base font-medium text-arsenic sm:text-xl">
              {t('productBrochure')}
            </h2>
            <div className="mb-6 last:mb-0">
              {data?.benefits_illustration_table && (
                <Link
                  className="mb-4 flex items-center last:mb-0"
                  href={convertUrl(data?.benefits_illustration_table).link}
                >
                  <Image
                    src="/svg/document.svg"
                    width={24}
                    height={24}
                    alt="Document"
                  />
                  <span className="ml-2.5 text-nickel">
                    {convertUrl(data?.benefits_illustration_table).name}
                  </span>
                </Link>
              )}
              {data?.documentation_url && (
                <Link
                  className="mb-4 flex items-center last:mb-0"
                  href={convertUrl(data?.documentation_url).link}
                >
                  <Image
                    src="/svg/document.svg"
                    width={24}
                    height={24}
                    alt="Document"
                  />
                  <span className="ml-2.5 text-nickel">
                    {convertUrl(data?.documentation_url).name}
                  </span>
                </Link>
              )}
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default DetailLifeInsurance;
