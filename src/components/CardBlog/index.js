import React, { useState } from 'react';
import classNames from 'class-names';
import Image from 'next/image';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import Link from 'next/link';
import Skeleton from 'react-loading-skeleton';

function CardBlog({ outstanding, vertical, locale, isLoading, ...data }) {
  const [isLoaded, setIsLoaded] = useState(false);

  const handleLoadComplete = () => setIsLoaded(true);

  const UserInfo = () => (
    <div className="flex items-center ">
      <div className="flex min-h-[20px] items-center">
        <Image
          src={data.user.avatar_profile_url ?? '/svg/avatar-default.svg'}
          width={20}
          height={20}
          alt={`Avatar-${data.user.first_name}-${data.user.last_name}`}
          className="h-5 rounded-full object-cover"
        />
        <p className="ml-2 text-sm text-nickel">{`${data.user.first_name} ${data.user.last_name}`}</p>
      </div>
      <p className="mx-2 h-4 w-0.5 bg-arsenic" />
      <p className="text-xs font-normal text-nickel">
        {dayjs(new Date(data.created_at)).format('DD MMMM YYYY')}
      </p>
    </div>
  );
  const Category = () => (
    <div className="mb-2 flex items-center lg:my-2">
      <p className="rounded-lg bg-lavender py-1 px-2 text-xs font-normal text-azure">
        {locale === 'vi'
          ? data.blog_category.vn_label
          : data.blog_category.en_label}
      </p>
    </div>
  );

  return (
    <Link
      href={`/blogs/${data.id}`}
      className={classNames('block last:mb-0', {
        'mb-8': !outstanding && !vertical,
        'mb-4': outstanding && !vertical,
      })}
    >
      <article
        className={classNames('flex h-full w-full border-b-platinum', {
          'border-b': (!outstanding && !vertical) || (outstanding && !vertical),
          'pb-4 sm:pb-8': !outstanding && !vertical,
          'pb-4': outstanding && !vertical,
          'flex-col rounded-2xl border': outstanding && vertical,
        })}
      >
        <div
          className={classNames('relative h-[76px] w-[76px]', {
            'sm:h-[144px] sm:w-[144px]': !outstanding && !vertical,
            'sm:h-[92px] sm:w-[92px]': outstanding && !vertical,
            'h-4/6 w-full': outstanding && vertical,
          })}
        >
          {(isLoading || !isLoaded) && <Skeleton width="100%" height="100%" />}
          {!isLoading && (
            <Image
              src={data.banner_image_url}
              fill
              alt={`Image ${data.title}`}
              className={classNames('object-cover', {
                'rounded-2xl':
                  (!outstanding && !vertical) || (outstanding && !vertical),
                'rounded-t-2xl': outstanding && vertical,
                'opacity-0': !isLoaded,
              })}
              onLoadingComplete={handleLoadComplete}
            />
          )}
        </div>
        <div
          className={classNames('w-[calc(100%-104px)]', {
            'sm:w-[calc(100%-156px)]': !outstanding && !vertical,
            'sm:w-[calc(100%-104px)]': outstanding && !vertical,
            'ml-3': (!outstanding && !vertical) || (outstanding && !vertical),
            'p-4': outstanding && vertical,
          })}
        >
          {(isLoading || !isLoaded) && <Skeleton count={6} />}
          {isLoaded && !isLoading && (
            <>
              <h3
                className={classNames(
                  'mb-1 text-base font-bold text-nickel sm:mb-2 sm:text-xl',
                  {
                    'sm:truncate':
                      (!outstanding && !vertical) || (outstanding && !vertical),
                  }
                )}
              >
                {data.title}
              </h3>
              <Category />
              {!outstanding && (
                <p className="lineClamp-2 mb-2 hidden  text-base font-normal text-nickel lg:block">
                  {data.description}
                </p>
              )}
              <UserInfo />
            </>
          )}
        </div>
      </article>
    </Link>
  );
}

CardBlog.propTypes = {
  vertical: PropTypes.bool,
  outstanding: PropTypes.bool,
};

export default CardBlog;
