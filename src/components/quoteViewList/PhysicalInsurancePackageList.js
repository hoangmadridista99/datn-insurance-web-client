import React, { useContext, useEffect, useState } from 'react';
import { useSession } from 'next-auth/react';
import { toast } from 'react-toastify';
import { DataContext } from 'src/store/GlobalState';
import {
  IconDropDown2,
  IconRatingStarTotal,
} from 'components/common/icon/Icon';

import { WindowScrollTop } from 'components/common/WindowScrollTop';
import { BLUE_DARK, INK_LIGHT } from 'src/utils/constants';
import { lockScroll } from 'src/utils/helper';
import RatingMaterialCarInsurance from './RatingMaterialCarInsurance';
import { useTranslation } from 'next-i18next';

function PhysicalInsurancePackageList({
  insurance,
  id,
  setDataInsuranceComparison,
  setHiddenAlertSuccessRating,
}) {
  const { t } = useTranslation('common');

  const session = useSession();
  const { dispatch, state } = useContext(DataContext);
  const { updateDataInsurance } = state;
  const dataInsuranceCheck = updateDataInsurance?.updateDataInsurance;

  const [hiddenDetail, setHiddenDetail] = useState(true);
  const [hiddenRatingInsurance, setHiddenRatingInsurance] = useState(true);

  const [statusChoosing, setStatusChoosing] = useState({
    rating: false,
    details: false,
  });

  useEffect(() => {
    if (!dataInsuranceCheck) {
      setDataInsuranceComparison([]);
      return;
    }
    setDataInsuranceComparison(dataInsuranceCheck);
  }, [dataInsuranceCheck]);

  const handleHiddenDetail = () => {
    if (!session?.data) {
      dispatch({
        type: 'LOGIN',
        loginNoAccount: { login: true },
      });
      toast.error(t('please_login_see_details'));
      lockScroll();
      WindowScrollTop(0);
      return;
    }
    setHiddenDetail(!hiddenDetail);
    setHiddenRatingInsurance(true);
  };

  const handleHiddenRatingInsurance = () => {
    if (!session?.data) {
      dispatch({
        type: 'LOGIN',
        loginNoAccount: { login: true },
      });
      toast.error(t('please_login_see_rating'));
      lockScroll();
      WindowScrollTop(0);
      return;
    }
    setHiddenRatingInsurance(!hiddenRatingInsurance);
    setHiddenDetail(true);
  };

  return (
    <>
      <div className="mt-5 flex justify-between gap-1 lg:gap-2">
        <div className="flex xl:w-1/2">
          <button
            type="button"
            onClick={handleHiddenRatingInsurance}
            className={`gap-4 px-2 xl:px-5 ${
              !hiddenRatingInsurance
                ? 'text-ultramarine-blue'
                : 'text-spanish-gray'
            }`}
            onMouseOver={() =>
              setStatusChoosing({ ...statusChoosing, rating: true })
            }
            onMouseOut={() =>
              setStatusChoosing({ ...statusChoosing, rating: false })
            }
          >
            <div className="flex items-center gap-1 text-base font-normal text-spanish-gray">
              <p
                className={`${
                  !hiddenRatingInsurance || statusChoosing.rating
                    ? 'text-ultramarine-blue'
                    : 'text-arsenic'
                } text-base`}
              >
                {insurance?.rating_scores || 0}
              </p>
              <IconRatingStarTotal />
              <p className="hidden xl:block">{`(${
                insurance?.total_rating || 0
              } ${t('rating')})`}</p>
              <div
                className={`${
                  !hiddenRatingInsurance && '-rotate-180'
                } origin-center duration-200`}
              >
                <IconDropDown2
                  stroke={`${
                    !hiddenRatingInsurance || statusChoosing.rating
                      ? BLUE_DARK
                      : INK_LIGHT
                  } `}
                />
              </div>
            </div>
          </button>
        </div>
        <div className="flex justify-end xl:w-1/2">
          <button
            type="button"
            onClick={handleHiddenDetail}
            className={`flex gap-1 lg:px-2 xl:gap-4 xl:px-3 ${
              !hiddenDetail ? 'text-arsenic' : 'text-spanish-gray'
            }`}
            onMouseOver={() =>
              setStatusChoosing({ ...statusChoosing, details: true })
            }
            onMouseOut={() =>
              setStatusChoosing({ ...statusChoosing, details: false })
            }
          >
            <p
              className={`${
                !hiddenDetail || statusChoosing.details
                  ? 'text-ultramarine-blue'
                  : 'text-arsenic'
              } flex w-full text-sm xl:text-base`}
            >
              <span className="hidden xl:block"> {t('seeDetails')}</span>
              <span className="xl:hidden">{t('details')}</span>
            </p>
            <div
              className={`${
                !hiddenDetail && '-rotate-180'
              } origin-center duration-200 `}
            >
              <IconDropDown2
                stroke={`${
                  !hiddenDetail || statusChoosing.details
                    ? BLUE_DARK
                    : INK_LIGHT
                } `}
              />
            </div>
          </button>
        </div>
      </div>

      {!hiddenDetail && (
        <div className="mt-6 text-sm sm:text-base">
          <p className="mb-4 font-bold sm:font-medium">
            {t('information')} {insurance?.company_name}
          </p>
          <p className="font-normal">{insurance?.insurance_description}</p>
        </div>
      )}
      {!hiddenRatingInsurance && (
        <RatingMaterialCarInsurance
          setHiddenAlertSuccessRating={setHiddenAlertSuccessRating}
          id={id}
        />
      )}
    </>
  );
}

export default PhysicalInsurancePackageList;
