import React from 'react';
import numeral from 'numeral';

import ButtonPrimary from 'components/common/ButtonPrimary';
import { useTranslation } from 'next-i18next';
import PhysicalInsurancePackageList from './PhysicalInsurancePackageList';
import { useRouter } from 'next/router';

function InformationInsurance({
  dataInsurance,
  setDataInsuranceComparison,
  dataInsuranceComparison,
  handleCal,
  idInsurance,
  totalRatingInsurance,
  setHiddenAlertSuccessRating,
}) {
  const router = useRouter();
  const handleSwitchingPage = () => {
    router.push({
      pathname: '/insuranceCarMaterial',
      query: idInsurance,
    });
  };
  const { t } = useTranslation('insuranceLocale');
  return (
    <div className="flex w-full flex-col gap-6">
      {dataInsurance?.map((insurance) => (
        <div
          key={insurance?.physical_setting_id}
          className="w-full rounded-2xl  bg-white p-6 shadow-listInsurance"
        >
          <div className="flex flex-col-reverse justify-between border-b border-platinum sm:flex-row">
            <div className="mb-5 flex w-full flex-col gap-4 sm:flex-row sm:gap-0">
              <div className="flex w-full items-center gap-4 sm:w-3/4">
                <div className="h-13 w-13 sm:h-16 sm:w-16">
                  <img
                    height={64}
                    width={64}
                    className="bg-none"
                    src={insurance?.company_logo_url}
                    alt="logoCompany"
                  />
                </div>

                <div>
                  <p className="text-xl font-bold text-arsenic sm:text-2xl">
                    {insurance?.company_name}
                  </p>
                  <p className="flex items-center gap-1 text-sm font-normal text-nickel sm:text-base sm:font-medium">
                    <span className="text-base font-bold text-ultramarine-blue sm:text-2xl">
                      {numeral(
                        handleCal(
                          insurance?.cost_of_purchasing,
                          insurance?.choosing_service_center,
                          insurance?.component_vehicle_theft,
                          insurance?.no_depreciation_cost,
                          insurance?.water_damage,
                          insurance?.insured_for_each_person
                        )
                      ).format('(0,0)')}
                      VNĐ
                    </span>
                    <span className="text-sm font-normal text-silver sm:text-base">
                      /{t('year')}
                    </span>
                  </p>
                </div>
              </div>
              <div
                className="flex w-full items-end sm:w-1/4"
                onClick={handleSwitchingPage}
              >
                <ButtonPrimary
                  cases={'medium'}
                  title={t('register')}
                  widthBtn="w-full"
                />
              </div>
            </div>
          </div>

          <PhysicalInsurancePackageList
            setHiddenAlertSuccessRating={setHiddenAlertSuccessRating}
            insurance={insurance}
            id={insurance?.physical_setting_id}
            setDataInsuranceComparison={setDataInsuranceComparison}
            dataInsuranceComparison={dataInsuranceComparison}
            totalRatingInsurance={totalRatingInsurance}
          />
        </div>
      ))}
    </div>
  );
}

export default InformationInsurance;
