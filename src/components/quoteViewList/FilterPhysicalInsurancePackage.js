import React, { useContext } from 'react';
import { useForm } from 'react-hook-form';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { DataContext } from 'src/store/GlobalState';
import { yupResolver } from '@hookform/resolvers/yup';
import { schemaFilterHealthInsurance } from 'src/utils/error';
import { useTranslation } from 'next-i18next';
import { checkStatusFormType, unlockScroll } from 'src/utils/helper';
import { TextFieldBlur } from 'components/common/TextField';
import {
  LIST_BENEFIT_INSURANCE_MATERIAL,
  LIST_TYPE_INSURANCE,
  additionalBenefits,
  advancedBenefits,
  carInformationDetails,
  transportationBusiness,
} from 'src/utils/constants';

function FilterPhysicalInsurancePackage({
  data,
  setHiddenFilter,
  setCurrentPage,
  formType,
  insuranceList,
  setCurrentFeeValue,
}) {
  const { t } = useTranslation(['common']);
  const router = useRouter();
  const { dispatch } = useContext(DataContext);
  const { register, handleSubmit } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schemaFilterHealthInsurance(t)),
    defaultValues: data,
  });

  const handleFilterInsurance = async (values) => {
    setCurrentPage(1);

    try {
      router.push({ pathname: router.pathname, query: values }, undefined, {
        shallow: false,
      });
      setHiddenFilter(false);
    } catch (error) {
      console.log(
        '🚀 ~ file: FilterLifeInsurance.js:59 ~ handleFilterInsurance ~ err:',
        error
      );
    }
    dispatch({ type: 'LOADING', payload: { loading: false } });
  };

  const handleCloseFilter = () => {
    setHiddenFilter(false);
    unlockScroll();
  };

  const handleCalculate = (e) => {
    setCurrentFeeValue((prev) => ({
      ...prev,
      [e.target.id]: e.target.checked,
    }));
  };
  return (
    <div>
      <form onSubmit={handleSubmit(handleFilterInsurance)}>
        <div className="bg-white p-6 shadow-listInsurance lg:rounded-2xl">
          <div className="mb-4 flex items-center justify-between border-b pb-6 text-2xl font-bold text-nickel">
            <p>{t('vehicleInformation')}</p>
            <div className="lg:hidden">
              <button type="button" onClick={handleCloseFilter}>
                <Image
                  src="/svg/close-circle.svg"
                  height={24}
                  width={24}
                  alt="close"
                />
              </button>
            </div>
          </div>
          <div className="mb-8 space-y-4 text-base font-normal">
            <p className="font-bold">{t('insuranceAvailable')}</p>
            {LIST_TYPE_INSURANCE(t)?.map((item) => (
              <div className="flex items-center gap-4" key={item?.value}>
                <input
                  type="radio"
                  id="typeInsuranceAvailable"
                  {...register('typeInsuranceAvailable')}
                  checked={checkStatusFormType(formType) || false}
                  disabled
                  className="h-5 w-5"
                />
                <label htmlFor={item?.value}>{item?.label}</label>
              </div>
            ))}
          </div>
          <div className="mb-8 space-y-4 text-base font-normal">
            <p className="font-bold">{t('isTransportationBusiness')}</p>
            {transportationBusiness(t, insuranceList)?.map((item) => (
              <div className="flex items-center gap-4" key={item?.id}>
                <input
                  type="radio"
                  name="transportationBusiness"
                  id={item?.id}
                  checked={item?.value === item?.defaultValue}
                  disabled
                  className="h-5 w-5"
                />
                <label htmlFor={item?.id}>{item?.label}</label>
              </div>
            ))}
          </div>
          {carInformationDetails(t, insuranceList)?.map((item) => (
            <div className="mb-8" key={item?.id}>
              <p className="mb-1.5">{item?.label}</p>
              <TextFieldBlur
                disabled
                value={item?.value}
                className="text-platinum"
              />
            </div>
          ))}
          <p className="mb-4 text-base font-bold">{t('benefit_base')}</p>
          {LIST_BENEFIT_INSURANCE_MATERIAL(t)?.map((item) => (
            <div key={item.id} className="mb-4 flex items-center gap-4">
              <input
                type="checkbox"
                id={item?.id}
                {...register('basicBenefitsList', { required: true })}
                className="h-5 w-5"
                checked={insuranceList?.[item.value] || false}
                value={insuranceList?.[item.value]}
                disabled
              />

              <label
                htmlFor={item?.id}
                className="text-base font-normal text-arsenic"
              >
                {item?.label}
              </label>
            </div>
          ))}
          <p className="mb-4 text-base font-bold">{t('benefit_hard')}</p>
          {advancedBenefits?.map((item) => (
            <div key={item.id} className="mb-4 flex items-center gap-4">
              <div className="h-5 w-5">
                <input
                  type="checkbox"
                  id={item?.id}
                  {...register('advancedBenefits', { required: true })}
                  className="h-5 w-5"
                  onChange={handleCalculate}
                />
              </div>

              <label
                htmlFor={item?.id}
                className="text-base font-normal text-arsenic"
              >
                {item?.label}
              </label>
            </div>
          ))}
          <p className="mb-4 text-base font-bold">{t('benefit_add')}</p>
          {additionalBenefits?.map((item) => (
            <div key={item.id} className="mb-4 flex items-center gap-4">
              <div>
                <input
                  type="checkbox"
                  id={item?.id}
                  {...register('additionalBenefits', { required: true })}
                  className="h-5 w-5"
                  onChange={handleCalculate}
                />
              </div>
              <label
                htmlFor={item?.id}
                className="text-base font-normal text-arsenic"
              >
                {item?.label}
              </label>
            </div>
          ))}
        </div>
      </form>
    </div>
  );
}

export default FilterPhysicalInsurancePackage;
