import Image from 'next/image';
import React from 'react';
import { STEP } from 'src/utils/constants';

export default function YearDate({
  setYearValue,
  yearValue,
  setValue,
  setError,
  setErrorValueNoNumber,
  id,
  listYearShow,
  setNumberYear,
  numberYear,
  numberYearMax,
}) {
  const handleDownYear = () => {
    setNumberYear(numberYear - STEP);
  };
  const handleUpYear = () => {
    setNumberYear(numberYear + STEP);
  };
  const handleChoiceYear = (year) => {
    setValue(id, year);
    setYearValue(year);
    setError(id, { message: '' });
    setErrorValueNoNumber({ errorValueYearOfBirth: '' });
  };

  return (
    <>
      <div className="z-50 h-full w-full rounded-md border border-chinese-silver bg-white p-4 shadow-formFilter">
        <div className="flex justify-between">
          <p className="text-base font-bold text-nickel">
            {listYearShow[0]} - {listYearShow.slice(-1)}
          </p>
          <div className="ml-5 mb-4 flex gap-2">
            <button
              type="button"
              onClick={handleDownYear}
              disabled={numberYear <= 2}
              className="hover:bg-slate-200"
            >
              <Image
                src="/svg/arrow-left-1.svg"
                width={20}
                height={20}
                alt="arrowLeft"
              />
            </button>

            <button
              type="button"
              onClick={handleUpYear}
              disabled={numberYear >= numberYearMax}
              className="rotate-180 hover:bg-slate-200"
            >
              <Image
                src="/svg/arrow-left-1.svg"
                width={20}
                height={20}
                alt="arrowLeft"
              />
            </button>
          </div>
        </div>
        <div className="grid grid-cols-4 gap-2.5">
          {listYearShow.map((year) => (
            <button
              type="button"
              key={year}
              onClick={() => handleChoiceYear(year)}
              className={`${
                yearValue === year
                  ? 'bg-ultramarine-blue text-cultured'
                  : 'text-nickel'
              } rounded px-2 py-1 text-base font-normal hover:bg-ultramarine-blue hover:text-cultured`}
            >
              {year}
            </button>
          ))}
        </div>
      </div>
    </>
  );
}
