import InformationDetail from 'components/common/listInsurance/InformationDetail';
import InformationDetailNoNone from 'components/common/listInsurance/InformationDetailNoNone';
import numeral from 'numeral';
import React from 'react';
import { useTranslation } from 'next-i18next';
import {
  descriptionDetailInsurance,
  descriptionDetailInsuranceHealth,
} from 'src/utils/constants';
import {
  formatCategory,
  formatCustomerSegment,
  formatInsuredPerson,
  formatProfession,
  formatTimeFeeDeadline,
} from 'src/utils/helper';

export default function Overview({ data }) {
  const { t } = useTranslation('insuranceDetailsComparison');

  const cssTitle = 'mt-6 mb-4 text-xl font-medium text-arsenic';
  return (
    <div>
      <h2 className={cssTitle}>
        {t('information_to')} {data?.name}
      </h2>
      <p className="text-sm font-normal text-nickel sm:text-base">
        {data?.description_insurance}
      </p>
      <h2 className={cssTitle}>{t('generalInformation')}</h2>
      <InformationDetailNoNone
        label={t('business_insurance')}
        content={data?.company?.long_name}
      />
      <InformationDetailNoNone
        label={t('type_insurance')}
        content={formatCategory(data?.insurance_category?.label)}
      />
      <InformationDetailNoNone
        label={t('name_insurance')}
        content={data?.name}
      />

      <h2 className={cssTitle}>{t('condition_part')}</h2>
      <InformationDetail
        label={t('age_eligibility')}
        content=<p>
          {t('from')} {data?.terms?.age_eligibility?.from} {t('to')}
          {data?.terms?.age_eligibility?.to} {t('age')}
        </p>
        level={data?.terms?.age_eligibility?.level}
        description={descriptionDetailInsurance.age_eligibility()}
      />
      <InformationDetail
        label={t('deadline_for_deal')}
        content=<p>
          {t('from')} {data?.terms?.deadline_for_deal?.from} {t('to')}
          {data?.terms?.deadline_for_deal?.to} {t('year')}
        </p>
        level={data?.terms?.deadline_for_deal?.level}
        description={descriptionDetailInsurance.age_of_contract_termination()}
      />
      <InformationDetail
        label={t('insurance_minimum_fee')}
        content=<p>
          {numeral(data?.terms?.insurance_minimum_fee?.value).format('(0,0)')}
          VNĐ
        </p>
        level={data?.terms?.insurance_minimum_fee?.level}
        description={descriptionDetailInsurance.insurance_minimum_fee()}
      />
      <InformationDetail
        content={
          data?.terms?.deadline_for_payment?.value &&
          data?.terms?.deadline_for_payment?.value
            .map((value) => formatTimeFeeDeadline(value))
            .join(', ')
        }
        label={t('deadline_for_payment')}
        level={data?.terms?.deadline_for_payment?.level}
        description={descriptionDetailInsurance.deadline_for_payment()}
      />
      <InformationDetail
        content={
          data?.terms?.insured_person?.value &&
          data?.terms?.insured_person?.value
            .map((value) => formatInsuredPerson(value))
            .join(', ')
        }
        label={t('insured_person')}
        level={data?.terms?.insured_person?.level}
        description={descriptionDetailInsurance.insured_person()}
      />
      <InformationDetail
        content={
          data?.terms?.profession?.text &&
          data?.terms?.profession?.text
            .map((value) => formatProfession(value))
            .join(', ')
        }
        label={t('profession')}
        level={data?.terms?.profession?.level}
        description={descriptionDetailInsurance.profession()}
      />
      <InformationDetail
        content={
          data?.terms?.customer_segment?.text &&
          data?.terms?.customer_segment?.text
            .map((value) => formatCustomerSegment(value))
            .join(', ')
        }
        label={t('customer_segment')}
        level={data?.terms?.customer_segment?.level}
        description={descriptionDetailInsuranceHealth.customer_segment()}
      />
      <InformationDetail
        content=<p>
          {t('from')}&nbsp;
          {numeral(data?.terms?.total_sum_insured?.from).format('(0,0)')} VNĐ
          &nbsp;{t('to')}&nbsp;
          {numeral(data?.terms?.total_sum_insured?.to).format('(0,0)')} VNĐ
        </p>
        label={t('total_sum_insured')}
        level={data?.terms?.total_sum_insured?.level}
        description={descriptionDetailInsurance.total_sum_insured()}
      />
      <InformationDetail
        description={descriptionDetailInsurance.monthly_fee()}
        content=<p>
          {t('from')}&nbsp;
          {numeral(data?.terms?.monthly_fee?.from).format('(0,0)')} VNĐ &nbsp;
          {t('to')}&nbsp;
          {numeral(data?.terms?.monthly_fee?.to).format('(0,0)')} VNĐ
        </p>
        label={t('monthly_fee')}
        level={data?.terms?.monthly_fee?.level}
      />

      <h2 className={cssTitle}>{t('customerOrientation')}</h2>
      <InformationDetail
        content={data?.customer_orientation?.insurance_scope?.text}
        label={t('insurance_scope')}
        level={data?.customer_orientation?.insurance_scope?.level}
        description={descriptionDetailInsurance.termination_conditions()}
      />
      <InformationDetail
        content={data?.customer_orientation?.waiting_period?.text}
        label={t('waiting_period')}
        level={data?.customer_orientation?.waiting_period?.level}
        description={descriptionDetailInsurance.termination_conditions()}
      />
      <InformationDetail
        content={data?.customer_orientation?.compensation_process?.text}
        label={t('compensation_process')}
        level={data?.customer_orientation?.compensation_process?.level}
        description={descriptionDetailInsurance.termination_conditions()}
      />
      <InformationDetail
        content={
          data?.customer_orientation?.reception_and_processing_time?.text
        }
        label={t('reception_and_processing_time')}
        level={data?.customer_orientation?.reception_and_processing_time?.level}
        description={descriptionDetailInsurance.termination_conditions()}
      />
    </div>
  );
}
