import InformationDetail from 'components/common/listInsurance/InformationDetail';
import React from 'react';
import { descriptionDetailInsuranceHealth } from 'src/utils/constants';
import { useTranslation } from 'next-i18next';

export default function Outpatient({ data }) {
  const { t } = useTranslation('common');

  return (
    <div className="mt-6">
      <InformationDetail
        content={data?.outpatient?.examination_and_treatment?.text}
        label={t('examination_and_treatment')}
        level={data?.outpatient?.examination_and_treatment?.level}
        description={descriptionDetailInsuranceHealth.examination_and_treatment()}
      />
      <InformationDetail
        content={data?.outpatient?.testing_and_diagnosis?.text}
        label={t('testing_and_diagnosis')}
        level={data?.outpatient?.testing_and_diagnosis?.level}
        description={descriptionDetailInsuranceHealth.testing_and_diagnosis()}
      />
      <InformationDetail
        content={data?.outpatient?.home_care?.text}
        label={t('home_care')}
        level={data?.outpatient?.home_care?.level}
        description={descriptionDetailInsuranceHealth.home_care()}
      />
      <InformationDetail
        content={data?.outpatient?.due_to_accident?.text}
        label={t('due_to_accident')}
        level={data?.outpatient?.due_to_accident?.level}
        description={descriptionDetailInsuranceHealth.due_to_accident()}
      />
      <InformationDetail
        content={data?.outpatient?.due_to_illness?.text}
        label={t('due_to_illness')}
        level={data?.outpatient?.due_to_illness?.level}
        description={descriptionDetailInsuranceHealth.due_to_illness()}
      />
      <InformationDetail
        content={data?.outpatient?.due_to_cancer?.text}
        label={t('due_to_cancer')}
        level={data?.outpatient?.due_to_cancer?.level}
        description={descriptionDetailInsuranceHealth.due_to_cancer()}
      />
      <InformationDetail
        content={data?.outpatient?.restore_functionality?.text}
        label={t('restore_functionality')}
        level={data?.outpatient?.restore_functionality?.level}
        description={descriptionDetailInsuranceHealth.restore_functionality()}
      />
    </div>
  );
}
