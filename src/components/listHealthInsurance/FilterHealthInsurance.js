import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import Image from 'next/image';
import ButtonPrimary from 'components/common/ButtonPrimary';
import InputChoiceYearDate from 'components/common/InputChoiceYearDate';
import {
  LIST_INSURED_PERSON,
  LIST_ROOM_BED,
  LIST_WORKS,
  STEP,
} from 'src/utils/constants';
import { yupResolver } from '@hookform/resolvers/yup';
import { schemaFilterHealthInsurance } from 'src/utils/error';
import { useTranslation } from 'next-i18next';
import { unlockScroll } from 'src/utils/helper';
import SelectFilter from 'components/common/SelectFilter';
const currentYear = new Date().getFullYear();

function FilterHealthInsurance({
  data,
  setHiddenFilter,
  setCurrentPage,
  // setIsSuggestion,
  getListInsurance,
  currentPage,
  sort,
  dispatch,
}) {
  const { is_obstetric, is_dental, ...defaultValues } = data;

  const { t } = useTranslation(['common']);
  const {
    register,
    handleSubmit,
    setError,
    setValue,
    formState: { errors },
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schemaFilterHealthInsurance(t)),
    values: defaultValues,
  });
  const dataProfessionDefault = data?.profession;

  const [defaultValue, setDefaultValue] = useState(dataProfessionDefault);
  const [errorValueNoNumber, setErrorValueNoNumber] = useState([]);
  const [yearValue, setYearValue] = useState();
  const [valueProfession, setValueProfession] = useState(LIST_WORKS(t)[0]);

  const onChangeYearOfBirth = (event) => {
    const {
      target: { value },
    } = event;

    setValue('year_of_birth', value);
    setYearValue(value);
    if (value > currentYear || value < currentYear - 100) {
      setError('year_of_birth', { message: '' });
      setErrorValueNoNumber({
        errorValueYearOfBirth: t('error_year'),
      });
      return;
    }
    setError('year_of_birth', { message: '' });
    setErrorValueNoNumber({
      errorValueYearOfBirth: '',
    });
  };

  const handleFilterInsurance = async (values) => {
    setCurrentPage(1);
    const isPassCheck = onCheckValueForm(values);
    if (!isPassCheck) return;

    try {
      getListInsurance(values, currentPage, sort).finally(() => {
        dispatch({ type: 'LOADING', payload: { loading: false } });
      });
      setHiddenFilter(false);
    } catch (error) {
      console.log(
        '🚀 ~ file: FilterLifeInsurance.js:59 ~ handleFilterInsurance ~ err:',
        error
      );
    }
  };

  const onCheckValueForm = ({
    room_type,
    insured_person,
    year_of_birth,
    profession,
  }) => {
    if (!room_type || room_type.length === 0) {
      setError('room_type', { message: t('error_room_type') });
    }

    if (!year_of_birth) {
      setError('year_of_birth', { message: t('error_year_of_birth') });
    }
    if (!profession) {
      setError('profession', { message: t('error_profession') });
    }
    if (!insured_person || insured_person.length === 0) {
      setError('insured_person', {
        message: t('error_insured_person'),
      });
    }

    if (
      room_type.length > 0 &&
      insured_person.length > 0 &&
      currentYear - 100 < parseInt(year_of_birth) &&
      parseInt(year_of_birth) < currentYear &&
      profession
    ) {
      return true;
    }
  };

  const handleCloseFilter = () => {
    setHiddenFilter(false);
    unlockScroll();
  };

  const handleChoseProfession = (e) => {
    setDefaultValue(e.value);
    setValueProfession(e);
    setValue('profession', e.value);
    setError('profession', { message: '' });
  };

  const [numberYear, setNumberYear] = useState(48);
  const listYearOfBirth = [
    ...Array.from({ length: 106 }, (_, i) => currentYear - i).reverse(),
  ];
  const listYearShow = listYearOfBirth?.slice(numberYear, numberYear + STEP);

  return (
    <div>
      <form onSubmit={handleSubmit(handleFilterInsurance)}>
        <div className="bg-white p-6 shadow-listInsurance lg:rounded-2xl">
          <div className="flex items-center justify-between border-b pb-6 text-2xl font-bold text-nickel">
            <p>{t('information_search')}</p>
            <div className="lg:hidden">
              <button type="button" onClick={handleCloseFilter}>
                <Image
                  src="/svg/close-circle.svg"
                  height={24}
                  width={24}
                  alt="close"
                />
              </button>
            </div>
          </div>

          <>
            <p className="mb-2 pt-4 text-base font-bold text-arsenic">
              {t('insured_person')}
            </p>
            {LIST_INSURED_PERSON(t)?.map((values) => (
              <div key={values.value} className="mb-4 flex items-center gap-4">
                <input
                  type="checkbox"
                  id="insured_person"
                  {...register('insured_person')}
                  className="h-5 w-5"
                  value={values?.value}
                />
                <p className="text-base font-normal text-arsenic">
                  {values?.label}
                </p>
              </div>
            ))}
            {errors?.insured_person && (
              <p className=" text-red-500 ">
                {errors?.insured_person?.message}
              </p>
            )}
          </>

          <InputChoiceYearDate
            yearValue={yearValue}
            register={register}
            errorValueNoNumber={errorValueNoNumber}
            errors={errors}
            onChangeYearOfBirth={onChangeYearOfBirth}
            setValue={setValue}
            setYearValue={setYearValue}
            setError={setError}
            setErrorValueNoNumber={setErrorValueNoNumber}
            title={t('year')}
            placeholder={t('choice_year')}
            id="year_of_birth"
            listYearShow={listYearShow}
            setNumberYear={setNumberYear}
            numberYear={numberYear}
            numberYearMax={96}
          />

          <div className="mb-8">
            <p className="mb-4 text-base font-bold text-arsenic">
              {t('profession')}
            </p>
            <SelectFilter
              onChange={handleChoseProfession}
              selected={valueProfession}
              options={LIST_WORKS(t)}
              defaultValue={defaultValue}
              errors={errors?.profession?.message}
            />
            {errors?.profession && (
              <p className=" text-red-500 ">{errors?.profession?.message}</p>
            )}
          </div>

          <div className="mb-8">
            <h2 className="mb-4 text-base font-bold text-arsenic">
              {t('room_type')}
            </h2>

            {LIST_ROOM_BED(t).map((values) => (
              <div key={values.value} className="mb-4 flex items-center gap-4">
                <input
                  type="checkbox"
                  id="room_type"
                  {...register('room_type', { required: true })}
                  className="h-5 w-5"
                  value={values?.value}
                />
                <p className="text-base font-normal text-arsenic">
                  {values?.label}
                </p>
              </div>
            ))}
            {errors?.room_type && (
              <p className=" text-red-500 ">{errors?.room_type?.message}</p>
            )}
          </div>

          <div className="mb-8">
            <h2 className="mb-4 text-base font-bold text-arsenic">
              {t('benefit_add')}
            </h2>

            <div className="mb-4 flex items-center gap-4">
              <input
                type="checkbox"
                id="is_dental"
                {...register('is_dental', { required: true })}
                className="h-5 w-5"
                defaultChecked={JSON.parse(is_dental)}
              />
              <p className="text-base font-normal text-arsenic">
                {t('is_dental')}
              </p>
            </div>

            <div className="mb-4 flex items-center gap-4">
              <input
                type="checkbox"
                id="is_obstetric"
                {...register('is_obstetric', { required: true })}
                defaultChecked={JSON.parse(is_obstetric)}
                className="h-5 w-5"
              />
              <p className="text-base font-normal text-arsenic">
                {t('is_obstetric')}
              </p>
            </div>
            {errors?.is_dental && (
              <p className=" text-red-500 ">{errors?.is_dental?.message}</p>
            )}
          </div>

          <ButtonPrimary
            type={'submit'}
            cases={'large'}
            title={t('recalculation')}
          />
        </div>
      </form>
    </div>
  );
}

export default FilterHealthInsurance;
