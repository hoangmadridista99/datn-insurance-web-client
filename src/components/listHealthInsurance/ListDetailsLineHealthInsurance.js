import DetailsLine from 'components/common/DetailsLine';
import {
  IconTickGood,
  IconTickGoodBetter,
  IconTickInformation,
  IconTickNone,
} from 'components/common/icon/Icon';
import numeral from 'numeral';
import React from 'react';
import { useTranslation } from 'next-i18next';
import { descriptionDetailInsuranceHealth } from 'src/utils/constants';
import { formatRoomType } from 'src/utils/helper';

export default function ListDetailsLineHealthInsurance({ insurance }) {
  const { t } = useTranslation(['common']);

  return (
    <div className="flex flex-col gap-2 py-2">
      <DetailsLine
        icon={<IconTickGoodBetter />}
        toolTipContent={descriptionDetailInsuranceHealth.room_type()}
        label={t('type_room')}
        value={insurance?.inpatient?.room_type?.values
          .map((value) => formatRoomType(value))
          .join(', ')}
      />

      <DetailsLine
        icon={<IconTickGood />}
        toolTipContent={descriptionDetailInsuranceHealth.for_hospitalization()}
        label={t('for_hospitalization')}
        value={`${t('max')} ${numeral(
          insurance?.inpatient?.for_hospitalization?.value
        ).format('(0,0)')} VNĐ/${t('day')}`}
      />

      <DetailsLine
        icon={<IconTickInformation />}
        toolTipContent={descriptionDetailInsuranceHealth.for_cancer()}
        label={t('for_cancer')}
        value={insurance?.inpatient?.for_cancer?.text}
      />

      <DetailsLine
        icon={insurance?.dental ? <IconTickGoodBetter /> : <IconTickNone />}
        toolTipContent={descriptionDetailInsuranceHealth.examination_and_diagnosis()}
        label={t('dental')}
        value={`${insurance?.dental ? t('yes') : t('no')}`}
      />

      <DetailsLine
        icon={insurance?.obstetric ? <IconTickGoodBetter /> : <IconTickNone />}
        toolTipContent={descriptionDetailInsuranceHealth.give_birth_normally()}
        label={t('obstetric')}
        value={`${insurance?.obstetric ? t('yes') : t('no')}`}
      />
    </div>
  );
}
