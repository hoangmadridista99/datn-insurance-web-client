import React from 'react';
import { useTranslation } from 'next-i18next';

export default function LinkedList({ data }) {
  const { t } = useTranslation('common');

  const listHospitals = data.hospitals;

  return (
    <div className="mt-6">
      <div className="flex gap-6 rounded-t-md border bg-cultured px-2 py-4 text-center text-base font-bold text-nickel">
        <p className="w-1/4">{t('name_hospital')}</p>
        <p className="w-2/5">{t('address')}</p>
        <p className="w-1/6">{t('Province/City')}</p>
        <p className="w-1/6">{t('District')}</p>
      </div>
      <div className="rounded-b-md border-x border-b border-platinum px-2">
        {listHospitals.map((items) => (
          <div
            key={items.id}
            className="flex gap-6 border-b border-cultured py-4 text-center text-base text-nickel"
          >
            <p className="w-1/4">{items?.name}</p>
            <p className="w-2/5">{items?.address}</p>
            <p className="w-1/6">{items?.district}</p>
            <p className="w-1/6">{items?.province}</p>
          </div>
        ))}
      </div>
    </div>
  );
}
