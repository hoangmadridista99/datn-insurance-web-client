import React, { useEffect, useState } from 'react';

import { getInsuranceIdAPI } from 'pages/api/insurance';
import Overview from './Overview';
import Inpatient from './Inpatient';
import Outpatient from './Outpatient';
import DentalObstetric from './DentalObstetric';
import LinkedList from './LinkedList';
import { useTranslation } from 'next-i18next';
import { LIST_SELECT_TITLES } from 'src/utils/constants';

function DetailHealthInsurance({ id }) {
  const { t } = useTranslation('common');

  const [handleSelectTitles, setHandleSelectTitles] = useState(t('over_view'));
  const [detailInsurance, setDetailInsurance] = useState();

  const getInsuranceIds = async () => {
    try {
      const response = await getInsuranceIdAPI(id);

      setDetailInsurance(response?.data);
    } catch (error) {
      console.log('🚀 ~ file: index.js:17 ~ getInsuranceIds ~ error:', error);
    }
  };

  const DetailByTitle = () => {
    switch (handleSelectTitles) {
      case t('boarding'):
        return <Inpatient data={detailInsurance} />;
      case t('outpatient'):
        return <Outpatient data={detailInsurance} />;
      case t('other_benefits'):
        return <DentalObstetric data={detailInsurance} />;
      case t('linked_list'):
        return <LinkedList data={detailInsurance} />;
      default:
        return <Overview data={detailInsurance} />;
    }
  };

  useEffect(() => {
    getInsuranceIds();
  }, [id]);

  const selectTitles = (list) => {
    setHandleSelectTitles(list);
  };
  return (
    <div className="pt-4">
      <div className="flex">
        {LIST_SELECT_TITLES(t).map((list) => (
          <button
            key={list?.id}
            type="button"
            className={`w-1/2 py-2 text-base hover:bg-slate-100 ${
              list?.value === handleSelectTitles
                ? 'border-b-2 border-ultramarine-blue font-medium text-arsenic'
                : 'border-b border-cultured font-normal text-spanish-gray'
            }`}
            onClick={() => selectTitles(list.value)}
          >
            {list?.value}
          </button>
        ))}
      </div>
      <div>
        <DetailByTitle />
      </div>
    </div>
  );
}

export default DetailHealthInsurance;
