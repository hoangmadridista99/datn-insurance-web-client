import InformationDetail from 'components/common/listInsurance/InformationDetail';
import React from 'react';
import { descriptionDetailInsuranceHealth } from 'src/utils/constants';
import { useTranslation } from 'next-i18next';

const cssTitle = 'mt-6 mb-4 text-xl font-medium text-arsenic';

export default function DentalObstetric({ data }) {
  const { t } = useTranslation('common');

  return (
    <div>
      <h2 className={cssTitle}>{t('dental')}</h2>
      <InformationDetail
        content={data?.dental?.examination_and_diagnosis?.text}
        label={t('examination_and_diagnosis')}
        level={data?.dental?.examination_and_diagnosis?.level}
        description={descriptionDetailInsuranceHealth.examination_and_diagnosis()}
      />
      <InformationDetail
        content={data?.dental?.gingivitis?.text}
        label={t('gingivitis')}
        level={data?.dental?.gingivitis?.level}
        description={descriptionDetailInsuranceHealth.gingivitis()}
      />
      <InformationDetail
        content={data?.dental?.xray_and_diagnostic_imaging?.text}
        label={t('xray_and_diagnostic_imaging')}
        level={data?.dental?.xray_and_diagnostic_imaging?.level}
        description={descriptionDetailInsuranceHealth.xray_and_diagnostic_imaging()}
      />
      <InformationDetail
        content={data?.dental?.filling_teeth_basic?.text}
        label={t('filling_teeth_basic')}
        level={data?.dental?.filling_teeth_basic?.level}
        description={descriptionDetailInsuranceHealth.filling_teeth_basic()}
      />
      <InformationDetail
        content={data?.dental?.root_canal_treatment?.text}
        label={t('root_canal_treatment')}
        level={data?.dental?.root_canal_treatment?.level}
        description={descriptionDetailInsuranceHealth.root_canal_treatment()}
      />
      <InformationDetail
        content={data?.dental?.dental_pathology?.text}
        label={t('dental_pathology')}
        level={data?.dental?.dental_pathology?.level}
        description={descriptionDetailInsuranceHealth.dental_pathology()}
      />
      <InformationDetail
        content={data?.dental?.dental_pathology?.text}
        label={t('dental_calculus')}
        level={data?.dental?.dental_calculus?.level}
        description={descriptionDetailInsuranceHealth.dental_calculus()}
      />

      <h2 className={cssTitle}>{t('is_obstetric')}</h2>
      <InformationDetail
        content={data?.obstetric?.give_birth_normally?.text}
        label={t('give_birth_normally')}
        level={data?.obstetric?.give_birth_normally?.level}
        description={descriptionDetailInsuranceHealth.give_birth_normally()}
      />
      <InformationDetail
        content={data?.obstetric?.caesarean_section?.text}
        label={t('caesarean_section')}
        level={data?.obstetric?.caesarean_section?.level}
        description={descriptionDetailInsuranceHealth.caesarean_section()}
      />
      <InformationDetail
        content={data?.obstetric?.obstetric_complication?.text}
        label={t('obstetric_complication')}
        level={data?.obstetric?.obstetric_complication?.level}
        description={descriptionDetailInsuranceHealth.obstetric_complication()}
      />
      <InformationDetail
        content={data?.obstetric?.give_birth_abnormality?.text}
        label={t('give_birth_abnormality')}
        level={data?.obstetric?.give_birth_abnormality?.level}
        description={descriptionDetailInsuranceHealth.give_birth_abnormality()}
      />
      <InformationDetail
        content={data?.obstetric?.after_give_birth_fee?.text}
        label={t('after_give_birth_fee')}
        level={data?.obstetric?.after_give_birth_fee?.level}
        description={descriptionDetailInsuranceHealth.after_give_birth_fee()}
      />
      <InformationDetail
        content={data?.obstetric?.before_discharged?.text}
        label={t('before_discharged')}
        level={data?.obstetric?.before_discharged?.level}
        description={descriptionDetailInsuranceHealth.before_discharged()}
      />
      <InformationDetail
        content={data?.obstetric?.postpartum_childcare_cost?.text}
        label={t('postpartum_childcare_cost')}
        level={data?.obstetric?.postpartum_childcare_cost?.level}
        description={descriptionDetailInsuranceHealth.postpartum_childcare_cost()}
      />
    </div>
  );
}
