import InformationDetail from 'components/common/listInsurance/InformationDetail';
import React from 'react';
import { descriptionDetailInsuranceHealth } from 'src/utils/constants';
import { formatRoomType } from 'src/utils/helper';
import { useTranslation } from 'next-i18next';

export default function Inpatient({ data }) {
  const { t } = useTranslation('common');

  return (
    <div className="mt-6">
      <InformationDetail
        content={data?.inpatient?.room_type?.values
          .map((value) => formatRoomType(value))
          .join(', ')}
        label={t('room_type')}
        level={data?.inpatient?.room_type?.level}
        description={descriptionDetailInsuranceHealth.room_type()}
      />
      <InformationDetail
        content={data?.inpatient?.for_cancer?.text}
        label={t('due_to_cancer')}
        level={data?.inpatient?.for_cancer?.level}
        description={descriptionDetailInsuranceHealth.for_cancer()}
      />
      <InformationDetail
        content={data?.inpatient?.for_illnesses?.text}
        label={t('for_illnesses')}
        level={data?.inpatient?.for_illnesses?.level}
        description={descriptionDetailInsuranceHealth.for_illnesses()}
      />
      <InformationDetail
        content={data?.inpatient?.for_accidents?.text}
        label={t('for_accidents')}
        level={data?.inpatient?.for_accidents?.level}
        description={descriptionDetailInsuranceHealth.for_accidents()}
      />
      <InformationDetail
        content={data?.inpatient?.for_surgical?.text}
        label={t('for_surgical')}
        level={data?.inpatient?.for_surgical?.level}
        description={descriptionDetailInsuranceHealth.for_surgical()}
      />
      <InformationDetail
        content={`${t('max')} ${
          data?.inpatient?.for_hospitalization?.value
        } VNĐ/${t('day')}`}
        label={t('for_hospitalization')}
        level={data?.inpatient?.for_hospitalization?.level}
        description={descriptionDetailInsuranceHealth.for_hospitalization()}
      />
      <InformationDetail
        content={data?.inpatient?.for_intensive_care?.text}
        label={t('for_intensive_care')}
        level={data?.inpatient?.for_intensive_care?.level}
        description={descriptionDetailInsuranceHealth.for_intensive_care()}
      />
      <InformationDetail
        content={data?.inpatient?.for_administrative?.text}
        label={t('for_administrative')}
        level={data?.inpatient?.for_administrative?.level}
        description={descriptionDetailInsuranceHealth.for_administrative()}
      />
      <InformationDetail
        content={data?.inpatient?.for_organ_transplant?.text}
        label={t('for_organ_transplant')}
        level={data?.inpatient?.for_organ_transplant?.level}
        description={descriptionDetailInsuranceHealth.for_organ_transplant()}
      />
    </div>
  );
}
