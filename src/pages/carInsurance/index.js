import LayoutWithHeader from 'components/common/LayoutWithHeader';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import React, { useState } from 'react';
import AboutInsurance from 'components/CarInsurance/AboutInsurance';
import bannerCarInsurance from '../../../public/images/category/bannerCarInsurance.png';
import Description from 'components/CarInsurance/Description';
import Question from 'components/LifeInsurance/question';
import InformationSend from 'components/LifeInsurance/InformationSend';
import detailCarInsurance from '../../../public/images/carInsurance/bannerCarInsurance.png';
import ContentBanner from 'components/common/ContentBanner';
import {
  questionsCompulsoryCarInsurance,
  questionsMaterialCarInsurance,
  titleTabMaterialCarQuestion,
} from 'src/utils/constants';

const listDescriptions = [
  {
    id: 1,
    detail: 'Simply dummy text of the printing and typesetting industry',
  },
  {
    id: 2,
    detail:
      'It has survived not only five centuries, but also the leap into electronic typesetting',
  },
  {
    id: 3,
    detail:
      'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipums',
  },
];

const contentTable = (t) => [
  {
    id: 1,
    title: t('whatIsCarInsurance'),
  },
  {
    id: 2,
    title: t('typesOfAutoInsurance'),
  },
  {
    id: 3,
    title: t('typesOfAutoInsurance'),
  },
  {
    id: 4,
    title: t('notCompensated'),
  },
];

const bannerList = (t) => [
  {
    id: 1,
    smallTitle: t('buyAtVeryLowPrice'),
    coreTitle: t('carInsurance'),
    image: bannerCarInsurance,
    buttonList: [
      {
        id: 1,
        type: 'bgWhite',
        case: 'medium',
        href: '/insuranceCarCompulsory',
        styleButton: 'w-1/2 sm:w-1/3 lg:w-57',
        title: (
          <p>
            {t('insurance')}&nbsp;
            <span className="hidden lg:inline-block">{t('car')}</span>
            <span className="hidden lg:inline-block">&nbsp;</span>
            {t('compulsory')}
          </p>
        ),
      },
      {
        id: 2,
        type: 'bgWhite',
        case: 'medium',
        href: '/insuranceCarMaterial',
        styleButton: 'w-1/2 sm:w-1/3 lg:w-57',
        title: (
          <p>
            {t('insurance')}&nbsp;
            <span className="hidden lg:inline-block">{t('car')}</span>
            <span className="hidden lg:inline-block">&nbsp;</span>
            {t('material')}
          </p>
        ),
      },
    ],
  },
];

function CarInsurance() {
  const { t } = useTranslation(['carInsurance']);

  const [currentTabQuestion, setCurrentTabQuestion] = useState(1);
  const handleSwitchingTabQuestion = (value) => {
    setCurrentTabQuestion(value);
  };

  return (
    <LayoutWithHeader>
      <ContentBanner
        bannerList={bannerList(t)}
        listDescriptions={listDescriptions}
      />
      <AboutInsurance
        contentTable={contentTable(t)}
        images={detailCarInsurance}
        smallTitle={t('shareKnowledge')}
        coreTitle={t('aboutAutoInsurance')}
      />
      <Description />
      <Question
        questions={questionsCompulsoryCarInsurance(t)}
        smallTitle={t('shareKnowledge')}
        coreTitle={t('frequentlyAskedQuestions')}
        partSecondQuestions={questionsMaterialCarInsurance(t)}
        currentTabQuestion={currentTabQuestion}
        titleTabQuestion={titleTabMaterialCarQuestion(t)}
        handleSwitchingTabQuestion={handleSwitchingTabQuestion}
      />
      <InformationSend />
    </LayoutWithHeader>
  );
}
export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'carInsurance',
      ])),
    },
  };
};
export default CarInsurance;
