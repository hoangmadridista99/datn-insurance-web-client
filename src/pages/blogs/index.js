/* eslint-disable indent */
import React, { useState, useEffect, useCallback } from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { toast } from 'react-toastify';

import LayoutWithHeader from 'components/common/LayoutWithHeader';
import CardBlog from 'components/CardBlog';
import Pagination from 'components/Pagination';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import Image from 'next/image';
import { useTranslation } from 'next-i18next';

const BaseUrl = process.env.NEXT_PUBLIC_BASE_URL;

const Blogs = ({ categories, locale, ...props }) => {
  const { t } = useTranslation('common');

  const [isLoaded, setIsLoaded] = useState(false);
  const [isLoadingPage, setIsLoadingPage] = useState(false);
  const [isLoadingCategory, setIsLoadingCategory] = useState(false);
  const [category, setCategory] = useState('all');
  const [blogs, setBlogs] = useState(() => props.blogs);
  const [totalPages, setTotalPages] = useState(() => props.pages);

  const [currentPage, setPage] = useState(1);

  const getData = useCallback(
    async (page, signal) => {
      let url = `${BaseUrl}v1/api/client/blogs?page=${page}&limit=10`;

      if (category !== 'all') {
        url += `&blog_category_id=${category}`;
      }

      const response = await fetch(url, { signal });

      if (response.status !== 200) {
        toast.error(t('no_list_blog'));

        return;
      }

      const result = await response.json();

      setBlogs(result.data);
      setTotalPages(result.meta.totalPages);
    },
    [category]
  );

  useEffect(() => {
    setIsLoaded(true);
  }, []);
  useEffect(() => {
    const controller = new AbortController();

    if (isLoaded && isLoadingPage) {
      getData(currentPage, controller.signal)
        .then(() => WindowScrollTop(0))
        .catch(() => setPage((prePage) => (prePage === 1 ? 1 : prePage - 1)))
        .finally(() => setTimeout(() => setIsLoadingPage(false), 500));
    }

    return () => controller.abort();
  }, [currentPage, isLoadingPage, getData]);
  useEffect(() => {
    const controller = new AbortController();

    if (isLoaded && isLoadingCategory) {
      getData(1, controller.signal)
        .then(() => setPage(1))
        .finally(() => setTimeout(() => setIsLoadingCategory(false), 500));
    }

    return () => controller.abort();
  }, [isLoadingCategory, getData]);
  useEffect(() => {
    if (isLoaded && !isLoadingCategory) {
      setIsLoadingPage(true);
    }
  }, [currentPage]);
  useEffect(() => {
    if (isLoaded) {
      setIsLoadingCategory(true);
    }
  }, [category]);

  const handleGetCategoryLabel = (category) =>
    locale === 'vi' ? category.vn_label : category.en_label;

  const handleClickCategory = (value) => {
    setCategory(value);
  };
  const handleClickPage = (value) => setPage(value + 1);

  return (
    <LayoutWithHeader backgroundFooter="bg-lotion">
      <div className="comparison-container bg-white py-10 lg:mb-20 ">
        <h2 className="mb-5 text-base font-bold text-raisin-black md:mb-10 md:text-3xl md:text-pixel-40 lg:leading-pixel-56">
          {t('blog_name')}
        </h2>
        <div className="flex flex-col gap-10 lg:flex-row">
          <div className="w-full lg:mr-1.5 lg:w-3/12">
            <div className="relative mb-6 last:mb-0">
              <input
                className="w-full rounded-lg border border-platinum py-2.25 pr-2.75 pl-10 text-base placeholder:text-chinese-silver"
                placeholder={t('enter_blog')}
              />
              <Image
                src="/svg/search.svg"
                width={24}
                height={24}
                alt="Search"
                className="absolute top-2.75 left-2.75"
              />
            </div>
            <div className="mb-6 last:mb-0">
              <h3 className="mb-4 hidden text-2xl font-bold text-raisin-black lg:block">
                {t('category')}
              </h3>
              <div
                className="after:content-[' '] relative inline-block cursor-pointer whitespace-nowrap p-2 text-sm text-nickel after:absolute after:left-0 after:bottom-[-2px] after:hidden after:h-[2px] after:w-full after:rounded-lg after:bg-azure last:mb-0 aria-checked:font-bold aria-checked:after:block lg:mb-3 lg:flex lg:items-center lg:justify-between lg:border-b lg:border-solid lg:border-cultured lg:text-base lg:after:hidden lg:hover:text-azure aria-checked:lg:font-normal aria-checked:lg:text-azure aria-checked:lg:after:hidden"
                aria-checked={category === 'all'}
                onClick={() => handleClickCategory('all')}
              >
                <p>{t('all')}</p>
                <span className="hidden lg:inline">
                  {categories.reduce(
                    (total, item) => total + item.quantity_of_blogs,
                    0
                  )}
                </span>
              </div>
              {categories.map((item) => (
                <div
                  key={item.id}
                  className="after:content-[' '] relative inline-block cursor-pointer whitespace-nowrap p-2 text-sm text-nickel after:absolute after:bottom-[-2px] after:left-0 after:hidden after:h-[2px] after:w-full after:rounded-lg after:bg-azure last:mb-0 aria-checked:font-bold aria-checked:after:block lg:mb-3 lg:flex lg:items-center lg:justify-between lg:border-b lg:border-solid lg:border-cultured lg:text-base lg:after:hidden lg:hover:text-azure aria-checked:lg:font-normal aria-checked:lg:text-azure aria-checked:lg:after:hidden"
                  onClick={() => handleClickCategory(item.id)}
                  aria-checked={category === item.id}
                >
                  <p>{handleGetCategoryLabel(item)}</p>
                  <span className="hidden lg:inline">
                    {item.quantity_of_blogs}
                  </span>
                </div>
              ))}
            </div>
            {/* <div className="mb-6 last:mb-0">
              <h3 className="mb-4 text-2xl font-bold text-raisin-black">
                Thời gian đăng bài
              </h3>
              <div className="mb-3 flex flex-col">
                <label className="mb-2 font-bold text-arsenic">Từ</label>
                <input
                  type="date"
                  className="blog-icon w-full rounded-md border border-chinese-silver py-2 px-3 text-chinese-silver placeholder:text-chinese-silver"
                  max={dayjs(Date.now()).format('YYYY-MM-DD')}
                />
              </div>
              <div className="mb-5 flex flex-col">
                <label className="mb-2 font-bold text-arsenic">Đến</label>
                <input
                  type="date"
                  className="blog-icon w-full rounded-md border border-chinese-silver py-2 px-3 text-chinese-silver placeholder:text-chinese-silver"
                  max={dayjs(Date.now()).format('YYYY-MM-DD')}
                />
              </div>
              <button className="w-full rounded-lg bg-ultramarine-blue py-2 text-center text-base text-[#f8f8f8]">
                Tìm kiếm
              </button>
            </div> */}
          </div>
          <div className="w-full pt-10 lg:ml-1.5 lg:w-9/12 lg:pt-0">
            <h3 className="hidden text-xl font-bold text-raisin-black md:text-pixel-32 md:leading-pixel-48 lg:mb-8 lg:block">
              {t('recent_posts')}
            </h3>
            <div>
              {blogs.map((item) => (
                <CardBlog
                  key={item.id}
                  isLoading={!isLoaded || isLoadingCategory || isLoadingPage}
                  locale={locale}
                  {...item}
                />
              ))}
            </div>
            <div className="mt-10 flex justify-end">
              <Pagination
                onClick={handleClickPage}
                page={currentPage}
                pages={totalPages}
              />
            </div>
          </div>
        </div>
      </div>
    </LayoutWithHeader>
  );
};

export const getServerSideProps = async ({ locale }) => {
  const urlCategories = `${BaseUrl}v1/api/client/blog-categories`;
  const urlBlogs = `${BaseUrl}v1/api/client/blogs?page=1&limit=10`;

  const [resCategories, resBlogs] = await Promise.all([
    fetch(urlCategories),
    fetch(urlBlogs),
  ]);

  let categories = [];
  let pages = 0;
  let blogs = [];

  if (resCategories.status === 200) {
    const result = await resCategories.json();

    categories = [...result];
  }
  if (resBlogs.status === 200) {
    const result = await resBlogs.json();

    blogs = [...(result?.data ?? [])];
    pages = result?.meta?.totalPages ?? 0;
  }

  return {
    props: {
      ...(await serverSideTranslations(locale, ['auth', 'home', 'common'])),
      categories,
      locale,
      blogs,
      pages,
    },
  };
};

export default Blogs;
