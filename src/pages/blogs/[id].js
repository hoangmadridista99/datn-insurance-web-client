import React, { useState, useEffect, useCallback } from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Image from 'next/image';
import dayjs from 'dayjs';
import { useSession, signOut } from 'next-auth/react';

const BaseUrl = process.env.NEXT_PUBLIC_BASE_URL;

import LayoutWithHeader from 'components/common/LayoutWithHeader';
import CommentBlog from 'components/CommentBlog';
import Pagination from 'components/Pagination';
import { toast } from 'react-toastify';
import ButtonPrimary from 'components/common/ButtonPrimary';

const BlogDetail = ({ locale, data, id }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [isCreate, setIsCreate] = useState(false);
  const [comments, setComments] = useState(null);
  const [totalPages, setTotalPages] = useState(0);
  const [currentPage, setCurrentPage] = useState(1);
  const [comment, setComment] = useState('');

  const session = useSession();

  const getData = useCallback(async (id, page, signal) => {
    const url = `${BaseUrl}v1/api/client/blog-comments/list?blog_id=${id}&page=${page}&limit=5`;

    const response = await fetch(url, {
      signal,
    });

    if (response.status !== 200) {
      setComments([]);

      return;
    }

    const result = await response.json();

    setComments(result.data);
    setTotalPages(result.meta.totalPages);
  }, []);
  const createComment = useCallback(
    async (blog_id, comment, signal) => {
      const url = `${BaseUrl}v1/api/client/blog-comments`;
      const body = {
        comment,
        blog_id,
      };

      const response = await fetch(url, {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${session?.data?.accessToken}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
        signal,
      });

      if (response.status === 401 || response.status === 403) {
        toast.info('Bạn không có quyền!');
        signOut();

        return;
      }

      if (response.status !== 204) {
        const result = await response.json();

        throw new Error(result.error_code);
      }
    },
    [session]
  );

  useEffect(() => {
    const controller = new AbortController();

    getData(id, 1, controller.signal).catch((error) => {
      if (error.name !== 'AbortError') {
        toast.error(error.message);
      }
    });

    return () => controller.abort();
  }, []);
  useEffect(() => {
    const controller = new AbortController();

    if (isLoading) {
      getData(id, 1, controller.signal)
        .then(() => setCurrentPage(1))
        .catch((error) => {
          if (error.name !== 'AbortError') {
            toast.error(error.message);
          }
        })
        .finally(() => setIsLoading(false));
    }

    return () => controller.abort();
  }, [isLoading]);
  useEffect(() => {
    const controller = new AbortController();

    if (!!comments && !isLoading) {
      getData(id, currentPage, controller.signal).catch((error) => {
        if (error.name !== 'AbortError') {
          toast.error(error.message);
        }
      });
    }

    return () => controller.abort();
  }, [currentPage]);
  useEffect(() => {
    const controller = new AbortController();

    if (isCreate && comment) {
      createComment(id, comment, controller.signal)
        .then(() => setComment(''))
        .catch((error) => {
          if (error.name !== 'AbortError') {
            toast.error(error.message);
          }
        })
        .finally(() => {
          setIsCreate(false);
          setIsLoading(true);
        });
    }

    return () => controller.abort();
  }, [isCreate, comment]);

  const handleKeyUp = (e) => {
    if (e.key === 'Enter' || e.keyCode === 13) return handleClickPost();
  };

  const handleClickPage = (value) => setCurrentPage(value + 1);
  const handleClickPost = () => {
    if (comment) {
      setIsCreate(true);
    }
  };

  const handleChangeComment = (e) => setComment(e.target.value);

  return (
    <LayoutWithHeader>
      <div className="bg-white py-10 px-4 sm:px-10 md:px-[60px] lg:px-[108px]">
        <div className="mb-4 flex justify-center">
          <div className="w-full lg:mr-[19px] lg:w-8/12">
            <div className="flex items-center">
              <div className="flex items-center">
                <Image
                  src={
                    data.user.avatar_profile_url ?? '/svg/avatar-default.svg'
                  }
                  width={20}
                  height={20}
                  alt={`Avatar ${data.user.first_name} ${data.user.last_name}`}
                  className="h-[20px] rounded-full object-cover"
                />
                <span className="ml-2 text-sm text-nickel">
                  {`${data.user.first_name} ${data.user.last_name}`}
                </span>
              </div>
              <div className="mx-1 h-3 w-0.5 bg-arsenic lg:h-4" />
              <span className="text-sm text-nickel">
                {dayjs(new Date(data.created_at)).format('DD MMMM YYYY')}
              </span>
            </div>
            <h2 className="mb-2 text-2xl font-bold text-arsenic lg:mb-3 lg:text-pixel-40 lg:leading-pixel-56">
              {data.title}
            </h2>
            <div className="mb-6 flex items-center">
              <div className="mr-2 rounded-lg bg-lavender p-1 text-xs text-azure last:mr-0">
                {locale === 'vi'
                  ? data.blog_category.vn_label
                  : data.blog_category.en_label}
              </div>
            </div>
            <p className="mb-6 text-sm text-nickel lg:text-lg">
              {data.description}
            </p>
            <div
              className="[&>*]:mb-6 [&>*]:text-nickel [&>p>img]:w-full [&>ul>li]:list-inside
              [&>ul>li]:list-disc"
              dangerouslySetInnerHTML={{ __html: decodeURI(data.content) }}
            />
            <div className="border-t border-t-lavender pt-10 pb-6">
              {session.data && (
                <div className="mb-2 rounded-lg bg-cultured p-4">
                  <div className="mb-2 flex items-center">
                    <Image
                      src={
                        session.data.avatar_profile_url ??
                        '/svg/avatar-default.svg'
                      }
                      width={20}
                      height={20}
                      alt="Avatar"
                      className="h-5 rounded-full object-cover"
                    />
                    <span className="ml-2 text-sm font-bold text-nickel">
                      {`${session.data.first_name} ${session.data.last_name}`}
                    </span>
                  </div>
                  <div className="relative">
                    <input
                      className="w-full rounded-md p-3 pr-[117px] text-sm placeholder:text-sm placeholder:text-chinese-silver"
                      placeholder="Viết đánh giá"
                      onChange={handleChangeComment}
                      value={comment}
                      onKeyUp={handleKeyUp}
                    />
                    <ButtonPrimary
                      type={'submit'}
                      title={'Gửi'}
                      onClick={handleClickPost}
                      widthBtn={
                        'absolute top-2 right-3 flex h-7 w-[93px] items-center justify-center rounded bg-ultramarine-blue text-sm text-cultured'
                      }
                      disabled={isCreate}
                    />
                  </div>
                </div>
              )}
              {(!comments || isLoading) && (
                <div className="flex justify-center py-5">
                  <div className="flex">
                    <div aria-label="Loading..." role="status">
                      <svg
                        className="h-6 w-6 animate-spin stroke-indigo-500"
                        viewBox="0 0 256 256"
                      >
                        <line
                          x1={128}
                          y1={32}
                          x2={128}
                          y2={64}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={24}
                        />
                        <line
                          x1="195.9"
                          y1="60.1"
                          x2="173.3"
                          y2="82.7"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={24}
                        />
                        <line
                          x1={224}
                          y1={128}
                          x2={192}
                          y2={128}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={24}
                        />
                        <line
                          x1="195.9"
                          y1="195.9"
                          x2="173.3"
                          y2="173.3"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={24}
                        />
                        <line
                          x1={128}
                          y1={224}
                          x2={128}
                          y2={192}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={24}
                        />
                        <line
                          x1="60.1"
                          y1="195.9"
                          x2="82.7"
                          y2="173.3"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={24}
                        />
                        <line
                          x1={32}
                          y1={128}
                          x2={64}
                          y2={128}
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={24}
                        />
                        <line
                          x1="60.1"
                          y1="60.1"
                          x2="82.7"
                          y2="82.7"
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth={24}
                        />
                      </svg>
                    </div>
                  </div>
                </div>
              )}
              {comments &&
                comments.map((item) => <CommentBlog key={item.id} {...item} />)}
              <div className="flex justify-end">
                <Pagination
                  page={currentPage}
                  pages={totalPages}
                  onClick={handleClickPage}
                />
              </div>
            </div>
          </div>
          {/* <div className="ml-[19px] mt-10 w-4/12">
            <h4 className="text-2xl font-bold text-raisin-black">
              Bài viết liên quan
            </h4>
            <div className="mt-4">
              <CardBlog outstanding />
              <CardBlog outstanding />
              <CardBlog outstanding />
              <CardBlog outstanding />
              <CardBlog outstanding />
            </div>
          </div> */}
        </div>
      </div>
    </LayoutWithHeader>
  );
};

export const getServerSideProps = async ({ locale, query }) => {
  const { id } = query;

  if (!id || typeof id !== 'string')
    return {
      notFound: true,
    };
  const url = `${BaseUrl}v1/api/client/blogs/${id}`;

  const response = await fetch(url);

  if (response.status !== 200)
    return {
      notFound: true,
    };

  const data = await response.json();

  return {
    props: {
      ...(await serverSideTranslations(locale, ['auth', 'home', 'common'])),
      locale,
      data,
      id,
    },
  };
};

export default BlogDetail;
