import Pagination from 'components/Pagination';
import ButtonSort from 'components/common/ButtonSort';
import LayoutWithHeader from 'components/common/LayoutWithHeader';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import ListDetailsLineHealthInsurance from 'components/listHealthInsurance/ListDetailsLineHealthInsurance';
import { useSession } from 'next-auth/react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React, {
  Fragment,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
import { DataContext } from 'src/store/GlobalState';
import { AUTHENTICATED, LIST_BUTTON_FILTER } from 'src/utils/constants';
import { useTranslation } from 'next-i18next';
import { getAppraisalDetails } from 'pages/api/insurance';
import InformationInsurance from 'components/quoteViewList/InformationInsurance';
import FilterPhysicalInsurancePackage from 'components/quoteViewList/FilterPhysicalInsurancePackage';
import AlertSuccessRating from 'components/common/listInsurance/AlertSuccessRating';
import { Dialog, Transition } from '@headlessui/react';

export default function QuoteViewList() {
  const { t } = useTranslation('common');
  const { dispatch } = useContext(DataContext);

  const session = useSession();

  const router = useRouter();
  const idInsurance = router.query;
  const data = router.query;

  const [currentPage, setCurrentPage] = useState(1);
  const [dataInsuranceComparison, setDataInsuranceComparison] = useState([]);
  const [hiddenFilter, setHiddenFilter] = useState(false);
  const [sort, setSort] = useState('');
  const [pagination, setPagination] = useState();
  const [hiddenAlertSuccessRating, setHiddenAlertSuccessRating] =
    useState(false);
  function closeModal() {
    setHiddenAlertSuccessRating(false);
  }

  const handleHiddenFilter = () => {
    setHiddenFilter(true);
  };

  const handleSort = (sort) => {
    setSort(sort);
  };

  const [insuranceList, setInsuranceList] = useState();
  const handleClickPage = (value) => {
    setCurrentPage(value + 1);
    WindowScrollTop(200);
  };

  const getAppraisalFormsList = useCallback(
    async (id) => {
      dispatch({ type: 'LOADING', payload: { loading: true } });

      try {
        const response = await getAppraisalDetails(
          session?.data?.accessToken,
          id
        );
        setInsuranceList(response?.data);
        setPagination(response?.data?.meta?.totalPages);
      } catch (error) {
        console.log('🚀 ========= error:', error);
      }
      dispatch({ type: 'LOADING', payload: { loading: false } });
    },
    [session?.data?.accessToken]
  );

  useEffect(() => {
    if (session.status === AUTHENTICATED) {
      getAppraisalFormsList(idInsurance.id);
    }
  }, [session?.data?.accessToken]);

  const [currentFeeValue, setCurrentFeeValue] = useState({
    choosing_service_center: false,
    component_vehicle_theft: false,
    no_depreciation_cost: false,
    water_damage: false,
    insured_for_each_person: false,
  });

  const handleCal = (fee, feeAdd1, feeAdd2, feeAdd3, feeAdd4, feeAdd5) => {
    return (
      fee +
      Number(currentFeeValue.choosing_service_center && feeAdd1) +
      Number(currentFeeValue.component_vehicle_theft && feeAdd2) +
      Number(currentFeeValue.no_depreciation_cost && feeAdd3) +
      Number(currentFeeValue.water_damage && feeAdd4) +
      Number(currentFeeValue.insured_for_each_person && feeAdd5)
    );
  };
  return (
    <LayoutWithHeader>
      {hiddenAlertSuccessRating && (
        <div className="fixed inset-0 z-50 flex items-center justify-center bg-rich-black">
          <AlertSuccessRating
            setHiddenAlertSuccessRating={setHiddenAlertSuccessRating}
          />
        </div>
      )}
      <Transition appear show={hiddenAlertSuccessRating} as={Fragment}>
        <Dialog
          as="div"
          className="relative z-10"
          onClose={closeModal}
        ></Dialog>
      </Transition>

      <div className="bg-cultured">
        <div className="comparison-container pb-16 pt-10">
          <div className="flex flex-col gap-6 lg:flex-row">
            <div className="hidden w-1/3 lg:block lg:w-1/2">
              <FilterPhysicalInsurancePackage
                data={data}
                setHiddenFilter={setHiddenFilter}
                setCurrentPage={setCurrentPage}
                formType={insuranceList?.form_type}
                insuranceList={insuranceList}
                setCurrentFeeValue={setCurrentFeeValue}
              />
            </div>
            <div className="lg:hidden">
              <button
                onClick={handleHiddenFilter}
                className="flex items-center gap-2 rounded-lg border border-nickel px-4 py-2"
              >
                <Image src="/svg/sort.svg" height={20} width={20} alt="sort" />
                <p className="text-base font-bold text-nickel">
                  {t('information_search')}
                </p>
              </button>
            </div>
            {hiddenFilter && (
              <div className="absolute inset-0 z-50 bg-rich-black lg:hidden">
                <FilterPhysicalInsurancePackage
                  data={data}
                  setHiddenFilter={setHiddenFilter}
                  setCurrentPage={setCurrentPage}
                  formType={insuranceList?.form_type}
                  insuranceList={insuranceList}
                  setCurrentFeeValue={setCurrentFeeValue}
                />
              </div>
            )}

            <div className="w-full">
              <div className="mb-5 flex gap-2">
                <Image
                  width={20}
                  height={20}
                  src="/images/listInsurance/document-filter.png"
                  alt="document_filter"
                />
                <p className="text-base font-normal text-nickel">
                  {insuranceList?.appraisal?.appraisal_data?.length}&nbsp;
                  {t('results_for')}
                </p>
                <p className="text-base font-medium text-maastricht-blue">
                  {t('insurance_material_car')}
                </p>
              </div>
              <div className="mb-6 grid grid-cols-2 gap-2 sm:flex sm:gap-4">
                {LIST_BUTTON_FILTER(t).map((list) => (
                  <ButtonSort
                    key={list?.id}
                    list={list}
                    sort={sort}
                    title={list?.label}
                    onClick={() => handleSort(list?.filterSort)}
                  />
                ))}
              </div>
              {insuranceList?.appraisal?.appraisal_data?.length !== 0 ? (
                <div className="w-full">
                  <InformationInsurance
                    setHiddenAlertSuccessRating={setHiddenAlertSuccessRating}
                    ListDetailsLine={ListDetailsLineHealthInsurance}
                    dataInsurance={insuranceList?.appraisal?.appraisal_data}
                    setDataInsuranceComparison={setDataInsuranceComparison}
                    dataInsuranceComparison={dataInsuranceComparison}
                    handleCal={handleCal}
                    idInsurance={idInsurance}
                  />
                </div>
              ) : (
                <div>{t('no_data')}</div>
              )}
              {pagination?.totalPages > 0 && (
                <div className="mt-5 flex justify-end">
                  <Pagination
                    page={currentPage}
                    pages={pagination?.totalPages}
                    onClick={handleClickPage}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </LayoutWithHeader>
  );
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'insuranceLocale',
      ])),
    },
  };
};
