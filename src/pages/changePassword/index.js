import { yupResolver } from '@hookform/resolvers/yup';
import LayoutWithHeader from 'components/common/LayoutWithHeader';
import { useSession } from 'next-auth/react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { changePasswordAPI } from 'pages/api/authentication';
import React, { useContext, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'next-i18next';
import { toast } from 'react-toastify';
import { DataContext } from 'src/store/GlobalState';
import { schemaChangePassword } from 'src/utils/error';
import IconEyeClose from 'components/common/icon/IconEyeClose';
import IconEyeOpen from 'components/common/icon/IconEyeOpen';
import {
  CURRENT_PASSWORD,
  NEW_PASSWORD,
  CONFIRM_NEW_PASSWORD,
} from 'src/utils/constants';
export default function ChangePassword() {
  const { t } = useTranslation(['common', 'auth']);
  const { dispatch } = useContext(DataContext);
  const router = useRouter();
  const session = useSession();

  const [isShowPassword, setIsShowPassword] = useState({
    currentPassword: true,
    newPassword: true,
    confirmNewPassword: true,
  });
  const handleShowHidePassword = (cases) => {
    switch (cases) {
      case CURRENT_PASSWORD:
        setIsShowPassword({
          ...isShowPassword,
          currentPassword: !isShowPassword.currentPassword,
        });
        break;
      case NEW_PASSWORD:
        setIsShowPassword({
          ...isShowPassword,
          newPassword: !isShowPassword.newPassword,
        });
        break;
      case CONFIRM_NEW_PASSWORD:
        setIsShowPassword({
          ...isShowPassword,
          confirmNewPassword: !isShowPassword.confirmNewPassword,
        });
        break;
    }
  };

  const {
    register,
    handleSubmit,

    formState: { errors },
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schemaChangePassword(t)),
  });
  const handleChangePassword = async (values) => {
    dispatch({ type: 'LOADING', payload: { loading: true } });

    try {
      await changePasswordAPI(session?.data?.accessToken, {
        params: values,
      });

      toast.success(t('auth:change_password_success'));

      router.push('/');
    } catch (error) {
      toast.error(t('auth:password_is_incorrect'));
    }
    dispatch({ type: 'LOADING', payload: { loading: false } });
  };
  return (
    <LayoutWithHeader>
      <div className="flex justify-center bg-lotion px-4 pt-10 pb-28 sm:px-10 md:px-20">
        <div className="w-full rounded-2xl border bg-white p-6 shadow-listInsurance lg:w-3/5">
          <form onSubmit={handleSubmit(handleChangePassword)}>
            <h1 className="mb-6 border-b pb-6 text-xl font-bold text-arsenic lg:text-2xl">
              {t('auth:change_password')}
            </h1>
            <div className="w-full">
              <label
                className="mb-2 text-sm font-bold text-arsenic md:text-base"
                htmlFor="idCurrentPassword"
              >
                {t('auth:current_password')}
              </label>
              <div className="mt-2 mb-6 flex rounded-md border border-chinese-silver p-3">
                <input
                  placeholder={t('auth:enter_current_password')}
                  className="w-full bg-transparent text-base font-normal outline-none"
                  maxLength="20"
                  type={isShowPassword.currentPassword ? 'password' : 'text'}
                  id="idCurrentPassword"
                  {...register('current_password')}
                />
                <div onClick={() => handleShowHidePassword(CURRENT_PASSWORD)}>
                  {!isShowPassword.currentPassword ? (
                    <IconEyeOpen />
                  ) : (
                    <IconEyeClose />
                  )}
                </div>
              </div>
              {errors?.current_password && (
                <p className=" text-red-500 ">
                  {errors?.current_password?.message}
                </p>
              )}
            </div>

            <div className="w-full">
              <label
                className="mb-2 text-sm font-bold text-arsenic md:text-base"
                htmlFor="idNewPassword"
              >
                {t('auth:new_password')}
              </label>
              <div className="mt-2 mb-6 flex rounded-md border border-chinese-silver p-3">
                <input
                  placeholder={t('auth:enter_new_password')}
                  className="w-full bg-transparent text-base font-normal outline-none"
                  maxLength="20"
                  type={isShowPassword.newPassword ? 'password' : 'text'}
                  autoComplete="new-password"
                  id="idNewPassword"
                  {...register('new_password')}
                />
                <div onClick={() => handleShowHidePassword(NEW_PASSWORD)}>
                  {!isShowPassword.newPassword ? (
                    <IconEyeOpen />
                  ) : (
                    <IconEyeClose />
                  )}
                </div>
              </div>
              {errors?.new_password && (
                <p className=" text-red-500 ">
                  {errors?.new_password?.message}
                </p>
              )}
            </div>

            <div className="w-full">
              <label
                className="mb-2 text-sm font-bold text-arsenic md:text-base"
                htmlFor="idConfirmNewPassword"
              >
                {t('auth:rePassword')}
              </label>
              <div className="mt-2 mb-6 flex rounded-md border border-chinese-silver p-3">
                <input
                  placeholder={t('auth:re_new_password')}
                  className="w-full bg-transparent text-base font-normal outline-none"
                  maxLength="20"
                  type={isShowPassword.confirmNewPassword ? 'password' : 'text'}
                  autoComplete="new-password"
                  id="confirmation_password"
                  {...register('confirmation_password')}
                />
                <div
                  onClick={() => handleShowHidePassword(CONFIRM_NEW_PASSWORD)}
                >
                  {!isShowPassword.confirmNewPassword ? (
                    <IconEyeOpen />
                  ) : (
                    <IconEyeClose />
                  )}
                </div>
              </div>
              {errors?.confirmation_password && (
                <p className=" text-red-500 ">
                  {errors?.confirmation_password?.message}
                </p>
              )}
            </div>

            <div className="flex justify-center lg:justify-end">
              <button
                type="submit"
                className="rounded-lg border bg-ultramarine-blue px-6 py-2 text-base font-bold text-cultured"
              >
                {t('auth:save_change')}
              </button>
            </div>
          </form>
        </div>
      </div>
    </LayoutWithHeader>
  );
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['auth', 'home', 'common'])),
    },
  };
};
