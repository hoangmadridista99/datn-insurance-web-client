import React from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

import LayoutWithHeader from 'components/common/LayoutWithHeader';
import healthInsurance1 from '../../../public/images/healthInsurance/healthInsurance1.png';
import bhnt2 from '../../../public/images/healthInsurance/Bhnt2.png';
import bhnt22 from '../../../public/images/healthInsurance/Bhnt22.png';
import InformationSend from 'components/LifeInsurance/InformationSend';
import OverviewHealthInsurance from 'components/healthInsurance/OverviewHealthInsurance';
import DescriptionHealthInsurance from 'components/healthInsurance/DescriptionHealthInsurance';
import healthInsurance from '../../../public/images/healthInsurance/healthInsurance.png';
import ContentBanner from 'components/common/ContentBanner';
import Question from 'components/LifeInsurance/question';

export default function HealthInsurance() {
  const { t } = useTranslation(['healthInsurance']);

  const bannerList = [
    {
      id: 1,
      smallTitle: t('buyAtVeryLowPrice'),
      coreTitle: t('healthInsurance'),
      image: healthInsurance,
      buttonList: [
        {
          id: 1,
          type: 'primary',
          case: 'medium',
          href: '/filterHealthInsurance',
          title: t('compareNow'),
          styleButton: 'w-57',
        },
      ],
    },
  ];

  const contentTable = [
    {
      id: 1,
      title: t('whatIsHealthInsurance'),
    },
    {
      id: 2,
      title: t('popularHeathInsurance'),
    },
    {
      id: 3,
      title: t('benefitsInsurance'),
    },
    {
      id: 4,
      title: t('experienceBuyingHealthInsurance'),
    },
  ];

  const insuranceHealthData = [
    {
      id: 1,
      insuranceName: t('humanAccidentInsurance'),
      title: t('humanAccidentInsuranceTitleDetail1'),
      detailTitle: [
        {
          detailId: 1,
          detail: t('humanAccidentInsuranceDetail1'),
        },
        {
          detailId: 2,
          detail: t('humanAccidentInsuranceDetail2'),
        },
      ],
      titleSecond: t('humanAccidentInsuranceTitleDetail2'),
    },
    {
      id: 2,
      insuranceName: t('commercialHealthInsurance'),
      title: (
        <span>
          {t('commercialHealthInsuranceTitle1')} <br />
          {t('note')}
        </span>
      ),
      detailTitle: [
        {
          detailId: 1,
          detail: t('commercialHealthInsuranceDetail1'),
        },
        {
          detailId: 2,
          detail: t('commercialHealthInsuranceDetail2'),
        },
      ],
    },
    {
      id: 3,
      insuranceName: t('healthCareInsurance'),
      title: t('healthCareInsuranceDetail'),
      detailTitle: [
        {
          detailId: 1,
          detail: t('healthCareInsuranceDetail1'),
        },
        {
          detailId: 2,
          detail: t('healthCareInsuranceDetail2'),
        },
        {
          detailId: 3,
          detail: t('healthCareInsuranceDetail3'),
        },
      ],
      titleSecond: t('forHealthCareInsurance'),
    },
    {
      id: 4,
      insuranceName: t('financialSecurityAgainstRisks'),
      title: t('financialSecurityAgainstRisksDetail'),
    },
    {
      id: 5,
      insuranceName: t('reputableMedicalFacility'),
      title: t('reputableMedicalFacilityDetail'),
    },
    {
      id: 6,
      insuranceName: t('diversityOfBenefits'),
      title: t('deversityOfBenefitsDetail'),
      images: bhnt22,
    },
    {
      id: 7,
      insuranceName: t('healthCareInsuranceForChild'),
      title: t('healthCareInsuranceForChildtitleDetail'),
      detailTitle: [
        {
          detailId: 1,
          detail: t('healthCareInsuranceForChildDetail1'),
        },
        {
          detailId: 2,
          detail: t('healthCareInsuranceForChildDetail2'),
        },
        {
          detailId: 3,
          detail: t('healthCareInsuranceForChildDetail3'),
        },
      ],
      titleSecond: t('humanAccidentInsuranceTitleDetail2'),
    },
    {
      id: 8,
      insuranceName: t('insuranceForYourFamily'),
      title: t('insuranceForYourFamilyTitleDetail'),
      detailTitle: [
        {
          detailId: 1,
          detail: t('insuranceForYourFamilyDetail1'),
        },
        {
          detailId: 2,
          detail: t('insuranceForYourFamilyDetail2'),
        },
        {
          detailId: 3,
          detail: t('insuranceForYourFamilyDetail3'),
        },
      ],
    },
    {
      id: 9,
      insuranceName: t('insuranceForBusinesses'),
      title: t('insuranceForBusinessesTitleDetail'),
      detailTitle: [
        {
          detailId: 1,
          detail: t('insuranceForBusinessesDetail1'),
        },
        {
          detailId: 2,
          detail: t('insuranceForBusinessesDetail2'),
        },
      ],
    },
  ];

  const questions = [
    {
      id: 1,
      title: t('whoShouldJoinHealthInsurance'),
      detail: t('whoShouldJoinHealthInsuranceAnswer'),
    },
    {
      id: 2,
      title: t('buyHealthInsurance'),
      detail: t('buyHealthInsuranceAnswer'),
    },
    {
      id: 3,
      title: t('insuranceParticipationFee'),
      detail: t('insuranceParticipationFee'),
    },
    {
      id: 4,
      title: t('criteriaForChoosing'),
      answerList: [
        {
          id: 1,
          detailQuestion: t('criteriaForChoosingDetail1'),
        },
        {
          id: 2,
          detailQuestion: t('criteriaForChoosingDetail2'),
        },
        {
          id: 3,
          detailQuestion: t('criteriaForChoosingDetail3'),
        },
        {
          id: 4,
          detailQuestion: t('criteriaForChoosingDetail4'),
        },
      ],
    },
  ];

  const listDescriptions = [
    {
      id: 1,
      detail: 'Simply dummy text of the printing and typesetting industry',
    },
    {
      id: 2,
      detail:
        'It has survived not only five centuries, but also the leap into electronic typesetting',
    },
    {
      id: 3,
      detail:
        'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipums',
    },
  ];

  return (
    <LayoutWithHeader>
      <ContentBanner
        bannerList={bannerList}
        listDescriptions={listDescriptions}
      />
      <div className="comparison-container mt-16 sm:mt-88 lg:mt-30">
        <OverviewHealthInsurance
          image={healthInsurance1}
          content={contentTable}
          smallTitle={t('shareKnowledge')}
          coreTitle={t('aboutHealthInsurance')}
        />
        <DescriptionHealthInsurance
          image={bhnt2}
          insuranceHealthData={insuranceHealthData}
        />
      </div>
      <Question
        questions={questions}
        smallTitle={t('shareKnowledge')}
        coreTitle={t('frequentlyAskedQuestions')}
      />
      <InformationSend />
    </LayoutWithHeader>
  );
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'healthInsurance',
      ])),
    },
  };
};
