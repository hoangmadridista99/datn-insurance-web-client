import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { getInsuranceObjectives } from 'pages/api/insurance';
import { convertObjectives } from 'src/utils/helper';
import numeral from 'numeral';
import {
  LIST_INSURED_PERSON,
  LIST_WORKS,
  STEP,
  TIME_INSURANCE,
  TOTAL_SUM_INSURED,
} from 'src/utils/constants';
import { useTranslation } from 'next-i18next';
import { schemaFilterInsurance } from 'src/utils/error';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import Button from 'components/common/Button';
import ButtonPrimary from 'components/common/ButtonPrimary';
import FormFilter from 'components/common/FormFilter';
import InputChoiceYearDate from 'components/common/InputChoiceYearDate';
import SelectFilter from 'components/common/SelectFilter';
import { useInputNumber } from 'src/hooks/useInputNumber';

const currentYear = new Date().getFullYear();
function FormFilterLifeInsurance({ insuranceObjectives }) {
  const router = useRouter();
  const { t } = useTranslation(['common']);
  const { onKeyDown } = useInputNumber();
  const {
    register,
    handleSubmit,
    setError,
    setValue,
    formState: { errors },
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schemaFilterInsurance(t)),
  });

  const [valueTotalSumMoney, setValueTotalSumMoney] = useState(null);
  const [valueTimeInsurance, setValueTimeInsurance] = useState(null);
  const [errorValueNoNumber, setErrorValueNoNumber] = useState();

  const [yearValue, setYearValue] = useState();
  const [valueProfession, setValueProfession] = useState();
  const [numberYear, setNumberYear] = useState(48);

  const listYearOfBirth = [
    ...Array.from({ length: 106 }, (_, i) => currentYear - i).reverse(),
  ];
  const listYearShow = listYearOfBirth?.slice(numberYear, numberYear + STEP);

  const handleChoseProfession = (e) => {
    setValueProfession(e);
    setValue('profession', e.value);
    setError('profession', { message: '' });
  };

  const handleFilterInsurance = (values) => {
    const { deadline_for_deal, total_sum_insured, ...data } = values;

    const isPassCheck = onCheckValueForm(values);
    if (!isPassCheck) return;

    let query = {
      ...Object.keys(data).reduce((result, key) => {
        if (data[key]) return { ...result, [key]: data[key] };

        return result;
      }, {}),
      total_sum_insured: parseInt(total_sum_insured.replace(/,/g, '')),
      deadline_for_deal: parseInt(deadline_for_deal.replace(/,/g, '')),
      year_of_birth: yearValue,
    };
    router.push({
      pathname: '/listLifeInsurance',
      query,
    });
  };

  const handleValueSumMoney = (list) => {
    setValue('total_sum_insured', list);
    setValueTotalSumMoney(list);
    setError('total_sum_insured', { message: '' });
  };

  const handleTimeInsurance = (list) => {
    setValue('deadline_for_deal', list);
    setValueTimeInsurance(list);
    setError('deadline_for_deal', { message: '' });
  };

  const onChangeTotal = (event) => {
    const {
      target: { value },
    } = event;
    if (value) {
      setValueTotalSumMoney(null);
    }
    const regex = /^[0-9,]*$/;
    const checkTotal = regex.test(value);
    setError('total_sum_insured', '');

    if (!checkTotal) {
      setError('total_sum_insured', { message: t('error_no_number') });
    } else {
      setError('total_sum_insured', { message: '' });
    }
    const formattedValue = numeral(value).format('(0,0)');

    setValue('total_sum_insured', formattedValue);
    setValueTotalSumMoney(formattedValue);
  };

  const onChangeDeadlineForDeal = (event) => {
    const {
      target: { value },
    } = event;

    if (value) {
      setValueTimeInsurance(null);
    }

    setError('deadline_for_deal', { message: '' });
    const formattedValue = numeral(value).format('(0,0)');

    setValue('deadline_for_deal', formattedValue);
    setValueTimeInsurance(formattedValue);
  };

  const onChangeYearOfBirth = (event) => {
    const {
      target: { value },
    } = event;

    if (value?.length === 0) return setYearValue(0);

    setYearValue(parseInt(value));
    setValue('year_of_birth', parseInt(value));

    if (value > currentYear || value < currentYear - 100) {
      setError('year_of_birth', { message: '' });
      return setErrorValueNoNumber({
        errorValueYearOfBirth: t('error_year'),
      });
    }
    setErrorValueNoNumber({ errorValueYearOfBirth: '' });
    setError('year_of_birth', { message: '' });
  };

  const onCheckValueForm = ({
    objective,
    insured_person,
    total_sum_insured,
    deadline_for_deal,
    profession,
    year_of_birth,
  }) => {
    if (!objective || objective.length === 0) {
      setError('objective', { message: t('error_objective') });
    }
    if (!profession) {
      setError('profession', { message: t('error_profession') });
    }
    if (!year_of_birth) {
      setError('year_of_birth', { message: t('error_year_of_birth') });
    }
    if (!total_sum_insured || total_sum_insured == 0) {
      setError('total_sum_insured', {
        message: t('error_total_sum_insured'),
      });
    }
    if (!insured_person || insured_person.length === 0) {
      setError('insured_person', {
        message: t('error_insured_person'),
      });
    }
    if (!deadline_for_deal || deadline_for_deal == 0) {
      setError('deadline_for_deal', {
        message: t('error_deadline_for_deal_no'),
      });
    }
    if (deadline_for_deal > 150) {
      setError('deadline_for_deal', {
        message: t('error_deadline_for_deal'),
      });
    }
    WindowScrollTop(400);
    if (
      objective.length > 0 &&
      insured_person.length > 0 &&
      total_sum_insured &&
      deadline_for_deal > 0 &&
      deadline_for_deal < 150 &&
      profession &&
      currentYear - 100 < parseInt(year_of_birth) &&
      parseInt(year_of_birth) < currentYear + 1
    ) {
      return true;
    }
    return;
  };

  return (
    <FormFilter>
      <div className="xl:px-52">
        <div className="mb-10 flex justify-center p-4 text-2xl font-bold text-cultured xl:text-pixel-40 xl:leading-pixel-56">
          {t('life_insurance')}
        </div>
        <div className="rounded-2xl bg-white p-6 shadow-formFilter xl:p-8">
          <form onSubmit={handleSubmit(handleFilterInsurance)}>
            <div className="xl:mb-8">
              <p className="mb-4 text-base font-bold text-arsenic">
                {t('target_insurance')}
              </p>
              {insuranceObjectives.map((list) => (
                <div key={list?.id} className="mb-6 flex">
                  <input
                    type="checkbox"
                    value={list?.objective_type}
                    id="objective"
                    {...register('objective')}
                    className="h-5 w-5 xl:h-6 xl:w-6"
                  />
                  <p className="ml-4 text-base font-normal text-arsenic">
                    {convertObjectives(list?.objective_type)}
                  </p>
                </div>
              ))}
              {errors?.objective && (
                <p className=" text-red-500 ">{errors?.objective?.message}</p>
              )}
            </div>

            <div className="mb-8">
              <p className="mb-4 text-base font-bold text-arsenic">
                {t('total_sum_insured')}
              </p>
              <div className="mb-4 flex w-full ">
                <input
                  type="text"
                  placeholder={t('please_sum_insured')}
                  id="total_sum_insured"
                  className={`w-full justify-between rounded-l-md border border-chinese-silver px-4 outline-none ${
                    errors?.total_sum_insured?.message
                      ? 'border-red-500'
                      : 'focus:border-ultramarine-blue'
                  } `}
                  {...register('total_sum_insured')}
                  onKeyDown={(event) =>
                    onKeyDown(event, 'customer_orientation.waiting_period')
                  }
                  onChange={onChangeTotal}
                />
                <p className="w-fit rounded-r-md border-y border-r border-chinese-silver bg-platinum px-6 py-3 text-base font-normal text-silver">
                  VND
                </p>
              </div>
              <div className="flex flex-wrap gap-2 xl:gap-4">
                {TOTAL_SUM_INSURED.map((item) => (
                  <div key={item} className="w-31 lg:w-34">
                    <Button
                      cases={'formFilterInsurance'}
                      valueFilter={valueTotalSumMoney}
                      title={item}
                      item={item}
                      onclick={() => handleValueSumMoney(item)}
                    />
                  </div>
                ))}
              </div>
              {errors?.total_sum_insured && (
                <p className=" text-red-500 ">
                  {errors?.total_sum_insured?.message}
                </p>
              )}
            </div>

            <div className="mb-8">
              <p className="mb-4 text-base font-bold text-arsenic">
                {t('insured_person')}
              </p>
              {LIST_INSURED_PERSON(t).map((values) => (
                <div
                  key={values.value}
                  className="mb-4 flex items-center gap-4"
                >
                  <input
                    type="checkbox"
                    id="insured_person"
                    {...register('insured_person')}
                    className="h-5 w-5"
                    value={values?.value}
                  />
                  <p className="text-base font-normal text-arsenic">
                    {values?.label}
                  </p>
                </div>
              ))}
              {errors?.insured_person && (
                <p className=" text-red-500 ">
                  {errors?.insured_person?.message}
                </p>
              )}
            </div>

            <InputChoiceYearDate
              yearValue={yearValue}
              register={register}
              errorValueNoNumber={errorValueNoNumber}
              errors={errors}
              onChangeYearOfBirth={onChangeYearOfBirth}
              setValue={setValue}
              setYearValue={setYearValue}
              setError={setError}
              setErrorValueNoNumber={setErrorValueNoNumber}
              title={t('year')}
              placeholder={t('choice_year')}
              id="year_of_birth"
              listYearShow={listYearShow}
              setNumberYear={setNumberYear}
              numberYear={numberYear}
              numberYearMax={96}
            />

            <div className="mb-8">
              <p className="mb-4 text-base font-bold text-arsenic">
                {t('profession')}
              </p>
              <SelectFilter
                onChange={handleChoseProfession}
                selected={valueProfession}
                options={LIST_WORKS(t)}
                errors={errors?.profession?.message}
                placeholder={t('common:error_profession')}
              />
              {errors?.profession && (
                <p className=" text-red-500 ">{errors?.profession?.message}</p>
              )}
            </div>

            <div className="mb-8">
              <p className="mb-4 text-base font-bold text-arsenic">
                {t('deadline_for_deal')}
              </p>
              <div className="mb-4 flex w-full justify-between">
                <input
                  type="text"
                  id="deadline_for_deal"
                  maxLength={3}
                  placeholder={t('error_deadline_for_deal_no')}
                  autoComplete="off"
                  {...register('deadline_for_deal')}
                  className={`w-full rounded-l-md border border-chinese-silver px-4 outline-none ${
                    errors?.deadline_for_deal?.message
                      ? 'border-red-500'
                      : 'focus:border-ultramarine-blue'
                  }`}
                  onKeyDown={(event) =>
                    onKeyDown(event, 'customer_orientation.waiting_period')
                  }
                  onChange={onChangeDeadlineForDeal}
                />

                <p className="rounded-r-md border-y border-r border-chinese-silver bg-platinum px-6 py-3 text-base font-normal text-silver">
                  {t('Year')}
                </p>
              </div>
              <div className="flex flex-wrap gap-2 xl:gap-4">
                {TIME_INSURANCE.map((item) => (
                  <div key={item} className="w-12">
                    <Button
                      cases={'formFilterInsurance'}
                      onclick={() => handleTimeInsurance(item)}
                      title={item}
                      valueFilter={valueTimeInsurance}
                      item={item}
                    />
                  </div>
                ))}
              </div>
              {errors?.deadline_for_deal && (
                <p className=" text-red-500 ">
                  {errors?.deadline_for_deal?.message}
                </p>
              )}
            </div>

            <div className="flex justify-center">
              <ButtonPrimary
                type={'submit'}
                cases={'large'}
                title={t('search_comparison')}
                widthBtn={'w-2/3 sm:w-1/3'}
              />
            </div>
          </form>
        </div>
      </div>
    </FormFilter>
  );
}

export default FormFilterLifeInsurance;

export const getServerSideProps = async ({ locale }) => {
  const insuranceObjectives = await getInsuranceObjectives();

  return {
    props: {
      insuranceObjectives: insuranceObjectives?.data,
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'insuranceLocale',
      ])),
    },
  };
};
