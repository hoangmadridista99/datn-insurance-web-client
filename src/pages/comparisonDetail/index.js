import LayoutWithHeader from 'components/common/LayoutWithHeader';
import BenefitKey from 'components/comparisonDetail/BenefitKey';
import BenefitKeyMobile from 'components/comparisonDetail/BenefitKeyMobile';
import Boarding from 'components/comparisonDetail/Boarding';
import BoardingMobile from 'components/comparisonDetail/BoardingMobile';
import InformationKey from 'components/comparisonDetail/InformationKey';
import InformationOther from 'components/comparisonDetail/InformationOther';
import InformationOtherMobile from 'components/comparisonDetail/InformationOtherMobile';
import LinkedInsuranceComparisonList from 'components/comparisonDetail/LinkedInsuranceComparisonList';
import LinkedInsuranceComparisonListMobile from 'components/comparisonDetail/LinkedInsuranceComparisonListMobile';
// import ListDetailInformationMobile from 'components/comparisonDetail/ListDetailInfomationMobile';
// import ListDetailInformation from 'components/comparisonDetail/ListDetailInformation';
import ListDetailInformation2 from 'components/comparisonDetail/ListDetailInformation2';
import OtherBenefits from 'components/comparisonDetail/OtherBenefits';
import OtherBenefitsMobile from 'components/comparisonDetail/OtherBenefitsMobile';
import OutpatientComparison from 'components/comparisonDetail/OutpatientComparison';
import OutpatientComparisonMobile from 'components/comparisonDetail/OutpatientComparisonMobile';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { getInsuranceIdAPI } from 'pages/api/insurance';
import React, { useContext, useEffect, useState } from 'react';
import { DataContext } from 'src/store/GlobalState';

function ComparisonDetail() {
  const router = useRouter();
  const dataFilterInsuranceId = router.query;
  const { state } = useContext(DataContext);

  const { updateDataInsurance } = state;
  const dataInsuranceCheck = updateDataInsurance?.updateDataInsurance;

  const listIdsInsurance = dataFilterInsuranceId.testData;

  console.log(
    '🚀 ~ file: index.js:32 ~ ComparisonDetail ~ listIdsInsurance:',
    listIdsInsurance
  );

  const [dataInsuranceComparison, setDataInsuranceComparison] = useState([]);

  const [indexInsuranceComparison, setIndexInsuranceComparison] = useState(0);
  const getInsuranceIds = async (id) => {
    try {
      return await getInsuranceIdAPI(id);
    } catch (error) {
      console.log('🚀 ~ file: index.js:17 ~ getInsuranceIds ~ error:', error);
    }
  };

  const getAll = async () => {
    const promise = listIdsInsurance.map((id) => getInsuranceIds(id));

    const responses = await Promise.all(promise);
    setDataInsuranceComparison(
      responses.map((response) => {
        return response?.data;
      })
    );
  };

  useEffect(() => {
    getAll();
  }, []);
  const handleSwip = (e) => {
    setIndexInsuranceComparison(e.activeIndex);
  };
  return (
    <LayoutWithHeader backgroundFooter="lg:bg-lotion">
      <InformationKey
        dataInsuranceComparison={dataInsuranceComparison}
        listIdsInsurance={listIdsInsurance}
        dataInsuranceCheck={dataInsuranceCheck}
        handleSwip={handleSwip}
        indexInsuranceComparison={indexInsuranceComparison}
      />
      {/* <ListDetailInformation
        dataInsuranceComparison={dataInsuranceComparison}
      /> */}
      <ListDetailInformation2
        dataInsuranceComparison={dataInsuranceComparison}
        indexInsuranceComparison={indexInsuranceComparison}
      />
      {/* <ListDetailInformationMobile
        dataInsuranceComparison={dataInsuranceComparison}
        indexInsuranceComparison={indexInsuranceComparison}
      /> */}
      {dataInsuranceComparison[0]?.insurance_category?.label === 'health' ? (
        <div className="lg:mb-30">
          <Boarding dataInsuranceComparison={dataInsuranceComparison} />
          <BoardingMobile
            dataInsuranceComparison={dataInsuranceComparison}
            indexInsuranceComparison={indexInsuranceComparison}
          />
          <OutpatientComparison
            dataInsuranceComparison={dataInsuranceComparison}
          />
          <OutpatientComparisonMobile
            dataInsuranceComparison={dataInsuranceComparison}
            indexInsuranceComparison={indexInsuranceComparison}
          />
          <OtherBenefits dataInsuranceComparison={dataInsuranceComparison} />
          <OtherBenefitsMobile
            dataInsuranceComparison={dataInsuranceComparison}
            indexInsuranceComparison={indexInsuranceComparison}
          />
          <LinkedInsuranceComparisonList
            dataInsuranceComparison={dataInsuranceComparison}
          />
          <LinkedInsuranceComparisonListMobile
            dataInsuranceComparison={dataInsuranceComparison}
            indexInsuranceComparison={indexInsuranceComparison}
          />
        </div>
      ) : (
        <div className="lg:mb-30">
          <BenefitKey dataInsuranceComparison={dataInsuranceComparison} />
          <BenefitKeyMobile
            dataInsuranceComparison={dataInsuranceComparison}
            indexInsuranceComparison={indexInsuranceComparison}
          />
          <InformationOther dataInsuranceComparison={dataInsuranceComparison} />
          <InformationOtherMobile
            dataInsuranceComparison={dataInsuranceComparison}
            indexInsuranceComparison={indexInsuranceComparison}
          />
        </div>
      )}
    </LayoutWithHeader>
  );
}

export default ComparisonDetail;

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'insuranceDetailsComparison',
      ])),
    },
  };
};
