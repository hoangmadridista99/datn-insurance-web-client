import LayoutWithHeader from 'components/common/LayoutWithHeader';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Image from 'next/image';
import { useRouter } from 'next/router';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'next-i18next';
import Pagination from 'components/Pagination';
import { DataContext } from 'src/store/GlobalState';
import { useSession } from 'next-auth/react';
import MaterialInsurance from 'components/listUserInsurance/MaterialInsurance';
import { deleteAppraisalInsurance, getAppraisals } from 'pages/api/insurance';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination as pagination } from 'swiper';

import 'swiper/css';
import 'swiper/css/pagination';
import PaginationDetails from 'components/common/PaginationDetails';
import { toast } from 'react-toastify';
import AlertDelete from 'components/common/AlertDelete';
import { AUTHENTICATED, insuranceTypes } from 'src/utils/constants';
import TabSwitching from 'components/listUserInsurance/TabSwitching';
import AssessmentApplicationList from 'components/listUserInsurance/AssessmentApplicationList';
import AlertEdit from 'components/common/AlertEdit';

export default function ListUserInsurance() {
  const session = useSession();
  const { dispatch } = useContext(DataContext);

  const { t } = useTranslation(['listUserInsurance']);

  const router = useRouter();

  const [currentPage, setCurrentPage] = useState(1);

  const pageSize = 10;
  const [totalPages, setTotalPages] = useState(1);
  const [totalItems, setTotalItems] = useState(1);
  const [currentTab, setCurrentTab] = useState(1);
  const [isCancerRegister, setIsCancerRegister] = useState(false);
  const [toggleAlertEdit, setToggleAlertEdit] = useState(false);
  const [valueEditInsuranceCarMaterial, setValueEditInsuranceCarMaterial] =
    useState();

  const handleCancerRegister = (value, id) => {
    setIdInsurance(id);
    setIsCancerRegister(value);
  };

  const handleClickPage = (value) => {
    setCurrentPage(value + 1);
    getAppraisalFormsList(value + 1).finally(() =>
      dispatch({ type: 'LOADING', payload: { loading: false } })
    );
    WindowScrollTop(200);
  };

  const handleTabSwitching = (id) => {
    setCurrentTab(id);
  };

  const [appraisalFormsList, setAppraisalFormsList] = useState();

  const getAppraisalFormsList = async (page) => {
    dispatch({ type: 'LOADING', payload: { loading: true } });

    try {
      const response = await getAppraisals(session?.data?.accessToken, {
        params: {
          limit: pageSize,
          page: page,
        },
      });
      setAppraisalFormsList(response?.data?.data);
      setTotalPages(response?.data?.meta?.totalPages);
      setTotalItems(response?.data?.meta?.totalItems);
    } catch (error) {
      console.log('🚀 ========= error:', error);
    }
    dispatch({ type: 'LOADING', payload: { loading: false } });
  };

  const handleDeleteInsurance = async (id) => {
    dispatch({ type: 'LOADING', payload: { loading: true } });
    try {
      await deleteAppraisalInsurance(session?.data?.accessToken, id);
      getAppraisalFormsList(1);
      toast.success(t('cancerRegisterSuccess'));
    } catch (error) {
      console.log('🚀 ========= error:', error);
    }
    dispatch({ type: 'LOADING', payload: { loading: false } });
  };

  useEffect(() => {
    if (session.status === AUTHENTICATED) {
      getAppraisalFormsList(1);
    }
  }, [session?.data?.accessToken]);

  const [isOpenModal, setIsOpenModal] = useState(false);
  const [idInsurance, setIdInsurance] = useState();

  const handleOpenModal = (value, id) => {
    setIsOpenModal(value);
    setIdInsurance(id);
  };

  return (
    <LayoutWithHeader>
      <div className="comparison-container mt-4 px-4 sm:px-10 xl:px-27">
        <button
          type="button"
          onClick={router.back}
          className="mb-4 flex items-center gap-1"
        >
          <Image
            width={24}
            height={24}
            src="/svg/back-concern.svg"
            alt="back-concern"
          />
          <p className="text-sm font-normal text-azure">{t('auth:back')}</p>
        </button>

        <div className="mb-10">
          <p className="mb-2 text-pixel-32 font-bold leading-pixel-48">
            {t('insuranceList')}
          </p>
          <p className="text-base font-normal">
            {t('statusTrackingAndManagement')}
          </p>
        </div>

        <div className="hidden lg:flex">
          <TabSwitching
            currentTab={currentTab}
            handleTabSwitching={handleTabSwitching}
          />

          <div className="w-2/5 border-b border-platinum"></div>
        </div>
        <div className="relative border-b border-platinum lg:hidden">
          <Swiper
            slidesPerView={2.25}
            modules={[pagination]}
            spaceBetween={4}
            breakpoints={{
              375: {
                slidesPerView: 2.25,
                spaceBetween: 4,
              },
              640: {
                slidesPerView: 6,
                spaceBetween: 4,
              },
            }}
            className="mx-auto flex flex-row gap-1"
          >
            {insuranceTypes(t)?.map((item) => (
              <SwiperSlide
                key={item.id}
                className={`${
                  currentTab === item.id ? 'text-azure' : 'text-nickel'
                } w-fit cursor-pointer rounded-t-sm border border-b-0 border-platinum px-3 py-2 text-xs`}
                onClick={() => handleTabSwitching(item?.id)}
              >
                {item.title}
              </SwiperSlide>
            ))}
          </Swiper>
        </div>

        <AssessmentApplicationList
          setValueEditInsuranceCarMaterial={setValueEditInsuranceCarMaterial}
          setToggleAlertEdit={setToggleAlertEdit}
          tab={currentTab}
          insuranceList={appraisalFormsList}
          handleOpenModal={handleOpenModal}
          handleCancerRegister={handleCancerRegister}
        />

        <div className="flex items-center justify-between">
          <PaginationDetails
            currentPage={currentPage}
            totalItems={totalItems}
            pageSize={pageSize}
            totalPages={totalItems}
          />
          {totalPages > 1 && (
            <Pagination
              page={currentPage}
              pages={totalPages}
              onClick={handleClickPage}
            />
          )}
        </div>
      </div>
      {isOpenModal && (
        <MaterialInsurance
          idInsurance={idInsurance}
          setIsOpenModal={setIsOpenModal}
          setToggleAlertEdit={setToggleAlertEdit}
          setValueEditInsuranceCarMaterial={setValueEditInsuranceCarMaterial}
        />
      )}
      {isCancerRegister && (
        <div className="fixed inset-0 z-50 flex h-screen items-center justify-center bg-rich-black">
          <AlertDelete
            handleCancerRegister={handleCancerRegister}
            handleDeleteInsurance={handleDeleteInsurance}
            idInsurance={idInsurance}
          />
        </div>
      )}
      {toggleAlertEdit && (
        <div className="fixed inset-0 z-50 flex h-screen items-center justify-center bg-rich-black">
          <AlertEdit
            valueEditInsurance={valueEditInsuranceCarMaterial}
            setToggleAlertEdit={setToggleAlertEdit}
          />
        </div>
      )}
    </LayoutWithHeader>
  );
}
export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'listUserInsurance',
      ])),
    },
  };
};
