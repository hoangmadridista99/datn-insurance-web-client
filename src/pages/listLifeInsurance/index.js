/* eslint-disable indent */
import LayoutWithHeader from 'components/common/LayoutWithHeader';
import React, {
  Fragment,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import {
  filterInsuranceGetAPI,
  getInsuranceObjectives,
} from 'pages/api/insurance';
import { DataContext } from 'src/store/GlobalState';
import Pagination from 'components/Pagination';
import Image from 'next/image';
import { LIST_BUTTON_FILTER } from 'src/utils/constants';
import { useSession } from 'next-auth/react';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import ButtonSort from 'components/common/ButtonSort';
import ListDetailsLineLifeInsurance from 'components/listLifeInsurance/ListDetailsLineLifeInsurance';
import FilterLifeInsurance from 'components/listLifeInsurance/FilterLifeInsurance';
import InformationInsuranceTop from 'components/common/listInsurance/InformationInsuranceTop';
import InformationInsuranceBot from 'components/common/listInsurance/InformationInsuranceBot';
import DetailLifeInsurance from 'components/listLifeInsurance/DetailLifeInsurance';
import { useTranslation } from 'next-i18next';
import AlertSuccessRating from 'components/common/listInsurance/AlertSuccessRating';
import { Dialog, Transition } from '@headlessui/react';

function ListLifeInsurance({ insuranceObjectives }) {
  const { t } = useTranslation('common');
  const { dispatch, state } = useContext(DataContext);

  const { updateDataInsurance } = state;
  const dataInsuranceCheck = updateDataInsurance?.updateDataInsurance;

  const session = useSession();

  const router = useRouter();
  const [dataFilter, setDataFilter] = useState(router.query);

  const [dataInsurance, setDataInsurance] = useState([]);
  const [dataInsuranceTopObjectives, setDataInsuranceTopObjectives] =
    useState();
  const [dataInsuranceTopTotalSumInsured, setDataInsuranceTopTotalSumInsured] =
    useState();
  const [isSuggestion, setIsSuggestion] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [dataInsuranceComparison, setDataInsuranceComparison] = useState([]);
  const [hiddenDetailTop, setHiddenDetailTop] = useState();
  const [hiddenRatingInsuranceTop, setHiddenRatingInsuranceTop] = useState();
  const [selectChangeBg, setSelectChangeBg] = useState();
  const [hiddenFilter, setHiddenFilter] = useState(false);
  const [sort, setSort] = useState('');
  const pageSize = 5;
  const [pagination, setPagination] = useState();
  const [hiddenAlertSuccessRating, setHiddenAlertSuccessRating] =
    useState(false);
  function closeModal() {
    setHiddenAlertSuccessRating(false);
  }

  useEffect(() => {
    if (router.pathname === '/listLifeInsurance') {
      if (dataInsuranceCheck?.length === 3) {
        return dispatch({
          type: 'COMPARISON',
          comparison: { comparison: false },
          updateDataInsurance: { updateDataInsurance: [] },
        });
      }
    }
  }, [router.pathname]);

  useEffect(() => {
    let { objective, insured_person } = router.query;

    if (!Array.isArray(objective)) {
      objective = objective.split(', ');
    }

    if (!Array.isArray(insured_person)) {
      insured_person = insured_person.split(', ');
    }
    setDataFilter({ ...router.query, objective, insured_person });
  }, [router.query]);

  const getListInsurance = useCallback(
    async (query, page, sort) => {
      dispatch({ type: 'LOADING', payload: { loading: true } });

      const params = {
        ...query,
        limit: pageSize,
        page: page,
        is_suggestion: isSuggestion,
        order_by: sort,
      };

      if (!sort) delete params.order_by;

      try {
        const response = await filterInsuranceGetAPI(
          {
            params: params,
          },
          session?.data?.accessToken
        );

        setHiddenDetailTop();
        setHiddenRatingInsuranceTop();
        setSelectChangeBg();
        if (!isSuggestion && currentPage === 1) {
          setDataInsurance(response?.data?.data);
          setPagination(response?.data?.meta);
          setDataInsuranceTopObjectives(response?.data?.objective_suggestion);
          setDataInsuranceTopTotalSumInsured(
            response?.data?.insured_suggestion
          );
          setIsSuggestion(true);
        } else {
          setPagination(response?.data?.meta);
          setDataInsurance(response?.data?.data);
        }
      } catch (error) {
        console.log(
          '🚀 ~ file: index.js:40 ~ getListInsurance ~ error:',
          error
        );
      }
    },
    [session?.data?.accessToken, isSuggestion]
  );

  useEffect(() => {
    getListInsurance(dataFilter, currentPage, sort).finally(() => {
      dispatch({ type: 'LOADING', payload: { loading: false } });
    });
  }, [sort]);

  const handleClickPage = (value) => {
    setCurrentPage(value + 1);
    getListInsurance(dataFilter, value + 1, sort).finally(() =>
      dispatch({ type: 'LOADING', payload: { loading: false } })
    );

    WindowScrollTop(200);
  };

  const handleHiddenFilter = () => {
    setHiddenFilter(true);
  };

  const isSuggestionTop =
    dataInsuranceTopObjectives &&
    dataInsuranceTopTotalSumInsured &&
    currentPage === 1;

  const handleSort = (sort) => {
    setSort(sort);
  };

  return (
    <LayoutWithHeader>
      {hiddenAlertSuccessRating && (
        <div className="fixed inset-0 z-50 flex items-center justify-center bg-rich-black">
          <AlertSuccessRating
            setHiddenAlertSuccessRating={setHiddenAlertSuccessRating}
          />
        </div>
      )}
      <Transition appear show={hiddenAlertSuccessRating} as={Fragment}>
        <Dialog
          as="div"
          className="relative z-10"
          onClose={closeModal}
        ></Dialog>
      </Transition>

      <div className="bg-cultured">
        <div className="comparison-container pb-16 pt-10">
          <div className="flex flex-col gap-6 lg:flex-row">
            <div className="hidden w-1/3 lg:block lg:w-1/2">
              <FilterLifeInsurance
                dispatch={dispatch}
                sort={sort}
                currentPage={currentPage}
                getListInsurance={getListInsurance}
                setIsSuggestion={setIsSuggestion}
                data={dataFilter}
                pageSize={pageSize}
                insuranceObjectives={insuranceObjectives}
                setDataInsurance={setDataInsurance}
                setCurrentPage={setCurrentPage}
              />
            </div>
            <div className="lg:hidden">
              <button
                onClick={handleHiddenFilter}
                className="flex items-center gap-2 rounded-lg border border-nickel px-4 py-2"
              >
                <Image src="/svg/sort.svg" height={20} width={20} alt="sort" />
                <p className="text-base font-bold text-nickel">
                  {t('information_search')}
                </p>
              </button>
            </div>
            {hiddenFilter && (
              <div className="absolute inset-0 z-50 bg-rich-black lg:hidden">
                <FilterLifeInsurance
                  dispatch={dispatch}
                  sort={sort}
                  currentPage={currentPage}
                  getListInsurance={getListInsurance}
                  setIsSuggestion={setIsSuggestion}
                  data={dataFilter}
                  pageSize={pageSize}
                  insuranceObjectives={insuranceObjectives}
                  setDataInsurance={setDataInsurance}
                  setHiddenFilter={setHiddenFilter}
                  setCurrentPage={setCurrentPage}
                />
              </div>
            )}

            <div className="w-full">
              <div className="mb-5 flex gap-2">
                <Image
                  width={20}
                  height={20}
                  src="/images/listInsurance/document-filter.png"
                  alt="document_filter"
                />
                <p className="text-base font-normal text-nickel">
                  {pagination?.totalItems} {t('results_for')}
                </p>
                <p className="text-base font-medium text-maastricht-blue">
                  {t('life_insurance')}
                </p>
              </div>
              {isSuggestionTop && (
                <div className="mb-5 w-full rounded-2xl bg-white p-3 shadow-listInsurance">
                  <InformationInsuranceTop
                    setHiddenAlertSuccessRating={setHiddenAlertSuccessRating}
                    DetailInsurance={DetailLifeInsurance}
                    ListDetailsLine={ListDetailsLineLifeInsurance}
                    dataInsuranceTopObjectives={dataInsuranceTopObjectives}
                    dataInsuranceTopTotalSumInsured={
                      dataInsuranceTopTotalSumInsured
                    }
                    dataInsuranceComparison={dataInsuranceComparison}
                    setDataInsuranceComparison={setDataInsuranceComparison}
                    currentPage={currentPage}
                    hiddenDetailTop={hiddenDetailTop}
                    setHiddenDetailTop={setHiddenDetailTop}
                    hiddenRatingInsuranceTop={hiddenRatingInsuranceTop}
                    setHiddenRatingInsuranceTop={setHiddenRatingInsuranceTop}
                    selectChangeBg={selectChangeBg}
                    setSelectChangeBg={setSelectChangeBg}
                  />
                </div>
              )}
              <div className="mb-6 grid grid-cols-2 gap-2 sm:flex sm:gap-4">
                {LIST_BUTTON_FILTER(t).map((list) => (
                  <ButtonSort
                    key={list?.id}
                    list={list}
                    sort={sort}
                    title={list?.label}
                    onClick={() => handleSort(list?.filterSort)}
                  />
                ))}
              </div>
              {dataInsurance?.length !== 0 ? (
                <div className="w-full">
                  <InformationInsuranceBot
                    setHiddenAlertSuccessRating={setHiddenAlertSuccessRating}
                    DetailInsurance={DetailLifeInsurance}
                    ListDetailsLine={ListDetailsLineLifeInsurance}
                    dataInsurance={dataInsurance}
                    setDataInsuranceComparison={setDataInsuranceComparison}
                    dataInsuranceComparison={dataInsuranceComparison}
                  />
                </div>
              ) : (
                <div>{t('no_data')}</div>
              )}
              {pagination?.totalPages > 0 && (
                <div className="mt-5 flex justify-end">
                  <Pagination
                    page={currentPage}
                    pages={pagination?.totalPages}
                    onClick={handleClickPage}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </LayoutWithHeader>
  );
}

export default ListLifeInsurance;

export const getServerSideProps = async ({ locale }) => {
  const insuranceObjectives = await getInsuranceObjectives();

  return {
    props: {
      insuranceObjectives: insuranceObjectives?.data,
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'insuranceLocale',
      ])),
    },
  };
};
