import LayoutWithHeader from 'components/common/LayoutWithHeader';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import React from 'react';
import InformationSend from 'components/LifeInsurance/InformationSend';
import KeyInformation from 'components/LifeInsurance/keyInformation';
import Description from 'components/LifeInsurance/description';
import Question from 'components/LifeInsurance/question';
import ContentBanner from 'components/common/ContentBanner';
import banner2 from '../../../public/images/category/banner2.png';
import { useTranslation } from 'next-i18next';

const bannerList = (t) => [
  {
    id: 1,
    smallTitle: t('buyAtVeryLowPrice'),
    coreTitle: t('lifeInsurance'),
    image: banner2,
    buttonList: [
      {
        id: 1,
        type: 'primary',
        case: 'medium',
        href: '/filterLifeInsurance',
        title: t('compareNow'),
        styleButton: 'w-57',
      },
    ],
  },
];

const questions = (t) => [
  {
    id: 1,
    title: t('bestLifeInsurance'),
    question: t('bestLifeInsuranceAnswer'),
  },
  {
    id: 2,
    title: t('documentsAreRequired'),
    question: t('documentsAreRequiredDetail'),
    details: [
      {
        detailId: 1,
        detail: t('documentsAreRequiredAnswer1'),
      },
      {
        detailId: 2,
        detail: t('documentsAreRequiredAnswer2'),
      },
      {
        detailId: 3,
        detail: t('documentsAreRequiredAnswer3'),
      },
    ],
  },
  {
    id: 3,
    title: t('notReceiveCompensation'),
    question: t('notReceiveCompensationDetail'),
    details: [
      {
        detailId: 1,
        detail: t('notReceiveCompensationAnswer1'),
      },
      {
        detailId: 2,
        detail: t('notReceiveCompensationAnswer2'),
      },
      {
        detailId: 3,
        detail: t('notReceiveCompensationAnswer3'),
      },
      {
        detailId: 4,
        detail: t('notReceiveCompensationAnswer4'),
      },
      {
        detailId: 5,
        detail: t('notReceiveCompensationAnswer5'),
      },
    ],
  },
  {
    id: 4,
    title: t('premiumPaymentDelayed'),
    question: t('premiumPaymentDelayedAnswer'),
  },
  {
    id: 5,
    title: t('possibleToAdvance'),
    question: t('possibleToAdvanceAnswer'),
  },
];

function LifeInsurance() {
  const { t } = useTranslation(['lifeInsurance']);

  return (
    <LayoutWithHeader>
      <ContentBanner bannerList={bannerList(t)} detail={t('titleDetail')} />
      <KeyInformation />
      <Description />
      <Question
        questions={questions(t)}
        smallTitle={t('answers')}
        coreTitle={t('frequentlyAskedQuestions')}
      />
      <InformationSend />
    </LayoutWithHeader>
  );
}

export default LifeInsurance;

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'lifeInsurance',
      ])),
    },
  };
};
