import { SessionProvider } from 'next-auth/react';
import React from 'react';
import { QueryClient, QueryClientProvider } from 'react-query';
import 'react-toastify/dist/ReactToastify.css';
import '../styles/globals.css';
import Loading from 'components/Loading';
import { DataProvider } from 'src/store/GlobalState';
import { ToastContainer } from 'react-toastify';
import { appWithTranslation } from 'next-i18next';
import Head from 'next/head';
import LoginNoAccount from 'components/LoginNoAccount';
import ComparisonInsurance from 'components/ComparisonInsurance';
import { Roboto } from 'next/font/google';
import 'react-loading-skeleton/dist/skeleton.css';

const roboto = Roboto({
  weight: ['400', '500', '700'],
  // weight: '400',

  subsets: ['latin'],
  variable: '--font-roboto',
});

function App({ Component, pageProps: { session, ...pageProps } }) {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        // ✅ globally default to 20 seconds
        staleTime: 1000 * 20,
      },
    },
  });

  return (
    <>
      <Head>
        <title>Trang chủ - Website Sosanh24</title>
      </Head>
      <SessionProvider session={session} className={roboto.className}>
        <QueryClientProvider client={queryClient}>
          <DataProvider>
            <Loading />
            <ToastContainer />
            <LoginNoAccount />
            <main className={`${roboto.variable} font-roboto`}>
              <Component {...pageProps} />
            </main>
            <ComparisonInsurance />
          </DataProvider>
        </QueryClientProvider>
      </SessionProvider>
    </>
  );
}

export default appWithTranslation(App);
