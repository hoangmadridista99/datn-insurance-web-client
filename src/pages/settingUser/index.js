import LayoutWithHeader from 'components/common/LayoutWithHeader';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Image from 'next/image';
import React, { useContext, useEffect, useState, useRef } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'next-i18next';
import { IconUpload } from 'components/common/icon/Icon';
import { useSession } from 'next-auth/react';
import { patchProfileUser } from 'pages/api/user';
import { DataContext } from 'src/store/GlobalState';
import { toast } from 'react-toastify';
import ButtonPrimary from 'components/common/ButtonPrimary';
import { GENDER } from 'src/utils/constants';

export default function SettingUser() {
  const { t } = useTranslation(['auth', 'common']);
  const { data: session, update } = useSession();
  const dataProfileUser = session;

  const { dispatch } = useContext(DataContext);

  const [fileImage, setFileImage] = useState(null);
  const { register, handleSubmit, setValue, reset } = useForm();

  useEffect(() => {
    const dataProfile = {
      first_name: dataProfileUser?.first_name,
      last_name: dataProfileUser?.last_name,
      phone: dataProfileUser?.phone,
      email: dataProfileUser?.email,
      gender: dataProfileUser?.gender,
      date_of_birth: dataProfileUser?.date_of_birth,
    };

    reset(dataProfile);
  }, [dataProfileUser]);

  const inputFileRef = useRef(null);

  const handlePostImage = (event) => {
    const maxSize = 1024 * 1024 * 15;
    const file = event.target.files[0];
    if (file > maxSize) {
      return toast.error(t('auth:photo_max'));
    }
    if (file) {
      const reader = new FileReader();

      reader.onload = () => {
        const data = reader.result;

        setFileImage(data);
      };

      reader.readAsDataURL(file);
      setValue('file', file);
    }
  };

  const handleDeleteImagePost = () => {
    setFileImage(null);
    setValue('file', undefined);
  };

  const handleSettingAccount = async (values) => {
    dispatch({ type: 'LOADING', payload: { loading: true } });

    try {
      const response = await patchProfileUser(session?.accessToken, {
        params: { ...values },
      });

      toast(t('auth:successfully_updated'));

      if (response?.data?.avatar_profile_url) {
        update({
          ...values,
          avatar_profile_url: response?.data?.avatar_profile_url,
        });
      } else {
        update({ ...dataProfileUser, ...values });
      }
      setFileImage(null);
    } catch (error) {
      toast.error(t(`common:error.${error?.response?.data?.error_code}`));
    }

    dispatch({ type: 'LOADING', payload: { loading: false } });
  };
  return (
    <LayoutWithHeader>
      <form onSubmit={handleSubmit(handleSettingAccount)}>
        <div className="bg-lotion">
          <div className="comparison-container gap-10 pt-10 pb-28 md:flex">
            <div className="md:w-1/3">
              <div className=" rounded-2xl border bg-white px-6 pb-6 shadow-listInsurance md:p-6">
                <h1 className="border-b py-4 text-xl font-bold text-arsenic md:mb-6 md:pb-6 md:text-2xl">
                  {t('auth:avatar')}
                </h1>
                <div>
                  <div className="flex justify-end">
                    {fileImage && (
                      <button
                        onClick={handleDeleteImagePost}
                        className="rounded-full border px-3 py-1"
                      >
                        X
                      </button>
                    )}
                  </div>
                  <div className="my-6 flex flex-col items-center justify-center md:mt-0">
                    {!dataProfileUser?.avatar_profile_url && !fileImage && (
                      <>
                        <div className="max-h-30 max-w-30">
                          <Image
                            src={
                              dataProfileUser?.gender === 'female'
                                ? '/images/navbar/logoUserFemale.png'
                                : '/images/navbar/logoUserMale.png'
                            }
                            width={120}
                            height={120}
                            alt="logoUser"
                          />
                        </div>
                        <p className="w-full text-center">
                          {t('auth:maximum_image_size')}
                        </p>
                      </>
                    )}

                    {(fileImage || dataProfileUser?.avatar_profile_url) && (
                      <div className="flex h-32 w-32 justify-center rounded-full border-4 border-solid border-lavender">
                        <Image
                          width={1200}
                          height={1200}
                          src={fileImage ?? dataProfileUser?.avatar_profile_url}
                          alt="logoCompany"
                          className="rounded-full"
                        />
                      </div>
                    )}
                  </div>
                </div>
                <div className="flex justify-center">
                  <div className="bg-red-500">
                    <input
                      ref={inputFileRef}
                      type="file"
                      id="file"
                      className="hidden"
                      onChange={handlePostImage}
                      accept=".jpg, .jpeg, .png"
                    />
                  </div>
                  <button
                    type="button"
                    onClick={() => inputFileRef?.current?.click()}
                    className="flex gap-2 rounded-lg border border-solid border-ultramarine-blue py-2 px-4"
                  >
                    <IconUpload />
                    <p className="text-base font-bold text-ultramarine-blue">
                      {t('auth:upload')}
                    </p>
                  </button>
                </div>
              </div>
            </div>
            <div className="mt-16 rounded-2xl border bg-white px-6 pb-6 shadow-listInsurance md:mt-0 md:w-2/3 md:p-6">
              <h1 className="mb-6 border-b py-4 text-xl font-bold text-arsenic md:pb-6 md:text-2xl">
                {t('auth:personal_information')}
              </h1>
              <div className="mb-6 justify-between gap-6 md:flex">
                <div className="mb-6 w-full md:mb-0">
                  <label
                    className="mb-2 text-sm font-bold text-arsenic md:text-base"
                    htmlFor="first_name"
                  >
                    {t('auth:firstName')}
                  </label>
                  <div className="mt-2 rounded-md border border-chinese-silver p-3">
                    <input
                      className="w-full bg-transparent text-base font-normal outline-none"
                      type="text"
                      maxLength="15"
                      id="first_name"
                      {...register('first_name')}
                    />
                  </div>
                </div>
                <div className="w-full">
                  <label
                    className="mb-2 text-sm font-bold text-arsenic md:text-base"
                    htmlFor="last_name"
                  >
                    {t('auth:lastName')}
                  </label>
                  <div className="mt-2 rounded-md border border-chinese-silver p-3">
                    <input
                      className="w-full bg-transparent text-base font-normal outline-none"
                      type="text"
                      maxLength="15"
                      id="last_name"
                      {...register('last_name')}
                    />
                  </div>
                </div>
              </div>

              <div className="mb-6 flex items-center gap-6">
                <label className='htmlFor="gender" mb-2 text-sm font-bold text-arsenic md:text-base'>
                  {t('auth:gender')}
                </label>
                <div className="flex gap-4">
                  {GENDER(t).map((list) => (
                    <div
                      key={list?.value}
                      className="mb-5 mt-4 flex items-center gap-4"
                    >
                      <input
                        type="radio"
                        className="h-6 w-6"
                        id="gender"
                        {...register('gender')}
                        value={list?.value}
                      />
                      <p className="text-sm font-normal md:text-base">
                        {list?.label}
                      </p>
                    </div>
                  ))}
                </div>
              </div>

              <div className="mb-6">
                <label
                  className="mb-2 text-sm font-bold text-arsenic md:text-base"
                  htmlFor="email"
                >
                  {t('auth:email')}
                </label>
                <div className="mt-2 rounded-md border border-platinum bg-cultured p-3 text-platinum">
                  <input
                    className="w-full cursor-not-allowed bg-transparent text-base font-normal outline-none"
                    id="email"
                    value={dataProfileUser?.email}
                    disabled={true}
                    {...register('email')}
                  />
                </div>
              </div>

              <div className="mb-6 justify-between gap-6 md:flex">
                <div className="mb-6 w-full md:mb-0">
                  <label
                    className="mb-2 text-sm font-bold text-arsenic md:text-base"
                    htmlFor="phone"
                  >
                    {t('auth:phone')}
                  </label>
                  <div className="mt-2 rounded-md border border-platinum bg-cultured p-3 text-platinum">
                    <input
                      className="w-full cursor-not-allowed bg-transparent text-base font-normal outline-none"
                      id="phone"
                      disabled={true}
                      value={dataProfileUser?.phone}
                      {...register('phone')}
                    />
                  </div>
                </div>
                <div className="w-full">
                  <label
                    className="mb-2 text-sm font-bold text-arsenic md:text-base"
                    htmlFor="date_of_birth"
                  >
                    {t('auth:date_of_birth')}
                  </label>
                  <div className="mt-2 rounded-md border border-chinese-silver p-3">
                    <input
                      className="w-full bg-transparent text-base font-normal outline-none"
                      type="date"
                      id="date_of_birth"
                      {...register('date_of_birth')}
                    />
                  </div>
                </div>
              </div>

              <div className="flex justify-center md:justify-end">
                <ButtonPrimary
                  type={'submit'}
                  cases={'medium'}
                  title={t('auth:save_change')}
                  widthBtn={'justify-self-end w-1/3 xl:w-1/4'}
                />
              </div>
            </div>
          </div>
        </div>
      </form>
    </LayoutWithHeader>
  );
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['auth', 'home', 'common'])),
    },
  };
};
