import axios from 'axios';

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

export const filterInsuranceGetAPI = async ({ params }, token) => {
  const { objective, insured_person, ...dataQuery } = params;
  const query = Object.keys(dataQuery).reduce(
    (result, key, i) =>
      `${i === 0 ? result : `${result}&`}${key}=${dataQuery[key]}`,
    ''
  );
  const queryObjective = [
    ...(typeof objective === 'string' ? objective.split(', ') : objective),
  ].reduce((result, item) => `${result}&objective=${item}`, '');
  const queryInsuredPerson = [
    ...(typeof insured_person === 'string'
      ? insured_person.split(', ')
      : insured_person),
  ].reduce((result, item) => `${result}&insured_person=${item}`, '');
  const url = `${BASE_URL}v1/api/client/insurances/life?${query}&${queryObjective}&${queryInsuredPerson}`;

  let headers = {};
  if (token) {
    headers = {
      Authorization: `Bearer ${token}`,
    };
  }

  return await axios.get(url, {
    headers,
  });
};

export const getInsuranceObjectives = async () => {
  return await axios.get(`${BASE_URL}v1/api/client/insurance-objectives`);
};

export const getProfileUsers = async (token) => {
  return await axios.get(`${BASE_URL}v1/api/client/users/profile`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const getInsuranceIdAPI = async (id) => {
  return await axios.get(`${BASE_URL}v1/api/client/insurances/${id}`);
};

export const getRatingInsurance = async ({ id, params }) => {
  return await axios.get(`${BASE_URL}v1/api/client/ratings/insurances/${id}`, {
    params,
  });
};

export const createRatingInsurance = async (token, { params }) => {
  return await axios.post(
    `${BASE_URL}v1/api/client/ratings`,
    {
      ...params,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const addInsuranceConcern = async (token, { params }) => {
  return await axios.post(
    `${BASE_URL}v1/api/client/user-insurances`,
    { ...params },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const getInsuranceConcernUserAPI = async (token, { params }) => {
  return await axios.get(`${BASE_URL}v1/api/client/user-insurances`, {
    params,

    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const getAppraisals = async (token, { params }) => {
  return await axios.get(`${BASE_URL}v1/api/client/appraisal-forms/users`, {
    params,

    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const getAppraisalDetails = async (token, id) => {
  return await axios.get(`${BASE_URL}v1/api/client/appraisal-forms/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const deleteAppraisalInsurance = async (token, id) => {
  return await axios.delete(`${BASE_URL}v1/api/client/appraisal-forms/${id}`, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const getRatingCarInsurance = async ({ id, params }) => {
  return await axios.get(
    `${BASE_URL}v1/api/client/car-insurance-ratings/car-insurances/${id}`,
    {
      params,
    }
  );
};

export const createRatingCarInsurance = async (token, { params }) => {
  return await axios.post(
    `${BASE_URL}v1/api/client/car-insurance-ratings`,
    {
      ...params,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};
