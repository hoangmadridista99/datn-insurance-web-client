import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';
import { ERROR_SIGN_IN } from 'src/utils/error';
const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

export const authOptions = {
  // Configure one or more authentication providers
  providers: [
    CredentialsProvider({
      type: 'credentials',
      credentials: {},
      async authorize(credentials) {
        try {
          const authResponse = await fetch(
            `${BASE_URL}v1/api/client/auth/login`,
            {
              method: 'POST',
              body: JSON.stringify({
                destination: credentials.destination,
                password: credentials.password,
              }),
              headers: { 'Content-Type': 'application/json' },
            }
          );

          const user = await authResponse.json();

          if (!authResponse.ok) {
            switch (authResponse.status) {
              case 422:
                throw ERROR_SIGN_IN.ERROR_422;
              case 403:
                throw ERROR_SIGN_IN.ERROR_403;
              case 401:
                throw ERROR_SIGN_IN.ERROR_401;
              default:
                throw Error('Lỗi máy chủ!');
            }
          }

          if (authResponse.ok) {
            return { ...user, name: user?.first_name };
          }
          return null;
        } catch (error) {
          throw Error(error);
        }
      },
    }),
  ],

  callbacks: {
    async jwt({ token, trigger, user, session }) {
      // Persist the Credentials access_token to the token right after signin
      if (user) {
        token.accessToken = user.accessToken;
        token.id = user.id;
        token.role = user.role;
        token.first_name = user.first_name;
        token.last_name = user.last_name;
        token.phone = user.phone;
        token.email = user.email;
        token.gender = user.gender;
        token.date_of_birth = user.date_of_birth;
        token.avatar_profile_url = user.avatar_profile_url;
      }
      if (trigger === 'update') {
        token.first_name = session.first_name;
        token.last_name = session.last_name;
        token.gender = session.gender;
        token.date_of_birth = session.date_of_birth;
        token.avatar_profile_url = session.avatar_profile_url;
      }
      return token;
    },

    async session({ session, token }) {
      // Send properties to the client, like an access_token from a provider.
      session.accessToken = token.accessToken;
      session.role = token.role;
      session.first_name = token.first_name;
      session.last_name = token.last_name;
      session.phone = token.phone;
      session.email = token.email;
      session.gender = token.gender;
      session.date_of_birth = token.date_of_birth;
      session.avatar_profile_url = token.avatar_profile_url;

      return session;
    },
  },

  async redirect({ url, baseUrl }) {
    if (url.startsWith('/')) return `${baseUrl}${url}`;
    else if (new URL(url).origin === baseUrl) return url;
    return baseUrl;
  },

  session: {
    strategy: 'jwt',
    maxAge: 30 * 24 * 60 * 60 * 365, // 365 days
    updateAge: 24 * 60 * 60, // 24 hours
  },

  pages: {
    signIn: '/auth/login',
  },

  secret: process.env.NEXTAUTH_SECRET,
  // Enable debug messages in the console if you are having problems
  debug: true,
};

export default NextAuth(authOptions);
