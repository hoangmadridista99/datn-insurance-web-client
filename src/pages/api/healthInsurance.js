import axios from 'axios';

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

export const getListHealthInsurances = async ({ params }, token) => {
  const { room_type, insured_person, ...dataQuery } = params;

  const query = Object.keys(dataQuery).reduce(
    (result, key, i) =>
      `${i === 0 ? result : `${result}&`}${key}=${dataQuery[key]}`,
    ''
  );
  const queryObjective = [
    ...(typeof room_type === 'string' ? room_type.split(', ') : room_type),
  ].reduce((result, item) => `${result}&room_type=${item}`, '');
  const queryInsuredPerson = [
    ...(typeof insured_person === 'string'
      ? insured_person.split(', ')
      : insured_person),
  ].reduce((result, item) => `${result}&insured_person=${item}`, '');
  const url = `${BASE_URL}v1/api/client/insurances/health?${query}&${queryObjective}&${queryInsuredPerson}`;

  let headers = {};
  if (token) {
    headers = {
      Authorization: `Bearer ${token}`,
    };
  }

  return await axios.get(url, {
    headers,
  });
};
