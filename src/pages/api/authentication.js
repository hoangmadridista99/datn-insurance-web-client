import axios from 'axios';

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

export const registerNewUserAPI = async ({
  params,
  address,
  destination_type,
  otp_code,
}) => {
  return await axios.post(`${BASE_URL}v1/api/client/auth/register`, {
    ...params,
    otp_code,
    address,
    destination_type,
  });
};

export const registerNewVendorAPI = async ({
  params,
  destination_type,
  otp_code,
}) => {
  return await axios.post(`${BASE_URL}v1/api/vendor/auth/register`, {
    ...params,
    otp_code,
    destination_type,
  });
};

export const getOTP = async ({ params, type, destination_type }) => {
  return await axios.post(`${BASE_URL}v1/api/client/otps/send-otp`, {
    ...params,
    destination_type,
    type,
  });
};

export const getOTPVendor = async ({ params, type, destination_type }) => {
  return await axios.post(`${BASE_URL}v1/api/vendor/otps/register/send-otp`, {
    ...params,
    destination_type,
    type,
  });
};

export const getCompanies = async () =>
  await axios.get(`${BASE_URL}v1/api/client/companies`);

export const changePasswordAPI = async (token, { params }) => {
  return await axios.post(
    `${BASE_URL}v1/api/client/auth/change-password`,
    {
      ...params,
    },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};
