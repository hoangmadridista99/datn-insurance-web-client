import axios from 'axios';

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

// export const getUserProfileAPI = async () => {
//   try {
//     const { data } = await axiosInstance.get(
//       `${BASE_URL}v1/api/client/users/profile`
//     )
//     if (data) {
//       return data
//     }
//   } catch (error) {
//     console.log('🚀 ===== error:', error)
//   }
//   return null
// }

export const patchProfileUser = async (token, { params }) => {
  return await axios.patch(
    `${BASE_URL}v1/api/client/users/profile`,
    {
      ...params,
    },
    {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
    }
  );
};

export const getUserProfileAPI = async (token) => {
  return await axios.get(
    `${BASE_URL}v1/api/client/users/profile`,

    {
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`,
      },
    }
  );
};
