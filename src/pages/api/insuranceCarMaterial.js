import axios from 'axios';

const BASE_URL = process.env.NEXT_PUBLIC_BASE_URL;

export const postInsuranceCarMaterial = async (token, { params }) => {
  return await axios.post(`${BASE_URL}v1/api/client/appraisal-forms`, params, {
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'multipart/form-data',
    },
  });
};

export const patchInsuranceCarMaterial = async (token, id, { params }) => {
  return await axios.patch(
    `${BASE_URL}v1/api/client/appraisal-forms/${id}`,
    params,
    {
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'multipart/form-data',
      },
    }
  );
};

export const getInsuranceCarPartner = async ({ params }) => {
  return await axios.get(`${BASE_URL}v1/api/client/car-insurances`, {
    params,
  });
};

export const getInsuranceCarSetting = async () => {
  return await axios.get(
    `${BASE_URL}v1/api/client/car-insurance-settings/mandatory/details`
  );
};

export const postInsuranceMandatory = async (token, { params }) => {
  return await axios.post(
    `${BASE_URL}v1/api/client/mandatory-forms`,
    { ...params },
    {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    }
  );
};
