import axios from 'axios';
import { getSession } from 'next-auth/react';

// axios instance for making requests
const axiosInstance = axios.create();

// request interceptor for adding token
axiosInstance.interceptors.request.use(async (config) => {
  const session = await getSession();
  // add token to request headers
  config.headers.Authorization = `Bearer ${session?.accessToken}`;
  return config;
});

export default axiosInstance;
