import ButtonBgWhite from 'components/common/ButtonBgWhite';
import LayoutWithHeader from 'components/common/LayoutWithHeader';
import DetailsStepOne from 'components/insuranceCarCompulsory/DetailsStepOne';
import DetailsStepTwo from 'components/insuranceCarCompulsory/DetailsStepTwo';
import PayInsurance from 'components/insuranceCarCompulsory/PayInsurance';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import React, { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'next-i18next';
import { stepFillInformation } from 'src/utils/stepHooks';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import { DataContext } from 'src/store/GlobalState';
import {
  getInsuranceCarPartner,
  postInsuranceMandatory,
} from 'pages/api/insuranceCarMaterial';
import { toast } from 'react-toastify';
import { useSession } from 'next-auth/react';
import { yupResolver } from '@hookform/resolvers/yup';
import { schemaCarMaterial } from 'src/utils/error';
import numeral from 'numeral';
import {
  lockScroll,
  regexCarLicense,
  regexEmail,
  regexPhoneNumber,
} from 'src/utils/helper';

export default function InsuranceCardCompulsory() {
  const { t } = useTranslation(['common', 'auth']);
  const { dispatch } = useContext(DataContext);
  const session = useSession();

  const {
    register,
    setError,
    handleSubmit,
    setValue,
    resetField,
    control,
    formState: { errors },
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schemaCarMaterial()),
  });

  const [stepsCurrent, setStepsCurrent] = useState(1);
  const [informationInsurance, setInformationInsurance] = useState();
  const [partners, setPartners] = useState();
  const [totalFeeOfYear, setTotalFeeOfYear] = useState();
  const [validateRule, setValidateRule] = useState(false);
  const [timeDeadlineForDeal, setTimeDeadlineForDeal] = useState();
  const [valueTypeCarNoBusiness, setValueTypeCarNoBusiness] = useState();
  const [valueCarSeats, setValueCarSeats] = useState();
  const [typeCarBusiness, setTypeCarBusiness] = useState();
  const [refreshLabelInput, setRefreshLabelInput] = useState(true);
  const [valueSeat, setValueSeat] = useState();

  const yearCurrent = Number(timeDeadlineForDeal?.slice(0, 4));
  const timeNew = timeDeadlineForDeal?.replace(yearCurrent, yearCurrent + 1);

  const dataPartnerInsurance = async () => {
    try {
      const response = await getInsuranceCarPartner({
        params: {
          insurance_type: 'mandatory',
          limit: 10,
          page: 1,
        },
      });
      setPartners(response?.data.data);
    } catch (error) {
      console.log('🚀 ~ file: index.js:154 ~ nextSteps ~ error:', error);
    }
  };

  useEffect(() => {
    dataPartnerInsurance();
  }, []);

  const detailsSteps = () => {
    switch (stepsCurrent) {
      case 1:
        return (
          <DetailsStepOne
            resetField={resetField}
            valueSeat={valueSeat}
            setValueSeat={setValueSeat}
            refreshLabelInput={refreshLabelInput}
            setRefreshLabelInput={setRefreshLabelInput}
            valueTypeCarNoBusiness={valueTypeCarNoBusiness}
            setValueTypeCarNoBusiness={setValueTypeCarNoBusiness}
            valueCarSeats={valueCarSeats}
            setValueCarSeats={setValueCarSeats}
            typeCarBusiness={typeCarBusiness}
            setTypeCarBusiness={setTypeCarBusiness}
            setTimeDeadlineForDeal={setTimeDeadlineForDeal}
            timeNew={timeNew}
            partners={partners}
            control={control}
            errors={errors}
            register={register}
            setError={setError}
            setValue={setValue}
            setTotalFeeOfYear={setTotalFeeOfYear}
            totalFeeOfYear={totalFeeOfYear}
          />
        );
      case 2:
        return (
          <DetailsStepTwo
            valueSeat={valueSeat}
            timeNew={timeNew}
            partners={partners}
            dataInformationInsurance={informationInsurance}
            totalFeeOfYear={totalFeeOfYear}
            setValidateRule={setValidateRule}
            validateRule={validateRule}
          />
        );
      case 3:
        return <PayInsurance />;
    }
  };

  const backSteps = () => {
    if (stepsCurrent === 1) return;
    setStepsCurrent(stepsCurrent - 1);
    if (stepsCurrent === 2) return setValidateRule(false);
    if (stepsCurrent === 3) return setValidateRule(true);
  };

  const sendInformation = async (values) => {
    const isPassCheckOne = onCheckValueFormOne(values);

    if (!isPassCheckOne) return;
    setInformationInsurance(values);

    if (stepsCurrent === 2) {
      WindowScrollTop(0);
      if (!session?.data) {
        dispatch({
          type: 'LOGIN',
          loginNoAccount: { login: true },
        });
        toast.error(t('please_login_buy_insurance'));
        lockScroll();
        return;
      }

      const { car_seats, is_transportation_business, ...data } = values;
      const params = {
        ...data,
        is_transportation_business: Boolean(is_transportation_business),
        car_seats: car_seats ? Number(car_seats) : Number(valueSeat),
        expiration_to: timeNew,
      };
      dispatch({ type: 'LOADING', payload: { loading: true } });
      try {
        await postInsuranceMandatory(session?.data?.accessToken, {
          params: params,
        });
        setStepsCurrent(stepsCurrent + 1);
        dispatch({ type: 'LOADING', payload: { loading: false } });
        toast.success(t('registerSuccess'));
      } catch (error) {
        toast.error(t(`common:error.${error?.response?.data?.error_code}`));
        console.log('🚀 ~ file: index.js:154 ~ nextSteps ~ error:', error);
        dispatch({ type: 'LOADING', payload: { loading: false } });
      }
    }
    setStepsCurrent(stepsCurrent + 1);
    setValidateRule(true);

    if (stepsCurrent > 2) return;
  };

  const onCheckValueFormOne = ({
    is_transportation_business,
    car_type,
    registration_holder_name,
    registration_address,
    registration_brand,
    registration_car_license_plate,
    certificate_holder_name,
    certificate_holder_phone,
    certificate_holder_identification_card,
    certificate_holder_email,
    certificate_holder_address,
    car_insurance_id,
    expiration_from,
    registration_chassis_number,
    registration_engine_number,
  }) => {
    const checkPhoneNumber = regexPhoneNumber.test(certificate_holder_phone);
    const checkEmail = regexEmail.test(certificate_holder_email);
    const checkCarLicense = regexCarLicense.test(
      registration_car_license_plate
    );

    if (!is_transportation_business) {
      setError('is_transportation_business', { message: t('please_choice') });
    }
    if (!car_type) {
      setError('car_type', { message: t('please_choice') });
    }
    if (!registration_holder_name) {
      setError('registration_holder_name', { message: t('please_choice') });
    }
    if (!registration_address) {
      setError('registration_address', { message: t('please_choice') });
    }
    if (!registration_brand) {
      setError('registration_brand', { message: t('please_choice') });
    }
    if (!registration_car_license_plate) {
      setError('registration_car_license_plate', {
        message: t('please_choice'),
      });
    }
    if (!checkCarLicense) {
      setError('registration_car_license_plate', {
        message: t('please_registration_car'),
      });
    }
    if (!certificate_holder_name) {
      setError('certificate_holder_name', { message: t('please_choice') });
    }
    if (!certificate_holder_phone) {
      setError('certificate_holder_phone', { message: t('please_choice') });
    }
    if (!checkPhoneNumber) {
      setError('certificate_holder_phone', {
        message: t('auth:schema.phone_matches'),
      });
    }
    if (!certificate_holder_identification_card) {
      setError('certificate_holder_identification_card', {
        message: t('please_choice'),
      });
    }
    if (
      certificate_holder_identification_card.length !== 12 &&
      certificate_holder_identification_card.length !== 9
    ) {
      setError('certificate_holder_identification_card', {
        message: 'Vui lòng nhâp đúng số CMND hoặc CCCD',
      });
    }
    if (!certificate_holder_email) {
      setError('certificate_holder_email', { message: t('please_choice') });
    }
    if (!checkEmail) {
      setError('certificate_holder_email', {
        message: t('auth:schema.email'),
      });
    }
    if (!certificate_holder_address) {
      setError('certificate_holder_address', { message: t('please_choice') });
    }
    if (!car_insurance_id) {
      setError('car_insurance_id', { message: t('please_choice') });
    }
    if (!expiration_from) {
      setError('expiration_from', { message: t('please_choice') });
    }
    if (!registration_engine_number) {
      setError('registration_engine_number', { message: t('please_choice') });
    }
    if (!registration_chassis_number) {
      setError('registration_chassis_number', { message: t('please_choice') });
    }

    WindowScrollTop(200);
    const require =
      is_transportation_business &&
      car_type &&
      registration_holder_name &&
      registration_address &&
      registration_brand &&
      (certificate_holder_identification_card.length === 9 ||
        certificate_holder_identification_card.length === 12) &&
      certificate_holder_name &&
      certificate_holder_name &&
      certificate_holder_email &&
      certificate_holder_address &&
      car_insurance_id &&
      expiration_from &&
      checkPhoneNumber &&
      checkEmail &&
      (refreshLabelInput
        ? registration_car_license_plate && checkCarLicense
        : registration_chassis_number && registration_engine_number);
    if (require) {
      return true;
    }
    return;
  };
  return (
    <LayoutWithHeader backgroundFooter="lg:bg-lotion">
      <div className="comparison-container pt-8 lg:pt-14">
        <div className="mb-12 items-center justify-between md:flex">
          <p className="text-2xl font-bold text-arsenic lg:text-pixel-32 lg:leading-pixel-48">
            {t('insurance_card_compulsory')}
          </p>
          <div className="flex items-center">
            {stepFillInformation(t, stepsCurrent).map((steps) => (
              <div key={steps.id} className="flex">
                {steps?.nameStep && <>{steps?.nameStep}</>}
                {steps?.space && (
                  <div className="flex items-center md:hidden">
                    {steps?.space}
                  </div>
                )}
              </div>
            ))}
          </div>
        </div>

        <form>{detailsSteps()}</form>

        <div className="sticky left-27 bottom-0 z-40 mb-28 w-full bg-white">
          <div className="w-full justify-between py-6 md:flex">
            <div className="mb-3 text-sm font-normal md:mb-0 lg:text-base">
              <span className="mr-2 text-nickel">{t('total_sum')}</span>
              <span className="text-2xl font-bold text-arsenic">
                {numeral(totalFeeOfYear).format('(0,0)')} VNĐ
              </span>
              <span className="text-silver">/{t('Year')}</span>
            </div>
            <div className="w-full gap-4 md:flex md:w-2/5">
              <div className="mb-3 w-full md:mb-0">
                <ButtonBgWhite
                  cases="large"
                  widthBtn="text-ultramarine-blue border-ultramarine-blue w-full"
                  title={t('Back')}
                  onClick={backSteps}
                />
              </div>
              <div className="w-full">
                <button
                  type="button"
                  disabled={validateRule}
                  onClick={handleSubmit(sendInformation)}
                  className={`${
                    validateRule
                      ? 'bg-zinc-400 text-white'
                      : 'bg-ultramarine-blue from-egyptian-blue to-ultramarine-blue text-cultured'
                  } w-full rounded-lg py-3 text-base font-bold hover:bg-gradient-to-r`}
                >
                  {t('Buy')}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </LayoutWithHeader>
  );
}
export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['auth', 'home', 'common'])),
    },
  };
};
