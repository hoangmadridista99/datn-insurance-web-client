import React, { useState } from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import banner from '../../../public//images/category/i1.png';
import { useTranslation } from 'next-i18next';
import banner2 from '../../../public/images/category/banner2.png';
import bgMobile from '../../../public/images/healthInsurance/bannerMobile.png';
import bgTablet from '../../../public/images/healthInsurance/bannerIpad.png';
import bannerCarInsurance from '../../../public/images/category/bannerCarInsurance.png';
import bannerHealthInsurance from '../../../public/images/healthInsurance/healthInsurance.png';

import Image from 'next/image';
import OurCustomer from 'components/common/OurCustomer';
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import 'swiper/css/pagination';

// import required modules
import { Pagination } from 'swiper';
import LayoutWithHeader from 'components/common/LayoutWithHeader';
import Link from 'next/link';
import ContentBanner from 'components/common/ContentBanner';
import { categoryInsurance } from 'src/utils/constants';

const listDescriptions = [
  {
    id: 1,
    detail: 'Simply dummy text of the printing and typesetting industry',
  },
  {
    id: 2,
    detail:
      'It has survived not only five centuries, but also the leap into electronic typesetting',
  },
  {
    id: 3,
    detail:
      'It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipums',
  },
];
const categoryInsuranceList = [
  {
    id: 1,
    title: 'risksAndPrecautions',
  },
  {
    id: 2,
    title: 'responsibilitiesAndLaws',
  },
  {
    id: 3,
    title: 'tourism',
  },
  {
    id: 4,
    title: 'housesAndProperties',
  },
  {
    id: 5,
    title: 'pet',
  },
  {
    id: 6,
    title: 'medical',
  },
  {
    id: 7,
    title: 'vehiclesAndVehicles',
  },
  {
    id: 8,
    title: 'health',
  },
  {
    id: 9,
    title: 'salaryAndRetirement',
  },
];

export default function Index() {
  const [bannerID, setBannerID] = useState(1);
  const [toggleId, setToggleId] = useState(1);

  const handleToggle = (id) => {
    setToggleId(id);
  };

  const handleBanner = (id) => {
    setBannerID(id);
  };
  const { t } = useTranslation(['category']);

  const bannerList = [
    {
      id: 1,
      smallTitle: t('buyAtVeryLowPrice'),
      coreTitle: t('lifeInsurance'),
      image: banner2,
      buttonList: [
        {
          id: 1,
          type: 'primary',
          case: 'medium',
          href: '/filterLifeInsurance',
          title: t('compareNow'),
          styleButton: 'w-57',
        },
      ],
    },
    {
      id: 2,
      smallTitle: t('buyAtVeryLowPrice'),
      coreTitle: t('carInsurance'),
      image: bannerCarInsurance,
      buttonList: [
        {
          id: 1,
          type: 'primary',
          case: 'medium',
          href: '',
          title: t('compareNow'),
          styleButton: 'w-57',
        },
      ],
    },
    {
      id: 3,
      smallTitle: t('buyAtVeryLowPrice'),
      coreTitle: t('healthInsurance'),
      image: bannerHealthInsurance,
      buttonList: [
        {
          id: 1,
          type: 'primary',
          case: 'medium',
          href: '/filterHealthInsurance',
          title: t('compareNow'),
          styleButton: 'w-57',
        },
      ],
    },
  ];

  return (
    <LayoutWithHeader>
      <div>
        <div className="relative">
          <div>
            <Image
              src={banner}
              alt="bgBanner"
              className="absolute -z-50 hidden h-full max-h-[488px] w-full lg:block"
            />
          </div>
          <div className="absolute inset-0 -z-10 w-full sm:hidden">
            <Image src={bgMobile} fill alt="banner" />
          </div>
          <div className="absolute inset-0 -z-10 hidden w-full sm:block lg:hidden">
            <Image src={bgTablet} fill alt="banner" />
          </div>
          <ContentBanner
            listDescriptions={listDescriptions}
            handleBanner={handleBanner}
            bannerID={bannerID}
            bannerList={bannerList}
          />
        </div>
        <div className="comparison-container mb-16">
          <p className="mt-16 text-center text-pixel-32 font-bold leading-pixel-48 sm:mt-22 sm:mb-6">
            {t('listOfInsurance')}
          </p>
          <div className="mx-auto hidden flex-row gap-2 overflow-y-auto whitespace-nowrap sm:mb-10 sm:flex sm:flex-wrap sm:justify-center sm:gap-4 sm:gap-y-3 sm:whitespace-normal lg:w-[808px]">
            {categoryInsuranceList.map((item) => (
              <p
                key={item.id}
                className={`${
                  toggleId === item.id && 'border-b-2 border-azure font-bold'
                } box-content cursor-pointer p-2 text-base text-nickel`}
                onClick={() => handleToggle(item.id)}
              >
                {t(item.title)}
              </p>
            ))}
          </div>
          <div className="mt-6 mb-10 sm:hidden">
            <Swiper
              slidesPerView={'auto'}
              modules={[Pagination]}
              className="mx-auto flex flex-row gap-2"
            >
              {categoryInsuranceList.map((item) => (
                <SwiperSlide
                  key={item.id}
                  className={`${
                    toggleId === item.id && 'border-b-2 border-azure font-bold'
                  } box-content cursor-pointer p-2 text-base text-nickel`}
                  style={{ width: 'fit-content' }}
                  onClick={() => handleToggle(item?.id)}
                >
                  {t(item.title)}
                </SwiperSlide>
              ))}
            </Swiper>
          </div>

          <div className="grid grid-cols-1 gap-6 sm:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
            {categoryInsurance(t)?.map(
              (insurance) =>
                insurance?.id === toggleId &&
                insurance?.insurances.map((item) => (
                  <Link key={item?.id} href={item?.link}>
                    <div className="box-content flex h-14 cursor-pointer gap-3 rounded-xl border border-platinum p-6 hover:bg-blue-50">
                      <Image
                        src={item.logo}
                        width={40}
                        height={40}
                        alt="logo"
                      />
                      <div className="flex flex-col justify-center gap-1">
                        <p className="text-sm font-bold">{t(item.name)}</p>
                        {item.save}
                      </div>
                    </div>
                  </Link>
                ))
            )}
            {/* {CATEGORY_INSURANCES[toggleId - 1]?.insurances.map((item) => (
              <Link key={item?.id} href={item?.link}>
                <div className="box-content flex h-14 cursor-pointer gap-3 rounded-xl border border-platinum p-6 hover:bg-blue-50">
                  <Image src={item.logo} width={40} height={40} alt="logo" />
                  <div className="flex flex-col justify-center gap-1">
                    <p className="text-sm font-bold">{t(item.name)}</p>
                    <p className="text-xs font-normal">{item.save}</p>
                  </div>
                </div>
              </Link>
            ))} */}
          </div>
        </div>

        <OurCustomer />
      </div>
    </LayoutWithHeader>
  );
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'category',
      ])),
    },
  };
};
