import Pagination from 'components/Pagination';
import ButtonSort from 'components/common/ButtonSort';
import LayoutWithHeader from 'components/common/LayoutWithHeader';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import InformationInsuranceBot from 'components/common/listInsurance/InformationInsuranceBot';
import DetailHealthInsurance from 'components/listHealthInsurance/DetailHealthInsurance';
// import InformationInsuranceTop from 'components/common/listInsurance/InformationInsuranceTop';
import FilterHealthInsurance from 'components/listHealthInsurance/FilterHealthInsurance';
import ListDetailsLineHealthInsurance from 'components/listHealthInsurance/ListDetailsLineHealthInsurance';
import { useSession } from 'next-auth/react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { getListHealthInsurances } from 'pages/api/healthInsurance';
import React, {
  Fragment,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';
import { DataContext } from 'src/store/GlobalState';
import { LIST_BUTTON_FILTER } from 'src/utils/constants';
import { useTranslation } from 'next-i18next';
import AlertSuccessRating from 'components/common/listInsurance/AlertSuccessRating';
import { Dialog, Transition } from '@headlessui/react';

export default function ListHealthInsurance() {
  const { t } = useTranslation('common');
  const { dispatch, state } = useContext(DataContext);
  const { updateDataInsurance } = state;
  const dataInsuranceCheck = updateDataInsurance?.updateDataInsurance;

  const session = useSession();

  const router = useRouter();
  const [dataFilter, setDataFilter] = useState(router.query);

  const [dataInsurance, setDataInsurance] = useState([]);

  // const [dataInsuranceTopObjectives, setDataInsuranceTopObjectives] =
  //   useState();
  // const [dataInsuranceTopTotalSumInsured, setDataInsuranceTopTotalSumInsured] =
  //   useState();
  // const [isSuggestion, setIsSuggestion] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [dataInsuranceComparison, setDataInsuranceComparison] = useState([]);
  // const [hiddenDetailTop, setHiddenDetailTop] = useState();
  // const [hiddenRatingInsuranceTop, setHiddenRatingInsuranceTop] = useState();
  // const [selectChangeBg, setSelectChangeBg] = useState();
  const [hiddenFilter, setHiddenFilter] = useState(false);
  const [sort, setSort] = useState('');
  const pageSize = 5;
  const [pagination, setPagination] = useState();
  const [hiddenAlertSuccessRating, setHiddenAlertSuccessRating] =
    useState(false);
  function closeModal() {
    setHiddenAlertSuccessRating(false);
  }

  const handleHiddenFilter = () => {
    setHiddenFilter(true);
  };

  const handleSort = (sort) => {
    setSort(sort);
  };

  useEffect(() => {
    if (router.pathname === '/listHealthInsurance') {
      if (dataInsuranceCheck?.length === 3) {
        return dispatch({
          type: 'COMPARISON',
          comparison: { comparison: false },
          updateDataInsurance: { updateDataInsurance: [] },
        });
      }
    }
  }, [router.pathname]);

  useEffect(() => {
    let { room_type, insured_person } = router.query;

    if (!Array.isArray(room_type)) {
      room_type = room_type.split(', ');
    }

    if (!Array.isArray(insured_person)) {
      insured_person = insured_person.split(', ');
    }

    setDataFilter({ ...router.query, room_type, insured_person });
  }, [router.query]);

  const getListInsurance = useCallback(
    async (query, page, sort) => {
      dispatch({ type: 'LOADING', payload: { loading: true } });

      const params = {
        ...query,
        limit: pageSize,
        page: page,
        order_by: sort,
      };

      if (!sort) delete params.order_by;

      try {
        const response = await getListHealthInsurances(
          {
            params: params,
          },
          session?.data?.accessToken
        );
        // setHiddenDetailTop();
        // setHiddenRatingInsuranceTop();
        // setSelectChangeBg();
        setPagination(response?.data?.meta);
        setDataInsurance(response?.data?.data);
      } catch (error) {
        console.log(
          '🚀 ~ file: index.js:40 ~ getListInsurance ~ error:',
          error
        );
      }
    },
    [session?.data?.accessToken]
  );

  useEffect(() => {
    getListInsurance(dataFilter, currentPage, sort).finally(() => {
      dispatch({ type: 'LOADING', payload: { loading: false } });
    });
  }, [sort]);

  const handleClickPage = (value) => {
    setCurrentPage(value + 1);
    getListInsurance(dataFilter, value + 1, sort).finally(() =>
      dispatch({ type: 'LOADING', payload: { loading: false } })
    );

    WindowScrollTop(200);
  };

  // const isSuggestionTop =
  //   dataInsuranceTopObjectives &&
  //   dataInsuranceTopTotalSumInsured &&
  //   currentPage === 1;

  return (
    <LayoutWithHeader>
      {hiddenAlertSuccessRating && (
        <div className="fixed inset-0 z-50 flex items-center justify-center bg-rich-black">
          <AlertSuccessRating
            setHiddenAlertSuccessRating={setHiddenAlertSuccessRating}
          />
        </div>
      )}
      <Transition appear show={hiddenAlertSuccessRating} as={Fragment}>
        <Dialog
          as="div"
          className="relative z-10"
          onClose={closeModal}
        ></Dialog>
      </Transition>

      <div className="bg-cultured">
        <div className="comparison-container pb-16 pt-10">
          <div className="flex flex-col gap-6 lg:flex-row">
            <div className="hidden w-1/3 lg:block lg:w-1/2">
              <FilterHealthInsurance
                dispatch={dispatch}
                sort={sort}
                currentPage={currentPage}
                getListInsurance={getListInsurance}
                // setIsSuggestion={setIsSuggestion}
                data={dataFilter}
                pageSize={pageSize}
                setDataInsurance={setDataInsurance}
                setCurrentPage={setCurrentPage}
              />
            </div>
            <div className="lg:hidden">
              <button
                onClick={handleHiddenFilter}
                className="flex items-center gap-2 rounded-lg border border-nickel px-4 py-2"
              >
                <Image src="/svg/sort.svg" height={20} width={20} alt="sort" />
                <p className="text-base font-bold text-nickel">
                  {t('information_search')}
                </p>
              </button>
            </div>
            {hiddenFilter && (
              <div className="absolute inset-0 z-50 bg-rich-black lg:hidden">
                <FilterHealthInsurance
                  dispatch={dispatch}
                  sort={sort}
                  currentPage={currentPage}
                  getListInsurance={getListInsurance}
                  // setIsSuggestion={setIsSuggestion}
                  data={dataFilter}
                  pageSize={pageSize}
                  setDataInsurance={setDataInsurance}
                  setHiddenFilter={setHiddenFilter}
                  setCurrentPage={setCurrentPage}
                />
              </div>
            )}

            <div className="w-full">
              <div className="mb-5 flex gap-2">
                <Image
                  width={20}
                  height={20}
                  src="/images/listInsurance/document-filter.png"
                  alt="document_filter"
                />
                <p className="text-base font-normal text-nickel">
                  {pagination?.totalItems} {t('results_for')}
                </p>
                <p className="text-base font-medium text-maastricht-blue">
                  {t('health_insurance')}
                </p>
              </div>
              {/* {isSuggestionTop && (
                <div className="mb-5 w-full rounded-2xl bg-white p-3 shadow-listInsurance">
                  <InformationInsuranceTop
                    DetailInsurance
                    ListDetailsLine
                    dataInsuranceTopObjectives={dataInsuranceTopObjectives}
                    dataInsuranceTopTotalSumInsured={
                      dataInsuranceTopTotalSumInsured
                    }
                    dataInsuranceComparison={dataInsuranceComparison}
                    setDataInsuranceComparison={setDataInsuranceComparison}
                    currentPage={currentPage}
                    hiddenDetailTop={hiddenDetailTop}
                    setHiddenDetailTop={setHiddenDetailTop}
                    hiddenRatingInsuranceTop={hiddenRatingInsuranceTop}
                    setHiddenRatingInsuranceTop={setHiddenRatingInsuranceTop}
                    selectChangeBg={selectChangeBg}
                    setSelectChangeBg={setSelectChangeBg}
                  />
                </div>
              )} */}
              <div className="mb-6 grid grid-cols-2 gap-2 sm:flex sm:gap-4">
                {LIST_BUTTON_FILTER(t).map((list) => (
                  <ButtonSort
                    key={list?.id}
                    list={list}
                    sort={sort}
                    title={list?.label}
                    onClick={() => handleSort(list?.filterSort)}
                  />
                ))}
              </div>
              {dataInsurance?.length !== 0 ? (
                <div className="w-full">
                  <InformationInsuranceBot
                    setHiddenAlertSuccessRating={setHiddenAlertSuccessRating}
                    DetailInsurance={DetailHealthInsurance}
                    ListDetailsLine={ListDetailsLineHealthInsurance}
                    dataInsurance={dataInsurance}
                    setDataInsuranceComparison={setDataInsuranceComparison}
                    dataInsuranceComparison={dataInsuranceComparison}
                  />
                </div>
              ) : (
                <div>{t('no_data')}</div>
              )}
              {pagination?.totalPages > 0 && (
                <div className="mt-5 flex justify-end">
                  <Pagination
                    page={currentPage}
                    pages={pagination?.totalPages}
                    onClick={handleClickPage}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </LayoutWithHeader>
  );
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'insuranceLocale',
        'insuranceDetailsComparison',
      ])),
    },
  };
};
