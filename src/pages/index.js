import AboutUs from 'components/Home/about-us';
import BannerHome from 'components/Home/bannerHome';
import CaseStudy from 'components/Home/caseStudy';
import InsuranceMarquee from 'components/Home/insuranceMarquee';
import OtherInsurance from 'components/Home/otherInsurance';
import SelectInsurance from 'components/Home/selectInsurance';
import SelectNeed from 'components/Home/selectNeed';
import SelectRate from 'components/Home/selectRate';
import Subscribe from 'components/Home/subscribe';
import LayoutWithHeader from 'components/common/LayoutWithHeader';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import React from 'react';

export default function Home() {
  return (
    <LayoutWithHeader>
      <BannerHome />
      <OtherInsurance />
      <InsuranceMarquee />
      <SelectInsurance />
      <SelectNeed />
      <SelectRate />
      <CaseStudy />
      <AboutUs />
      <Subscribe />
    </LayoutWithHeader>
  );
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['auth', 'home', 'common'])),
    },
  };
};
