import ButtonBgWhite from 'components/common/ButtonBgWhite';
import LayoutWithHeader from 'components/common/LayoutWithHeader';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import React, { useContext, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'next-i18next';
import DetailsStepThere from 'components/insuranceCarMaterial/DetailsStepThere';
import DetailsStepOne from 'components/insuranceCarMaterial/DetailsStepOne';
import DetailsStepTwo from 'components/insuranceCarMaterial/DetailsStepTwo';
import PayInsurance from 'components/insuranceCarMaterial/PayInsurance';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import { yupResolver } from '@hookform/resolvers/yup';
import { schemaCarMaterial } from 'src/utils/error';
import {
  patchInsuranceCarMaterial,
  postInsuranceCarMaterial,
} from 'pages/api/insuranceCarMaterial';
import { useSession } from 'next-auth/react';
import { toast } from 'react-toastify';
import AlertSuccess from 'components/common/AlertSuccess';
import { DataContext } from 'src/store/GlobalState';
import { stepFillInformationMaterial } from 'src/utils/stepHooks';
import { useRouter } from 'next/router';
import checkDefaultValues from 'components/insuranceCarMaterial/checkDefaultValues';
import {
  lockScroll,
  regexCarLicense,
  regexEmail,
  regexPhoneNumber,
} from 'src/utils/helper';
import dayjs from 'dayjs';

export default function InsuranceCardMaterial() {
  const { t } = useTranslation(['common', 'auth']);
  const { dispatch } = useContext(DataContext);
  const session = useSession();
  const router = useRouter();

  const idInsurance = router.query?.id;
  const status = router.query?.status;
  const defaultImage = router.query?.certificate_image_url;
  const defaultImageMultiple = router.query?.vehicle_condition_image_urls;
  const defaultInvoiceType = router.query?.invoice_type;
  const defaultBankName = router.query?.bank_name;
  const defaultFormType = router.query?.form_type;

  const {
    register,
    handleSubmit,
    control,
    setValue,
    setError,
    getValues,
    resetField,
    formState: { errors },
  } = useForm({
    mode: 'onChange',
    defaultValues: status ? checkDefaultValues({ values: router.query }) : '',
    resolver: yupResolver(schemaCarMaterial()),
  });

  const acceptStep = () => {
    if (idInsurance && status) {
      return 1;
    }
    if (idInsurance) {
      return 3;
    }
    return 1;
  };

  const [stepCurrent, setStepCurrent] = useState(acceptStep());
  const [informationInsurance, setInformationInsurance] = useState();
  const [hiddenAlertSuccess, setHiddenAlertSuccess] = useState(false);
  const [fileImage, setFileImage] = useState(null);
  const [updateImageUrl, setUpdateImageUrl] = useState([]);
  const [valueLabelInvoice, setValueLabelInvoice] = useState();
  const [valueSwitchInvoice, setValueSwitchInvoice] = useState(true);
  const [valueSwitchBankBenefit, setValueSwitchBankBenefit] = useState(true);
  const [acceptRule, setAcceptRule] = useState(false);
  const [timeDeadlineForDeal, setTimeDeadlineForDeal] = useState();
  const [timeCertificateExpirationDate, setTimeCertificateExpirationDate] =
    useState();

  const newDateCurrentCertificateExpiration = dayjs(
    timeCertificateExpirationDate
  )
    .add(1, 'day')
    .toISOString();

  const newYearCurrentCertificateExpiration = dayjs(
    newDateCurrentCertificateExpiration
  )
    .add(1, 'year')
    .toISOString();

  const timeNew = dayjs(timeDeadlineForDeal).add(1, 'year').toISOString();

  useEffect(() => {
    if (stepCurrent === 3) {
      setAcceptRule(true);
    }
  }, [stepCurrent]);

  const detailsSteps = () => {
    switch (stepCurrent) {
      case 1:
        return (
          <DetailsStepOne
            defaultValueTargetUseBusiness={router.query.purpose}
            errors={errors}
            register={register}
            setValue={setValue}
            setError={setError}
          />
        );
      case 2:
        return (
          <DetailsStepTwo
            newYearCurrentCertificateExpiration={
              newYearCurrentCertificateExpiration
            }
            newDateCurrentCertificateExpiration={
              newDateCurrentCertificateExpiration
            }
            setTimeCertificateExpirationDate={setTimeCertificateExpirationDate}
            timeNew={timeNew}
            setTimeDeadlineForDeal={setTimeDeadlineForDeal}
            resetField={resetField}
            valueSwitchBankBenefit={valueSwitchBankBenefit}
            setValueSwitchBankBenefit={setValueSwitchBankBenefit}
            valueSwitchInvoice={valueSwitchInvoice}
            setValueSwitchInvoice={setValueSwitchInvoice}
            valueLabelInvoice={valueLabelInvoice}
            setValueLabelInvoice={setValueLabelInvoice}
            defaultInvoiceType={defaultInvoiceType}
            updateImageUrl={updateImageUrl}
            setUpdateImageUrl={setUpdateImageUrl}
            defaultImageMultiple={defaultImageMultiple}
            defaultImage={defaultImage}
            fileImage={fileImage}
            setFileImage={setFileImage}
            getValues={getValues}
            errors={errors}
            setError={setError}
            information={informationInsurance}
            control={control}
            register={register}
            setValue={setValue}
          />
        );
      case 3:
        return (
          <DetailsStepThere
            idInsurance={idInsurance}
            acceptRule={acceptRule}
            setAcceptRule={setAcceptRule}
          />
        );
      case 4:
        return <PayInsurance />;
    }
  };

  const backSteps = () => {
    if (stepCurrent === 1) {
      if (status) {
        router.push('/listUserInsurance');
      }
      return;
    }
    if (stepCurrent === 3) {
      router.push('/listUserInsurance');
    }
    setStepCurrent(stepCurrent - 1);
  };

  const nextSteps = async (values) => {
    const {
      invoice_type,
      invoice_address,
      invoice_email,
      invoice_name,
      invoice_tax_id,
      bank_name,
      bank_branch,
      branch_address,
      insurance_period_from,
      insurance_period_to,
      staff_full_name,
      staff_phone_number,
      ...dataForm
    } = values;
    if (stepCurrent === 1) {
      if (!status) {
        setValueLabelInvoice('individual');
      }

      const isPassCheckOne = onCheckValueFormOne(values);
      if (!isPassCheckOne) return;
      setInformationInsurance(values);
      WindowScrollTop(300);
      if (status) {
        defaultInvoiceType
          ? setValueSwitchInvoice(false)
          : setValueLabelInvoice('individual');
        defaultBankName && setValueSwitchBankBenefit(false);
      }
    }
    if (stepCurrent === 2) {
      WindowScrollTop(0);
      if (!session?.data) {
        dispatch({
          type: 'LOGIN',
          loginNoAccount: { login: true },
        });
        toast.error(t('please_login_buy_insurance'));
        lockScroll();
        return;
      }

      try {
        let update_image_urls = null;
        if (updateImageUrl.length > 0) {
          update_image_urls = updateImageUrl;
        }
        if (updateImageUrl.length === 0) {
          if (dataForm?.files) {
            update_image_urls = defaultImage
              ? [defaultImage]
              : defaultImageMultiple;
          }

          if (dataForm?.files?.length === 0) {
            update_image_urls = '';
          }
        }

        let insurance_period_to_type = '';
        if (dataForm?.form_type === 'unexpired') {
          insurance_period_to_type = newYearCurrentCertificateExpiration;
        }
        if (dataForm?.form_type !== 'unexpired') {
          insurance_period_to_type = timeNew;
        }

        const handleParams = {
          free_roadside_assistance: true,
          is_complete_vehicle_damage: true,
          is_component_vehicle_damage: true,
          is_deduct_one_million_vnd: true,
          is_total_vehicle_theft: true,
          update_image_urls: update_image_urls,
          invoice_type: invoice_type ? invoice_type : '',
          invoice_address: invoice_address ? invoice_address : '',
          invoice_email: invoice_email ? invoice_email : '',
          invoice_name: invoice_name ? invoice_name : '',
          invoice_tax_id: invoice_tax_id ? invoice_tax_id : '',
          bank_name: bank_name ? bank_name : '',
          bank_branch: bank_branch ? bank_branch : '',
          branch_address: branch_address ? branch_address : '',
          staff_full_name: staff_full_name ? staff_full_name : '',
          staff_phone_number: staff_phone_number ? staff_phone_number : '',
          insurance_period_from:
            dataForm?.form_type === 'unexpired'
              ? newDateCurrentCertificateExpiration
              : insurance_period_from,
          insurance_period_to: insurance_period_to
            ? ''
            : insurance_period_to_type,
          ...dataForm,
        };

        const body = new FormData();
        Object.keys(handleParams).forEach((key) => {
          if (key === 'files') {
            if (handleParams.form_type !== 'unexpired') {
              Array.from(handleParams[key]).forEach((file) => {
                body.append('files', file);
              });
            } else {
              body.append('files', handleParams[key]);
            }
            return;
          }
          if (key === 'update_image_urls') {
            body.append('update_image_urls', JSON.stringify(handleParams[key]));
            return;
          }
          if (key === 'is_transportation_business') {
            body.append(
              'is_transportation_business',
              Boolean(handleParams[key])
            );
            return;
          }
          if (key === 'insurance_period_to') {
            body.append('insurance_period_to', timeNew);
            return;
          }
          body.append(key, handleParams[key]);
        });

        const isPassCheckTwo = onCheckValueFormTwo(values);
        if (!isPassCheckTwo) return;
        if (status) {
          dispatch({ type: 'LOADING', payload: { loading: true } });

          await patchInsuranceCarMaterial(
            session?.data?.accessToken,
            idInsurance,
            {
              params: body,
            }
          );
        } else {
          dispatch({ type: 'LOADING', payload: { loading: true } });

          await postInsuranceCarMaterial(session?.data?.accessToken, {
            params: handleParams.files.length === 5 ? body : handleParams,
          });
        }

        setHiddenAlertSuccess(true);
      } catch (error) {
        toast.error(t(`common:error.${error?.response?.data?.error_code}`));
        console.log('🚀 ~ file: index.js:154 ~ nextSteps ~ error:', error);
      } finally {
        dispatch({ type: 'LOADING', payload: { loading: false } });
      }
      return;
    }
    if (values.form_type !== 'unexpired') {
      setValue('files', []);
    }

    if (stepCurrent > 3) return;
    setStepCurrent(stepCurrent + 1);
  };

  const onCheckValueFormOne = ({
    form_type,
    purpose,
    car_license_plate,
    seating_capacity,
    car_brand,
    car_model,
    car_version,
    year_of_production,
    is_transportation_business,
  }) => {
    const checkCarLicense = regexCarLicense.test(car_license_plate);

    if (!form_type) {
      setError('form_type', { message: t('please_choice') });
    }
    if (is_transportation_business === 'true') {
      if (!purpose) {
        setError('purpose', { message: t('please_choice') });
      }
    }
    if (!car_license_plate) {
      setError('car_license_plate', { message: t('please_choice') });
    }
    if (!checkCarLicense) {
      setError('car_license_plate', {
        message: t('please_registration_car'),
      });
    }
    if (!seating_capacity) {
      setError('seating_capacity', { message: t('please_choice') });
    }
    if (seating_capacity < 2 || seating_capacity > 11) {
      setError('seating_capacity', {
        message: t('please_seating_capacity'),
      });
    }
    if (!car_brand) {
      setError('car_brand', { message: t('please_choice') });
    }
    if (!car_model) {
      setError('car_model', { message: t('please_choice') });
    }
    if (!car_version) {
      setError('car_version', { message: t('please_choice') });
    }
    if (!year_of_production) {
      setError('year_of_production', { message: t('please_choice') });
    }
    WindowScrollTop(300);

    if (
      form_type &&
      ((is_transportation_business === 'true' && purpose) ||
        (is_transportation_business === 'false' && !purpose)) &&
      checkCarLicense &&
      seating_capacity > 1 &&
      seating_capacity < 12 &&
      car_brand &&
      car_model &&
      car_version &&
      year_of_production
    ) {
      return true;
    }
    return false;
  };

  const onCheckValueFormTwo = ({
    files,
    certificate_expiration_date,
    insurance_period_from,
    policyholder_full_name,
    policyholder_address,
    policyholder_phone_number,
    policyholder_email,
    form_type,
    invoice_type,
    invoice_address,
    invoice_email,
    invoice_name,
    invoice_tax_id,
    bank_name,
    bank_branch,
    branch_address,
    // staff_phone_number,
    // staff_full_name,
  }) => {
    const regexTax = /^\d{10}$|^\d{10}-\d{3}$/;
    const checkTax = regexTax.test(invoice_tax_id);
    const checkPhoneNumber = regexPhoneNumber.test(policyholder_phone_number);
    const checkEmailPolicyholder = regexEmail.test(policyholder_email);
    const checkEmailInvoice = regexEmail.test(invoice_email);
    // const checkPhoneNumberStaff = regexPhoneNumber.test(staff_phone_number);

    if (
      !status ||
      (defaultFormType === 'unexpired' && form_type !== defaultFormType) ||
      (defaultFormType !== 'unexpired' && form_type === 'unexpired')
    ) {
      if (files?.length < 5) {
        setError('files', { message: t('please_full_image') });
      }
      if (!files) {
        setError('files', { message: t('please_choice') });
      }
    }
    if (!certificate_expiration_date) {
      setError('certificate_expiration_date', {
        message: t('please_choice'),
      });
    }
    if (form_type !== 'unexpired') {
      if (!insurance_period_from) {
        setError('insurance_period_from', {
          message: t('please_choice'),
        });
      }
    }
    if (!policyholder_full_name) {
      setError('policyholder_full_name', {
        message: t('please_choice'),
      });
    }
    if (!policyholder_address) {
      setError('policyholder_address', {
        message: t('please_choice'),
      });
    }
    if (!policyholder_phone_number) {
      setError('policyholder_phone_number', {
        message: t('please_choice'),
      });
    }
    if (!checkPhoneNumber) {
      setError('policyholder_phone_number', {
        message: t('auth:schema.phone_matches'),
      });
    }
    if (!policyholder_email) {
      setError('policyholder_email', {
        message: t('please_choice'),
      });
    }
    if (!checkEmailPolicyholder) {
      setError('policyholder_email', {
        message: t('auth:schema.email'),
      });
    }
    if (!valueSwitchInvoice) {
      if (!invoice_type) {
        setError('invoice_type', {
          message: t('please_choice'),
        });
      }
      if (!invoice_address) {
        setError('invoice_address', {
          message: t('please_choice'),
        });
      }
      if (!invoice_email) {
        setError('invoice_email', {
          message: t('please_choice'),
        });
      }
      if (!checkEmailInvoice) {
        setError('invoice_email', {
          message: t('auth:schema.email'),
        });
      }
      if (!invoice_name) {
        setError('invoice_name', {
          message: t('please_choice'),
        });
      }
      if (!invoice_tax_id) {
        setError('invoice_tax_id', {
          message: t('please_choice'),
        });
      }
      if (!checkTax) {
        setError('invoice_tax_id', {
          message: 'Sai định dạng mã số thuế',
        });
      }
    }
    if (!valueSwitchBankBenefit) {
      if (!bank_name) {
        setError('bank_name', {
          message: t('please_choice'),
        });
      }
      if (!bank_branch) {
        setError('bank_branch', {
          message: t('please_choice'),
        });
      }
      if (!branch_address) {
        setError('branch_address', {
          message: t('please_choice'),
        });
      }
      // if (!staff_phone_number) {
      //   setError('staff_phone_number', {
      //     message: t('please_choice'),
      //   });
      // }
      // if (!checkPhoneNumberStaff) {
      //   setError('staff_phone_number', {
      //     message: t('auth:schema.phone_matches'),
      //   });
      // }
      // if (!staff_full_name) {
      //   setError('staff_full_name', {
      //     message: t('please_choice'),
      //   });
      // }
    }

    WindowScrollTop(200);
    const validateWithStatus =
      (defaultFormType === 'unexpired' && form_type !== defaultFormType) ||
      (defaultFormType !== 'unexpired' && form_type === 'unexpired')
        ? form_type === 'unexpired'
          ? certificate_expiration_date && files
          : files?.length === 5
        : status;
    if (
      policyholder_email &&
      checkEmailPolicyholder &&
      form_type &&
      policyholder_phone_number &&
      checkPhoneNumber &&
      policyholder_address &&
      policyholder_full_name &&
      (form_type !== 'unexpired'
        ? insurance_period_from
        : !insurance_period_from) &&
      (!status
        ? form_type === 'unexpired'
          ? certificate_expiration_date && files
          : !certificate_expiration_date && files?.length === 5
        : validateWithStatus) &&
      (!valueSwitchInvoice
        ? invoice_type &&
          invoice_address &&
          invoice_email &&
          invoice_name &&
          invoice_tax_id &&
          checkTax &&
          checkEmailInvoice
        : !invoice_type &&
          !invoice_address &&
          !invoice_email &&
          !invoice_name &&
          !invoice_tax_id) &&
      (!valueSwitchBankBenefit
        ? bank_name && bank_branch && branch_address
        : !bank_name && !bank_branch && !branch_address)
    ) {
      return true;
    }
    return false;
  };

  return (
    <LayoutWithHeader backgroundFooter="lg:bg-lotion">
      {hiddenAlertSuccess && (
        <div className="absolute inset-0 z-50 flex h-screen items-center justify-center bg-rich-black">
          <AlertSuccess />
        </div>
      )}
      {!hiddenAlertSuccess && (
        <>
          <form onSubmit={handleSubmit(nextSteps)}>
            <div className="comparison-container relative pt-3 sm:pt-5 md:pt-8 lg:pt-14">
              <div className="mb-12 justify-between text-center md:flex lg:text-start">
                <p className="text-2xl font-bold text-arsenic lg:text-pixel-32 lg:leading-pixel-48">
                  {t('insurance_car_material')}
                </p>
                <div className="flex items-center justify-center md:items-start lg:items-center">
                  {stepFillInformationMaterial(t, stepCurrent).map((steps) => (
                    <div key={steps.id} className="flex">
                      {steps?.nameStep && <>{steps?.nameStep}</>}
                      {steps?.space && (
                        <div className="flex items-center md:hidden">
                          {steps?.space}
                        </div>
                      )}
                    </div>
                  ))}
                </div>
              </div>
              {detailsSteps()}
            </div>
            <div className="sticky bottom-0 left-27 z-40 mb-28 w-full bg-white">
              <div className="comparison-container flex w-full justify-end py-4">
                <div className="w-full gap-4 sm:flex sm:w-2/3 lg:w-2/5">
                  <div className="mb-3 w-full md:mb-0">
                    <ButtonBgWhite
                      cases="large"
                      widthBtn="text-ultramarine-blue border-ultramarine-blue w-full"
                      title={t('Back')}
                      onClick={backSteps}
                    />
                  </div>
                  <div className="w-full">
                    <button
                      type="submit"
                      disabled={acceptRule}
                      className={`${
                        acceptRule
                          ? 'bg-zinc-400 text-white'
                          : 'bg-ultramarine-blue from-egyptian-blue to-ultramarine-blue text-cultured'
                      } w-full rounded-lg py-3 text-base font-bold hover:bg-gradient-to-r`}
                    >
                      {t('next')}
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </>
      )}
    </LayoutWithHeader>
  );
}
export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'listUserInsurance',
      ])),
    },
  };
};
