import { yupResolver } from '@hookform/resolvers/yup';
import ButtonPrimary from 'components/common/ButtonPrimary';
import FormFilter from 'components/common/FormFilter';
import InputChoiceYearDate from 'components/common/InputChoiceYearDate';
import SelectFilter from 'components/common/SelectFilter';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { useTranslation } from 'next-i18next';
import {
  LIST_INSURED_PERSON,
  LIST_ROOM_BED,
  LIST_WORKS,
  STEP,
} from 'src/utils/constants';
import { schemaFilterHealthInsurance } from 'src/utils/error';

const currentYear = new Date().getFullYear();
export default function FormFilterHealthInsurance() {
  const router = useRouter();

  const { t } = useTranslation(['common']);

  const {
    register,
    handleSubmit,
    setError,
    setValue,
    formState: { errors },
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schemaFilterHealthInsurance(t)),
  });

  const [errorValueNoNumber, setErrorValueNoNumber] = useState([]);
  const [yearValue, setYearValue] = useState();
  const [valueProfession, setValueProfession] = useState();
  const [numberYear, setNumberYear] = useState(48);
  const listYearOfBirth = [
    ...Array.from({ length: 106 }, (_, i) => currentYear - i).reverse(),
  ];
  const listYearShow = listYearOfBirth?.slice(numberYear, numberYear + STEP);

  const handleChoseProfession = (profession) => {
    setValueProfession(profession);
    setValue('profession', profession.value);
    setError('profession', { message: '' });
  };

  const onChangeYearOfBirth = (event) => {
    const {
      target: { value },
    } = event;

    if (value?.length === 0) return setYearValue(0);

    setYearValue(parseInt(value));
    setValue('year_of_birth', parseInt(value));

    if (value > currentYear || value < currentYear - 100) {
      setError('year_of_birth', { message: '' });
      return setErrorValueNoNumber({
        errorValueYearOfBirth: t('error_year'),
      });
    }
    setErrorValueNoNumber({ errorValueYearOfBirth: '' });
    setError('year_of_birth', { message: '' });
  };

  const handleFilterInsurance = (values) => {
    const isPassCheck = onCheckValueForm(values);
    if (!isPassCheck) return;

    router.push({
      pathname: '/listHealthInsurance',
      query: values,
    });
  };

  const onCheckValueForm = ({
    room_type,
    insured_person,
    year_of_birth,
    profession,
  }) => {
    if (!room_type || room_type.length === 0) {
      setError('room_type', { message: t('error_room_type') });
    }

    if (!year_of_birth) {
      setError('year_of_birth', { message: t('error_year_of_birth') });
    }
    if (!profession) {
      setError('profession', { message: t('error_profession') });
    }
    if (!insured_person || insured_person.length === 0) {
      setError('insured_person', {
        message: t('error_insured_person'),
      });
    }
    WindowScrollTop(200);
    if (
      room_type.length > 0 &&
      insured_person.length > 0 &&
      currentYear - 100 < parseInt(year_of_birth) &&
      parseInt(year_of_birth) < currentYear + 1 &&
      profession
    ) {
      return true;
    }
  };

  return (
    <FormFilter>
      <div className="xl:px-52">
        <div className="mb-10 flex justify-center p-4 text-2xl font-bold text-cultured xl:text-pixel-40 xl:leading-pixel-56">
          {t('health_insurance')}
        </div>
        <div className="rounded-2xl bg-white p-6 shadow-formFilter xl:p-8">
          <form onSubmit={handleSubmit(handleFilterInsurance)}>
            <div className="mb-8">
              <h2 className="mb-4 text-base font-bold text-arsenic">
                {t('insured_person')}
              </h2>

              {LIST_INSURED_PERSON(t).map((values) => (
                <div
                  key={values.value}
                  className="mb-4 flex items-center gap-4"
                >
                  <input
                    type="checkbox"
                    id="insured_person"
                    {...register('insured_person', { required: true })}
                    className="h-5 w-5"
                    value={values?.value}
                  />
                  <p className=" text-base font-normal text-arsenic">
                    {values?.label}
                  </p>
                </div>
              ))}
              {errors?.insured_person && (
                <p className=" text-red-500 ">
                  {errors?.insured_person?.message}
                </p>
              )}
            </div>

            <InputChoiceYearDate
              yearValue={yearValue}
              register={register}
              errorValueNoNumber={errorValueNoNumber}
              errors={errors}
              onChangeYearOfBirth={onChangeYearOfBirth}
              setValue={setValue}
              setYearValue={setYearValue}
              setError={setError}
              setErrorValueNoNumber={setErrorValueNoNumber}
              title={t('year')}
              placeholder={t('choice_year')}
              id="year_of_birth"
              listYearShow={listYearShow}
              setNumberYear={setNumberYear}
              numberYear={numberYear}
              numberYearMax={96}
            />

            <div className="mb-8">
              <p className="mb-4 text-base font-bold text-arsenic">
                {t('profession')}
              </p>
              <SelectFilter
                onChange={handleChoseProfession}
                selected={valueProfession}
                options={LIST_WORKS(t)}
                errors={errors?.profession?.message}
                placeholder={t('common:error_profession')}
              />
              {errors?.profession && (
                <p className=" text-red-500 ">{errors?.profession?.message}</p>
              )}
            </div>

            <div className="mb-8">
              <h2 className="mb-4 text-base font-bold text-arsenic">
                {t('room_type')}
              </h2>

              {LIST_ROOM_BED(t).map((values) => (
                <div
                  key={values.value}
                  className="mb-4 flex items-center gap-4"
                >
                  <input
                    type="checkbox"
                    id="room_type"
                    className="h-5 w-5"
                    value={values?.value}
                    {...register('room_type', { required: true })}
                  />
                  <p className="text-base font-normal text-arsenic">
                    {values?.label}
                  </p>
                </div>
              ))}
              {errors?.room_type && (
                <p className="text-red-500">{errors?.room_type?.message}</p>
              )}
            </div>

            <div className="mb-8">
              <h2 className="mb-4 text-base font-bold text-arsenic">
                {t('benefit_add')}
              </h2>

              <div className="mb-4 flex items-center gap-4">
                <input
                  type="checkbox"
                  id="is_dental"
                  {...register('is_dental', { required: true })}
                  className="h-5 w-5"
                />
                <p className="text-base font-normal text-arsenic">
                  {t('is_dental')}
                </p>
              </div>
              <div className="mb-4 flex items-center gap-4">
                <input
                  type="checkbox"
                  id="is_obstetric"
                  {...register('is_obstetric', { required: true })}
                  className="h-5 w-5"
                />
                <p className=" text-base font-normal text-arsenic">
                  {t('is_obstetric')}
                </p>
              </div>
              {errors?.is_dental && (
                <p className=" text-red-500 ">{errors?.is_dental?.message}</p>
              )}
            </div>

            <div className="flex justify-center">
              <ButtonPrimary
                type={'submit'}
                cases={'large'}
                title={t('search_comparison')}
                widthBtn={'w-2/3 sm:w-1/3'}
              />
            </div>
          </form>
        </div>
      </div>
    </FormFilter>
  );
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'insuranceLocale',
      ])),
    },
  };
};
