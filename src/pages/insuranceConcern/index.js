/* eslint-disable indent */
import LayoutWithHeader from 'components/common/LayoutWithHeader';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import Image from 'next/image';
import { useRouter } from 'next/router';
import {
  getInsuranceConcernUserAPI,
  getInsuranceObjectives,
} from 'pages/api/insurance';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useTranslation } from 'next-i18next';
import { DataContext } from 'src/store/GlobalState';
import { useSession } from 'next-auth/react';
import Pagination from 'components/Pagination';
import { WindowScrollTop } from 'components/common/WindowScrollTop';
import InformationInsuranceBot from 'components/common/listInsurance/InformationInsuranceBot';
import Select from 'components/common/Select';
import { CATEGORY } from 'src/utils/constants';

export default function InsuranceConcern() {
  const router = useRouter();
  const { t } = useTranslation(['common', 'auth']);
  const { dispatch } = useContext(DataContext);
  const session = useSession();

  const [dataInsuranceComparison, setDataInsuranceComparison] = useState([]);
  const [filterName, setFilterName] = useState();
  const [dataInsuranceConcern, setDataInsuranceConcern] = useState();
  const [pagination, setPagination] = useState();
  const pageSize = 5;
  const [currentPage, setCurrentPage] = useState(1);
  const [filterCategoryType, setFilterCategoryType] = useState(CATEGORY(t)[0]);

  const getListInsurance = useCallback(
    async (page, name, categoryType) => {
      dispatch({ type: 'LOADING', payload: { loading: true } });

      try {
        const response = await getInsuranceConcernUserAPI(
          session?.data?.accessToken,
          {
            params: {
              limit: pageSize,
              page: page,
              name: name,
              category_type: categoryType,
            },
          }
        );
        setDataInsuranceConcern(response?.data?.data);
        setPagination(response?.data?.meta);
      } catch (error) {
        console.log(
          '🚀 ~ file: index.js:40 ~ getListInsurance ~ error:',
          error
        );
      }
      dispatch({ type: 'LOADING', payload: { loading: false } });
    },
    [session?.data?.accessToken]
  );

  useEffect(() => {
    if (session.status === 'authenticated') {
      getListInsurance(1, filterName, filterCategoryType.value);
    }
  }, [session?.data?.accessToken, filterCategoryType.value]);

  const handleClickPage = (value) => {
    setCurrentPage(value + 1);
    getListInsurance(value + 1, filterName, filterCategoryType.value);
    WindowScrollTop(200);
  };

  const handleFilterInsurance = () => {
    setCurrentPage(1);
    getListInsurance(1, filterName, filterCategoryType.value);
  };

  const handleFilterInput = (e) => {
    setFilterName(e.target.value);
  };

  const handleSelectValueCategory = (e) => {
    setCurrentPage(1);
    setFilterCategoryType(e);
  };

  const handleOnKeyDown = (e) => {
    if (e.key === 'Enter') {
      handleFilterInsurance();
    }
  };

  return (
    <LayoutWithHeader>
      <div className="bg-cultured">
        <div className="comparison-container pt-4 pb-32">
          <button
            type="button"
            onClick={router.back}
            className="mb-4 flex gap-1"
          >
            <Image
              width={24}
              height={24}
              src="/svg/back-concern.svg"
              alt="back-concern"
            />
            <p className="text-sm font-normal text-azure">{t('auth:back')}</p>
          </button>
          <div className="lg:flex lg:justify-between">
            <div className="mb-4 flex items-center gap-2 ">
              <h1 className="text-2xl font-bold text-raisin-black lg:text-pixel-32 lg:leading-pixel-48">
                {t('auth:interestList')}
              </h1>
              <p className="pt-3 text-xs text-spanish-gray lg:text-sm">
                ({pagination?.totalItems} {t('auth:insurance_package')})
              </p>
            </div>
            <div className="flex w-full flex-col gap-5 sm:flex-row lg:w-1/2">
              <div className="flex h-10 w-full items-center gap-2 rounded-md border border-chinese-silver bg-white p-2 sm:w-3/5">
                <button type="button" onClick={handleFilterInsurance}>
                  <Image
                    src="/images/navbar/search.png"
                    alt="search"
                    width={24}
                    height={24}
                    priority="true"
                  />
                </button>
                <input
                  onKeyDown={handleOnKeyDown}
                  type="text"
                  placeholder={t('auth:Enter_insurance_name')}
                  className="w-full bg-white outline-none"
                  onChange={handleFilterInput}
                />
              </div>
              <div className="w-full sm:w-2/5">
                <Select
                  options={CATEGORY(t)}
                  onChange={handleSelectValueCategory}
                  selected={filterCategoryType}
                />
              </div>
            </div>
          </div>
          {(dataInsuranceConcern && (
            <div className="w-full">
              <InformationInsuranceBot
                dataInsurance={dataInsuranceConcern}
                setDataInsuranceComparison={setDataInsuranceComparison}
                dataInsuranceComparison={dataInsuranceComparison}
              />
            </div>
          )) || (
            <div className="flex h-72 justify-center py-28">
              <div className="flex flex-col items-center">
                <Image
                  width={70}
                  height={44}
                  src="/svg/mailbox.svg"
                  alt="mailbox"
                />
                <p className="text-sm font-normal text-nickel">
                  {t('auth:no_results_displayed')}
                </p>
              </div>
            </div>
          )}
          <div className="mt-5 flex justify-end">
            <Pagination
              page={currentPage}
              pages={pagination?.totalPages}
              onClick={handleClickPage}
            />
          </div>
        </div>
      </div>
    </LayoutWithHeader>
  );
}

export const getServerSideProps = async ({ locale }) => {
  const insuranceObjectives = await getInsuranceObjectives();

  return {
    props: {
      insuranceObjectives: insuranceObjectives?.data,
      ...(await serverSideTranslations(locale, [
        'auth',
        'home',
        'common',
        'insuranceLocale',
        'insuranceDetailsComparison',
      ])),
    },
  };
};
