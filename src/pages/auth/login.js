import BackgroundAuth from 'components/common/BackgroundAuth';
import React, { useContext, useState } from 'react';
import { useTranslation } from 'next-i18next';
import { toast } from 'react-toastify';
import { DataContext } from 'src/store/GlobalState';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { schemaSignInWithEmail, schemaSignInWithPhone } from 'src/utils/error';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import { signIn } from 'next-auth/react';
import Link from 'next/link';
import IconEyeClose from 'components/common/icon/IconEyeClose';
import IconEyeOpen from 'components/common/icon/IconEyeOpen';
import ButtonPrimary from 'components/common/ButtonPrimary';
import { numberInputInvalidChars, onKeyDown } from 'src/utils/helper';

function Login() {
  const { t } = useTranslation(['auth']);

  const router = useRouter();
  const { dispatch } = useContext(DataContext);

  const [signInPhone, setSignInPhone] = useState(true);
  const [signInEmail, setSignInEmail] = useState(false);

  const [passwordType, setPasswordType] = useState('password');

  const handleChangeStatePassword = () => {
    {
      passwordType === 'password' && setPasswordType('text');
      passwordType === 'text' && setPasswordType('password');
    }
  };

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    // mode: 'onChange',
    resolver: yupResolver(
      signInPhone ? schemaSignInWithEmail(t) : schemaSignInWithPhone(t)
    ),
  });

  const handleSignIn = async (data) => {
    try {
      dispatch({ type: 'LOADING', payload: { loading: true } });
      const response = await signIn('credentials', {
        destination: data.destination,
        password: data.password,
        redirect: false,
      });

      if (!response.ok) {
        toast.error(response.error);

        return;
      }
      toast(t('loginSuccess'));
      router.push('/');
    } catch (error) {
      toast.error(t('loginFailure'));

      console.log('🚀 ===== error:', error);
    } finally {
      dispatch({ type: 'LOADING', payload: { loading: false } });
    }
  };

  const hiddenEmail = () => {
    setSignInPhone(true);
    setSignInEmail(false);
    reset();
  };

  const hiddenPhone = () => {
    setSignInPhone(false);
    setSignInEmail(true);
    reset();
  };

  return (
    <BackgroundAuth>
      <form onSubmit={handleSubmit(handleSignIn)}>
        <h4 className="mb-10 text-2xl font-bold text-arsenic md:text-[32px] md:leading-[48px]">
          {t('login')}
        </h4>

        <div className="mb-6 flex justify-center md:mb-9 md:block">
          <button
            className={`w-1/2 px-6 py-3 text-sm font-bold leading-5 md:w-fit md:text-base md:leading-6 ${
              signInPhone
                ? 'rounded-lg bg-platinum text-black'
                : 'text-spanish-gray'
            }`}
            type="button"
            onClick={hiddenEmail}
          >
            {t('useEmail')}
          </button>

          <button
            className={`w-1/2 px-6 py-3 text-sm font-bold leading-5 md:w-fit md:text-base md:leading-6 ${
              signInEmail
                ? 'rounded-lg bg-platinum text-black'
                : 'text-spanish-gray'
            }`}
            type="button"
            onClick={hiddenPhone}
          >
            {t('usePhone')}
          </button>
        </div>

        {signInPhone && (
          <div className="relative mb-6">
            <label
              htmlFor="destination"
              className="text-sm font-bold leading-5 text-arsenic md:text-base md:leading-6"
            >
              {t('email')}
            </label>

            <input
              type="text"
              autoComplete="off"
              id="destination"
              placeholder={t('addressEmail')}
              className={`mt-2 w-full rounded-md border border-chinese-silver bg-transparent text-sm font-normal leading-5 outline-none md:text-base md:leading-6 ${
                errors?.destination?.message
                  ? 'border-red-500'
                  : 'focus:border-ultramarine-blue'
              }  p-2 md:p-3`}
              {...register('destination', { required: true })}
            />

            {errors?.destination && (
              <p className=" text-red-500 ">{errors?.destination?.message}</p>
            )}
          </div>
        )}

        {signInEmail && (
          <div className="relative mb-6">
            <label
              htmlFor="destination"
              className="text-sm font-bold leading-5 text-arsenic md:text-base md:leading-6"
            >
              {t('phone')}
            </label>
            <input
              type="number"
              onKeyDown={(e) => onKeyDown(e, numberInputInvalidChars)}
              id="destination"
              autoComplete="off"
              placeholder={t('phone')}
              className={`mt-2 w-full rounded-md border border-chinese-silver bg-transparent p-2 text-sm font-normal leading-5 outline-none [appearance:textfield] md:p-3 md:text-base md:leading-6 [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:appearance-none ${
                errors?.destination?.message
                  ? 'border-red-500'
                  : 'focus:border-ultramarine-blue'
              }`}
              {...register('destination', { required: true })}
            />
            {errors?.destination && (
              <p className=" text-red-500 ">{errors?.destination?.message}</p>
            )}
          </div>
        )}

        <div className="relative mb-10">
          <label
            htmlFor="password"
            className="text-sm font-bold leading-5 text-arsenic md:text-base md:leading-6"
          >
            {t('password')}
          </label>

          <div className="relative">
            <input
              type={passwordType}
              id="password"
              placeholder={t('pass')}
              className={`mt-2 w-full rounded-md border border-chinese-silver bg-transparent text-sm font-normal leading-5 outline-none  md:text-base md:leading-6 ${
                errors?.password?.message
                  ? 'border-red-500'
                  : 'focus:border-ultramarine-blue'
              }  p-2 md:p-3`}
              {...register('password', { required: true })}
            />
            <button
              type="button"
              onClick={handleChangeStatePassword}
              className="absolute top-4 right-3 md:top-5"
            >
              {passwordType === 'text' ? <IconEyeClose /> : <IconEyeOpen />}
            </button>
          </div>
          {errors?.password && (
            <p className=" text-red-500 ">{errors?.password?.message}</p>
          )}
        </div>
        <ButtonPrimary
          type={'submit'}
          cases={'medium'}
          title={t('login')}
          widthBtn={'mb-6 w-full'}
        />
        <div className="flex flex-wrap justify-center gap-2">
          <p className="text-sm font-normal leading-5 text-nickel md:text-base md:leading-6">
            {t('youUseAccount')}
          </p>

          <Link
            className="text-sm font-bold leading-5 text-ultramarine-blue underline md:text-base md:leading-6"
            href="/auth/register"
          >
            {t('register')}
          </Link>
        </div>
      </form>
    </BackgroundAuth>
  );
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, 'auth')),
    },
  };
};

export default Login;
