import React from 'react';

function Label({ title, htmlFor }) {
  return (
    <>
      <label
        className="mb-2 text-sm font-bold leading-5 text-arsenic md:text-base md:leading-6"
        htmlFor={htmlFor}
      >
        {title}
      </label>
    </>
  );
}

export default Label;
