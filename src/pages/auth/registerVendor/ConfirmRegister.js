import { registerNewVendorAPI } from 'pages/api/authentication';
import React, { useContext, useState } from 'react';
import { useTranslation } from 'next-i18next';
import { toast } from 'react-toastify';
import { DataContext } from 'src/store/GlobalState';
import { schemaRegisterOTP } from 'src/utils/error';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import SendOTP from './sendOTP';

function ConfirmRegister({
  getEmail,
  getPhone,
  setCountdownSendOTP,
  setHiddenOTP,
  getData,
  sendOTP,
  setSendOTP,
}) {
  const { dispatch } = useContext(DataContext);
  const { t } = useTranslation(['auth', 'common']);
  const {
    setError,
    formState: { errors },
  } = useForm({
    mode: 'onChange',
    resolver: yupResolver(schemaRegisterOTP(t)),
  });
  const router = useRouter();

  const [isOTPFilled, setIsOTPFilled] = useState('');

  const handleConfirmRegister = async () => {
    dispatch({ type: 'LOADING', payload: { loading: true } });
    try {
      const { company_id, ...params } = getData;

      let updateData = { ...params };

      if (company_id !== 'other') {
        updateData = { ...updateData, company_id };
      }

      const response = await registerNewVendorAPI({
        params: updateData,
        otp_code: isOTPFilled,
        destination_type: 'email',
      });

      if (response.status === 201) {
        toast(t('auth:registerSuccess'));
      }

      router.push('https://dev.admin.insurance.just.engineer/');
    } catch (error) {
      console.log('>>>>>>>>>>>>', error);
      toast.error(
        t(`common:error.${error?.response?.data?.error_code}`) ||
          t('auth:registerFail')
      );
    }
    dispatch({ type: 'LOADING', payload: { loading: false } });
  };
  const handleSendOTP = () => {
    setSendOTP('');
  };
  const onChangeOtp = (event) => {
    setIsOTPFilled(event.target.value);
    setError('otp_code', {
      message: '',
    });
  };
  const handleKeydownInput = (event) => {
    if (event.key === 'Enter') {
      handleConfirmRegister();
    }
  };

  return (
    <>
      <div className="mb-10">
        <label
          className="mb-2 text-base font-bold text-arsenic"
          htmlFor="otp_code"
        >
          {t('auth:otp_code')}
        </label>
        <div className="mt-2 mb-1 flex w-full items-center">
          <input
            onClick={handleSendOTP}
            placeholder="Mã OTP"
            className="w-3/4 rounded-l-lg border border-chinese-silver bg-transparent px-3 py-2.75 text-base font-normal outline-none [appearance:textfield] focus:border-ultramarine-blue [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:appearance-none"
            type="number"
            maxLength={8}
            autoComplete="otp_code"
            id="otp_code"
            onChange={onChangeOtp}
            value={isOTPFilled}
            onKeyDown={handleKeydownInput}
          />

          <div className="flex-1">
            <SendOTP
              setError={setError}
              setSendOTP={setSendOTP}
              getEmail={getEmail}
              getPhone={getPhone}
              setHiddenOTP={setHiddenOTP}
              setCountdownSendOTP={setCountdownSendOTP}
              setIsOTPFilled={setIsOTPFilled}
            />
          </div>
        </div>
        {sendOTP && (
          <p className="text-base font-normal text-apple">
            {t('auth:sendOTPwithEmail')} {sendOTP?.slice(0, 4)}***
            {sendOTP?.slice(-10)}
          </p>
        )}
        {errors.otp_code && (
          <p className=" text-red-500 ">{errors.otp_code.message}</p>
        )}
      </div>

      <button
        className={`mb-6 w-full rounded-lg border ${
          (!isOTPFilled && 'bg-platinum text-silver') ||
          'bg-ultramarine-blue text-cultured'
        }  py-3 text-base font-bold`}
        type="button"
        onClick={handleConfirmRegister}
        disabled={!isOTPFilled}
      >
        {t('auth:confirmWithOTP')}
      </button>
    </>
  );
}

export default ConfirmRegister;
