import Link from 'next/link';
import React, { useContext, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import BackgroundAuth from 'components/common/BackgroundAuth';
import { DataContext } from 'src/store/GlobalState';
import { toast } from 'react-toastify';
import { getOTP } from 'pages/api/authentication';
import Label from '../Label';
import { schemaRegister } from 'src/utils/error';
import { CalendarIcon, GENDER } from 'src/utils/constants';
import ConfirmRegister from './ConfirmRegister';
import DatePicker from 'react-date-picker';
import 'react-date-picker/dist/DatePicker.css';
import 'react-calendar/dist/Calendar.css';
import IconEyeClose from 'components/common/icon/IconEyeClose';
import IconEyeOpen from 'components/common/icon/IconEyeOpen';
import ButtonPrimary from 'components/common/ButtonPrimary';
import { numberInputInvalidChars, onKeyDown } from 'src/utils/helper';

export default function Register() {
  const { t } = useTranslation(['auth', 'common']);
  const { dispatch } = useContext(DataContext);

  const {
    register,
    handleSubmit,
    formState: { errors },
    setError,
    control,
  } = useForm({
    // mode: 'onChange',
    resolver: yupResolver(schemaRegister(t)),
  });

  const [hiddenOTP, setHiddenOTP] = useState(false);
  const [getEmail, setGetEmail] = useState([null]);
  const [getPhone, setGetPhone] = useState([null]);
  const [getData, setGetData] = useState([null]);
  const [signInPhone, setSignInPhone] = useState(true);
  const [signInEmail, setSignInEmail] = useState(false);
  const [sendOTP, setSendOTP] = useState('');
  const [passwordType, setPasswordType] = useState('password');
  const [rePasswordType, setRePasswordType] = useState('password');

  const handleChangeStatePassword = () => {
    {
      passwordType === 'password' && setPasswordType('text');
      passwordType === 'text' && setPasswordType('password');
    }
  };

  const handleChangeStateRePassword = () => {
    {
      rePasswordType === 'password' && setRePasswordType('text');
      rePasswordType === 'text' && setRePasswordType('password');
    }
  };

  const hiddenEmail = () => {
    setSignInPhone(true);
    setSignInEmail(false);
  };

  const hiddenPhone = () => {
    setSignInPhone(false);
    setSignInEmail(true);
  };

  const handleOTP = async (data) => {
    dispatch({ type: 'LOADING', payload: { loading: true } });

    try {
      await getOTP({
        params: { email: data?.email, phone: data?.phone },
        destination_type: 'email',
        type: 'register',
      });
      setHiddenOTP(true);
      setGetEmail(data?.email);
      setGetPhone(data?.phone);
      setGetData(data);
      setSendOTP(data?.email);
    } catch (error) {
      toast.error(t(`common:error.${error?.response?.data?.error_code}`));
      if (error?.response?.data?.error_code === 'SS24204') {
        setError('email', { message: t('common:error.SS24204') });
      }
      if (error?.response?.data?.error_code === 'SS24205') {
        setError('phone', { message: t('common:error.SS24205') });
      }
    }

    dispatch({ type: 'LOADING', payload: { loading: false } });
  };

  return (
    <BackgroundAuth top={'top-[156px]'}>
      <form>
        {!hiddenOTP && (
          <div>
            <p className="mb-10 text-2xl font-bold text-arsenic md:text-pixel-32 md:leading-pixel-48">
              {t('auth:registerAccount')}
            </p>

            <div className="mb-6 flex flex-wrap justify-between gap-6 sm:flex-nowrap">
              <div className="w-full">
                <Label title={t('auth:firstName')} htmlFor="first_name" />
                <input
                  placeholder={t('yourFirstName')}
                  maxLength="15"
                  className={`mt-2 w-full rounded-md border border-chinese-silver bg-transparent text-base font-normal outline-none ${
                    errors?.first_name
                      ? 'border-red-500'
                      : 'focus:border-ultramarine-blue'
                  } p-2 md:p-3`}
                  type="text"
                  autoComplete="off"
                  id="first_name"
                  {...register('first_name', { required: true })}
                />

                {errors?.first_name && (
                  <p className=" text-red-500 ">
                    {errors?.first_name?.message}
                  </p>
                )}
              </div>

              <div className="w-full">
                <Label title={t('auth:lastName')} htmlFor="last_name" />
                <input
                  placeholder={t('yourLastName')}
                  className={`mt-2 w-full rounded-md border border-chinese-silver bg-transparent text-base font-normal outline-none ${
                    errors?.last_name
                      ? 'border-red-500'
                      : 'focus:border-ultramarine-blue'
                  }  p-2 md:p-3`}
                  type="text"
                  maxLength="15"
                  autoComplete="off"
                  id="last_name"
                  {...register('last_name', { required: true })}
                />
                {errors?.last_name && (
                  <p className=" text-red-500 ">{errors?.last_name?.message}</p>
                )}
              </div>
            </div>

            <div className="mb-2 flex items-center gap-6">
              <label
                htmlFor="gender"
                className="text-sm font-bold text-arsenic md:text-base"
              >
                {t('auth:gender')}
              </label>
              <div className="flex flex-1 justify-evenly gap-4 md:justify-start">
                {GENDER(t).map((list) => (
                  <div
                    key={list?.value}
                    className="mb-5 mt-4 flex items-center gap-4 text-sm font-normal md:mr-10 md:text-base "
                  >
                    <input
                      type="radio"
                      className="h-5 w-5 md:h-6 md:w-6"
                      id="gender"
                      {...register('gender')}
                      value={list?.value}
                    />
                    <p>{list?.label}</p>
                  </div>
                ))}
              </div>
            </div>

            <div className="mb-6 ">
              {errors.gender && (
                <p className=" text-red-500 ">{errors.gender.message}</p>
              )}
            </div>

            <div className="mb-6 w-full">
              <Label title={t('auth:email')} htmlFor="email" />
              <input
                placeholder={t('addressEmail')}
                className={`mt-2 w-full rounded-md border border-chinese-silver bg-transparent text-sm font-normal outline-none md:text-base ${
                  errors?.email
                    ? 'border-red-500'
                    : 'focus:border-ultramarine-blue'
                }  p-2 md:p-3`}
                type="text"
                autoComplete="off"
                id="email"
                {...register('email', { required: true })}
                aria-invalid={errors.email ? 'true' : 'false'}
              />
              {errors?.email && (
                <p className="text-red-500">{errors?.email?.message}</p>
              )}
            </div>

            <div className="mb-6 flex flex-wrap justify-between gap-6 sm:flex-nowrap">
              <div className="w-full">
                <Label title={t('auth:phone')} htmlFor="phone" />

                <input
                  placeholder={t('phone')}
                  className={`mt-2 w-full rounded-md border border-chinese-silver bg-transparent text-sm font-normal outline-none [appearance:textfield] md:text-base [&::-webkit-inner-spin-button]:appearance-none [&::-webkit-outer-spin-button]:appearance-none ${
                    errors?.phone
                      ? 'border-red-500'
                      : 'focus:border-ultramarine-blue'
                  }  p-2 md:p-3`}
                  type="number"
                  onKeyDown={(e) => onKeyDown(e, numberInputInvalidChars)}
                  autoComplete="off"
                  id="phone"
                  {...register('phone', { required: true })}
                />
                {errors?.phone && (
                  <p className=" text-red-500 ">{errors?.phone?.message}</p>
                )}
              </div>

              <div className="w-full">
                <Label
                  title={t('auth:date_of_birth')}
                  htmlFor="date_of_birth"
                />

                <div
                  className={`mt-2 rounded-md border ${
                    errors.date_of_birth
                      ? 'border-red-500'
                      : 'border-chinese-silver'
                  } p-2 md:p-3`}
                >
                  <Controller
                    control={control}
                    name="date_of_birth"
                    render={({ field }) => (
                      <DatePicker
                        id="date_of_birth"
                        name="date_of_birth"
                        format="dd/MM/yyyy"
                        onChange={(e) => field.onChange(e)}
                        monthPlaceholder="MM"
                        dayPlaceholder="DD"
                        yearPlaceholder="YYYY"
                        value={field.value}
                        clearIcon={null}
                        className="h-6 w-full"
                        calendarIcon={CalendarIcon}
                      />
                    )}
                    {...register('date_of_birth', { required: true })}
                  />
                </div>

                {errors.date_of_birth && (
                  <p className="text-red-500">{errors.date_of_birth.message}</p>
                )}
              </div>
            </div>

            <div className="mb-6">
              <Label title={t('auth:password')} htmlFor="password" />
              <div className="relative">
                <input
                  placeholder={t('pass')}
                  className={`mt-2 w-full rounded-md border border-chinese-silver bg-transparent text-sm font-normal outline-none md:text-base ${
                    errors?.password
                      ? 'border-red-500'
                      : 'focus:border-ultramarine-blue'
                  }  flex p-2 md:p-3`}
                  type={passwordType}
                  maxLength="20"
                  autoComplete="new-password"
                  id="password"
                  {...register('password', { required: true })}
                />
                <button
                  type="button"
                  onClick={handleChangeStatePassword}
                  className="absolute right-2 top-2 md:right-3 md:top-3"
                >
                  {passwordType === 'text' ? <IconEyeClose /> : <IconEyeOpen />}
                </button>
              </div>
              {errors?.password && (
                <p className=" text-red-500 ">{errors?.password?.message}</p>
              )}
            </div>
            <div className="mb-10">
              <Label title={t('auth:rePassword')} htmlFor="rePassword" />
              <div className="relative">
                <input
                  placeholder={t('rePassword')}
                  className={`mt-2 w-full rounded-md border border-chinese-silver bg-transparent text-sm font-normal outline-none md:text-base ${
                    errors.confirmation_password
                      ? 'border-red-500'
                      : 'focus:border-ultramarine-blue'
                  }  flex p-2 md:p-3`}
                  type={rePasswordType}
                  maxLength="20"
                  autoComplete="new-password"
                  id="confirmation_password"
                  {...register('confirmation_password', { required: true })}
                />
                <button
                  type="button"
                  onClick={handleChangeStateRePassword}
                  className="absolute right-2 top-2 md:right-3 md:top-3"
                >
                  {rePasswordType === 'text' ? (
                    <IconEyeClose />
                  ) : (
                    <IconEyeOpen />
                  )}
                </button>
              </div>

              {errors.confirmation_password && (
                <p className="text-red-500">
                  {errors.confirmation_password.message}
                </p>
              )}
            </div>
            <ButtonPrimary
              type={'submit'}
              cases={'medium'}
              title={t('auth:registerAccount')}
              onClick={handleSubmit(handleOTP)}
              widthBtn={'mb-6 w-full'}
            />
          </div>
        )}
      </form>

      {!!hiddenOTP && (
        <div>
          <p className="mb-10 text-pixel-32 font-bold leading-pixel-48 text-arsenic">
            {t('auth:confirmOTP')}
          </p>

          <div className="mb-6 flex items-center">
            <button
              className={`w-1/2 px-6 py-3 text-base font-bold lg:w-fit ${
                (signInPhone && 'rounded-lg bg-platinum text-arsenic') ||
                'text-spanish-gray'
              }`}
              type="button"
              onClick={hiddenEmail}
            >
              {t('auth:useEmail')}
            </button>

            <button
              className={`w-1/2 px-6 py-3 text-base font-bold lg:w-fit ${
                (signInEmail && 'rounded-lg bg-platinum text-arsenic') ||
                'text-spanish-gray'
              }`}
              type="button"
              onClick={hiddenPhone}
            >
              {t('auth:usePhone')}
            </button>
          </div>

          <ConfirmRegister
            setHiddenOTP={setHiddenOTP}
            getData={getData}
            getPhone={getPhone}
            getEmail={getEmail}
            sendOTP={sendOTP}
            setSendOTP={setSendOTP}
          />
        </div>
      )}

      <div className="flex justify-center gap-2">
        <p className="text-sm font-normal text-nickel md:text-base">
          {t('auth:loginWithAccount')}
        </p>

        <Link
          className="text-sm font-bold text-ultramarine-blue underline md:text-base"
          href="/auth/login"
        >
          {t('auth:login')}
        </Link>
      </div>
    </BackgroundAuth>
  );
}

export const getServerSideProps = async ({ locale }) => {
  return {
    props: {
      ...(await serverSideTranslations(locale, ['auth', 'common'])),
    },
  };
};
