import { getOTP } from 'pages/api/authentication';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'next-i18next';
import { toast } from 'react-toastify';
import { DataContext } from 'src/store/GlobalState';

function SendOTP({
  getEmail,
  getPhone,
  setHiddenOTP,
  setSendOTP,
  setIsOTPFilled,
  setError,
}) {
  const { dispatch } = useContext(DataContext);
  const { t } = useTranslation(['auth', 'common']);
  const [hiddenResetOTP, setHiddenResetOTP] = useState(false);
  const [countdownSendOTP, setCountdownSendOTP] = useState(60);

  useEffect(() => {
    if (countdownSendOTP === 0) return setHiddenResetOTP(true);

    const intervalId = setInterval(() => {
      setCountdownSendOTP(countdownSendOTP - 1);
    }, 1000);

    return () => clearInterval(intervalId);
  }, [countdownSendOTP]);

  const handleOTP = async () => {
    dispatch({ type: 'LOADING', payload: { loading: true } });

    try {
      await getOTP({
        params: { email: getEmail, phone: getPhone },
        destination_type: 'email',
        type: 'register',
      });
      setCountdownSendOTP(60);
      setHiddenResetOTP(false);
      setHiddenOTP(true);
      setSendOTP(getEmail);
      setIsOTPFilled('');
      setError('otp_code', {
        message: '',
      });
    } catch (error) {
      toast.error(
        t(`common:error.${error?.response?.data?.error_code}`) ||
          t('auth:registerFail')
      );
    } finally {
      dispatch({ type: 'LOADING', payload: { loading: false } });
    }
  };

  return (
    <div className="w-full">
      {(hiddenResetOTP === true && (
        <button
          className="w-full whitespace-nowrap rounded-r-lg bg-ultramarine-blue px-4 py-3 text-base font-bold leading-6 text-cultured"
          type="button"
          onClick={handleOTP}
        >
          {t('auth:takeOTP')}
        </button>
      )) || (
        <div className="flex items-center justify-center rounded-r-lg bg-platinum px-4 py-3 text-base font-bold leading-6 text-silver">
          {countdownSendOTP}
        </div>
      )}
    </div>
  );
}

export default SendOTP;
