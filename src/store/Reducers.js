const reducers = (state, action) => {
  switch (action.type) {
    case 'LOADING':
      return {
        ...state,
        notify: action.payload,
      };
    case 'LOGIN':
      return {
        ...state,
        login: action.loginNoAccount,
      };
    case 'COMPARISON':
      return {
        ...state,
        comparison: action.comparison,
        updateDataInsurance: action.updateDataInsurance,
      };
    default:
      return state;
  }
};

export default reducers;
