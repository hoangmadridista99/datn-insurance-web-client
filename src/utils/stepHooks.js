import { IconCheck } from 'components/common/icon/Icon';
import React from 'react';

export const stepFillInformation = (t, steps) => {
  const space = () => (
    <div className="mb-3 h-0.5 w-18.75 rounded-md border bg-platinum"></div>
  );
  const stepsFill = ({ requestOne, requestTwo, numberStep, label }) => (
    <div className="flex items-start md:ml-8">
      <div className="w-16 md:flex md:w-full md:gap-3">
        <div className="mb-1 flex justify-center">
          <p
            className={`${
              requestOne
                ? 'bg-ultramarine-blue text-white'
                : 'text-spanish-gray'
            } flex h-6 w-6 items-center justify-center rounded-full border text-sm font-bold lg:h-8 lg:w-8 lg:text-base`}
          >
            {requestTwo ? <IconCheck /> : numberStep}
          </p>
        </div>
        <p
          className={`${
            requestOne ? 'text-ultramarine-blue' : 'text-spanish-gray'
          } text-center text-xs font-normal md:flex md:items-center lg:mb-1 lg:text-xl lg:font-bold`}
        >
          {label}
        </p>
      </div>
    </div>
  );

  return [
    {
      id: 1,
      nameStep: (
        <div className="flex items-start md:ml-8">
          <div className="w-16 md:flex md:w-full md:gap-3">
            <div className="mb-1 flex justify-center">
              <p className="flex h-6 w-6 items-center justify-center rounded-full border bg-ultramarine-blue text-sm font-bold text-white lg:h-8 lg:w-8 lg:text-base">
                {steps > 1 ? <IconCheck /> : '1'}
              </p>
            </div>
            <p className="text-center text-xs font-normal text-ultramarine-blue md:flex md:items-center lg:mb-1 lg:text-xl lg:font-bold">
              {t('enter_information')}
            </p>
          </div>
        </div>
      ),
    },
    {
      id: 2,
      space: <>{space()}</>,
    },
    {
      id: 3,
      nameStep: (
        <>
          {stepsFill({
            requestOne: steps > 1,
            requestTwo: steps > 2,
            numberStep: '2',
            label: t('accept'),
          })}
        </>
      ),
    },
    {
      id: 4,
      space: <>{space()}</>,
    },
    {
      id: 5,
      nameStep: (
        <>
          {stepsFill({
            requestOne: steps > 2,
            numberStep: '3',
            label: t('pay'),
          })}
        </>
      ),
    },
  ];
};

export const stepFillInformationMaterial = (t, steps) => {
  const space = () => (
    <div className="mb-3 h-0.5 w-7 rounded-md border bg-platinum sm:w-16"></div>
  );
  const stepsFill = ({ requestOne, requestTwo, numberStep, label }) => (
    <div className="flex items-start md:ml-8">
      <div className="w-16 sm:w-28 md:flex md:w-31 md:gap-3 lg:w-full">
        <div className="mb-1 flex justify-center">
          <p
            className={`${
              requestOne
                ? 'bg-ultramarine-blue text-white'
                : 'text-spanish-gray'
            } flex h-6 w-6 items-center justify-center rounded-full border text-sm font-bold sm:h-8 sm:w-8 sm:text-base`}
          >
            {requestTwo ? <IconCheck /> : numberStep}
          </p>
        </div>
        <p
          className={`${
            requestOne ? 'text-ultramarine-blue' : 'text-spanish-gray'
          } text-center text-xs font-normal sm:mb-1 sm:text-base sm:font-bold md:flex md:items-center lg:text-xl`}
        >
          {label}
        </p>
      </div>
    </div>
  );

  return [
    {
      id: 1,
      nameStep: (
        <div className="flex items-start md:ml-8">
          <div className="w-16 sm:w-31 md:flex md:w-full md:gap-3">
            <div className="mb-1 flex justify-center">
              <p className="flex h-6 w-6 items-center justify-center rounded-full border bg-ultramarine-blue text-sm font-bold text-white sm:h-8 sm:w-8 sm:text-base">
                {steps > 1 ? <IconCheck /> : '1'}
              </p>
            </div>
            <p className="text-center text-xs font-normal text-ultramarine-blue sm:mb-1 sm:text-base sm:font-bold md:flex md:items-center md:text-xl">
              Thông tin xe
            </p>
          </div>
        </div>
      ),
    },
    {
      id: 2,
      space: <>{space()}</>,
    },
    {
      id: 3,
      nameStep: (
        <>
          {stepsFill({
            requestOne: steps > 1,
            requestTwo: steps > 2,
            numberStep: '2',
            label: 'Thông tin bảo hiểm',
          })}
        </>
      ),
    },
    {
      id: 4,
      space: <>{space()}</>,
    },
    {
      id: 5,
      nameStep: (
        <>
          {stepsFill({
            requestOne: steps > 2,
            requestTwo: steps > 3,
            numberStep: '3',
            label: t('accept'),
          })}
        </>
      ),
    },
    {
      id: 6,
      space: <>{space()}</>,
    },
    {
      id: 7,
      nameStep: (
        <>
          {stepsFill({
            requestOne: steps > 3,
            numberStep: '4',
            label: t('pay'),
          })}
        </>
      ),
    },
  ];
};
