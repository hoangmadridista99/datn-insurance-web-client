import * as yup from 'yup';

export const ERROR_REGISTER_NEW_USER = {
  PHONE_NUMBER_ALREADY_EXIST: 'Số điện thoại đã được đăng ký.',
  EMAIL_ALREADY_EXIST: 'Email đã được đăng ký.',
};

export const ERROR_SIGN_IN = {
  ERROR_422: 'Tài khoản hoặc mật khẩu không đúng!',
  ERROR_403: 'Bạn không có quyền đăng nhập!',
  ERROR_401: 'Đăng nhập thất bại!',
};

export const schemaRegister = (t) =>
  yup
    .object({
      date_of_birth: yup
        .date()
        .typeError(t('auth:schema.date_of_birth_error'))
        .min(new Date('1910-01-01'), t('auth:schema.date_of_birth_min'))
        .max(new Date(), t('auth:schema.date_of_birth_max'))
        .required(t('auth:schema.date_of_birth_error')),
      first_name: yup
        .string()
        .matches(/^[\p{L}\s'.-]+$/u, t('auth:schema.first_name_matches'))
        .required(t('auth:schema.first_name_required')),
      last_name: yup
        .string()
        .matches(/^[\p{L}\s'.-]+$/u, t('auth:schema.last_name_matches'))
        .required(t('auth:schema.last_name_required')),
      password: yup
        .string()
        .required(t('auth:schema.password_required'))
        .min(8, t('auth:schema.password_min')),
      confirmation_password: yup
        .string()
        .oneOf([yup.ref('password'), null], t('auth:schema.rePassword_oneOf'))
        .required(t('auth:schema.rePassword_required')),
      email: yup
        .string()
        .matches(
          /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
          t('auth:schema.email')
        )
        .required(t('auth:schema.email_required')),
      phone: yup
        .string()
        .matches(
          /(84|0[3|5|7|8|9])+([0-9]{8})\b/,
          t('auth:schema.phone_matches')
        )
        .min(10, t('auth:schema.phone_min'))
        .max(11, t('auth:schema.phone_max')),
      gender: yup.string().required(t('auth:schema.gender')),
    })
    .required();

export const schemaRegisterOTP = (t) =>
  yup
    .object({
      otp_code: yup.string().required(t('auth:schema.otp_code')),
    })
    .required();

export const schemaRegisterVendor = (t) =>
  yup
    .object({
      date_of_birth: yup
        .date()
        .typeError(t('auth:schema.date_of_birth_error'))
        .min(new Date('1910-01-01'), t('auth:schema.date_of_birth_min'))
        .max(new Date(), t('auth:schema.date_of_birth_max'))
        .required(t('auth:schema.date_of_birth_error')),
      first_name: yup
        .string()
        .matches(/^[\p{L}\s'.-]+$/u, t('auth:schema.first_name_matches'))
        .required(t('auth:schema.first_name_required')),
      last_name: yup
        .string()
        .matches(/^[\p{L}\s'.-]+$/u, t('auth:schema.last_name_matches'))
        .required(t('auth:schema.last_name_required')),
      password: yup
        .string()
        .required(t('auth:schema.password_required'))
        .min(8, t('auth:schema.password_min')),
      confirmation_password: yup
        .string()
        .oneOf([yup.ref('password'), null], t('auth:schema.rePassword_oneOf'))
        .required(t('auth:schema.rePassword_required')),
      email: yup
        .string()
        .matches(
          /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
          t('auth:schema.email')
        )
        .required(t('auth:schema.email_required')),
      phone: yup
        .string()
        .matches(
          /(84|0[3|5|7|8|9])+([0-9]{8})\b/,
          t('auth:schema.phone_matches')
        )
        .min(10, t('auth:schema.phone_min'))
        .max(11, t('auth:schema.phone_max')),
      address: yup
        .string()
        .matches(
          /^[\w\d\sáàảãạâấầẩẫậăắằẳẵặđéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựýỳỷỹỵ',.]*$/i,
          t('auth:schema.address_matches')
        )
        .required(t('auth:schema.address_required')),
      company_id: yup.string().required(t('auth:schema.company_name_required')),
      gender: yup.string().required(t('auth:schema.gender')),
    })
    .required();

export const schemaSignInWithEmail = (t) =>
  yup.object({
    destination: yup
      .string()
      .matches(
        /^[a-zA-Z0-9][a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/,
        t('auth:schema.email')
      )
      .required(t('auth:schema.email_required')),
    password: yup
      .string()
      .required(t('auth:schema.password_required'))
      .min(8, t('auth:schema.password_min')),
  });

export const schemaSignInWithPhone = (t) =>
  yup.object({
    destination: yup
      .string()
      .matches(
        /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/,
        t('auth:schema.phone_matches')
      )
      .min(10, t('auth:schema.phone_min'))
      .max(11, t('auth:schema.phone_matches')),
    password: yup
      .string()
      .required(t('auth:schema.password_required'))
      .min(8, t('auth:schema.password_min')),
  });

export const schemaFilterInsurance = () => yup.object({});

export const schemaFilterHealthInsurance = () => yup.object({});

export const schemaChangePassword = (t) =>
  yup.object({
    current_password: yup
      .string()
      .required(t('auth:schema.password_current'))
      .min(8, t('auth:schema.password_min')),
    new_password: yup
      .string()
      .required(t('auth:schema.password_required'))
      .min(8, t('auth:schema.password_min')),
    confirmation_password: yup
      .string()
      .oneOf([yup.ref('new_password'), null], t('auth:schema.rePassword_oneOf'))
      .required(t('auth:schema.rePassword_required')),
  });

export const schemaCarMaterial = () => yup.object({});
