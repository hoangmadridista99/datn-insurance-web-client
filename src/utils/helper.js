import React from 'react';
import {
  IconRatingStarNone,
  IconRatingStarYellow,
  IconTickGood,
  IconTickGoodBetter,
  IconTickInformation,
  IconTickNone,
} from 'components/common/icon/Icon';
import { APPROVED, PAID, PENDING, REJECTED } from './constants';

export const convertObjectives = (objectives) => {
  switch (objectives) {
    case 'security':
      return 'Bảo hiểm nhân thọ mang tính bảo vệ';
    case 'retirement':
      return 'Bảo hiểm nhân thọ hưu trí';
    case 'business':
      return 'Bảo hiểm nhân thọ doanh nghiệp';
    case 'investment':
      return 'Bảo hiểm nhân thọ mang tính đầu tư';
    case 'savings':
      return 'Bảo hiểm nhân thọ mang tính tích lũy';
    case 'education':
      return 'Bảo hiểm nhân thọ mang tính giáo dục';
    default:
      return '-';
  }
};

export const formatLevelText = (level) => {
  switch (level) {
    case 'high':
      return (
        <div>
          <IconTickGoodBetter />
        </div>
      );
    case 'medium':
      return (
        <div>
          <IconTickGood />
        </div>
      );
    case 'not-support':
      return (
        <div>
          <IconTickInformation />
        </div>
      );
    case 'none':
      return (
        <div>
          <IconTickNone />
        </div>
      );
    default:
      return '-';
  }
};

export const formatTypeDeadlineForDeal = (type) => {
  switch (type) {
    case 'old':
      return 'tuổi';
    case 'year':
      return 'năm';
  }
};

export const convertUrl = (url) => {
  const key = '?key=pdf/';
  const [homepage, name] = url.split(key);
  const link = `${homepage}/download${key}${name}`;

  return { name, link };
};

export const convertRatingStar = (rating) => {
  const ratingStar = [
    {
      id: 1,
      rating: (rating >= 1 && (
        <div>
          <IconRatingStarYellow />
        </div>
      )) || (
        <div>
          <IconRatingStarNone />
        </div>
      ),
    },
    {
      id: 2,
      rating: (rating >= 2 && (
        <div>
          <IconRatingStarYellow />
        </div>
      )) || (
        <div>
          <IconRatingStarNone />
        </div>
      ),
    },
    {
      id: 3,
      rating: (rating >= 3 && (
        <div>
          <IconRatingStarYellow />
        </div>
      )) || (
        <div>
          <IconRatingStarNone />
        </div>
      ),
    },
    {
      id: 4,
      rating: (rating >= 4 && (
        <div>
          <IconRatingStarYellow />
        </div>
      )) || (
        <div>
          <IconRatingStarNone />
        </div>
      ),
    },
    {
      id: 5,
      rating: (rating >= 5 && (
        <div>
          <IconRatingStarYellow />
        </div>
      )) || (
        <div>
          <IconRatingStarNone />
        </div>
      ),
    },
  ];
  return (
    <div className="flex gap-1">
      {ratingStar.map((value) => (
        <div key={value.id}>{value?.rating}</div>
      ))}
    </div>
  );
};

export const timeCreateRating = (rating) => {
  console.log('🚀 ========= rating:', rating);
  return rating?.slice(0, 10);
};

export const formatTimeFeeDeadline = (time) => {
  switch (time) {
    case 'monthly':
      return 'hàng tháng';
    case 'yearly':
      return 'hàng năm';
    case 'flexible':
      return 'linh hoạt';
    case 'once':
      return 'đóng phí 1 lần';
    default:
      return '-';
  }
};

export const formatInsuredPerson = (value) => {
  switch (value) {
    case 'one-self':
      return 'mua cho bản thân';
    case 'other-person':
      return 'mua cho người thân';
    default:
      return '-';
  }
};

export const formatCustomerSegment = (value) => {
  switch (value) {
    case 'BUSINESS':
      return 'Doanh nghiệp';
    case 'INDIVIDUAL':
      return 'Cá nhân';
    case 'ORGANIZATION':
      return 'Tổ chức';
    case 'FAMILY':
      return 'Gia đình';
    default:
      return '-';
  }
};

export const formatProfession = (value) => {
  switch (value) {
    case 'EMPLOYER':
      return 'Làm thuê';
    case 'EMPLOYEE':
      return 'Làm chủ';
    case 'STUDENT':
      return 'Sinh viên';
    case 'OTHER_PROFESSION':
      return 'Nghành nghề khác';
    default:
      return '-';
  }
};
export const formatCurrencyVietnam = (number) => {
  let formattedNumber = number?.toLocaleString('vi');
  return formattedNumber;
};

export const formatRoomType = (room) => {
  switch (room) {
    case 'one-bed':
      return 'Phòng đơn';
    case 'two-beds':
      return 'Phòng đôi';
    case 'multi-beds':
      return 'Phòng đa giường';
    case 'vip-room':
      return 'Phòng vip';
    case 'special-room':
      return 'Phòng chăm sóc đặc biệt';
    default:
      return '-';
  }
};

export const formatCategory = (cate) => {
  switch (cate) {
    case 'health':
      return 'Bảo hiểm sức khoẻ';
    case 'life':
      return 'Bảo hiểm nhân thọ';
    default:
      return '-';
  }
};

export const lockScroll = () => {
  return (document.body.style.overflow = 'hidden');
};

export const unlockScroll = () => {
  return (document.body.style.overflow = 'scroll');
};

export const formatTimeDate = (timeDate) => {
  const date = new Date(timeDate);
  const formattedDate = `${date.getDate()}/${
    date.getMonth() + 1
  }/${date.getFullYear()}`;
  return <p>{formattedDate}</p>;
};

export const onKeyDown = (e, numberInputInvalidChars) => {
  if (numberInputInvalidChars.includes(e.key)) {
    e.preventDefault();
  }
};
export const onKeyDownForTax = (
  e,
  textInputValidChar = ['-', 'Backspace', 'Control', 'a', 'Enter', 'c', 'v']
) => {
  if (!textInputValidChar.includes(e.key)) {
    isNaN(parseInt(e.key)) && e.preventDefault();
  } else if (
    (!e.ctrlKey && e.key === 'a') ||
    (!e.ctrlKey && e.key === 'c') ||
    (!e.ctrlKey && e.key === 'v')
  ) {
    e.preventDefault();
  }
};
export const numberInputInvalidChars = ['-', '+', 'e', ',', '.'];

export const convertTypeCarBusiness = (type) => {
  switch (type) {
    case 'false':
      return 'Xe ô tô không kinh doanh vận tải';
    case 'true':
      return 'Xe ô tô kinh doanh vận tải';
  }
};

export const convertTypeCar = (type) => {
  switch (type) {
    case 'less-than-six':
      return 'Dưới 6 chỗ ngồi';
    case 'six-to-eleven':
      return '6 - 11 chỗ ngồi';
    case 'passenger-cargo-car':
      return 'Xe vừa chở người, vừa chở hàng (Pickup, minivan)';
    default:
      return `${type} chỗ ngồi`;
  }
};
export const checkStatusFormType = (formType) => {
  switch (formType) {
    case 'new-car':
      return true;
    case 'has-expired':
      return true;
    case 'unexpired':
      return true;
  }
};

export const statusInsurance = (status) => {
  switch (status) {
    case APPROVED:
      return 'bg-apple-lighter text-apple';
    case PENDING:
      return 'bg-cosmic-latte text-royal-orange';
    case REJECTED:
      return 'bg-linen text-vermilion';
    case PAID:
      return 'bg-lavender text-azure';
  }
};
export const statusTextInsurance = (t, status) => {
  switch (status) {
    case APPROVED:
      return t('successfulAppraisal');
    case PENDING:
      return t('waitingForAppraisal');
    case REJECTED:
      return t('appraisalFailed');
    case PAID:
      return t('paid');
  }
};

export const regexPhoneNumber = /(03|05|07|08|09|01[2|6|8|9])+([0-9]{8})\b/;

export const regexEmail =
  /^[a-zA-Z0-9][a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

export const regexCarLicense =
  /^[1-9]{1}[0-9]{1}[A-HJ-NP-TV-Z]{1}-\d{3}\.?\d{2}$/;
