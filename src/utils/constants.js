import React from 'react';
import IconFb from 'components/common/icon/IconFb';
import IconInstagram from 'components/common/icon/IconInsta';
import IconLinkedin from 'components/common/icon/IconLinkedin';
import IconTwitter from 'components/common/icon/IconTwitter';
import { IconArrowDown } from 'components/common/icon/Icon';
import dayjs from 'dayjs';
import numeral from 'numeral';
import ListUlInsurance from 'components/healthInsurance/listUlInsurance';

export const LIST_WORKS = (t) => [
  {
    name: t('common:employee'),
    value: 'EMPLOYEE',
  },
  {
    name: t('common:employer'),
    value: 'EMPLOYER',
  },
  {
    name: t('common:student'),
    value: 'STUDENT',
  },
  {
    name: t('common:other_profession'),
    value: 'OTHER_PROFESSION',
  },
];

export const TOTAL_SUM_INSURED = [
  '100,000,000',
  '200,000,000',
  '300,000,000',
  '400,000,000',
  '500,000,000',
];

export const LIST_ICON = [
  { id: 'I1', icon: <IconFb />, url: '/' },
  { id: 'I2', icon: <IconInstagram />, url: '/' },
  { id: 'I3', icon: <IconLinkedin />, url: '/' },
  { id: 'I4', icon: <IconTwitter />, url: '/' },
];

export const TIME_INSURANCE = ['5', '10', '15', '20', '25'];

export const LIST_BUTTON_FILTER = (t) => [
  { id: 'BF1', label: t('alpha_asc'), filterSort: 'alpha_asc' },
  { id: 'BF2', label: t('alpha_desc'), filterSort: 'alpha_desc' },
  { id: 'BF3', label: t('benefit_best'), filterSort: '' },
  { id: 'BF4', label: t('min_price'), filterSort: 'min_price' },
];

export const MONEY_SUM = (t) => [
  { value: 100000000, name: '100.000.000 VNĐ' },
  { value: 200000000, name: '200.000.000 VNĐ' },
  { value: 300000000, name: '300.000.000 VNĐ' },
  { value: 400000000, name: '400.000.000 VNĐ' },
  { value: 500000000, name: '500.000.000 VNĐ' },
  { value: 'hidden', name: t('common:other_value') },
];

export const TIME_INSURANCE_FILTER = (t) => [
  { value: 5, name: '5 năm' },
  { value: 10, name: '10 năm' },
  { value: 15, name: '15 năm' },
  { value: 20, name: '20 năm' },
  { value: 25, name: '25 năm' },
  { value: 'hidden', name: t('common:other_value') },
];

export const GENDER = (t) => [
  { value: 'male', label: t('common:male') },
  { value: 'female', label: t('common:female') },
];

export const LIST_BENEFIT_INSURANCE = (t) => [
  {
    label: t('death_or_disability'),
    value: 'death_or_disability',
  },
  {
    label: t('serious_illnesses'),
    value: 'serious_illnesses',
  },
  { label: t('health_care'), value: 'health_care' },
  {
    label: t('investment_benefit'),
    value: 'investment_benefit',
  },
  {
    label: t('increasing_value_bonus'),
    value: 'increasing_value_bonus',
  },
  { label: t('for_child'), value: 'for_child' },
  { label: t('flexible_and_diverse'), value: 'flexible_and_diverse' },
  { label: t('termination_benefits'), value: 'termination_benefits' },
  { label: t('expiration_benefits'), value: 'expiration_benefits' },
  { label: t('fee_exemption'), value: 'fee_exemption' },
];

export const listKeyInformation = (t) => {
  return [
    { id: '01', label: t('WhatIsLifeInsurance') },
    { id: '02', label: t('shouldBuyInsurance') },
    { id: '03', label: t('buyLifeInsurance') },
    {
      id: '04',
      label: t('howMuchLifeInsuranceIsSafe'),
    },
    { id: '05', label: t('yearsOfPremiumPayment') },
  ];
};

export const DefaultValueFormFilter = {
  objective: null,
  total_sum_insured: null,
  current_income_level_from: null,
  insured_person: null,
  year_of_birth: null,
  profession: null,
  deadline_for_deal: null,
  current_income_level_to: null,
};

export const ratingInsuranceOfUsers = (scoreRating) => [
  {
    className: `flex h-8 w-8 items-center justify-center rounded ${
      (scoreRating >= 1 && 'bg-maize') || 'bg-chinese-silver'
    } `,
    value: 1,
  },
  {
    className: `flex h-8 w-8 items-center justify-center rounded ${
      (scoreRating >= 2 && 'bg-maize') || 'bg-chinese-silver'
    } `,
    value: 2,
  },
  {
    className: `flex h-8 w-8 items-center justify-center rounded ${
      (scoreRating >= 3 && 'bg-maize') || 'bg-chinese-silver'
    } `,
    value: 3,
  },
  {
    className: `flex h-8 w-8 items-center justify-center rounded ${
      (scoreRating >= 4 && 'bg-maize') || 'bg-chinese-silver'
    } `,
    value: 4,
  },
  {
    className: `flex h-8 w-8 items-center justify-center rounded ${
      (scoreRating >= 5 && 'bg-maize') || 'bg-chinese-silver'
    } `,
    value: 5,
  },
];

export const descriptionDetailInsurance = {
  age_eligibility: () => {
    return (
      <div>
        <p className="mb-3">
          Thông tin về độ tuổi tham gia bảo hiểm là một trong những yếu tố quan
          trọng trong việc đánh giá rủi ro của một chương trình bảo hiểm. Độ
          tuổi của người tham gia bảo hiểm ảnh hưởng đến chi phí bảo hiểm và xác
          suất các sự cố xảy ra.
        </p>
        <p>
          Thường thì, các chương trình bảo hiểm sẽ có giới hạn độ tuổi tối đa và
          tối thiểu cho phép tham gia bảo hiểm. Giới hạn này có thể khác nhau
          tùy thuộc vào loại bảo hiểm và nhà cung cấp bảo hiểm.
        </p>
      </div>
    );
  },
  age_of_contract_termination: () => {
    return (
      <div>
        <p className="mb-3">
          Thời hạn hợp đồng bảo hiểm là thời gian mà một chương trình bảo hiểm
          có hiệu lực và thời gian kết thúc của chương trình đó. Đây là một phần
          quan trọng của điều kiện và điều khoản của chương trình bảo hiểm, và
          được thỏa thuận giữa nhà cung cấp bảo hiểm và người tham gia bảo hiểm.
        </p>
        <p className="mb-3">
          Khi thời hạn hợp đồng bảo hiểm kết thúc, người tham gia bảo hiểm có
          thể gia hạn hợp đồng hoặc đăng ký lại bảo hiểm để tiếp tục sử dụng
          chương trình bảo hiểm. Trong một số trường hợp, khi thời hạn hợp đồng
          bảo hiểm kết thúc, người tham gia bảo hiểm có thể mất quyền lợi bảo
          hiểm, và phải đăng ký lại với các điều kiện và điều khoản mới.
        </p>
        <p>
          Một số nhà cung cấp bảo hiểm cũng cung cấp cho người tham gia bảo hiểm
          các tùy chọn thời hạn hợp đồng khác nhau, cho phép họ lựa chọn thời
          gian bảo hiểm phù hợp với nhu cầu của mình. Vì vậy, quy định về thời
          hạn hợp đồng bảo hiểm cũng là một yếu tố quan trọng khi người tham gia
          bảo hiểm lựa chọn một chương trình bảo hiểm phù hợp với nhu cầu của
          họ.
        </p>
      </div>
    );
  },
  insurance_minimum_fee: () => {
    return (
      <>
        <p className="mb-3">
          Phí bảo hiểm tối thiểu là số tiền phải trả hàng tháng hoặc hàng năm để
          đảm bảo được sự bảo vệ từ chương trình bảo hiểm. Đây là mức phí tối
          thiểu mà một người tham gia bảo hiểm phải trả để có được mức bảo hiểm
          tối thiểu từ nhà cung cấp bảo hiểm.
        </p>
        <p className="mb-3">
          Mức phí bảo hiểm tối thiểu thường được xác định bởi nhà cung cấp bảo
          hiểm dựa trên nhiều yếu tố, bao gồm độ tuổi, tình trạng sức khỏe,
          ngành nghề và mức độ rủi ro của khách hàng. Một số yếu tố như lịch sử
          y tế, tuổi tác và hạng ngành nghề sẽ có ảnh hưởng đến mức phí bảo hiểm
          tối thiểu. Tuy nhiên, việc đưa ra mức phí bảo hiểm tối thiểu cũng phụ
          thuộc vào loại hình bảo hiểm và quy định của pháp luật trong từng quốc
          gia hoặc khu vực.
        </p>
        <p>
          Mức phí bảo hiểm tối thiểu thường là mức phí thấp nhất mà một người
          tham gia bảo hiểm có thể trả để được bảo hiểm. Tuy nhiên, việc trả phí
          bảo hiểm tối thiểu không đảm bảo rằng mức độ bảo vệ của chương trình
          bảo hiểm sẽ đáp ứng đầy đủ nhu cầu của người tham gia bảo hiểm. Do đó,
          nếu có khả năng, người tham gia bảo hiểm nên xem xét đăng ký các tùy
          chọn bảo hiểm phù hợp với nhu cầu và khả năng tài chính của mình để
          đảm bảo mức độ bảo vệ tối đa.
        </p>
      </>
    );
  },
  deadline_for_payment: () => {
    return (
      <>
        <p className="mb-3">
          Thời hạn đóng phí của bảo hiểm nhân thọ là thời gian mà người tham gia
          bảo hiểm phải đóng phí để được bảo hiểm. Đây là một khoảng thời gian
          xác định trước trong hợp đồng bảo hiểm, thông thường từ 1 đến 20 năm
          hoặc thậm chí cả đời. Thời gian đóng phí cũng phụ thuộc vào loại hình
          bảo hiểm và quy định của pháp luật trong từng quốc gia hoặc khu vực.
        </p>
        <p className="mb-3">
          Trong suốt thời gian đóng phí, người tham gia bảo hiểm sẽ đóng một
          khoản phí nhất định hàng tháng hoặc hàng năm cho nhà cung cấp bảo
          hiểm. Số tiền này sẽ được tính dựa trên nhiều yếu tố, bao gồm độ tuổi,
          tình trạng sức khỏe, ngành nghề và mức độ rủi ro của khách hàng.
        </p>
        <p>
          Khi thời hạn đóng phí của bảo hiểm nhân thọ kết thúc, người tham gia
          bảo hiểm có thể tiếp tục đóng phí để giữ chế độ bảo hiểm hoặc có thể
          rút tiền hoặc chuyển sang chế độ đóng phí giảm dần. Nếu không đóng phí
          tiếp theo, chế độ bảo hiểm sẽ bị hủy và người tham gia sẽ không được
          bảo hiểm nữa.
        </p>
      </>
    );
  },
  insured_person: () => (
    <>
      <p className="mb-3">
        Đối tượng được bảo hiểm là những người được bảo vệ bởi hợp đồng bảo hiểm
        nhân thọ. Đối tượng này bao gồm những người đã mua một chính sách bảo
        hiểm nhân thọ và đóng phí bảo hiểm để đảm bảo cho bản thân hoặc cho gia
        đình mình trong tình huống không may xảy ra.
      </p>
      <p className="mb-3">
        Thường thì, đối tượng được bảo hiểm trong bảo hiểm nhân thọ là người có
        thu nhập, đặc biệt là những người có gia đình, con cái hoặc người phụ
        thuộc. Các khoản bồi thường trong trường hợp người được bảo hiểm mất sức
        lao động, mắc bệnh hoặc chết đều được thanh toán cho đối tượng này hoặc
        cho người được chỉ định của họ.
      </p>
      <p>
        Một số loại bảo hiểm nhân thọ còn bao gồm việc đầu tư để tích lũy tiền
        và tăng giá trị tài sản. Trong trường hợp này, đối tượng được bảo hiểm
        sẽ là những người đầu tư vào các chính sách bảo hiểm nhân thọ và hưởng
        lợi từ lợi nhuận đầu tư.
      </p>
    </>
  ),
  profession: () => (
    <>
      <p className="mb-3">
        Nghề nghiệp trong bảo hiểm nhân thọ là một trong những yếu tố quan trọng
        được xem xét khi mua một chính sách bảo hiểm. Nó ảnh hưởng trực tiếp đến
        phí bảo hiểm mà bạn phải trả hàng tháng hoặc hàng năm.
      </p>
      <p className="mb-3">
        Người làm nghề nghiệp có mức độ nguy hiểm cao hoặc có nguy cơ mắc các
        bệnh liên quan đến nghề nghiệp thường sẽ phải trả phí bảo hiểm cao hơn
        so với người làm nghề ít nguy hiểm hơn. Vì lý do này, việc xác định đúng
        nghề nghiệp của bạn rất quan trọng, nó giúp bạn định hình chi phí phù
        hợp cho chính sách bảo hiểm của mình.
      </p>
      <p className="mb-3">
        Ngoài ra, một số chính sách bảo hiểm nhân thọ còn có các điều kiện đặc
        biệt đối với những người làm một số nghề nghiệp nhất định. Ví dụ, những
        người làm nghề lái xe taxi hoặc nghề hàng không có thể phải trả phí bảo
        hiểm cao hơn và bị hạn chế về mức độ bảo hiểm được phép mua.
      </p>
      <p>
        Vì vậy, trước khi mua bảo hiểm nhân thọ, bạn nên xác định rõ nghề nghiệp
        của mình và tìm hiểu kỹ về các điều kiện và giới hạn trong chính sách
        bảo hiểm. Nếu cần, bạn cũng nên tham khảo ý kiến của chuyên gia bảo hiểm
        để có được lựa chọn tốt nhất cho tình huống của mình.
      </p>
    </>
  ),
  termination_conditions: () => (
    <>
      <p className="mb-3">
        Điều kiện kết thúc hợp đồng trong bảo hiểm nhân thọ là những điều kiện
        được quy định trước đó mà khi xảy ra, hợp đồng bảo hiểm sẽ chấm dứt.
        Điều kiện kết thúc hợp đồng thường bao gồm những trường hợp sau:
      </p>
      <p className="ml-1">
        1. Hết hạn hợp đồng: Điều kiện này áp dụng cho các hợp đồng bảo hiểm có
        thời hạn nhất định, khi hợp đồng đã đến ngày hết hạn thì sẽ kết thúc.
      </p>
      <p className="ml-1">
        2. Chấm dứt hợp đồng do người tham gia muốn rút tiền: Người tham gia có
        quyền rút tiền trước khi hợp đồng kết thúc, tuy nhiên sẽ bị mất một phần
        lợi nhuận và phí.
      </p>
      <p className="ml-1">
        3. Chấm dứt hợp đồng do người tham gia không đóng phí đúng hạn: Nếu
        người tham gia không đóng phí bảo hiểm đúng hạn, họ sẽ bị tạm ngừng
        quyền lợi và sau đó nếu không đóng phí bảo hiểm kịp thời thì hợp đồng sẽ
        bị chấm dứt.
      </p>
      <p className="ml-1 mb-3">
        4. Chấm dứt hợp đồng do người tham gia qua đời: Khi người tham gia chết,
        hợp đồng bảo hiểm cũng sẽ kết thúc và người được quyền lợi sẽ được hưởng
        tiền bảo hiểm.
      </p>
      <p>
        Các điều kiện kết thúc hợp đồng được quy định rõ ràng trong điều khoản
        hợp đồng bảo hiểm, vì vậy khi mua bảo hiểm, bạn nên đọc kỹ và hiểu rõ
        các điều khoản này để đảm bảo quyền lợi của mình được bảo vệ và tránh
        những rủi ro không mong muốn.
      </p>
    </>
  ),
  total_sum_insured: () => (
    <>
      <p className="mb-3">
        Tổng tiền được bảo hiểm là số tiền mà người tham gia bảo hiểm đóng phí
        hàng tháng hoặc định kỳ cho đến khi hợp đồng kết thúc hoặc đến khi người
        tham gia qua đời (tùy thuộc vào loại hợp đồng). Tổng tiền này sẽ được
        trả cho người được bảo hiểm hoặc người được chỉ định trong hợp đồng bảo
        hiểm trong trường hợp xảy ra sự kiện được bảo hiểm, chẳng hạn như bệnh
        tật, tai nạn hoặc tử vong.
      </p>
      <p>
        Việc tính toán tổng tiền được bảo hiểm sẽ phụ thuộc vào nhiều yếu tố
        như: tuổi của người được bảo hiểm, nghề nghiệp, mức độ rủi ro, số tiền
        đóng phí hàng tháng hoặc định kỳ. Tùy thuộc vào mức độ bảo vệ và các
        điều kiện của hợp đồng, tổng tiền được bảo hiểm có thể được điều chỉnh
        để phù hợp với nhu cầu và khả năng tài chính của người tham gia bảo
        hiểm.
      </p>
    </>
  ),
  monthly_fee: () => (
    <>
      <p className="mb-3">
        Số tiền phải bỏ ra hàng tháng là khoản phí bảo hiểm mà người tham gia
        bảo hiểm phải trả cho công ty bảo hiểm nhằm mục đích đảm bảo quyền lợi
        bảo vệ và hỗ trợ tài chính trong trường hợp xảy ra rủi ro về sức khỏe,
        thương tật hay tử vong.
      </p>
      <p className="mb-3">
        Số tiền phải bỏ ra hàng tháng phụ thuộc vào nhiều yếu tố như độ tuổi,
        tình trạng sức khỏe, nghề nghiệp, mức độ rủi ro, mức độ bảo vệ mong muốn
        và sản phẩm bảo hiểm được chọn. Việc tính toán số tiền này được thực
        hiện dựa trên bảng phí của công ty bảo hiểm, đồng thời còn phụ thuộc vào
        quy định của pháp luật và các quy định của từng sản phẩm bảo hiểm cụ
        thể.
      </p>
      <p>
        Để lựa chọn được sản phẩm bảo hiểm phù hợp và số tiền phải bỏ ra hàng
        tháng hợp lý, người tham gia bảo hiểm nên tìm hiểu và tham khảo ý kiến
        của chuyên gia tài chính, đồng thời nắm rõ các chi tiết, điều kiện và
        quyền lợi của sản phẩm bảo hiểm.
      </p>
    </>
  ),
  death_or_disability: () => (
    <>
      <p className="mb-3">
        Quyền lợi bảo vệ trước tử vong/thương tật là một trong những quyền lợi
        cơ bản và quan trọng nhất của người được bảo hiểm. Theo đó, khi người
        được bảo hiểm gặp phải tử vong hoặc bị thương tật do tai nạn hoặc bệnh
        tật, người được bảo hiểm sẽ được trả một khoản tiền bảo hiểm đã được
        thỏa thuận trước đó trong hợp đồng.
      </p>
      <p className="mb-3">
        Khoản tiền bảo hiểm này có thể được trả một lần duy nhất hoặc theo các
        khoản trả thường xuyên trong thời gian nhất định tùy thuộc vào thỏa
        thuận giữa người được bảo hiểm và công ty bảo hiểm. Trong trường hợp
        người được bảo hiểm gặp phải tử vong, khoản tiền bảo hiểm sẽ được trả
        cho người được chỉ định trong hợp đồng bảo hiểm (người thụ hưởng).
      </p>
      <p>
        Quyền lợi bảo vệ trước tử vong/thương tật là một trong những quyền lợi
        cơ bản trong bảo hiểm nhân thọ, giúp đảm bảo tài chính cho người được
        bảo hiểm và gia đình của họ trong trường hợp xấu nhất có thể xảy ra.
      </p>
    </>
  ),
  serious_illnesses: () => (
    <>
      <p className="mb-3">
        Quyền lợi bảo vệ trước bệnh ung thư và hiểm nghèo là một trong những
        quyền lợi quan trọng trong bảo hiểm nhân thọ. Đây là một dạng bảo vệ tài
        chính cho người tham gia bảo hiểm trong trường hợp phải chịu chi phí
        điều trị ung thư hoặc mắc phải các bệnh hiểm nghèo khác.
      </p>
      <p className="mb-3">
        Theo đó, khi người tham gia bảo hiểm bị ung thư hoặc các bệnh hiểm nghèo
        khác, họ sẽ được hưởng quyền lợi bảo vệ tài chính để trang trải chi phí
        điều trị và chăm sóc sức khỏe. Cụ thể, quyền lợi này bao gồm:
      </p>
      <ul className="mb-3">
        <li>
          Chi trả một khoản tiền theo hợp đồng bảo hiểm, được quy định trước đó,
          để hỗ trợ chi phí điều trị bệnh ung thư hoặc các bệnh hiểm nghèo khác.
        </li>
        <li>
          Được hưởng quyền lợi miễn phí, tạm ngừng đóng phí hoặc giảm phí trong
          một thời gian nhất định để đảm bảo người tham gia bảo hiểm không bị
          mất quyền lợi khi phải chịu chi phí điều trị bệnh.
        </li>
        <li>
          Được hỗ trợ tài chính cho việc chăm sóc sức khỏe và phục hồi sau khi
          điều trị bệnh ung thư hoặc các bệnh hiểm nghèo khác.
        </li>
      </ul>
      <p>
        Tuy nhiên, các điều kiện và quy định về quyền lợi này có thể khác nhau
        tùy từng sản phẩm bảo hiểm và từng nhà bảo hiểm cụ thể. Người tham gia
        bảo hiểm nên tìm hiểu kỹ các điều kiện và quy định của sản phẩm bảo hiểm
        trước khi đăng ký để có sự lựa chọn và quyết định đúng đắn.
      </p>
    </>
  ),
  health_care: () => (
    <>
      <p className="mb-3">
        Quyền lợi chăm sóc y tế là một trong những quyền lợi quan trọng được bảo
        vệ cho khách hàng. Theo đó, nếu khách hàng bị ốm đau hoặc cần điều trị y
        tế thì họ có quyền được bảo hiểm trả chi phí liên quan đến việc khám
        chữa bệnh hoặc phẫu thuật. Cụ thể, quyền lợi chăm sóc y tế trong bảo
        hiểm nhân thọ có thể bao gồm:
      </p>
      <ol className="mb-3">
        <li>
          Chi phí khám bệnh: Bảo hiểm nhân thọ có thể bảo vệ chi phí khám bệnh
          định kỳ, kiểm tra sức khỏe hoặc xét nghiệm y tế định kỳ của khách
          hàng.
        </li>
        <li>
          Chi phí điều trị: Nếu khách hàng bị ốm đau hoặc cần điều trị y tế thì
          bảo hiểm sẽ bảo vệ chi phí liên quan đến việc khám chữa bệnh hoặc phẫu
          thuật, bao gồm cả viện phí, thuốc, chi phí phẫu thuật và các dịch vụ y
          tế khác.
        </li>
        <li>
          Chi phí điều trị ung thư: Bảo hiểm nhân thọ có thể bảo vệ chi phí điều
          trị ung thư, bao gồm cả chi phí điều trị truyền máu và chi phí thuốc
          chống ung thư.
        </li>
        <li>
          Chi phí phục hồi sức khỏe: Nếu khách hàng phải nghỉ làm để điều trị
          hoặc phục hồi sức khỏe thì bảo hiểm có thể trả một phần hoặc toàn bộ
          lương của khách hàng trong thời gian đó.
        </li>
      </ol>
      <p>
        Với quyền lợi chăm sóc y tế này, khách hàng có thể yên tâm hơn về khả
        năng đối phó với các rủi ro liên quan đến sức khỏe, đồng thời giúp giảm
        bớt gánh nặng tài chính khi mắc bệnh và cần điều trị.
      </p>
    </>
  ),
  investment_benefit: () => (
    <>
      <p className="mb-3">
        Sản phẩm liên kết chung là một sản phẩm bảo hiểm nhân thọ kết hợp với
        sản phẩm đầu tư, cho phép người tham gia có thể đầu tư vào các quỹ đầu
        tư để tăng cường lợi nhuận đầu tư của mình.
      </p>
      <p className="mb-3">
        Quyền lợi đầu tư của sản phẩm liên kết chung trong bảo hiểm nhân thọ cho
        phép người tham gia được hưởng lợi nhuận từ các quỹ đầu tư mà họ đã chọn
        đầu tư vào. Số tiền lợi nhuận này được tính dựa trên tỷ lệ lợi nhuận mà
        quỹ đầu tư đó đạt được. Tuy nhiên, lợi nhuận đầu tư có thể không được
        đảm bảo và có thể có rủi ro về việc giảm giá trị đầu tư.
      </p>
      <p>
        Người tham gia cần phải tìm hiểu kỹ về sản phẩm liên kết chung và các
        quỹ đầu tư trước khi đầu tư, để đảm bảo rằng họ có đầy đủ thông tin và
        hiểu rõ rủi ro liên quan.
      </p>
    </>
  ),
  increasing_value_bonus: () => (
    <>
      <p className="mb-3">
        Các quyền lợi thưởng gia tăng giá trị Hợp đồng trong bảo hiểm nhân thọ
        là những quyền lợi bổ sung mà người được bảo hiểm có thể nhận được nếu
        hợp đồng bảo hiểm đạt được những mục tiêu cụ thể. Đây là một cách để
        động viên khách hàng giữ hợp đồng bảo hiểm và giúp tăng giá trị cho hợp
        đồng đó.
      </p>
      <p className="mb-3">
        Các quyền lợi thưởng này có thể được cung cấp dưới dạng tiền mặt hoặc
        tăng tỷ lệ bảo hiểm. Ví dụ, nếu người được bảo hiểm không có yêu cầu bồi
        thường trong một số năm, họ có thể được hưởng quyền lợi thưởng giảm phí
        hoặc quyền lợi tăng tỷ lệ bảo hiểm. Ngoài ra, nếu khách hàng giữ hợp
        đồng bảo hiểm trong một khoảng thời gian nhất định, họ có thể nhận được
        quyền lợi thưởng với giá trị tăng theo thời gian.
      </p>
      <p>
        Tuy nhiên, các quyền lợi thưởng này thường có các điều kiện cụ thể và có
        thể khác nhau giữa các công ty bảo hiểm khác nhau. Do đó, trước khi ký
        kết hợp đồng bảo hiểm, người được bảo hiểm cần phải tìm hiểu kỹ về các
        quyền lợi thưởng gia tăng giá trị hợp đồng của từng công ty bảo hiểm để
        có được quyết định đúng đắn và có lợi nhất cho mình.
      </p>
    </>
  ),
  for_child: () => (
    <>
      <p className="mb-3">
        Trong bảo hiểm nhân thọ, quyền lợi cho con được cung cấp để bảo vệ tương
        lai của con cái của chủ sở hữu hợp đồng bảo hiểm. Quyền lợi này có thể
        bao gồm một số chương trình như sau:
      </p>
      <ol className="mb-3">
        <li>
          1. Miễn phí phí bảo hiểm: Một số chương trình bảo hiểm nhân thọ cho
          phép chủ sở hữu hợp đồng được miễn phí các khoản phí bảo hiểm trong
          một khoảng thời gian nhất định nếu họ có con trong thời gian sử dụng
          hợp đồng.
        </li>
        <li>
          2. Bảo vệ ngay từ khi sinh: Chương trình này cung cấp bảo hiểm cho con
          ngay từ lúc sinh ra mà không cần kiểm tra y tế và thời gian chờ đợi.
        </li>
        <li>
          3. Bảo vệ cho con trong tương lai: Chương trình bảo vệ cho con trong
          tương lai giúp đảm bảo rằng con cái của chủ sở hữu hợp đồng sẽ được
          bảo vệ với một số khoản bảo hiểm khi chúng lớn lên.
        </li>
        <li>
          4. Sự tăng giá trị cho quyền lợi của hợp đồng: Một số chương trình bảo
          hiểm nhân thọ cho phép chủ sở hữu hợp đồng tăng giá trị quyền lợi của
          họ cho con cái bằng cách thêm tiền vào hợp đồng.
        </li>
      </ol>
      <p>
        Tất cả các chương trình này giúp đảm bảo rằng con cái của chủ sở hữu hợp
        đồng sẽ được bảo vệ và có một tương lai tài chính ổn định.
      </p>
    </>
  ),
  termination_benefits: () => (
    <>
      <p className="mb-3">
        Quyền lợi kết thúc hợp đồng trong bảo hiểm nhân thọ là một trong những
        thông tin quan trọng mà khách hàng cần phải tìm hiểu trước khi quyết
        định mua bảo hiểm. Theo đó, khi hợp đồng bảo hiểm kết thúc, khách hàng
        có quyền lựa chọn nhận toàn bộ giá trị tiền mà mình đã đóng cho hợp
        đồng, hoặc nhận một khoản tiền thích hợp tương đương với giá trị tiền đã
        đóng.
      </p>
      <p>
        Tuy nhiên, việc nhận được khoản tiền này còn phụ thuộc vào nhiều yếu tố
        như thời điểm kết thúc hợp đồng, thời gian thực hiện yêu cầu thanh toán,
        và các khoản phí được tính cho việc xử lý hợp đồng. Do đó, để tận dụng
        tối đa quyền lợi này, khách hàng cần đọc kỹ các điều khoản của hợp đồng
        và liên hệ với nhà cung cấp bảo hiểm để biết thêm thông tin chi tiết.
      </p>
    </>
  ),
  expiration_benefits: () => (
    <>
      <p>
        Quyền lợi đáo hạn là một trong những quyền lợi của chủ sở hữu hợp đồng
        bảo hiểm nhân thọ. Khi hợp đồng bảo hiểm đáo hạn, người được bảo hiểm sẽ
        nhận được số tiền đã được tích lũy trong quá trình đóng phí bảo hiểm
        trong suốt thời gian hợp đồng. Đây là khoản tiền không phải là quyền lợi
        bảo vệ của bảo hiểm nhân thọ mà là số tiền đã được tích lũy từ việc đóng
        phí. Khi hợp đồng bảo hiểm đáo hạn, chủ sở hữu hợp đồng có thể lựa chọn
        nhận số tiền này một lần duy nhất hoặc chia nhỏ ra thành nhiều lần để
        nhận trong tương lai.
      </p>
    </>
  ),
  flexible_and_diverse: () => (
    <>
      <p className="mb-3">
        Quyền lợi miễn đóng phí là một trong những quyền lợi được cung cấp trong
        bảo hiểm nhân thọ, cho phép người tham gia bảo hiểm được miễn các khoản
        phí đóng góp vào tài khoản bảo hiểm của mình trong một khoảng thời gian
        nhất định nếu như anh ta/ cô ta gặp phải một số tình huống xấu nhất
        định, ví dụ như mắc các bệnh ung thư nghiêm trọng hoặc bị thương tật
        nặng.
      </p>
      <p>
        Khi được áp dụng, quyền lợi này sẽ giúp cho người tham gia bảo hiểm
        không phải lo lắng về việc đóng phí trong khoảng thời gian đó, và vẫn
        được hưởng các quyền lợi bảo vệ khác từ sản phẩm bảo hiểm nhân thọ mà
        anh ta/ cô ta đã đăng ký. Tuy nhiên, để được miễn phí, anh ta/ cô ta cần
        phải đáp ứng các điều kiện quy định trong hợp đồng bảo hiểm nhân thọ.
      </p>
    </>
  ),
  acceptance_rate: () => (
    <>
      <p>
        Đây là tỷ lệ thành công khi khách hàng đăng ký mua bảo hiểm nhân thọ và
        ký hợp đồng với công ty bảo hiểm. Tỷ lệ này thường phụ thuộc vào nhiều
        yếu tố như sản phẩm bảo hiểm, quy trình đăng ký, điều kiện sức khỏe của
        khách hàng và chiến lược kinh doanh của công ty bảo hiểm.
      </p>
    </>
  ),
  completion_time_deal: () => (
    <>
      <p className="mb-3">
        Đây là thời gian mà khách hàng cần để hoàn tất quá trình đăng ký mua bảo
        hiểm nhân thọ, từ khi nộp hồ sơ đến khi ký hợp đồng. Thời gian này
        thường khác nhau tùy theo công ty bảo hiểm, loại sản phẩm bảo hiểm và
        quy trình đăng ký. Dưới đây là các mốc xếp hạng:
      </p>
      <ul>
        <li>Nhanh</li>
        <li>Trung bình</li>
        <li>Chậm</li>
      </ul>
    </>
  ),
  end_of_process: () => (
    <>
      <p className="mb-3">
        Đây là quy trình để chấm dứt hợp đồng bảo hiểm nhân thọ giữa khách hàng
        và công ty bảo hiểm. Quy trình này có thể bao gồm việc đăng ký yêu cầu
        chấm dứt, đánh giá giá trị hợp đồng và thực hiện việc thanh toán cho
        khách hàng nếu có khoản tiền bảo hiểm được trả về. Dưới đây là các mốc
        xếp hạng:
      </p>
      <ul>
        <li>Nhanh</li>
        <li>Trung bình</li>
        <li>Chậm</li>
      </ul>
    </>
  ),
  withdrawal_time: () => (
    <>
      <p className="mb-3">
        Đây là thời gian mà khách hàng có thể rút tiền từ sản phẩm bảo hiểm nhân
        thọ, sau khi hoàn tất các điều kiện và quy định của hợp đồng. Thời gian
        này cũng khác nhau tùy theo sản phẩm bảo hiểm và quy trình của công ty
        bảo hiểm. Dưới đây là các mốc xếp hạng:
      </p>
      <ul>
        <li>Nhanh</li>
        <li>Trung bình</li>
        <li>Chậm</li>
      </ul>
    </>
  ),
  reception_and_processing_time: () => (
    <>
      <p className="mb-3">
        Đây là thời gian mà công ty bảo hiểm cần để xử lý và tiếp nhận hồ sơ
        đăng ký mua bảo hiểm nhân thọ của khách hàng. Thời gian này phụ thuộc
        vào quy trình của công ty bảo hiểm và khối lượng công việc đang được xử
        lý. Dưới đây là các mốc xếp hạng:
      </p>
      <ul>
        <li>Nhanh</li>
        <li>Trung bình</li>
        <li>Chậm</li>
      </ul>
    </>
  ),
};

export const descriptionDetailInsuranceHealth = {
  customer_segment: () => {
    return (
      <p className="mb-3">
        Đối tượng khách hàng (hay còn được gọi là khách hàng mục tiêu) là nhóm
        người mà một doanh nghiệp hay tổ chức hướng đến và muốn tiếp cận để bán
        sản phẩm hoặc cung cấp dịch vụ.
      </p>
    );
  },
  room_type: () => {
    return (
      <div>
        <p className="mb-3">
          Loại phòng bệnh trong bảo hiểm là một dạng phòng bệnh cụ thể mà bảo
          hiểm y tế hoặc bảo hiểm sức khỏe cung cấp bảo hiểm cho. Phòng bệnh
          trong bảo hiểm thường áp dụng cho các trường hợp bệnh tại bệnh viện
          hoặc cơ sở y tế. Đây là dạng bảo hiểm tập trung vào việc chi trả các
          chi phí liên quan đến việc điều trị và nằm viện trong các phòng bệnh
          cụ thể.
        </p>
        <p>
          Việc chọn giữa phòng bệnh 1 giường,2 giường hay 3 giường thường phụ
          thuộc vào yêu cầu cụ thể của bệnh nhân và tài chính của khách hàng.
          Một số người có nhu cầu riêng tư và muốn tận hưởng sự thoải mái tối đa
          có thể chọn phòng bệnh 1 giường. Trong khi đó, những người khác có thể
          lựa chọn phòng bệnh 2 giường hoặc 3 giường để giảm chi phí hoặc khi
          không cần sự riêng tư cao.
        </p>
      </div>
    );
  },
  for_cancer: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Điều trị ung thư" trong bảo hiểm y tế là một dịch vụ bảo hiểm đặc biệt
          nhằm bảo vệ và hỗ trợ tài chính cho người được chẩn đoán mắc bệnh ung
          thư. Điều trị ung thư đòi hỏi các liệu pháp, quá trình chăm sóc và chi
          phí liên quan khá cao, do đó, việc có một định khoản bảo hiểm cho điều
          trị ung thư giúp giảm bớt gánh nặng tài chính cho người bệnh.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Điều trị ung thư" trong bảo hiểm y tế thường bao gồm các dịch
          vụ và trợ giúp sau:`}
        </p>
        <p className="mb-3">1. Chi phí điều trị</p>
        <p className="mb-3">2. Thuốc và liệu pháp</p>
        <p>3. Chăm sóc hậu quả</p>
      </div>
    );
  },
  for_illnesses: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Điều trị nội trú do bệnh tật" trong bảo hiểm y tế là một giá trị bảo
          hiểm được cung cấp để bảo vệ và chi trả cho các chi phí liên quan đến
          việc điều trị bệnh tật trong môi trường nội trú, tức là khi bệnh nhân
          được nhập viện và ở lại trong bệnh viện hoặc cơ sở y tế tương tự để
          nhận chăm sóc và điều trị.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Điều trị nội trú do bệnh tật" trong bảo hiểm y tế thường
          bao gồm:`}
        </p>
        <p className="mb-3">1. Phòng bệnh và dịch vụ y tế </p>
        <p className="mb-3">2. Chi phí điều trị</p>
        <p>3. Chăm sóc sau phẫu thuật</p>
      </div>
    );
  },
  for_accidents: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Điều trị nội trú do tai nạn" trong bảo hiểm y tế là một giá trị bảo
          hiểm được cung cấp để bảo vệ và chi trả cho các chi phí liên quan đến
          việc điều trị y tế trong môi trường nội trú khi bệnh nhân gặp tai nạn.
          Điều này áp dụng khi bệnh nhân phải nhập viện và ở lại trong bệnh viện
          hoặc cơ sở y tế tương tự để nhận chăm sóc và điều trị do tai nạn.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Điều trị nội trú do tai nạn" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Phòng bệnh và dịch vụ y tế</p>
        <p className="mb-3">2. Chi phí điều trị </p>
        <p>3. Chăm sóc hậu quả</p>
      </div>
    );
  },
  for_surgical: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Chi phí phẫu thuật" trong bảo hiểm là một giá trị bảo hiểm được cung
          cấp để bảo vệ và chi trả cho các chi phí liên quan đến quá trình phẫu
          thuật y tế. Điều này áp dụng khi bệnh nhân cần phải tiến hành phẫu
          thuật để điều trị một căn bệnh, chấn thương hoặc để thực hiện một thủ
          tục y tế cụ thể.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Chi phí phẫu thuật" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí phẫu thuật</p>
        <p className="mb-3">2. Phòng bệnh và dịch vụ y tế</p>
        <p>3. Chăm sóc hậu quả</p>
      </div>
    );
  },
  for_hospitalization: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Chi phí nằm viện" trong bảo hiểm y tế là một giá trị bảo hiểm được
          cung cấp để bảo vệ và chi trả cho các chi phí liên quan đến việc ở lại
          trong bệnh viện hoặc cơ sở y tế tương tự trong quá trình nằm viện để
          nhận chăm sóc và điều trị y tế.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Chi phí nằm viện" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Phòng bệnh và dịch vụ y tế</p>
        <p className="mb-3">2. Chi phí điều trị</p>
        <p>3. Chăm sóc hậu quả</p>
      </div>
    );
  },
  for_intensive_care: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Chi phí hồi sức tích cực" trong bảo hiểm y tế là một giá trị bảo hiểm
          được cung cấp để bảo vệ và chi trả cho các chi phí liên quan đến việc
          cung cấp chăm sóc hồi sức tích cực trong một môi trường y tế đặc biệt
          như đơn vị hồi sức tích cực (ICU) hoặc đơn vị chăm sóc đặc biệt (CCU).
          Điều này áp dụng khi bệnh nhân cần nhận được chăm sóc đặc biệt và theo
          dõi chặt chẽ do tình trạng y tế nghiêm trọng hoặc nguy hiểm đe dọa
          tính mạng.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Chi phí hồi sức tích cực" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chăm sóc hồi sức tích cực </p>
        <p className="mb-3">2. Dịch vụ y tế và y tá </p>
        <p>3. Dược phẩm và xét nghiệm</p>
      </div>
    );
  },
  for_administrative: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Chi phí hành chính, chi phí máu, huyết tương" trong bảo hiểm y tế là
          các chi phí liên quan đến quá trình chăm sóc y tế và điều trị, bao gồm
          các chi phí văn phòng, quản lý, xử lý máu và sử dụng huyết tương.`}
        </p>
        <p className="mb-3">
          Chi phí hành chính: Đây là các chi phí liên quan đến quản lý và vận
          hành các hoạt động hành chính trong quá trình chăm sóc y tế. Nó bao
          gồm các chi phí như phí tiếp nhận, xử lý hồ sơ, hồ sơ bệnh án, thanh
          toán, quản lý hợp đồng và các dịch vụ hành chính khác.
        </p>
        <p className="mb-3">
          Chi phí máu: Đây là các chi phí liên quan đến việc sử dụng máu và các
          sản phẩm máu trong quá trình điều trị y tế. Điều này bao gồm việc thu
          mua, lưu trữ, kiểm định và xử lý máu và các sản phẩm máu như hồng cầu,
          tiểu cầu, plasma và các thành phần máu khác.
        </p>
        <p>
          Chi phí huyết tương: Đây là các chi phí liên quan đến việc sử dụng
          huyết tương và các sản phẩm từ huyết tương trong quá trình chăm sóc y
          tế. Huyết tương là phần lỏng của máu sau khi loại bỏ các yếu tố đông
          máu. Các sản phẩm từ huyết tương được sử dụng để điều trị các bệnh lý,
          bổ sung yếu tố miễn dịch, cung cấp chất dinh dưỡng và tái tạo mô.
        </p>
      </div>
    );
  },
  for_organ_transplant: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Chi phí cấy ghép bộ phận" trong bảo hiểm y tế là một giá trị bảo hiểm
          được cung cấp để bảo vệ và chi trả cho các chi phí liên quan đến việc
          thực hiện quá trình cấy ghép bộ phận như cấy ghép tim, thận, gan,
          phổi, xương, mắt và các bộ phận khác.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Chi phí cấy ghép bộ phận" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí tiến hành cấy ghép </p>
        <p className="mb-3">2. Chi phí nghiên cứu và kiểm định </p>
        <p>3. Chi phí hậu quả và hỗ trợ </p>
      </div>
    );
  },
  examination_and_treatment: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Khám và điều trị tại phòng khám" trong bảo hiểm y tế đề cập đến quyền
          lợi và chi trả cho các chi phí liên quan đến việc khám và điều trị tại
          các phòng khám y tế, cơ sở y tế hoặc phòng mạch tư nhân. Đây là một
          hình thức chăm sóc y tế không yêu cầu nằm viện hoặc điều trị tại bệnh
          viện.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Khám và điều trị tại phòng khám" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí khám bệnh</p>
        <p className="mb-3">2. Chi phí xét nghiệm và x-ray</p>
        <p>3. Chi phí điều trị và kê đơn thuốc</p>
      </div>
    );
  },
  testing_and_diagnosis: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Xét nghiệm và chẩn đoán" trong bảo hiểm y tế đề cập đến quyền lợi và
          chi trả cho các chi phí liên quan đến việc thực hiện các xét nghiệm y
          tế và quá trình chẩn đoán để đánh giá và xác định tình trạng sức khỏe
          của một người.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Xét nghiệm và chẩn đoán" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí xét nghiệm</p>
        <p className="mb-3">2. Chi phí chẩn đoán</p>
        <p>3. Chi phí thăm khám chuyên gia</p>
      </div>
    );
  },
  home_care: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Chi phí y tá chăm sóc tại nhà sau khi xuất viện" trong bảo hiểm y tế
          đề cập đến quyền lợi và chi trả cho các chi phí liên quan đến việc
          cung cấp dịch vụ chăm sóc y tế tại nhà sau khi người được bảo hiểm
          xuất viện từ bệnh viện hoặc cơ sở y tế.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Chi phí y tá chăm sóc tại nhà sau khi xuất viện" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. hi phí y tá chăm sóc </p>
        <p className="mb-3">2. Chi phí y tế gia đình </p>
        <p className="mb-3">3. Chi phí hậu quả và hỗ trợ </p>
        <p>
          {`Quyền lợi "Chi phí y tá chăm sóc tại nhà sau khi xuất viện" trong bảo
          hiểm y tế giúp đảm bảo rằng người được bảo hiểm có được sự chăm sóc y
          tế và hỗ trợ sau khi xuất viện từ bệnh viện. Tuy nhiên, phạm vi và mức
          độ bảo hiểm cho quyền lợi này sẽ khác nhau tùy thuộc vào từng hợp đồng
          bảo hiểm.`}
        </p>
      </div>
    );
  },
  due_to_accident: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Điều trị ngoại trú do tai nạn" trong bảo hiểm y tế đề cập đến quyền
          lợi và chi trả cho các chi phí liên quan đến việc điều trị y tế và
          chăm sóc ngoại trú sau khi xảy ra tai nạn.`}
        </p>
        <p className="mb-3">
          {`Điều trị ngoại trú do tai nạn" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí khám và điều trị</p>
        <p className="mb-3">2. Chi phí xét nghiệm và chẩn đoán</p>
        <p className="mb-3">3. Chi phí vật tư y tế</p>
        <p>
          {`Phạm vi và mức độ bảo hiểm cho "Điều trị ngoại trú do tai nạn" sẽ khác
          nhau tùy thuộc vào từng hợp đồng bảo hiểm cụ thể. Việc kiểm tra kỹ các
          điều khoản và điều kiện của hợp đồng sẽ giúp hiểu rõ hơn về quyền lợi
          và giới hạn bảo hiểm được cung cấp.`}
        </p>
      </div>
    );
  },
  due_to_illness: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Điều trị ngoại trú do bệnh tật" trong bảo hiểm y tế đề cập đến quyền
          lợi và chi trả cho các chi phí liên quan đến việc điều trị y tế và
          chăm sóc ngoại trú khi người được bảo hiểm không cần phải nhập viện do
          bệnh tật.`}
        </p>
        <p className="mb-3">
          {`Điều trị ngoại trú do bệnh tật" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí khám và điều trị </p>
        <p className="mb-3">2. Chi phí xét nghiệm và chẩn đoán </p>
        <p>3. Chi phí vật tư y tế </p>
      </div>
    );
  },
  due_to_cancer: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Điều trị ung thư" trong bảo hiểm y tế là một dịch vụ bảo hiểm đặc
          biệt nhằm bảo vệ và hỗ trợ tài chính cho người được chẩn đoán mắc bệnh
          ung thư. Điều trị ung thư đòi hỏi các liệu pháp, quá trình chăm sóc và
          chi phí liên quan khá cao, do đó, việc có một định khoản bảo hiểm cho
          điều trị ung thư giúp giảm bớt gánh nặng tài chính cho người bệnh.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Điều trị ung thư" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí điều trị. </p>
        <p className="mb-3">2. Thuốc và liệu pháp </p>
        <p>3. Chăm sóc hậu quả </p>
      </div>
    );
  },
  restore_functionality: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Điều trị phục hồi chức năng" trong bảo hiểm y tế đề cập đến quyền lợi
          và chi trả cho các chi phí liên quan đến việc khôi phục và cải thiện
          chức năng của cơ thể sau khi xảy ra bệnh hoặc tai nạn.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Điều trị phục hồi chức năng" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Trị liệu vật lý và nghề nghiệp </p>
        <p className="mb-3">2. Trị liệu ngôn ngữ và thần kinh </p>
        <p>3. Hỗ trợ hồi phục chức năng </p>
      </div>
    );
  },
  examination_and_diagnosis: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Khám và chẩn đoán" trong bảo hiểm y tế đề cập đến quyền lợi và chi
          trả cho các chi phí liên quan đến việc thăm khám bác sĩ và quá trình
          chẩn đoán tình trạng sức khỏe.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Khám và chẩn đoán" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí khám bệnh </p>
        <p className="mb-3">2. Chi phí xét nghiệm và chẩn đoán </p>
        <p>3. Chi phí điều trị ban đầu</p>
      </div>
    );
  },
  gingivitis: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Viêm lợi (nướu) / viêm nha chu" trong bảo hiểm y tế là một điều khoản
          hoặc quyền lợi bảo hiểm liên quan đến việc chi trả chi phí điều trị và
          chăm sóc y tế cho các vấn đề liên quan đến viêm lợi hoặc viêm nha chu.`}
        </p>
        <p className="mb-3">
          Viêm lợi (nướu) và viêm nha chu là các vấn đề thông thường liên quan
          đến sự viêm và mất sức khỏe của nướu và mô xung quanh răng. Các triệu
          chứng thông thường của viêm lợi và viêm nha chu bao gồm viêm nướu,
          chảy máu nướu, sưng, đau và rụng răng. Nguyên nhân của viêm lợi và
          viêm nha chu có thể bao gồm cách vệ sinh răng miệng không đúng cách,
          chấn thương, gen di truyền, bệnh lý nướu, hút thuốc lá và các yếu tố
          khác.
        </p>
        <p className="mb-3">
          {`Quyền lợi "Viêm lợi (nướu) / viêm nha chu" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí điều trị </p>
        <p>2. Chi phí chẩn đoán </p>
      </div>
    );
  },
  xray_and_diagnostic_imaging: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Chụp X-quang và cận lâm sàng" trong bảo hiểm y tế đề cập đến quyền
          lợi và chi trả cho các chi phí liên quan đến việc thực hiện các xét
          nghiệm hình ảnh như chụp X-quang và các xét nghiệm cận lâm sàng.`}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Chụp X-quang và cận lâm sàng" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí chụp X-quang </p>
        <p className="mb-3">2. Chi phí xét nghiệm cận lâm sàng </p>
        <p>
          Các xét nghiệm cận lâm sàng thông thường bao gồm siêu âm, MRI (cộng
          hưởng từ hạt nhân), CT scan (scan máy tính), PET scan (scan chẩn đoán
          phát xạ), xét nghiệm tim mạch, xét nghiệm nhiễm trùng và nhiều xét
          nghiệm khác để đánh giá chức năng của cơ thể và xác định các bệnh lý.{' '}
        </p>
      </div>
    );
  },
  filling_teeth_basic: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Trám răng bằng chất liệu thông thường" trong bảo hiểm y tế đề cập đến
          quyền lợi và chi trả cho chi phí điều trị trám răng bằng các chất liệu
          truyền thống.`}
        </p>
        <p className="mb-3">
          Khi có nhu cầu trám răng do sự hư hỏng, sứt mẻ hoặc mất mảnh răng,
          việc sử dụng chất liệu trám răng thông thường là một phương pháp điều
          trị phổ biến. Các chất liệu thông thường được sử dụng để trám răng bao
          gồm amalgam (hợp kim chì), composite (nhựa tổng hợp) và gốc thủy tinh
          ionomer (GI). Mỗi chất liệu có ưu điểm và hạn chế riêng.
        </p>
        <p className="mb-3">
          {`Quyền lợi "Trám răng bằng chất liệu thông thường" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí trám răng </p>
        <p>2. Chi phí khám và chẩn đoán </p>
      </div>
    );
  },
  root_canal_treatment: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Điều trị tủy" trong bảo hiểm y tế đề cập đến quyền lợi và chi trả cho
          chi phí điều trị tủy răng trong quá trình chăm sóc và điều trị nha
          khoa.`}
        </p>
        <p className="mb-3">
          Điều trị tủy răng là một quy trình nha khoa nhằm điều trị các vấn đề
          tủy răng, bao gồm viêm tủy, viêm nướu, nhiễm trùng và các vấn đề khác
          liên quan đến tủy răng. Quá trình này thường bao gồm loại bỏ tủy răng
          bị tổn thương hoặc nhiễm trùng, làm sạch và khử trùng khoang tủy, và
          điền bằng chất liệu phù hợp để tái xây dựng răng.
        </p>
        <p className="mb-3">
          {`Quyền lợi "Điều trị tủy" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí điều trị tủy </p>
        <p>2. Chi phí tủy răng nhân tạo </p>
      </div>
    );
  },
  dental_pathology: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Nhổ răng bệnh lý (bao gồm tiểu phẫu), phẫu thuật cắt chóp răng, lấy u
          vôi răng" trong bảo hiểm y tế đề cập đến quyền lợi và chi trả cho các
          quy trình nha khoa liên quan đến việc loại bỏ răng bệnh lý, tiến hành
          phẫu thuật cắt chóp răng và lấy u vôi răng.`}
        </p>
        <p className="mb-3">1. Nhổ răng bệnh lý </p>
        <p className="mb-3">2. Phẫu thuật cắt chóp răng </p>
        <p>3. Lấy u vôi răng </p>
      </div>
    );
  },
  dental_calculus: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Lấy cao răng" hoặc "Cạo vôi răng" trong bảo hiểm y tế đề cập đến
          quyền lợi và chi trả cho quá trình loại bỏ chất cặn bám (cao răng) và
          vôi trên bề mặt răng để duy trì sức khỏe răng miệng.`}
        </p>
        <p className="mb-3">
          Lấy cao răng hoặc cạo vôi răng là một quy trình nha khoa nhằm loại bỏ
          chất cặn bám, vôi và các mảng màu trên bề mặt răng. Chất cặn bám và
          vôi có thể gây ra vi khuẩn, viêm nhiễm, hình thành sâu răng và gây ra
          các vấn đề sức khỏe răng miệng khác. Quá trình này thường được thực
          hiện bằng cách sử dụng các công cụ chuyên dụng để làm sạch và cạo vôi
          trên bề mặt răng.
        </p>
        <p className="mb-3">
          {`Quyền lợi "Lấy cao răng" hoặc "Cạo vôi răng" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí lấy cao răng</p>
        <p>2. Chi phí khám và chẩn đoán</p>
      </div>
    );
  },
  give_birth_normally: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Sinh thường" trong bảo hiểm đề cập đến quyền lợi và chi trả cho quá
          trình sinh con tự nhiên của phụ nữ.`}
        </p>
        <p className="mb-3">
          Sinh thường (hay còn được gọi là sinh tự nhiên, sinh đẻ) là quá trình
          mà phụ nữ mang thai sẽ trải qua để sinh con một cách tự nhiên, không
          thông qua các phương pháp phẫu thuật hay can thiệp y tế. Quá trình
          sinh thường bao gồm các giai đoạn như mở tử cung, chuyển dạ, đẩy và
          sinh con.
        </p>
        <p className="mb-3">
          {`Quyền lợi "Sinh thường" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí chăm sóc sinh thường </p>
        <p>2. Chi phí hậu quả sinh thường </p>
      </div>
    );
  },
  caesarean_section: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Sinh mổ" trong bảo hiểm đề cập đến quyền lợi và chi trả cho quá trình
          sinh con thông qua phương pháp phẫu thuật mổ (hay còn gọi là phẫu
          thuật mổ đẻ hoặc phẫu thuật mổ sinh).`}
        </p>
        <p className="mb-3">
          Sinh mổ là một quá trình sinh con mà phụ nữ mang thai sẽ trải qua bằng
          cách thông qua một ca phẫu thuật mổ. Quá trình này thường được thực
          hiện khi có những lý do y tế đặc biệt, như trường hợp thai nhi không
          thể sinh tự nhiên hoặc có nguy cơ cao cho sức khỏe của mẹ và thai nhi.{' '}
        </p>
        <p className="mb-3">
          {`Quyền lợi "Sinh mổ" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí phẫu thuật sinh mổ </p>
        <p>2. Chi phí chăm sóc sau sinh mổ </p>
      </div>
    );
  },
  obstetric_complication: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Tai biến sản khoa" trong bảo hiểm đề cập đến quyền lợi và chi trả cho
          các sự cố y tế và biến chứng xảy ra trong quá trình mang thai, sinh
          con và sau sinh.`}
        </p>
        <p className="mb-3">
          Tai biến sản khoa bao gồm các vấn đề y tế và biến chứng không mong
          muốn mà phụ nữ có thể gặp phải trong quá trình mang thai, sinh con
          hoặc sau sinh. Đây có thể là các vấn đề nghiêm trọng như nhiễm trùng,
          xuất huyết, suy tim sau sinh, rối loạn đông máu, tổn thương cơ tử
          cung, nhiễm trùng vết mổ, và các vấn đề khác liên quan đến sức khỏe
          sản khoa.
        </p>
        <p className="mb-3">
          {`Quyền lợi "Tai biến sản khoa" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí điều trị </p>
        <p className="mb-3">2. Chi phí nằm viện </p>
        <p>3. Chi phí chăm sóc sau sinh </p>
      </div>
    );
  },
  give_birth_abnormality: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Bất thường trong quá trình mang thai và các bệnh lý phát sinh nguyên
          nhân do thai kỳ" trong bảo hiểm đề cập đến quyền lợi và chi trả cho
          các vấn đề y tế và bệnh lý phát sinh trong quá trình mang thai có
          nguyên nhân từ thai kỳ.`}
        </p>
        <p className="mb-3">
          Trong quá trình mang thai, có thể xảy ra các vấn đề y tế và bệnh lý
          phát sinh không mong muốn hoặc bất thường, có nguyên nhân từ thai kỳ.
          Đây có thể là các tình trạng như sảy thai, thai ngoài tử cung, thiếu
          máu thai nhi, thai non, thai đa, dị tật bẩm sinh, suy thai, tử vong
          thai nhi, và các vấn đề khác liên quan đến thai kỳ.
        </p>
        <p className="mb-3">
          {`Quyền lợi "Bất thường trong quá trình mang thai và các bệnh lý phát sinh nguyên nhân do thai kỳ" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí điều trị </p>
        <p className="mb-3">2. Chi phí nằm viện </p>
        <p>3. Chi phí chăm sóc sau sinh</p>
      </div>
    );
  },
  after_give_birth_fee: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Chi phí khám trước khi sinh" trong bảo hiểm đề cập đến quyền lợi và
          chi trả cho việc khám bệnh và chẩn đoán trước khi sinh.`}
        </p>
        <p className="mb-3">
          Khi phụ nữ mang thai, việc khám bệnh và chẩn đoán trước khi sinh là
          rất quan trọng để đảm bảo sức khỏe của mẹ và thai nhi. Trong quá trình
          này, các xét nghiệm, kiểm tra sức khỏe, và chẩn đoán sẽ được thực hiện
          để đánh giá tình trạng của mẹ và thai nhi, phát hiện các vấn đề y tế
          tiềm ẩn, và lập kế hoạch chăm sóc và điều trị phù hợp.
        </p>
        <p className="mb-3">
          {`Quyền lợi "Chi phí khám trước khi sinh" trong bảo hiểm y tế thường bao
          gồm:`}
        </p>
        <p className="mb-3">1. Chi phí khám bệnh </p>
        <p>2. Chi phí chẩn đoán</p>
      </div>
    );
  },
  before_discharged: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Chi phí điều trị ngay sau khi xuất viện và/hoặc 1 lần tái khám" trong
          bảo hiểm đề cập đến quyền lợi và chi trả cho việc điều trị ngay sau
          khi xuất viện và/hoặc một lần tái khám sau khi xuất viện.`}
        </p>
        <p>
          Sau khi xuất viện từ bệnh viện, có thể xảy ra trường hợp cần tiếp tục
          điều trị và chăm sóc sức khỏe ngay sau khi rời viện. Điều này bao gồm
          việc tái khám và tiếp tục điều trị theo chỉ định của bác sĩ để đảm bảo
          sự hồi phục và phục hồi sau khi xuất viện. Chi phí điều trị ngay sau
          khi xuất viện và/hoặc một lần tái khám được bảo hiểm chi trả để hỗ trợ
          các chi phí liên quan đến việc này.
        </p>
      </div>
    );
  },
  postpartum_childcare_cost: () => {
    return (
      <div>
        <p className="mb-3">
          {`"Chi phí chăm sóc trẻ sau sinh/năm" trong bảo hiểm đề cập đến quyền
          lợi và chi trả cho các chi phí liên quan đến chăm sóc sức khỏe và y tế
          của trẻ sau khi sinh trong một năm.`}
        </p>
        <p>
          Sau khi trẻ được sinh ra, việc chăm sóc và bảo vệ sức khỏe của trẻ rất
          quan trọng. Chi phí chăm sóc trẻ sau sinh/năm trong bảo hiểm bao gồm
          các khoản chi trả cho việc khám bệnh, chẩn đoán, tiêm phòng, các xét
          nghiệm và kiểm tra y tế định kỳ, điều trị các bệnh lý phát sinh trong
          giai đoạn sau sinh, và các dịch vụ y tế khác cần thiết để đảm bảo sức
          khỏe và phát triển của trẻ.
        </p>
      </div>
    );
  },
};

export const categoryInsurance = (t) => {
  return [
    {
      id: 1,
      name: 'Rủi ro & đề phòng',
      insurances: [
        {
          id: 1,
          logo: '/svg/heart.svg',
          name: 'lifeInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">56%</span>
            </p>
          ),
          link: '/lifeInsurance',
        },
        {
          id: 2,
          logo: '/svg/car.svg',
          name: 'carInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '/carInsurance',
        },
        {
          id: 3,
          logo: '/svg/airplane.svg',
          name: 'travelInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">34%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 4,
          logo: '/svg/pet.svg',
          name: 'petInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">40%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 5,
          logo: '/svg/home.svg',
          name: 'homeAndPersonalPropertyInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 6,
          logo: '/svg/health.svg',
          name: 'healthInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">16%</span>
            </p>
          ),
          link: '/healthInsurance',
        },
        {
          id: 7,
          logo: '/svg/nurse.svg',
          name: 'medicalInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">90%</span>
            </p>
          ),
          link: '',
        },
      ],
    },
    {
      id: 2,
      name: 'Trách nhiệm & pháp luật',
      insurances: [
        {
          id: 1,
          logo: '/svg/heart.svg',
          name: 'lifeInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">56%</span>
            </p>
          ),
          link: '/lifeInsurance',
        },
        {
          id: 2,
          logo: '/svg/car.svg',
          name: 'carInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 3,
          logo: '/svg/airplane.svg',
          name: 'travelInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">34%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 4,
          logo: '/svg/pet.svg',
          name: 'petInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">40%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 5,
          logo: '/svg/home.svg',
          name: 'homeAndPersonalPropertyInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 6,
          logo: '/svg/health.svg',
          name: 'healthInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">16%</span>
            </p>
          ),
          link: '/healthInsurance',
        },
      ],
    },
    {
      id: 3,
      name: 'Du lịch',
      insurances: [
        {
          id: 1,
          logo: '/svg/heart.svg',
          name: 'lifeInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">56%</span>
            </p>
          ),
          link: '/lifeInsurance',
        },
        {
          id: 2,
          logo: '/svg/car.svg',
          name: 'carInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '/carInsurance',
        },
        {
          id: 3,
          logo: '/svg/airplane.svg',
          name: 'travelInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">34%</span>
            </p>
          ),
          link: '',
        },
      ],
    },
    {
      id: 4,
      name: 'Nhà cửa & tài sản',
      insurances: [
        {
          id: 1,
          logo: '/svg/heart.svg',
          name: 'lifeInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">56%</span>
            </p>
          ),
          link: '/lifeInsurance',
        },
        {
          id: 2,
          logo: '/svg/car.svg',
          name: 'carInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '/carInsurance',
        },
        {
          id: 3,
          logo: '/svg/airplane.svg',
          name: 'travelInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">34%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 4,
          logo: '/svg/pet.svg',
          name: 'petInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">40%</span>
            </p>
          ),
          link: '',
        },
      ],
    },
    {
      id: 5,
      name: 'Thú cưng',
      insurances: [
        {
          id: 1,
          logo: '/svg/heart.svg',
          name: 'lifeInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">56%</span>
            </p>
          ),
          link: '/lifeInsurance',
        },
        {
          id: 2,
          logo: '/svg/car.svg',
          name: 'carInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '/carInsurance',
        },
        {
          id: 3,
          logo: '/svg/airplane.svg',
          name: 'travelInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">34%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 4,
          logo: '/svg/pet.svg',
          name: 'petInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">40%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 5,
          logo: '/svg/home.svg',
          name: 'homeAndPersonalPropertyInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 6,
          logo: '/svg/health.svg',
          name: 'healthInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">16%</span>
            </p>
          ),
          link: '/healthInsurance',
        },
        {
          id: 7,
          logo: '/svg/nurse.svg',
          name: 'medicalInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">90%</span>
            </p>
          ),
          link: '',
        },
      ],
    },
    {
      id: 6,
      name: 'Y tế',
      insurances: [
        {
          id: 1,
          logo: '/svg/heart.svg',
          name: 'lifeInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">56%</span>
            </p>
          ),
          link: '/lifeInsurance',
        },
        {
          id: 2,
          logo: '/svg/car.svg',
          name: 'carInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '/carInsurance',
        },
        {
          id: 3,
          logo: '/svg/airplane.svg',
          name: 'travelInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">34%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 4,
          logo: '/svg/pet.svg',
          name: 'petInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">40%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 5,
          logo: '/svg/home.svg',
          name: 'homeAndPersonalPropertyInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 6,
          logo: '/svg/health.svg',
          name: 'healthInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">16%</span>
            </p>
          ),
          link: '/healthInsurance',
        },
      ],
    },
    {
      id: 7,
      name: 'Xe cộ & phương tiện',
      insurances: [
        {
          id: 1,
          logo: '/svg/heart.svg',
          name: 'lifeInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">56%</span>
            </p>
          ),
          link: '/lifeInsurance',
        },
        {
          id: 2,
          logo: '/svg/car.svg',
          name: 'carInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '/carInsurance',
        },
      ],
    },
    {
      id: 8,
      name: 'Sức khỏe',
      insurances: [
        {
          id: 1,
          logo: '/svg/heart.svg',
          name: 'lifeInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">56%</span>
            </p>
          ),
          link: '/lifeInsurance',
        },
        {
          id: 2,
          logo: '/svg/car.svg',
          name: 'carInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '/carInsurance',
        },
        {
          id: 3,
          logo: '/svg/airplane.svg',
          name: 'travelInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">34%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 4,
          logo: '/svg/pet.svg',
          name: 'petInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">40%</span>
            </p>
          ),
          link: '',
        },
      ],
    },
    {
      id: 9,
      name: 'Lương & hưu trí',
      insurances: [
        {
          id: 1,
          logo: '/svg/heart.svg',
          name: 'lifeInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">56%</span>
            </p>
          ),
          link: '/lifeInsurance',
        },
        {
          id: 2,
          logo: '/svg/car.svg',
          name: 'carInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '/carInsurance',
        },
        {
          id: 3,
          logo: '/svg/airplane.svg',
          name: 'travelInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">34%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 4,
          logo: '/svg/pet.svg',
          name: 'petInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">40%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 5,
          logo: '/svg/home.svg',
          name: 'homeAndPersonalPropertyInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">80%</span>
            </p>
          ),
          link: '',
        },
        {
          id: 6,
          logo: '/svg/health.svg',
          name: 'healthInsurance',
          save: (
            <p className="text-xs font-normal">
              {t('saveUpTo')}{' '}
              <span className="text-xs font-normal text-azure">16%</span>
            </p>
          ),
          link: '/healthInsurance',
        },
      ],
    },
  ];
};

export const BLUE_DARK = '#2B59FF';
export const INK_LIGHT = '#404249';
export const INK_LIGHT_TEST = '#8F929D';
export const DARK_BLACK = '#000';
export const SILVER = '#ABADB5';
export const STEP = 12;

export const CalendarIcon = () => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M8 2V5"
        stroke="#6C6F7B"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M16 2V5"
        stroke="#6C6F7B"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M3.5 9.09009H20.5"
        stroke="#6C6F7B"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M21 8.5V17C21 20 19.5 22 16 22H8C4.5 22 3 20 3 17V8.5C3 5.5 4.5 3.5 8 3.5H16C19.5 3.5 21 5.5 21 8.5Z"
        stroke="#6C6F7B"
        strokeWidth="1.5"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.6947 13.7H15.7037"
        stroke="#6C6F7B"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.6947 16.7H15.7037"
        stroke="#6C6F7B"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.9955 13.7H12.0045"
        stroke="#6C6F7B"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.9955 16.7H12.0045"
        stroke="#6C6F7B"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8.29431 13.7H8.30329"
        stroke="#6C6F7B"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M8.29431 16.7H8.30329"
        stroke="#6C6F7B"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};

export const LIST_INSURED_PERSON = (t) => [
  { value: 'one-self', label: t('one-self') },
  { value: 'other-person', label: t('other-person') },
];

export const LIST_ROOM_BED = (t) => [
  { value: 'one-bed', label: t('one-bed') },
  { value: 'two-beds', label: t('two-beds') },
  { value: 'multi-beds', label: t('multi-beds') },
  { value: 'vip-room', label: t('vip-room') },
  { value: 'special-room', label: t('special-room') },
];

export const LIST_SELECT_TITLES = (t) => [
  { id: 1, value: t('over_view') },
  { id: 2, value: t('boarding') },
  { id: 3, value: t('outpatient') },
  { id: 4, value: t('other_benefits') },
  { id: 5, value: t('linked_list') },
];

export const CATEGORY = (t) => [
  { value: '', name: t('common:type_insurance') },
  { value: 'life', name: t('common:life_insurance') },
  { value: 'health', name: t('common:health_insurance') },
];

export const FILTER_RATINGS = (t) => [
  { value: '', name: t('show_all') },
  { value: 'DESC', name: t('DESC') },
  { value: 'ASC', name: t('ASC') },
];

export const LIFE = 'life';
export const HEALTH = 'health';
export const BUSINESS = 'business';
export const CURRENT_PASSWORD = 'current_password';
export const NEW_PASSWORD = 'new_password';
export const CONFIRM_NEW_PASSWORD = 'confirm_new_password';

export const FROM_TO_AGE = 'from_to_age';
export const FROM_TO_YEAR = 'from_to_year';
export const INSURED_PERSON = 'insured_person';
export const FORMAT_TIME_FEE_DEADLINE = 'format_time_fee_deadline';
export const FORMAT_MONEY = 'format_money';
export const PROFESSION = 'profession';
export const FROM_TO_MONEY = 'from_to_money';
export const NO_SUPPORT = 'no_support';
export const NORMAL = 'normal';
export const MONEY = 'money';
export const YEAR = 'year';
export const AGE = 'age';

export const LIST_HEADERS = (t) => [
  {
    id: 'H1',
    href: '/category',
    label: t('auth:insurance'),
    className:
      'flex items-center gap-2 p-4 text-base font-normal text-cultured hover:bg-azure',
    classNameHover:
      'z-50 hidden w-full gap-10 rounded-b-lg border-x-2 border-b-2 border-ultramarine-blue bg-white p-10 group-hover:flex',
    arrow_down: <IconArrowDown />,
    contentHover: [
      {
        idTitle: 'H11',
        titleKey: t('auth:risks_and_precautions'),
        children: [
          {
            idChild: 'H111',
            href: '/lifeInsurance',
            label: t('auth:insuranceLife'),
          },
          {
            idChild: 'H112',
            href: '/lifeInsurance',
            label: t('auth:maternity_insurance'),
          },
        ],
      },
      {
        idTitle: 'H12',
        titleKey: t('auth:health_and_family'),
        children: [
          {
            idChild: 'H121',
            href: '/healthInsurance',
            label: t('auth:health_insurance'),
          },
          {
            idChild: 'H122',
            href: '/lifeInsurance',
            label: t('auth:family_insurance'),
          },
        ],
      },
      {
        idTitle: 'H13',
        titleKey: t('auth:cars_and_motorbikes'),
        children: [
          {
            idChild: 'H131',
            href: '/carInsurance',
            label: t('auth:cars_insurance'),
          },
          {
            idChild: 'H132',
            href: '/lifeInsurance',
            label: t('auth:motorbikes_insurance'),
          },
        ],
      },
      {
        idTitle: 'H14',
        titleKey: t('auth:tourism'),
        children: [
          {
            idChild: 'H141',
            href: '/lifeInsurance',
            label: t('auth:flight_insurance'),
          },
        ],
      },
    ],
  },
  {
    id: 'H2',
    href: '/blogs',
    label: t('auth:blog'),
    className:
      'flex items-center gap-2 p-4 text-base font-normal text-cultured hover:bg-azure',
  },
];

export const APPROVED = 'approved';
export const PENDING = 'pending';
export const REJECTED = 'rejected';
export const PAID = 'paid';

export const appraisalInformation = (insurance) => {
  return [
    {
      id: 1,
      title:
        insurance?.form_type === 'unexpired' ? 'expiryDateOfOldContract' : '',
      value:
        insurance?.form_type === 'unexpired'
          ? dayjs(insurance?.certificate_expiration_date).format('DD/MM/YYYY')
          : '',
    },
    {
      id: 2,
      title: 'appraisalPictures',
      image:
        insurance?.form_type === 'unexpired'
          ? insurance?.certificate_image_url
          : insurance?.vehicle_condition_image_urls,
    },
  ];
};
export const checkStatusInsurance = (t, status, insurance) => {
  switch (status) {
    case APPROVED:
      return numeral(insurance?.car_cost).format('(0,0)') + 'vnđ';
    case PENDING:
      return (
        <span className="text-royal-orange">{t('awaitingAppraisal')}</span>
      );
    case REJECTED:
      return <span className="text-spanish-gray">{t('unableToAppraise')}</span>;
  }
};
export const purposeUsingCar = (t, purpose) => {
  switch (purpose) {
    case 'family-car':
      return t('familyCar');
    case 'contracted-service':
      return t('serviceContractVehicle');
    case 'technology-business':
      return t('technologyBusinessCar');
  }
};

export const carInformation = (t, insurance) => {
  return [
    {
      id: 1,
      title: 'purposeOfUsing',
      value: purposeUsingCar(t, insurance?.purpose),
    },
    {
      id: 2,
      title: 'licensePlates',
      value: insurance?.car_license_plate,
    },
    {
      id: 3,
      title: 'brand',
      value: insurance?.car_brand,
    },
    {
      id: 4,
      title: 'model',
      value: insurance?.car_model,
    },
    {
      id: 5,
      title: 'version',
      value: insurance?.car_version,
    },
    {
      id: 6,
      title: 'yearOfManufacture',
      value: insurance?.year_of_production,
    },
    {
      id: 7,
      title: 'numberOfSeats',
      value: insurance?.seating_capacity,
    },
    {
      id: 8,
      title: 'vehicleValue',
      value: checkStatusInsurance(t, insurance?.form_status, insurance),
    },
  ];
};

export const listCheckbox = (insurance) => {
  return [
    {
      benefitId: 1,
      title: 'Quyền lợi cơ bản (Mặc định)',
      benefitDetails: [
        {
          id: 1,
          title: 'deduct1000000',
          value: insurance?.is_deduct_one_million_vnd,
        },
        {
          id: 2,
          title: 'totalVehicleLoss',
          value: insurance?.is_complete_vehicle_damage,
        },
        {
          id: 3,
          title: 'totalVehicleLoss',
          value: insurance?.is_component_vehicle_damage,
        },
        {
          id: 4,
          title: 'theWholeCarWasStolen',
          value: insurance?.is_total_vehicle_theft,
        },
        {
          id: 5,
          title: 'freeRescue',
          value: insurance?.free_roadside_assistance,
        },
      ],
    },
    {
      benefitId: 2,
      title: 'Quyền lợi nâng cao',
      benefitDetails: [
        {
          id: 1,
          title: 'chooseGenuineRepairFacility',
          value: insurance?.is_choosing_service_center,
        },
        {
          id: 2,
          title: 'partTheft',
          value: insurance?.is_component_vehicle_theft,
        },
        {
          id: 3,
          title: 'noAdditionalDepreciation',
          value: insurance?.is_no_depreciation_cost,
        },
        {
          id: 4,
          title: 'engineDamage',
          value: insurance?.is_water_damage,
        },
      ],
    },
    {
      benefitId: 3,
      title: 'Quyền lợi bổ sung',
      benefitDetails: [
        {
          id: 1,
          title: 'accidentInsurance',
          value: insurance?.is_insured_for_each_person,
        },
      ],
    },
  ];
};

export const contractInformationList = (insurance) => {
  return [
    {
      id: 1,
      title: 'insurancePeriod1year',
      from: dayjs(insurance?.insurance_period_from).format('DD/MM/YYYY'),
      to: dayjs(insurance?.insurance_period_to).format('DD/MM/YYYY'),
    },
    {
      id: 2,
      title: 'contractHolder',
      value: insurance?.policyholder_full_name,
    },
    {
      id: 3,
      title: 'address',
      value: insurance?.policyholder_address,
    },
    {
      id: 4,
      title: 'phone',
      value: insurance?.policyholder_phone_number,
    },
    {
      id: 5,
      title: 'Email',
      value: insurance?.policyholder_email,
    },
  ];
};
export const formatTypeOfInvoiceIssuance = (t, status) => {
  switch (status) {
    case 'individual':
      return t('individual');
    case 'business':
      return t('business');
  }
};

export const bankBeneficiaryVehicleInformation = (insurance) => {
  return [
    {
      id: 1,
      title: 'bankName',
      value: insurance?.bank_name,
    },
    {
      id: 2,
      title: 'bankBranch',
      value: insurance?.bank_branch,
    },
    {
      id: 3,
      title: 'branchAddress',
      value: insurance?.branch_address,
    },
    {
      id: 4,
      title: 'nameOfStaff',
      value: insurance?.staff_full_name,
    },
    {
      id: 5,
      title: 'phone',
      value: insurance?.staff_phone_number,
    },
  ];
};
export const electronicBill = (t, insurance) => {
  return [
    {
      id: 1,
      title: 'type',
      value: formatTypeOfInvoiceIssuance(t, insurance?.invoice_type),
    },
    {
      id: 2,
      title: 'companyName',
      value: insurance?.invoice_name,
    },
    {
      id: 3,
      title: 'address',
      value: insurance?.invoice_address,
    },
    {
      id: 4,
      title: 'taxCode',
      value: insurance?.invoice_tax_id,
    },
    {
      id: 5,
      title: 'Email',
      value: insurance?.invoice_email,
    },
  ];
};

export const AUTHENTICATED = 'authenticated';
export const EDIT = 'edit';
export const SEE_DETAILS = 'seeDetails';
export const SEE_QUOTE = 'seeQuote';

export const userInsuranceButtonList = (statusInsurance) => {
  return [
    {
      id: 1,
      title: 'edit',
      value: 'edit',
      state: statusInsurance === PAID,
    },
    {
      id: 2,
      title: 'seeDetails',
      value: 'seeDetails',
      state: false,
    },
    {
      id: 3,
      title: 'seeQuote',
      value: 'seeQuote',
      state: statusInsurance !== APPROVED,
    },
    {
      id: 4,
      title: 'cancelRegistration',
      value: 'cancelRegistration',
      state: statusInsurance === PAID,
    },
  ];
};

export const LIST_TYPE_INSURANCE = (t) => [
  { value: 'unexpired', label: t('unexpired') },
  { value: 'has-expired', label: t('has_expired') },
  { value: 'new-car', label: t('new_car') },
];
export const USE_BUSINESS = (t) => [
  { value: false, label: t('none') },
  { value: true, label: t('yes') },
];
export const TARGET_USE_BUSINESS = (t) => [
  { value: 'family-car', name: t('car_family') },
  { value: 'contracted-service', name: t('contracted_service') },
  {
    value: 'technology-business',
    name: t('technology_business'),
  },
];
export const LIST_BENEFIT_INSURANCE_MATERIAL = (t) => [
  {
    label: t('is_deduct_one_million_vnd'),
    value: 'is_deduct_one_million_vnd',
  },
  {
    label: t('is_complete_vehicle_damage'),
    value: 'is_complete_vehicle_damage',
  },
  {
    label: t('is_component_vehicle_damage'),
    value: 'is_component_vehicle_damage',
  },
  {
    label: t('is_total_vehicle_theft'),
    value: 'is_total_vehicle_theft',
  },
  {
    label: t('free_roadside_assistance'),
    value: 'free_roadside_assistance',
  },
];
export const LIST_BENEFIT_ADVANCED = (t) => [
  {
    label: t('is_choosing_service_center'),
    value: 'is_choosing_service_center',
  },
  {
    label: t('is_component_vehicle_theft'),
    value: 'is_component_vehicle_theft',
  },
  {
    label: t('is_no_depreciation_cost'),
    value: 'is_no_depreciation_cost',
  },
  {
    label: t('is_water_damage'),
    value: 'is_water_damage',
  },
];
export const REQUIRE_UPLOAD_IMAGES = (t) => [
  { id: 1, label: t('right_diagonal_angle') },
  { id: 2, label: t('left_diagonal_angle') },
  { id: 3, label: t('after_right_diagonal_angle') },
  { id: 4, label: t('after_left_diagonal_angle') },
  { id: 5, label: t('image_taplo') },
];
export const BUSINESS_OR_PERSONAL = (t) => [
  { value: 'business', label: t('business') },
  { value: 'individual', label: t('individual') },
];
export const transportationBusiness = (t, insuranceList) => [
  {
    id: 1,
    label: t('yes'),
    defaultValue: true,
    value: insuranceList?.is_transportation_business,
  },
  {
    id: 2,
    label: t('No'),
    defaultValue: false,
    value: insuranceList?.is_transportation_business,
  },
];

export const carInformationDetails = (t, insuranceList) => [
  {
    id: 'car_license_plate',
    label: t('car_license_plate'),
    value: insuranceList?.car_license_plate,
  },
  {
    id: 'seating_capacity',
    label: t('number_seats'),
    value: insuranceList?.seating_capacity,
  },
  {
    id: 'car_brand',
    label: t('mall'),
    value: insuranceList?.car_brand,
  },
  {
    id: 'car_model',
    label: t('car_model'),
    value: insuranceList?.car_model,
  },
  {
    id: 'car_version',
    label: t('car_version'),
    value: insuranceList?.car_version,
  },
  {
    id: 'year_of_production',
    label: t('year_of_production'),
    value: insuranceList?.year_of_production,
  },
];

export const advancedBenefits = [
  {
    id: 'choosing_service_center',
    label: 'Lựa chọn cơ sở sửa chữa chính hãng',
  },
  {
    id: 'component_vehicle_theft',
    label: 'Mất cắp bộ phận',
  },
  {
    id: 'no_depreciation_cost',
    label: 'Không tính khấu hao phụ tùng, vật tư thay mới',
  },
  {
    id: 'water_damage',
    label: 'Thiệt hại động cơ do ảnh hưởng của nước',
  },
];

export const additionalBenefits = [
  {
    id: 'insured_for_each_person',
    label: 'Bảo hiểm tai nạn người ngồi trên xe: 100.000.000 vnđ/ chỗ',
  },
];

export const insuranceTypes = (t) => [
  {
    id: 1,
    title: t('carInsurance'),
  },
  {
    id: 2,
    title: t('motorcycleInsurance'),
  },
  {
    id: 3,
    title: t('lifeInsurance'),
  },
  {
    id: 4,
    title: t('healthInsurance'),
  },
];

export const questionsCompulsoryCarInsurance = (t) => [
  {
    id: 1,
    title: t('question11'),
    detail: (
      <ListUlInsurance
        data={[
          {
            detailId: 1,
            detail: t('answer1question11'),
          },
          {
            detailId: 2,
            detail: t('answer2question11'),
          },
        ]}
        styleLi="text-sm"
      />
    ),
  },
  {
    id: 2,
    title: t('question21'),
    detail: t('answerQuestion21'),
  },
  {
    id: 3,
    title: t('question31'),
    answerList: [
      {
        id: 1,
        detailQuestion: t('answer1question31'),
      },
      {
        id: 2,
        detailQuestion: t('answer2question31'),
      },
      {
        id: 3,
        detailQuestion: t('answer3question31'),
      },
      {
        id: 4,
        detailQuestion: t('answer4question31'),
      },
    ],
  },
  {
    id: 4,
    title: t('question41'),
    detail: (
      <ListUlInsurance
        data={[
          {
            detailId: 1,
            detail: t('answer1question41'),
          },
          {
            detailId: 2,
            detail: t('answer2question41'),
          },
        ]}
        styleLi="text-sm"
      />
    ),
  },
];

export const questionsMaterialCarInsurance = (t) => [
  {
    id: 1,
    title: t('proceduresForClaiming'),
    answerList: [
      {
        id: 1,
        detailQuestion: t('answer1question4'),
      },
      {
        id: 2,
        detailQuestion: t('answer2question4'),
      },
      {
        id: 3,
        detailQuestion: t('answer3question4'),
      },
      {
        id: 4,
        detailQuestion: t('answer4question4'),
      },
      {
        id: 5,
        detailQuestion: t('answer5question4'),
      },
    ],
  },
  {
    id: 2,
    title: t('validityOfAutoInsurance'),
    detail: t('validityOfAutoInsuranceAnswer'),
  },
  {
    id: 3,
    title: t('howLongIsAutoInsurance'),
    detail: t('insurancePeriod'),
  },
  {
    id: 4,
    title: t('question4'),
    detail: t('question4Answer'),
  },
];

export const titleTabMaterialCarQuestion = (t) => [
  {
    id: 1,
    title: (
      <span>
        {t('compulsoryAutoInsurance')}&nbsp;
        <span className="hidden sm:inline">(TNDS)</span>&nbsp;
      </span>
    ),
  },
  {
    id: 2,
    title: t('carMaterialInsurance'),
  },
];
