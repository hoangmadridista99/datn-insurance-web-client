import { useEffect, useState } from 'react';

export const useIntersection = (ref, options) => {
  const [intersectionObserverEntry, setIntersectionObserverEntry] =
    useState(null);
  const [observer, setObserver] = useState(null);

  const frozen = !!intersectionObserverEntry && options.allowOnce;

  const handleIntersect = (entries) => {
    setIntersectionObserverEntry(entries[0].isIntersecting);
  };

  useEffect(() => {
    if (frozen) return;

    // eslint-disable-next-line no-underscore-dangle
    const _observer = new IntersectionObserver(handleIntersect, options);

    _observer.observe(ref?.current);
    setObserver(_observer);

    return () => _observer?.disconnect();
  }, [ref.current, options.root, options.rootMargin, options.threshold, frozen]);

  useEffect(
    () => () => {
      observer?.disconnect();
    },
    []
  );

  return intersectionObserverEntry;
};
